---
layout: markdown_page
title: "Category Direction - Feature Flags"
---

- TOC
{:toc}

## Feature Flags

Feature Flags can be used as part of software development to enable a feature to be tested even before it is completed and ready for release. 
A feature flag is used to enable or disable the feature during run time. 
The unfinished features are hidden (toggled) so they do not appear in the user interface. 

Tying into our [progressive delivery](https://about.gitlab.com/direction/ops/#progressive-delivery) strategy, this allows many small incremental versions of software to be delivered without the cost of constant branching and merging. 

Our ultimate goal is to provide an easy way to configure and monitor feature flags that integrate into the continuous development cycle. Visual ties from feature flags to ongoing issues and merge requests will help understand the state
and exposure of each feature and a comprehensive dashboard will allow developers to take action when necessary in order to minimize risk and technical debt. 

Feature flags unlock faster, more agile delivery workflows by providing
flexible control over deployments to a specific environment or audience. In addition it reduces risk to production. In case a problem is detected, 
disabling the feature is as easy as turning off a switch. 

Feature Flags is built with an [Unleash](https://github.com/Unleash/unleash)-compatible
API, ensuring interoperability with any other compatible tooling,
and taking advantage of the various client libraries available for
Unleash. Unleash have recently announced that they are spinning up a hosted (paid) option while maintaining their open source offering. We will be monitoring this closely. 

While Feature flags are a great tool for incremental delivery, they also introduce technical debt, sometimes feature flags are forgotten and left as stale code. In order to overcome this challenge we plan to notify users when the time comes to expire a feature flag [gitlab#8527](https://gitlab.com/gitlab-org/gitlab/issues/8527).

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AFeature%20Flags)
- [Overall Vision](/direction/ops/#release)
- [UX Research](https://gitlab.com/gitlab-org/ux-research/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Arelease&label_name[]=Category%3AFeature%20Flags)
- [Documentation](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html)

## What's Next & Why

We have completed the rearchitecture of Feature Flags, adding the ability to support multiple strategies per environment via API and UI ([gitlab#204895](https://gitlab.com/gitlab-org/gitlab/issues/204895 and [gitlab#35555](https://gitlab.com/gitlab-org/gitlab/issues/35555)). 
We are currently enabling this feature for dogfooding and will test this out internally before publishing this ability to the wider community. If you are interested in participating in this beta trial please signup through [gitlab#216372](https://gitlab.com/gitlab-org/gitlab/-/issues/216372).

To make working with userIDs easier, we plan to introduce the list strategy, which will allow you to store userIDs in lists that can be reused across multiple feature flags. This will be accomplished via API support in [gitlab#205409](https://gitlab.com/gitlab-org/gitlab/issues/205409) and UI in [gitlab#13308](https://gitlab.com/gitlab-org/gitlab/issues/13308).



## Maturity Plan

This category is currently at the "Viable" maturity level, and our next maturity target is Complete (see our [definitions of maturity levels](/direction/maturity/)).

Our focus at the moment is on using the feature internally to deliver GitLab itself. This is driving new requirements to the base features that are out there, and also helping us to ensure the list for `complete` maturity is accurate. Our plan is for our feature flag solution to compete with other products on the market such as [LaunchDarkly](https://launchdarkly.com/) or [Rollout](https://rollout.io/). As we work towards `complete` maturity, our expectation is that our primary adopters of this feature will be pre-existing GitLab users looking for incremental value. For buyers who are considering replacing JIRA, and looking for something that integrates feature flags with issues, we can also provide a valuable solution as we head towards `complete` maturity.

Key deliverables to achieve this are:

- [% rollout for Feature Flags](https://gitlab.com/gitlab-org/gitlab/issues/8240) (Complete)
- [UserID-based access](https://gitlab.com/gitlab-org/gitlab/issues/11459) (Complete)
- [Multiple strategies per Feature Flags](https://gitlab.com/gitlab-org/gitlab/issues/35554) (Complete)
- [Feature Flag Gradual Rollout strategy based on lists](https://gitlab.com/gitlab-org/gitlab/issues/13308)
- [Add ability to associate feature flag with contextual issue/epic/MR](https://gitlab.com/gitlab-org/gitlab/issues/26456)
- [Move features to core: "Feature Flags"](https://gitlab.com/gitlab-org/gitlab/-/issues/212318)
- [A/B testing based on Feature Flags](https://gitlab.com/gitlab-org/gitlab/issues/34813)
- [Cookie-based access](https://gitlab.com/gitlab-org/gitlab/issues/11456)
- [Add Rule based Feature Flag rollout strategy support](https://gitlab.com/gitlab-org/gitlab/issues/33315)
- [Permissions for Feature Flags](https://gitlab.com/gitlab-org/gitlab/issues/8239)

If you are interested in understanding the roadmap in a more granular fashion, you are welcome to follow these epics:
- [Refactor Feature Flags UX](https://gitlab.com/groups/gitlab-org/-/epics/2130)
- [Feature Flags Improvements - user action enhancements](https://gitlab.com/groups/gitlab-org/-/epics/2134)
- [Feature Flags Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2236)

## Competitive Landscape

Other feature flag products offer more comprehensive targeting and
configuration. The simplicity of our solution is actually a strength
compared to this in some cases, but there is some basic functionality
still to add. As we are rigorously working to close the gaps with the competitors, our next strategy to tackle will be the ability to configure feature flags based on groups [gitlab-ee#13308](https://gitlab.com/gitlab-org/gitlab-ee/issues/13308)

There is a detailed LaunchDarkly comparison from when the project
was first being conceived [here](https://docs.google.com/spreadsheets/d/1p3QhVvdL7-RCD2pd8mm5a5q38D958a-vwPPWnBT4pmE/edit#gid=0).

We are conducting a renewed competitor review of LaunchDarkly in [gitlab#197727](https://gitlab.com/gitlab-org/gitlab/issues/197727). If you have additional 
insights or are interested in joining in the conversation, please comment on the issue. 

## Analyst Landscape

Analysts are recognizing that this sort of capability is becoming
more a part of what's fundamentally needed for a continuous delivery
platform, in order to minimize blast radius from changes. Often,
solutions in this space are complex and hard to get up and running
with, and they are not typically bundled or well integrated with CD
solutions. 

This backs up our desire to not over complicated the solution space
here, and highlights the need for guidance. [gitlab#9450](https://gitlab.com/gitlab-org/gitlab/issues/9450)
introduces new in-product documentation to help development and
operations teams learn how to successfully adopt feature flags.

## Top Customer Success/Sales Issue(s)

Since GitLab serves as a single application tool, users who use our feature flags and issue management, can associate feature flags 
directly from the issue and vice versa and view the current deployment status via ([gitlab#26456](https://gitlab.com/gitlab-org/gitlab/issues/26456)).

This will provide visibility from the issue itself, and will let you know which feature flag is associated with it, its status, and percent rollout from the feature flag view, enabling you control and insights from wherever you wish to manage your feature flags.

## Top Customer Issue(s)

- Our most popular customer issue is to ope-source Feature Flags via [gitlab#212318](https://gitlab.com/gitlab-org/gitlab/-/issues/212318).
## Top Internal Customer Issue(s)

- Enabling Unleash in our codebase to enable internal use of our feature flag solution ([gitlab#26842](https://gitlab.com/gitlab-org/gitlab/-/issues/26842)) is our main focus, as part of our values we believe it is important for us to use our own features and collect feedback to improve the experience of the greater GitLab community. 

### Delivery Team

- Feature Flags: [framework#216](https://gitlab.com/gitlab-com/gl-infra/delivery/issues/216) and [framework#32](https://gitlab.com/gitlab-com/gl-infra/delivery/issues/32)

## Top Vision Item(s)

One of our main themes in CI/CD is [Progressive delivery](https://about.gitlab.com/direction/ops/#progressive-delivery). Feature flags, by definition is a form of progressive delivery as it allows you to deploy code incrementally and control the audience that will receive the new code. 

Our top vision item is to allow simple monitoring and management of the feature flags in the system, which can become a complex take once there are many feature flags in place. 
A feature flag dashboard as described in [gitlab#2236](https://gitlab.com/groups/gitlab-org/-/epics/2236) will make this an easy task.


