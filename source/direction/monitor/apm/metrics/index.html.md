---
layout: markdown_page
title: "Category Direction - Metrics"
---

- TOC
{:toc}

## Metrics

### Introduction and how you can help
Thanks for visiting this category strategy page on Metrics in GitLab. This category belongs to and is maintained by the [APM](/handbook/engineering/development/ops/monitor/APM/) group of the Monitor stage.

Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your Metrics usage, we'd especially love to hear your use case(s).

* [Maturity Plan](#maturity-plan)
* [Related Issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aapm&label_name[]=Category%3AMetrics)
* [Monitor Stage Direction Page](/direction/monitor/)

## What are metrics 

Metrics help users understand the health and status of your services and essential for ensuring the reliability and stability of those services., metrics normally represents by raw measurements of resource usage over time (e.g. measure memory usage every 10 second). Some metrics represent the status of an operating system (CPU, memory usage). Other types of data tied to the specific functionality of a component (requests per second, latency or error rates).
The most straightforward metrics, to begin with, are those already exposed by your operating system hence easier to collect (e.g. Kubernetes metrics).
For other components, especially your applications, you may have to add code or interfaces to expose the metrics you care about. Exposing metrics is sometimes known as [instrumentation](https://about.gitlab.com/direction/monitor/apm/workflow/instrument/), the collection of metrics from an end point is called scraping.

## Our mission

Provide users with information about the health and performance of their infrastructure, applications, and system for insights into reliability, stability, and performance.

## Target audience 

Metrics are essential for all users across the DevOps spectrum. From developers who should understand the performance impact of changes they are making, as well as operators responsible for keeping production services online.

Our vision is that in 1-2 years, GitLab metrics is the main day-to-day tool for monitoring cloud-native applications for SMBs. 

Currently, we are targeting [Software Developer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer) and [DevOps Engineer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer) working in a SMB from the following reasons: 

* SMBs are more likely to be cloud-native - no doubt that the trend for large enterprises is going cloud-native. However, they will remain hybrid and need a full-blown monitoring solution to cover both approaches. 
* Cloud-native is one of the [fastest-growing markets in IT](https://www.gartner.com/smarterwithgartner/cloud-shift-impacts-all-it-markets), given how GitLab is positioned in this space going after these segment will allow us to gain market share and quickly expand.
* Developers in this market segment are more likely to be a complete DevOps engineer, responsible for development, deployment and monitoring.
* SMBs are less willing to spend their budget on expensive monitoring systems - our base monitoring features are free for all GitLab users.
* This strategy means that it is less likely we'll compete head-on with the established vendors there are in the market today. 

## Current experience 

The experience today offers you to deploy Prometheus instance into a project cluster by a push of a button, the Prometheus instance will run as a GitLab managed application. Once deployed, it will automatically collect key metrics from the running application which are displayed on an out of the box [dashboard](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#using-the-metrics-dashboard). Our dashboards provide you with the needed [flexability](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#defining-custom-dashboards-per-project) to display any metric you desire you can set up alerts, configure [variables](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#templating-variables-for-metrics-dashboards) on a generic dashboard, drill into the [relavant logs](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#view-logs-ultimate) to troubleshoot your service and more... 
If you already have a running Prometheus deploy into your cluster simply [connect](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#external-prometheus-instances) it to your GitLab and start using our GitLab metrics dashboard.

## Targeted workflows

The target workflow includes a few important use cases:
1. Configuring GitLab to monitor an application should be as easy as possible. To the degree possible given the environment, we should automate this activity for our users.
1. Dashboards should automatically populate with relevant metrics that were detected, however still offer flexibility to be customized as needed for a specific use case or application. The dashboards themselves should offer the visualizations required to best represent the data.
1. When troubleshooting, we should offer the ability to easily explore the data to help understand potential relationships and create/share one off dashboards.
1. Service Level Objectives should be able to be defined, with corresponding impact on Error Budgets when they are not met.

## What's Next & Why
TThe APM team current focus is on [Dogfooding metrics](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/6508). The team is leading the initiative of migrating all the useful dashboards our infrastructure team is using for monitoring Gitlab.com from Grafana to GitLab metrics charts. Today the team was able to migrate 20 dashboards while doing so successfully we've identified [critical](https://gitlab.com/groups/gitlab-org/-/epics/2541) and [non-critical](https://gitlab.com/groups/gitlab-org/-/epics/2597) gaps. Those issues will enable our Infrastructure team to start using GitLab metrics charts instead of Grafana. This will initiate a feedback loop to improve our solution. 


## Maturity Plan
* [Critical gaps from Grafana](https://gitlab.com/groups/gitlab-org/-/epics/2541)
* [non-critical gaps](https://gitlab.com/groups/gitlab-org/-/epics/2597)


