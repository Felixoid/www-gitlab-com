---
layout: markdown_page
title: "Category Direction - Container Registry"
---

- TOC
{:toc}

## Container Registry

The GitLab Container Registry is a secure and private registry for Docker images. Built on open source software and completely integrated within GitLab.  Use GitLab CI/CD to create and publish branch/release specific images. Use the GitLab API to manage the registry across groups and projects. Use the user interface to discover and manage your team's images. GitLab will provide a Lovable container registry experience by being the single location for the entire DevOps Lifecycle, not just a portion of it. We will provide many of the features expected of a container registry, but without the weight and complexity of a single-point solution. 

There have been several interesting market changes in the past several months. JetBrains has recently announced [support for a container registry](https://blog.jetbrains.com/space/2020/01/14/introducing-space-packages), including cross-project search. Meanwhile, CodeFresh has [deprecated their container registry](https://codefresh.io/codefresh-news/deprecation-of-the-codefresh-private-registry/). We are monitoring this closely and if you have any feedback about what's working well for you or what you would like to see us improve, please reach out to me directly via [E-mail](mailto:trizzi@gitlab.com).

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3AContainer+Registry)
- [Overall Vision](https://about.gitlab.com/direction/ops/#package)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## What's Next & Why

GitLab's Container Registry is robust, and highly integrated into your CI/CD process. It meets the needs of most container based application development teams. As a result, our highest priority for the Container Registry is to lower the cost of storage on behalf of our customers and for GitLab.com. Those costs tend to grow without sufficient container registry management. [gitlab-#434](https://gitlab.com/groups/gitlab-org/-/epics/434) captures all of the work we are planning to accomplish that goal. 

[gitlab-#2313](https://gitlab.com/groups/gitlab-org/-/epics/2313) will allow Administrators to run [garbage collection](https://docs.gitlab.com/ee/administration/packages/container_registry.html#container-registry-garbage-collection) without requiring any downtime or setting the registry to read-only mode. 

[gitlab-#2270](https://gitlab.com/groups/gitlab-org/-/epics/2270), expands the utility of our [Image Expiration Policies](https://docs.gitlab.com/ee/user/packages/container_registry/#expiration-policy). [gitlab-#208193](https://gitlab.com/gitlab-org/gitlab/-/issues/208193) will improve the performance of the tag deletion process, so that we can enable the feature for all projects on  GitLab.com. 

[gitlab-#217702](https://gitlab.com/gitlab-org/gitlab/-/issues/217702) will resolve a bug in which tags are not being deleted properly when using the Container Registry user interface. This has resulted in you having to manually intervene following the workaround [here](https://docs.gitlab.com/ee/user/packages/container_registry/#unable-to-change-path-or-transfer-a-project).

## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is Complete (see our [definitions of maturity levels](/direction/maturity/)).

For a list of key deliverables and expected outcomes, check out the epic, [Make the Container Registry Complete](https://gitlab.com/groups/gitlab-org/-/epics/2899), which includes links and expected timing for each issue.


## Competitive Landscape

Open source container registries such as [Docker Hub](https://hub.docker.com/) and Red Hat's [Quay](https://quay.io/) offer users a single location to build, analyze and distribute their container images. 

The primary reason people don’t use DockerHub is that they need a private registry and one that lives alongside their source code and pipelines. They like to be able to use pre-defined environment variables for cataloging and discovering images. Often DockerHub is used as a base image for a test, but if you are building an app, you will likely customize an image to fit your application and save it GitLab's private registry alongside your source code.

[JFrog](https://jfrog.com/artifactory/) and [Sonatype](https://www.sonatype.com/nexus-repository-sonatype) both offer support for building and deploying Docker images. JFrog offers their container registry as part of their community edition as well. 

JFrog integrates with several different [CI servers through dedicated plug-ins](https://www.jfrog.com/confluence/display/RTF/Build+Integration), including Jenkins and Azure DevOps, but does not yet support GitLab. However, you can still connect to your Artifactory repository from GitLab CI. Here is an example of how to [deploy Maven projects to Artifactory with GitLab CI/CD](https://docs.gitlab.com/ee/ci/examples/artifactory_and_gitlab/index.html).

GitHub offers a [container registry](https://help.github.com/en/articles/configuring-docker-for-use-with-github-package-registry) as part of their package registry offering.

JetBrains offers a [container registry](https://www.jetbrains.com/help/space/container-registry.html) that allows you to add a project repository and publish images and tags using the Docker client or your JetBrains project. Although they do not currently have any documentation for administrative features, such as cleanup policies or garbage collection.

## Top Customer Success/Sales Issue(s)

The top Customer Success/Sales issue is [gitlab-#196124](https://gitlab.com/gitlab-org/gitlab/-/issues/196124), which will enable support of the expiration policies for all projects. 


## Top Customer Issue(s)

The top customer issue is [gitlab-2313](https://gitlab.com/groups/gitlab-org/-/epics/2313), which will remove the requirement for down time and unblock all of our customers (and GitLab) from running garbage collection. 


## Top Internal Customer Issue(s)

The top internal customer issue is tied to storage optimization. [gitlab-#2313](https://gitlab.com/groups/gitlab-org/-/epics/2313) will allow the Infrastructure team to lower the total cost of the GitLab.com Container Registry by implementing online garbage collection and removal of blobs. 

## Top Vision Item(s)

We've learned from a recent [survey](https://gitlab.com/gitlab-org/ux-research/issues/328) and subsequent [user interviews](https://gitlab.com/gitlab-org/ux-research/issues/329), that users navigate to the Container Registry user interface for [one of three reasons](https://gitlab.com/gitlab-org/uxr_insights/issues/617).

- To look up which image or tag I should be using in my environment (32%)
- To verify my CI pipeline built the image as expected	(28%)
- To ensure my image was uploaded correctly (22%)

Our top vision item, [gitlab-#3211](https://gitlab.com/groups/gitlab-org/-/epics/3211) which will redesign the user interface of the Container Registry.
