
# Microsoft Competitive Assessment Key Findings 



1. **Single pre-integrated solutions for DevOps are starting to resonate.**    \
 \
Multiple data points indicate that the market is starting to perceive Microsoft And GitLab as the top two end to end DevOps solutions today.  Azure DevOps is viewed as a unified suite with a single user interface.  \

2. **Microsoft’s target TAM for developer tools investments extends beyond the immediate DevOps tools TAM.**   Microsoft’s DevOps solution offering is a means to grow their market share in the Cloud TAM which is approximately $69b. This means Microsoft will be willing to sacrifice DevOps tools in order to win Cloud business. \

3. **Microsoft has strong enterprise relationships and multi-year ELAs** 
 \
ELAs make it easy for customers to pick and choose the products they need with minimal friction from the “buffet” of Microsoft choices under the ELA. Microsoft products only have to be “good enough” for the convenience that ELAs offer. Azure DevOps users are happy with Azure DevOps and see it as a single app for most of their flow/work - integrated “enough” for what they need to do now. \

 

4. **Product Level Differences between GitLab and Microsoft Azure DevOps:**

    1. Application security is only available in Azure DevOps through integration with partner products, while GitLab offers extensive built-in application security scanning. (Advantage - **GitLab**)
    2. Azure DevOps does not provide built-in App Performance Monitoring (APM). Although Microsoft has Azure Monitoring, it is not integrated with Azure DevOps. On the other hand, GitLab provides monitoring built-in. (Advantage - **GitLab**)
    3. For support of traditional Release Management, Azure DevOps provides many primitives which GitLab doesn’t yet have but is working on, such as Release Approval Gates and Release Auditing.(Advantage - **Azure DevOps**) However, Azure DevOps lacks Progressive Delivery capabilities such as Feature Flags and Review Apps, which GitLab has. (Advantage - **GitLab**)
    4. GitLab lacks Manual Testing Management capabilities, while Azure DevOps provides Azure Test Plans built-in for a separate licensing fee. (Advantage - **Azure DevOps**)
    5. Azure DevOps provides persona-centric dashboards and views that resonate with customers, allowing them to reach not only the developers but the mindshare of executive leadership and therefore, sell higher in the enterprise. GitLab has some built-in graphs and dashboards, but nothing as extensive as the Azure DevOps offering, and not targeted at higher roles in the enterprise. (Advantage - **Azure DevOps**)
    6. Azure DevOps provides an easy to use Marketplace of plugins/extensions which makes Azure DevOps’ extensibility prevalent at key places in the user experience, such as when authoring a pipeline. In contrast, GitLab does not provide a marketplace of plugins for integrations into pipelines. (Advantage - **Azure DevOps**)




1. **Microsoft has accelerated its rate of innovation. **Azure DevOps and GitHub are now releasing quicker than GitLab. 

    Evidence can be seen by an analysis of the [2019 Azure DevOps Services release notes](https://docs.microsoft.com/en-us/azure/devops/release-notes/2019/sprint-156-update) (showing 12 releases in 7 months - 2019-01-14 to 2019-08-12) and release cadence acceleration from a constant 4 to 3 weeks. The [GitHub changelog](https://github.blog/changelog/) also shows us that the GitHub team also has a high release cadence, releasing features on a 1-2 day cadence. => Sid: Can we count new features velocity instead of release frequency?

1. **Microsoft is working to build stickiness around long term dependencies with Azure services**, in particular higher-value services like translation, image recognition, etc.   

2. **Microsoft dev tool customers are not universally positive about Azure DevOps and/or GitHub.**  Those customers who _have chosen to move to Azure DevOps and/or GitHub _generally like the Azure DevOps / GitHub tools. ** **However, the larger Microsoft tool customer base (legacy TFS/VSTS and others) are not too eager to be pushed into Azure DevOps and/or GitHub.  

3. **Azure DevOps MarketPlace and GitHub MarketPlace are key differentiators for Microsoft in terms of providing extensibility to their platforms.**  Customers want the  ability to extend Azure DevOps/GitHub and to integrate with their existing tools.   \
 \
Microsoft does a good job of being extensible through their Marketplace for plugins although the Marketplace offerings are similar to Jenkins plugins (not all MS owned or guaranteed, many not rated and of questionable reliability). Under the covers, the complexity of multi-app shows when using Azure DevOps with plugins. \

4. **Microsoft has a strong partner ecosystem** for reaching a broader market and appropriate buyers (management/IT consulting and technology partners) and offering a complete solution for successful implementation (professional services).   
 \
Microsoft has a strong partner ecosystem in place today to enable a broader reach and adoption of its offerings. 

5. **Microsoft’s breadth remains both its key strength and weakness.**  Microsoft’s strength is the wide range of Office productivity tools, developer tools, and enterprise IT management solutions that are pervasive in many enterprises.     
 \
Microsoft’s breadth is also a weakness driven by inherent corporate and organizational complexity (multiple competing business units within the same structure).  This complexity creates multiple product and pricing/licensing options, product group competition and misalignment in business goals.

6. **Microsoft customers still do not believe they are a cloud “neutral vendor”** believing that working with Microsoft predisposes them to being locked in with a single vendor. This is despite Microsoft itself appearing to have genuinely embraced a vendor-neutral, run anywhere philosophy (example - ability to work with K8, etc.).  For example, their messaging states - “Continuously build, test, and deploy to any platform and cloud.”
