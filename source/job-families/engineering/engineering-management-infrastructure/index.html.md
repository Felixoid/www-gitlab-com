---
layout: job_family_page
title: "Engineering Management - Infrastructure"
---

## Engineering Manager, Delivery

The Engineering Manager, Delivery directly manages the engineering team responsible for tasks relating to
release management of GitLab releases as well as continuous delivery/deployment on GitLab.com.
Team mandate is described in the [team handbook](/handbook/engineering/infrastructure/team/delivery/)
and it consists of [Site Reliability Engineers](/job-families/engineering/site-reliability-engineer/) and
[Backend Engineers](/job-families/engineering/backend-engineer/).
The Engineering Manager's responsibility is primarily their team. They need to
be familiar with GitLab's underlying technologies and processes to be able to support their team growth, as well as be the final decisive vote in cases where team is blocked on a decision.

Their primary focus is hiring and building a world-class team and putting them in the best position to succeed. They must also efficiently coordinate
across departments to accomplish collaborative goals.

### Responsibilities

  - Hire an incredible team that lives our [values](/handbook/values/)
  - Improve the happiness and productivity of the team
  - Hold regular 1:1's with team members
  - Drive self-managed release and GitLab.com continuous deployment processes
  - Manage agile projects
  - Work across sections within engineering
  - Improve the quality, security and performance of the product
  - Work on small changes outside of the critical path

### Requirements

  - 2-5 years managing software engineering teams
  - Strong technical background
  - Demonstrated teamwork in a peak performance organization
  - Experience running a consumer scale platform
  - Product company experience
  - Enterprise software company experience or startup experience
  - Computer science education or equivalent experience
  - Passionate about open source and developer tools
  - Exquisite communication skills
  - [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)
  - Experience in managing a remote team
  - Ability to use GitLab

### Performance Indicators

Engineering Manager, Delivery, have the following job-family performance indicators:

  - [GitLab.com Availability](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#gitlab-com-availability)
  - [Hiring Actual vs Plan](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#hiring-actual-vs-plan)
  - [Diversity](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#diversity)
  - [Handbook Update Frequency](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#handbook-update-frequency)
  - [Team Member Retention](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#team-member-retention)
  - [Mean Time to Production](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#mean-time-to-production-mttp)

## Engineering Manager, Reliability

The Engineering Manager, Reliability directly manages the engineering team relentlessly focused on GitLab.com's
performant reliability, a team composed of [DBRE](/job-families/engineering/database-reliability-engineer/)s and
[SRE](/job-families/engineering/site-reliability-engineer/)s. They see the team as their product. They may work on
small features or bugs to keep their technical skills sharp and stay familiar with the code, but they emphasize
hiring a world-class team and putting them in the best position to succeed. They own the delivery of product
commitments and they are always looking to improve the productivity of their team. They must also coordinate
across departments to accomplish collaborative goals.

### Responsibilities
 - Hire an incredible team that lives our [values](/handbook/values/)
 - Improve the happiness and productivity of the team
 - Work on small features and bugs (nothing critical path)
 - Own Incident and Change Management, RCA, and Error Budget Management
 - Hold regular 1:1's with team members
 - Manage agile projects
 - Work across sections within engineering
 - Improve the quality, security and performance of the product

### Requirements
 - 2-5 years managing software engineering teams
 - Demonstrated teamwork in a peak performance organization
 - Experience running a consumer scale platform
 - Product company experience
 - Enterprise software company experience
 - Computer science education or equivalent experience
 - Passionate about open source and developer tools
 - Exquisite communication skills
 - [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

### Nice-to-haves
 - Online community participation
 - Remote work experience
 - Startup experience
 - Significant open source contributions

### Performance Indicators

Engineering Manager, Infrastructure, have the following job-family performance indicators:

- [GitLab.com Availability](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#gitlab-com-availability)
- [Hiring Actual vs Plan](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#hiring-actual-vs-plan)
- [Infrastructure Cost per GitLab.com Monthly Active Users](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#infrastructure-cost-per-gitlab-com-monthly-active-users)
- [Infrastructure Cost vs Plan](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#infrastructure-cost-vs-plan)
- [Diversity](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#diversity)
- [Handbook Update Frequency](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#handbook-update-frequency)
- [Team Member Retention](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#team-member-retention)

## Engineering Manager, Scalability

The Engineering Manager, Scalability directly manages the engineering team responsible for GitLab and GitLab.com at scale, working on the highest priority scalability items in the application in close coordination with **Reliability Engineering** teams and providing feedback to other Engineering teams so they can become better at scalability as well. Team mandate is described in the [team handbook](/handbook/engineering/infrastructure/team/scalability/) and it consists of [Backend Engineers](/job-families/engineering/backend-engineer/) and [Frontend Engineers](https://about.gitlab.com/job-families/engineering/frontend-engineer/).

The Engineering Manager's responsibility is primarily their team. They need to be familiar with GitLab's underlying technologies and processes to be able to support their team growth, as well as be the final decisive vote in cases where team is blocked on a decision.

Their primary focus is hiring and building a world-class team and putting them in the best position to succeed. They must also efficiently coordinate across departments to accomplish collaborative goals.

### Responsibilities

  - Hire an incredible team that lives our [values](/handbook/values/)
  - Improve the happiness and productivity of the team
  - Hold regular 1:1's with team members
  - Drive scalability initiatives got GitLab and GitLab.com, and coordinate with the Development Department through the [Availability and Scalability Board](https://gitlab.com/groups/gitlab-org/-/boards/1193197?&label_name[]=gitlab.com&label_name[]=infradev)
  - Manage agile projects
  - Work across sections within Engineering
  - Improve the scalability of the product
  - Work on small changes outside of the critical path

### Requirements

  - 2-5 years managing software engineering teams
  - Strong technical background
  - Demonstrated teamwork in a peak performance organization
  - Experience running a consumer scale platform
  - Product company experience
  - Enterprise software company experience or startup experience
  - Computer science education or equivalent experience
  - Passionate about open source and developer tools
  - Exquisite communication skills
  - [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)
  - Experience in managing a remote team

### Performance Indicators

Engineering Manager, Scalability, have the following job-family performance indicators:

  - [GitLab.com Availability](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#gitlab-com-availability)
  - [Hiring Actual vs Plan](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#hiring-actual-vs-plan)
  - [Diversity](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#diversity)
  - [Handbook Update Frequency](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#handbook-update-frequency)
  - [Team Member Retention](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#team-member-retention)

## Senior Engineering Manager, Delivery and Scalability

The Senior Engineering Manager, Delivery and Scalability manages multiple teams within the Infrastructure Department (Delivery and Scalability) that work on GitLab.com and contribute to our on-premise product. They see their teams as their products. They are capable of managing multiple teams and projects at the same time. They are expert recruiters of both engineers and managers. They can also grow the existing talent on their teams. This is a leadership role is a that models the behaviors we want to see in our teams and holds others accountable when necessary. And they create the collaborative and productive environment in which managers, reliability, backand and froentend engineers do their work and succeed.

### Responsibilities

- Manage the Delivery and Scalability Infrastructure teams
- Conduct managerial interviews for candidates, and train engineering managers to do said interviews
- Generate and implement process improvements, especially cross-team processes
- Hold regular [1:1s](https://about.gitlab.com/handbook/leadership/1-1/) with team managers and skip-level 1:1s with all members of their team
- Management mentorship

#### Requirements

- Technical credibility: Past experience as a product engineer and leading teams thereof
- Management credibility: Past experience (3 to 5 years) as an engineering manager
- Ability to understand, communicate and improve the quality of multiple teams
- Demonstrate longevity at at least one recent job
- Ability to be successful managing at a remote-only company
- Humble, servant leader

#### Nice-to-have Requirements

- Be a user of GitLab, or familiar with our company
- Prior Developer Platform or Tool industry experience
- Prior product company experience
- Prior high-growth startup experience
- Experience working on systems at massive (i.e. consumer) scale
- Deep open source experience
- Experience working with global teams
- We value diversity, inclusion and belonging in leadership
- Be inquisitive: Ask great question

- Hire and manage multiple teams that live our [values](/handbook/values/)
- Measure and improve the happiness and productivity of the team

### Performance Indicators

The Senior Engineering Manager, Delivery and Scalability, has the following performance indicators:

- [GitLab.com Availability](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#gitlab-com-availability)
- [Hiring Actual vs Plan](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#hiring-actual-vs-plan)
- [Diversity](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#diversity)
- [Handbook Update Frequency](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#handbook-update-frequency)
- [Team Member Retention](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#team-member-retention)
- [Mean Time to Production](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#mean-time-to-production-mttp)

## Director of Infrastructure, Reliability

The Director of Engineering, Reliability manages the Engineeing Reliability teams within the Infrastructure Department that work on GitLab.com and contribute to our on-premise product. They see their teams as their products. They are capable of managing multiple teams and projects at the same time. They are expert recruiters of DBREs, SREs and managers. They can also grow the existing talent on their teams. Like all GitLab Engineering Directors, this role is a senior leader that models the behaviors we want to see in our teams and holds others accountable when necessary. And they create the collaborative and productive environment in which SREs and SRE managers do their work and succeed.

### Responsibilities

- Hire and manage multiple infrastructure teams that live our [values](/handbook/values/)
- Measure and improve the happiness and productivity of the team
- Drive quarterly OKRs
- Work across departments within engineering
- Write public blog posts and speak at conferences
- Own the availability, reliability, and performance of GitLab.com
- Drive process for incident management
- Drive process for project management with transparent status and high productivity
- Plan ahead of required system capacity
- Drive infrastructure cost efficiency
- Communicate clearly and concisely with peers within Engineering and executives
- Create easy-to-understand SLOs and transparent dashboards

### Requirements

- 5 years managing multiple operations teams
- Excellent recruiter capable of attracting top talent
- Experience with consumer-level scale systems
- Passionate about open source and developer tools
- Experience in a peak performance organization
- Enterprise software company experience
- Product company experience
- Startup experience

### Nice-to-haves

- Candidates with diverse backgrounds
- Prior remote work experience
- Significant open source contributions
- Experience with global teams

### Performance Indicators

The Director of Infrastructure, Reliability has the following performance indicators:

- [GitLab.com Availability](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#gitlab-com-availability)
- [Hiring Actual vs Plan](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#hiring-actual-vs-plan)
- [Infrastructure Cost per GitLab.com Monthly Active Users](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#infrastructure-cost-per-gitlab-com-monthly-active-users)
- [Infrastructure Cost vs Plan](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#infrastructure-cost-vs-plan)
- [Diversity](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#diversity)
- [Handbook Update Frequency](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#handbook-update-frequency)
- [Team Member Retention](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#team-member-retention)

## VP of Infrastructure

The VP of Infrastructure manages multiple teams that work on GitLab.com and contribute to our on-premise product. They see their teams as their products. They are capable of managing multiple teams and projects at the same time. They are expert recruiters of DBREs, SREs, Backend and Frontend Engineers, and managers. They can also grow the existing talent on their teams. Like all GitLab Engineering Directors, this role is a senior leader that models the behaviors we want to see in our teams and holds others accountable when necessary. And they create the collaborative and productive environment in which SREs and SRE managers do their work and succeed.

### Responsibilities

- Hire and manage multiple infrastructure teams that live our [values](/handbook/values/)
- Measure and improve the happiness and productivity of the team
- Define the agile development and continuous delivery process
- Drive quarterly OKRs
- Work across departments within engineering
- Write public blog posts and speak at conferences
- Own the availability, reliability, and performance of GitLab.com
- Drive process for incident management
- Drive process for project management with transparent status and high productivity
- Plan ahead of required system capacity
- Drive infrastructure cost efficiency
- Communicate clearly and concisely with peers within Engineering and executives
- Create easy-to-understand SLOs and transparent dashboards

### Requirements

- 10 years managing multiple operations teams
- Excellent recruiter capable of attracting top talent
- Experience with consumer-level scale systems
- Passionate about open source and developer tools
- Experience in a peak performance organization
- Enterprise software company experience
- Product company experience
- Startup experience

### Nice-to-haves

- Candidates with diverse backgrounds
- Prior remote work experience
- Significant open source contributions
- Experience with global teams

### Performance Indicators

The VP Infrastructure, has the following performance indicators:

- [GitLab.com Availability](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#gitlab-com-availability)
- [Hiring Actual vs Plan](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#hiring-actual-vs-plan)
- [Infrastructure Cost per GitLab.com Monthly Active Users](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#infrastructure-cost-per-gitlab-com-monthly-active-users)
- [Infrastructure Cost vs Plan](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#infrastructure-cost-vs-plan)
- [Diversity](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#diversity)
- [Handbook Update Frequency](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#handbook-update-frequency)
- [Team Member Retention](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#team-member-retention)
