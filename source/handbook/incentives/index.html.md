---
layout: handbook-page-toc
title: "Incentives at GitLab"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The following incentives are available for GitLab team members. Also see our separate page on [benefits](/handbook/total-rewards/benefits/) available to GitLab team members.

### IACV Target Dinner Evangelism Reimbursement

As of Q2 FY21 we have removed the IACV Target Dinner incentive.

### Discretionary Bonuses

#### Discretionary Bonuses per Team Member

The number of discretionary bonuses given divided by the total number of team members, in a given period as defined. The discretionary bonuses per team member target is > 0.1. This analysis can be found on the [People Group Dashboard](https://app.periscopedata.com/app/gitlab/482006/People-Group-KPIs).

Discretionary bonuses are measured in BambooHR, as are the number of team members, in a given period as defined.

1. Calculate the number of the discretionary bonuses per month
  * Pull the "Bonus" Report from BambooHR.
  * Add the report to the metrics spreadsheet. Remove all data for bonuses other than discretionary bonuses and those not within the last rolling 12 months.
  * Populate the Last Rolling 12 Months Expense Table
1. Determine the percentage of bonuses granted for the Company
  * Using a pivot table, add in the count per month as well as headcount as of the last day of the month.
  * The formula will convert this to a percentage.
1. Determine the spread of bonuses granted by Division
  * Using the report filtered in the first section, generate a pivot table around the number of bonuses granted by division for the metrics month currently under review. For example, on July 1st you would be looking at the June metrics.
  * Pull the headcount report to add in the number of people in each division for that month.
  * The formulas will determine the percent per division on bonuses granted
  * Review the delta of the percent of headcount granted a bonus and add it to the Prior Change Column.
1. Outline any large deltas and note any takeaways for review at the next monthly metric meeting for People Ops.

<iframe class="dashboard-embed" src="https://app.periscopedata.com/shared/fe963ce6-510e-44ac-88f0-052002b6dc30?embed=true" height="2400"> </iframe>

#### Discretionary Bonuses for Individuals

1. Every now and then, individual GitLab team members really shine as they live our values.  We especially like to celebrate the special moments that exemplify the behavior we want to see in all GitLab team members.
1. We recognize this through the #thanks channel, and sometimes also through a discretionary bonus.
1. [Anybody can recommend a discretionary bonus for a GitLab team member to the GitLab team member's manager](/handbook/incentives/#process-for-recommending-a-team-member-for-a-bonus-in-bamboohr) for a $1,000 at [the exchange rate](/handbook/total-rewards/compensation/#exchange-rates).
1. We are fixing this amount at $1,000 thoughtfully and purposefully. We want the focus to be on the value and the behavior, not on the perceived monetary impact to GitLab. This is about recognition.
1. A general guideline is that 1 in 10 team members might receive a discretionary bonus each month. This can vary greatly as we don't give out bonuses just to hit a quota, nor would we hold one back because a certain number had already been awarded.
1. There is no limit to the frequency with which someone can receive a bonus. If someone deserves a bonus a day after being nominated for one we should do a second one.
1. As with other bonuses, only active GitLab team members can be nominated to receive discretionary bonuses.
   Should a nominated team member leave GitLab, they will not receive a receive discretionary bonus.
   It's at the discretion of the manager if they still want to publicly recognize the team member's behavior for the nomination.

#### Valid and Invalid criteria for discretionary bonuses

Discretionary bonuses are to celebrate team members that live by our values. To make sure we are recognizing the right behaviors, below are some criteria to help you decide if a bonus meets the requirements for approval.

*Valid bonus criteria*

*  Going above and beyond what is expected in regard to the team member's role description.
*  Truly exceptional work.  Good and great is already expected in a team member's work and performance.
*  Providing additional coverage or project load for other team members.
*  Team member's work results in higher productivity or improved processes that create efficiences and results. This may also result in identifying cost saving initatives.

*Invalid bonus criteria*

* Supplement to income.
* For being nice and friendly.
* For being helpful.
* Doing a project or task that is core to their role.
* For help onboarding a new GitLab team member.
* Hitting sales target or quota
* Providing customer insight
* Partnering or collaborating with other groups within GitLab
* Working long hours or on weekends

*Example of a valid discretionary bonus*
The reason for the bonus is that this person (X, a Technical Writer) has single-handedly stepped in to act as a part-time documentation engineer in the absence of a full-time engineer.

The person's history of contributions is plain to see here: (Nominator included links to boards, issues or merge requests)

In particular, X's implementation of a Global Navigation and GitLab iconography for documentation are real highlights of X's impact for end-users. For developers, they have modernized the codebase and continues to work toward making `gitlab-docs` just like any other GitLab project for developers.

X's dedication to the Technical Writing team over a long period of time motivates me to do more than just thank X in #thanks channel. X has specifically:

* Been active to see the Technical Writing team to succeed: https://about.gitlab.com/handbook/values/#see-others-succeed

* X delivers for the team in lots of MVCs: https://about.gitlab.com/handbook/values/#minimum-viable-change-mvc, so we can get early value. For example, making iconography available using Markdown first, and planning to deliver other implementations later.

* X solutions are boring: https://about.gitlab.com/handbook/values/#boring-solutions, in that X aims to reuse as much GitLab code as possible, and raising instances where that isn’t the case as technical debt. For example, docs icons are the same library as icons in the product.

*Examples of invalid discretionary bonus*

* "X has been an integral part to my onboarding experience, working tirelessly to help me get access and answer to questions.  X truly embodies GitLab's value of effficiency, collaboration and results"
* "X has continually met or exceeding their sales quota for 3 months"
* "X is always willing to jump in even at the last moments to help out a team member"


For managers: as a reminder, please make sure when you are submitting the bonus in BambooHR that your description is clear and includes the valid criteria data needed for managers and PBP to review and approve.  Any bonus submitted that does not include clear and valid reasons for submittal may be denied and the manager will be asked to provide further content and details.  

#### Process for Recommending a Team Member for a Bonus in BambooHR

**Any GitLab team member**

1. Write a few sentences describing how the GitLab team member has demonstrated a specific GitLab value in their work.  Please make sure you have viewed the valid and invalid criteria listed above.  The nomination request should tie to our values and be detailed enough to ensure that the nomination meets the criteria.
1. Email that sentence to the GitLab team member's [manager](/company/team/org-chart/), suggesting a discretionary bonus, and set-up a 15 minute Zoom meeting to discuss your proposal.
1. Remember that the manager may or may not agree and has full discretion (with approval of their manager) on whether that person gets a bonus. If you are a people manager and you are not approving the team members nomination for a discretionary bonus, in the spirit of transparency and collaboration, it is strongly suggested that you have a conversation with the team member on why you are not approving the discretionary bonus.
1. Don't forget that you can also use the ```#thanks``` channel to recognize people, as well.

**Manager Process**

1. Login to [BambooHR](https://gitlab.bamboohr.com/home).
1. Navigate to the team member you would like to give the discretionary bonus to via the search field in the top right.
1. In the top right hand corner, click Request a Change.
1. Select Bonus.
1. Bonus date: today.
1. Bonus amount: 1000 USD
1. Bonus type: Discretionary Bonus
1. Nominated by: select relevant option
1. Bonus comments: Include the justification details how the GitLab team-member demonstrated one or more of our [values](/handbook/values) in this specific instance. There is a 255 character limit in this field. Please be as detailed as possible to ensure the approval chain has enough information to approve the bonus based on the valid criteria.  
1. Comment for the Approvers: Give the most relevant URL for the work they did, something interesting for others to look at.
1. Submit the bonus request.
1. Once the request is reviewed by the manager and People Business Partner for the group, you will receive an email from BambooHR with the subject line: "Your Bonus request has a response".
  * Verify the status of the request: either "Bonus Approved" or "Bonus Denied" You can also use this link to view the request to see if there were any comments (specifically if the request was denied.)
1. If the bonus is not approved, please do not communicate your nomination to the team member. Work with the manager, or whoever denied the bonus, to understand reasons why your nomination was not approved. Please note there may be confidential reasons (e.g., performance, conduct) for denying a bonus that a manager may not want to share with another team member.
  * If nominated by someone other than yourself, let that team member know as soon as possible if the bonus was denied. Keep in mind not to share confidential details.
1. Once approved please notify the team member outlining the reasons for the bonus. Please let the team member know they will see the bonus on the next available paycheck. If they are a contractor, please let them know to add this bonus amount to their next invoice.
1. Lastly, you must copy and paste the justification and url into the GitLab Slack channel [#team-member-updates](https://gitlab.slack.com/archives/CL55Q4U0K) to recognize their outstanding efforts (once the team member has been communicated to 1:1). Be sure to @ the person that's receiving the bonus as well so they're notified of the posting. Remember, a discretionary bonus is for recognition so make sure to take this final step.
1. If nominated by someone other than yourself, remember to keep that team member updated on the progress of their nomination. Particularly on when you will announce it in Slack!

**Approval Process:**

1. The Manager's Manager receives an alert from BambooHR and can approve or deny.
1. The request is then sent to the PBP of the group and to confirm that there is justification comment in the Bamboo record.
1. Once approved, the request is sent to the Total Rewards Coordinator to process the bonus in the applicable payroll. BambooHR automatically notifies the manager via email. If the Total Rewards Coordinator is taking time off, the Total Rewards Analyst will approve and process.
  * Approve the request within BambooHR and ensure the date and amount transferred properly to the BambooHR profile.
1. Notify Payroll of the bonus by updating the proper payroll google sheet based on entity.
  * United States: "Payroll Changes", Contractors: "Non-US contractor bonus changes", Everyone else: "Monthly payroll changes for non-US international team members".

#### Discretionary Bonuses for Working Groups
1. Sometimes a [working group](/company/team/structure/#working-groups) strongly displays GitLab Values over a period, project or situation. For this case, we have group discretionary bonuses.
1. As with individuals, we recognize those who make up that group through the #thanks channel and sometimes through a group discretionary bonus.
1. [Anybody can recommend a discretionary bonus for a working group through the managers of the individuals involved](/handbook/incentives/#process-for-recommending-a-team-for-a-bonus-in-bamboohr) for $100 per person at [the exchange rate](/handbook/total-rewards/compensation/#exchange-rates).

#### Process for Recommending a Working Group for a Bonus in BambooHR

**Any GitLab team-member**

1. Write a 1 sentence description of how the working group has demonstrated a specific GitLab value in their work.
1. Email that sentence to the managers of the individuals, suggesting a discretionary bonus, and set-up a 15 minute Zoom meeting with all the managers to discuss your proposal.
1. Remember that the manager(s) may or may not agree and they have full discretion (with approval of their manager) on whether their reports get a bonus.  Don't forget that you can also use the #thanks channel to recognize people, as well.

**Manager Process**

1. Login to BambooHR.
1. For each team member, select the team members you would like to give the discretionary bonus to and do the following:
1. In the top right hand corner, click Request a Change.
1. Bonus date: today
1. Bonus amount: $1000 (this is fixed, we don't vary the amount)
1. Bonus type: Discretionary Bonus
1. Bonus comments: 1-sentence justification that details how the team demonstrated one or more of our [values](/handbook/values) in this specific instance.
1. Comments for the approvers: leave empty
1. Submit the bonus requests to your manager for approval.
1. Once approved, you must copy and paste the justification and url into the GitLab Slack channel [#team-member-updates](https://gitlab.slack.com/archives/CL55Q4U0K) to recognize the teams outstanding efforts (once the team has been communicated to 1:1). Be sure to @ the people in teh team that's receiving the bonus as well so they're notified of the posting. Remember, a discretionary bonus is for recognition so make sure to take this final step.

**Approval Process:**

1. The Manager's Manager receives an alert from BambooHR and can approve or deny.
1. The request is then sent to the People Ops Admins for final approval and to confirm that there is a 1-sentence justification comment in the Bamboo record.
1. Once approved, the request is sent to the Total Rewards Analyst to process the bonus in the applicable payroll and notify the manager via email.
1. Notify Payroll of the bonus.
1. Per the email from Total Rewards Analysts, the manager will notify the team member of the bonus and the nomintaor will announce it on the GitLab Slack channel [#team-member-updates](https://gitlab.slack.com/archives/CL55Q4U0K). The nominator announces the “who” and “why” of the bonus. The announcement should include the 1-sentence justification of the bonus.

#### GitLab Anniversaries

At 10:00 UTC on Thursday of every week, `PeopleOps Bot` slack bot will send a shout out on the `#team-member-updates` channel congratulating all team members who celebrates a hire date anniversary that week.
Visit our [GitLab Anniversary Gift](/handbook/people-group/celebrations/#anniversary-gifts) section for more information about the gifts.

### Real Examples of Real Team Members Who Received Bonuses for Doing Great Things

* This document presents the case for awarding a UX team member an incentive. The UX team member is reliable, fair and respectful, consistently acting in the best interest of the company as well as the team.
  * Collaboration: The UX team member took on the extra duties of UX Lead and handled the interim duties seamlessly. She responded kindly to the community feedback on sidebar issue in 9.0 well. She personally helped the VP of Engineering finish the merit review process for the UX team.
  * The UX team member has greatly helped the UX Lead transition to her new role by assisting with meetings, transferring knowledge openly, and being available for questions whenever necessary.
  * Results and Efficiency: The UX team member quickly delivered screenshots for a partnership in a day or two. She did a great job with the UX team updates, providing clear and visual screenshots of what the team was working on. She helped the team deliver the UX improvements shown in those updates.

* A Support team member received a bonus for:
  * Results & boring solutions: He managed to swap the database from PG9.2 to PG9.6 without significant downtime. It was even boring. ­
  * Sharing: His issues, guidelines, monitoring, you­name­it are exemplary. He keeps raising the bar and leaving a written trace to follow when he is not around. ­
  * Efficiency: He always hits the nail and does the right thing, has a great sense of priorities and can jump into production to solve a _right now_ pain in a heartbeat. ­
  * Quirkiness: What to say? Do you want someone washing grapes or painting a wall in a call, just invite him.

* A Marketing team member received a bonus for:
  * Transparency: The marketing team member always works in the open. In our 1:1s she is very clear on her focus and aligns priorities with team priorities. Every thing she is working on links to an issue.
  * Efficiency: The marketing team member is an excellent example of someone who can get multiple things done in a short amount of time. She can efficiently manage many high quality projects without getting bogged down in the details.
  * Collaboration: The marketing team member worked with the VP of Scaling to update the general handbook to make it prettier. This shows she collaborates well outside of her functional group. The marketing team member has also been helping a colleague with content management.
  * Directness: The marketing team member gives excellent review feedback on blog posts. She is very direct and not afraid of perfection.

* A Product team member received a bonus for:
  * Collaboration:  Works together well with everyone and actively recruits opinions across the organization.
  * Results: Shipping consistent and meaningful improvements in issues, board, etc.
  * Efficiency: Actively avoids meetings and encourages async work.
  * Iteration: Reduces everything to its very minimal iteration, not paying with quality or usability, yet moving forward with each release.

### Referral Bonuses

Chances are that if you work at GitLab, you have great friends and peers who would be fantastic additions to our [Team](/company/team/) and who may be interested in one of the current [vacancies](/jobs/). To help us grow the team with exceptional people, we have [Referral Bonuses](/handbook/hiring/referral-process/) that work as follows:

We want to encourage and support [diversity](/handbook/values) and [frugality](/handbook/values) on our team and in our hiring practices, so we will offer the following referral bonuses once the new team member has been with the company for 3 months. We will award the highest cumulative dollar amount as outlined below:
* [$500](/handbook/total-rewards/compensation/#exchange-rates) base referral bonus for a new hire.
* [$1,000](/handbook/total-rewards/compensation/#exchange-rates) supplemental referral bonus for a hire from a [Location factor](/handbook/total-rewards/compensation/compensation-calculator/calculator/) less than 0.65.
* [$1,500](/handbook/total-rewards/compensation/#exchange-rates) supplemental referral bonus for a hire from an ["underrepresented group"](/handbook/incentives/#explanation-and-examples-of-underrepresented-groups).

The following is an example of a cumlative Referral Bonuses:
* Hire a refered Strategic Account Leader: $500
* With a location factor of 0.6: + $1,000
* who self-identifies as a underrepresented minority: + $1,500
* Cumulative referral bonus to the referring party: **$3,000**

#### How to make a referral

For information regarding the program details and team member eligibility and understanding, please visit our [guide in the Hiring section of GitLab's handbook](/handbook/hiring/referral-process/).

### Explanation and Examples of Underrepresented Groups

An underrepresented group describes a subset of a population that holds a smaller percentage within a significant subgroup than the subset holds in the general population. The accepted definition of “underrepresented minorities” from the National Science Foundation and other major research institutions focuses on racial and ethnic groups whose representation in a profession is significantly below their representation in the general population.

This currently includes:
* Women in Engineering, Product & Sales - Globally
* Women in [Leadership](/company/team/structure/#organizational-chart) - Globally
* African American/Black - Globally
* Hispanic American/Latino - Globally
* Native American/Native - Globally
* Hawaiian/Pacific Islander - Globally
* Military Veteran - North America

While any non-majority group in the workplace is a “minority”, they are not considered to be “underrepresented” unless their race/ethnic representation in the general population is higher than their representation in the profession.  Total Rewards Analysts will confirm the bonus amount when it is entered into BambooHR.
    * If the new hire is from an underrepresented group as defined above, the higher amount will be paid to the team member for the referral.
    * _Exceptions: no bonuses for hiring people who report to you directly or are in your direct reporting chain, and no bonus for the executive team (VP and above)._

* In the event that more than one GitLab employee refers the same team member for the same role the People Ops team will ask the new team member to confirm who they were referred by (who should get the credit). If they mention two or more people then the bonus will be split between them.

* In the event that someone wants to refer another candidate to GitLab before they have started the referring party must have a sign contract at the time of the new candidate's application.

* In the event that a GitLab sourcer adds a candidate to GreenHouse and the recruiter screens the candidate a referring party cannot be added to their profile after. The candidate source would be Prospecting by the GitLab sourcer.

If a team member has been referred, the People Experience team will review team members' self-identification data in BambooHR including Gender, Ethnicity and Veteran status to determine if the team member qualifies as belonging to an underrepresented group. The People Experience team will edit the referrer's referral bonus as applicable. Total Rewards Analysts will process the bonus.

#### Document a future bonus in BambooHR

1. Go to the team member who referred the new team member in BambooHR
1. Under the "Jobs" tab
1. Under the "Bonus" table, click "Add Bonus"
1. The bonus date is 3 months from the new team member's start date
1. Enter the bonus amount, dependent on above
1. Enter the bonus type to be a Referral Bonus
1. Enter a note stating that this is a future bonus (this will be changed once the bonus has been paid)

#### Notification to Process a Bonus

1. BambooHR will send an email to Total Rewards Analysts on the date that the referral bonus should be paid for processing through the applicable payroll system.
1. Notify Payroll of the bonus by updating the proper payroll google sheet based on entity.
  * United States: "Payroll Changes", Contractors: "Non-US contractor bonus changes", Everyone else: "Monthly payroll changes for non-US international team members".
1. Once the bonus has been processed, change the note in BambooHR from "future" to "paid".
1. Reply all to the email confirming the bonus has been processed in BambooHR and payroll.

### Visiting Grant

As we continue to prioritize the health and safety of our team members, GitLab is temporarily suspending the Visiting Grant program  until further notice.  We continue to monitor the situation and will update this information and the [travel policy](/handbook/travel/#travel-guidance-covid-19) accordingly.

This means that at this time we ask team members to refrain from booking travel that would normally be covered under the visiting grant. This includes local events and co-working days that don't require long-distance travel. In the event you had previously planned a visit, please cancel it and attempt to maximize any credits for future travel or refunds that you may be eligible for.

For those team members who have already completed visits, please expense your visit per the instructions below.

GitLab is an [all-remote company](/company/culture/all-remote/) with GitLab team-members all over the world (see the map on our [Team page](/company/team/). If you want to visit other team members to get to know them, GitLab will assist with travel expenses (flights, trains, and ground transportation to and from airport) for a total of up to $150 for every team member that you visit and work with. Please note lodging is excluded. To be clearer, if you meet 2 [GitLab team members](https://about.gitlab.com/handbook/communication/#top-misused-terms) during your visit, the maximum limit of your visiting grant could be $150x2. You don't need to work on the same project or team, either, so long as you discuss work for at least part of the time you're meeting. Before booking any travel using the travel grant budget please discuss your proposed travel with your manager. We encourage team members to utilize the travel grant, however in some cases - for example, a team member has performance issues related to their role - the visiting grant would not be applicable.

Note that meals and local travel while visiting are not typically covered for anyone as that wouldn't really be fair to those being visited. It may be acceptable to cover a meal, however, if the meeting is pre-announced to other people in the region to encourage as many of them to attend as possible and there are four or more GitLab team-members attending.

There are many regular meet-ups of GitLab team-members in many cities. We have a [shared calendar][meetup-calendar] to view the schedule. Subscribe to it, make use of the visiting grant, and join meet-ups near you (or far away!).

Douwe and Robert took advantage of the visiting grant when they traveled to [49 different colleagues in 20 cities, in 14 countries, on five continents, in 6 months](/blog/2017/01/31/around-the-world-in-6-releases/). Inspired by them, Dimitrie went on a similar journey and provided a [great template](https://docs.google.com/spreadsheets/d/1Cwi2PfO1HeDm8Lap9J2tPmfYFaC8AmmKQg1fCmtdGI4/edit?usp=sharing). Also, check out [tips on working remotely while abroad](/company/culture/all-remote/#tips-on-working-remotely-while-abroad).

To claim the grant, include a line item on your expense report or invoice along with the receipt of your flights, trains, and/or transportation to and from the airport with a list of the team members you visited. The expense report may be submitted during the first month of travel or up to 3 months after your trip has concluded. That said, if it's more [frugal](/handbook/values/#efficiency) to book and expense your travel further in advance, please do so.

[meetup-calendar]: https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV85cWZnajRuMm9nZHMydmlhMDFrb3ZoaGpub0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t

### Significant Life Event Grants

As we continue to prioritize the health and safety of our team members, GitLab is temporarily suspending the Significant Life Event Grant program until further notice.  We continue to monitor the situation and will update this information and the [travel policy](/handbook/travel/#travel-guidance-covid-19) accordingly.

Recognizing that [GitLab Team Members](https://about.gitlab.com/handbook/communication/#top-misused-terms) may wish to share significant events in each other's lives, such as weddings or civil partnerships, GitLab will assist with travel expenses to attend these events. This grant works the same way as the [Visiting Grant](#visiting-grant), except the reimbursement limit is $300 per team member you visit at an event.

#### Sharing your Visiting Grant stories

If you're a GitLab team member who has traveled and utilized GitLab's Visiting Grant or are planning to do so soon, consider sharing your story! You can submit your experience using the [GitLab Visiting Grant Story Submission Form](https://docs.google.com/forms/d/1wjK5R994ahCirsyEQoDuQpcEfOaNWr1_mpa1LkYR2yg). If you've made use of the Significant Life Event Grant, please check with the team member who invited you to their event if they are comfortable with you sharing their story first.

These stories are useful in showing the world how we stay connected as a [geographically diverse team](/company/culture/inclusion/#fully-distributed-and-completely-connected). This is important as we [recruit the world's best talent to join us](/jobs/), as well as encouraging colleagues to take a leap, explore a new culture, and visit a team member in a new locale. They may be shared on GitLab's social media platforms, on hiring portals such as Glassdoor, or on the [GitLab blog](/blog/).

Each quarter, 3 GitLab team members who submit Visiting Grant stories will be randomly selected for a $50 voucher for the [GitLab Swag Shop](https://shop.gitlab.com/)!

### GitLab Gold

Every GitLab team member can request the [Gold](/pricing/#gitlab-com) tier for GitLab.com.
In case a team member has separate private and work accounts on GitLab.com, they can request it for both. This incentive **does not** apply to groups owned by GitLab team members (Group-level Gold features such as epics will not be available for Gold GitLab team-member personal accounts, for instance).

In order to request this benefit please [submit this form](https://docs.google.com/forms/d/e/1FAIpQLSddexI8VZTCiyxme1_7QtbQZ6WoIJRlHdaI2Gi6PD8Eti-DLQ/viewform). Your account(s) will be upgraded to the Gold tier within 30 minutes of submission.
