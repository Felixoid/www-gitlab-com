---
layout: handbook-page-toc
title: "Sales Development"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

As a Sales Development Representative (SDR), you focus on outreach, prospecting, and lead qualification. To do so, you need to have an understanding of not only product and industry knowledge, but also sales soft skills, and internal tools and processes. This handbook page will act as a guide to those topics as well as general information about the SDR team.

## Reaching the Sales Development Team (internally)

#### Slack Channels

* **Main Channel** = [`#sdr_global`](https://gitlab.slack.com/messages/C2V1KLY0Z)      
* **Announcements** = [`#sdr-fyi`](https://app.slack.com/client/T02592416/C011P828JRL)       
* **Conversations** = [`#sdr-conversations`](https://gitlab.slack.com/messages/CD6NDT44F)       

##### AMER
* **All** = [`#sdr_amer`](https://gitlab.slack.com/messages/CM2GAVC78)      
* **Commercial + LATAM** = [`#sdr_amer_commercial`](https://gitlab.slack.com/messages/CM276FD2B)        
* **East** = [`#sdr_amer_east`](https://gitlab.slack.com/messages/CGTF184EB)      
* **West** = [`#sdr_amer_west`](https://gitlab.slack.com/messages/CLU1A6BA5)
* **Named** = [`#sdr_amer_named`](https://app.slack.com/client/T02592416/CUFRP6U6Q)
* **Acceleration** = [`#sdr_acceleration_team`](https://gitlab.slack.com/messages/CKTJDKNRX)

##### EMEA
* **All** = [`#sdr_emea`](https://gitlab.slack.com/messages/CCULKLB71)      
* **Commercial** = [`#sdr_emea_commercial`](https://gitlab.slack.com/messages/CM0BYV7CM)
* **Acceleration** = [`#acceleration_emea`](https://gitlab.slack.com/messages/GV864K8KD)

##### APAC
* **All** = [`#sdr_apac`](https://gitlab.slack.com/messages/CM0BPBEQM)

#### Issue Boards & Team Labels 

[SDR Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/707128): used to track relevant GitLab issues involving the SDR team. This is a global issue board and will capture all issues in any group/sub-group in the GitLab.com repo when any of the following *scoped* labels are used. 

[SDR Event Tracker](https://gitlab.com/groups/gitlab-com/-/boards/1718115): used to track upcoming events globally.

- `SDR` - issues concerning the SDR team. This label will typically be removed and changed to one of the below labels once accepted by our team.
- `SDR::Priority` - projects that we would like brought into RevOps meeting for feedback/next steps from other teams
- `SDR::Planning` - Discussion about next steps is in progress for issues concerning the SDR team
- `SDR::In Progress` - SDR action item is presently being worked on
- `SDR::On Hold` - SDR project is put on hold after agreement from SDR leadership team 
- `SDR::Watching` - No direct SDR action item at this time, but SDR awareness is needed for potential support/questions
- `SDR::Enablement Series` - Label to track and monitor upcoming topics for the SDR enablement series. All of these issue roll up to this epic.
- `SDR::AMER Event Awareness` - Americas SDR awareness is needed for potential support/questions in regards to events
- `SDR::APAC Event Awareness` - APAC SDR awareness is needed for potential support/questions in regards to events
- `SDR::EMEA Event Awareness` - EMEA SDR awareness is needed for potential support/questions in regards to events

## Onboarding
In your first month at GitLab we want to help ensure you have everything you need to be successful in your job. You will go through enablement videos, live sessions and activities covering a wide range of getting started topics. 
- [SDR onboarding goals and process](/handbook/marketing/revenue-marketing/sdr/sdr-playbook-onboarding/)

## Segmentation
The SDR team aligns to the [Commercial](handbook/sales/commercial/) and Large sales teams. These teams are broken down into three segments: Large, Mid-Market and SMB which are based on the total employee count of the Global account. *Note: The commercial sales team includes both Mid-Market and SMB. This segmentation allows SDRs and Sales to be targeted in their approach and messaging. The segments are aligned to a region/vertical and then divided one step further via territories in the regions. Our single source of truth for determining number of employees is DataFox followed by DiscoverOrg. 
* [Sales segmentation](/handbook/business-ops/resources/#segmentation)
* [Sales territories](/handbook/sales/territories/)
* [Determining if a lead is in your territory](/handbook/business-ops/resources/#account-ownership-rules-of-engagement)
* [SDR and Sales alignment](/handbook/marketing/revenue-marketing/sdr/sales-sdr-alignment/) - working with your SAL or AE!

**SDR Team Breakdown**
Geo = SDR teams that align directly to sales territories and AE/SALs that work a geo territory
     *  Commercial (MM & SMB)
     *  West Enterprise
     *  East Enterprise
[Named](/handbook/marketing/revenue-marketing/sdr/#named-sdr-team) = SDR team that supports only Named SALs/Accounts
[Acceleration](/handbook/marketing/revenue-marketing/sdr/#acceleration-marketing) = Aligns to account based marketing target accounts and Large geo territories for a period of 60 days in which they will work closely with the geo SDR to warm up the territory. To read more about this alignment please head over to the [Account Based Marketing page](/handbook/marketing/revenue-marketing/account-based-marketing/)

## SDR Compensation and Quota

**Quota**

You will have a monthly quota that is based soley on [Sales Accepted Opportunities (SAOs)](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao). During the Ramp Period (Months 1-2) once you have achieved quota, each additional opportunity is paid at the Month 3 base rate.
*  Month 1 - 0% SAOs 
    *  During your first month of employment you will be paid your OTE Commission by completing the GitLab SDR Onboarding assessments and issue. 
*  Month 2 - 50% SAOs
*  Month 3 onwards - 100% SAOs  

**Accelerator** 

If you exceed your SAO quota, there is an accelerator based on progressive tiers of quota attainment:

* Base tier, 0-100%: Base Rate Payment (Monthly Variable @ Target/quota)
* Accelerator 1, 101%-150%: 1.1 times Base Rate Payment in excess of Quota
* Accelerator 2, > 150%: 1.5 times Base Rate Payment of quota

**Closed won business from SAOs** 

As an SDR at GitLab, you will receive 1% commission for any closed won opportunity you produce. To be paid on your closed won business, your activity should clearly depict that your prospecting work sourced the opportunity.

**SAO Salesforce Reports**
* [Outbound SDR created opportunities](https://gitlab.my.salesforce.com/00O61000003nmhe)
* [Outbound SDR sales accepted opportunities](https://gitlab.my.salesforce.com/00O61000003nmhU?dbw=1)
*If you believe an opportunity you sourced is not reflected in the reporting, notify your manager.

**Activity & Results Metrics**

While the below measurements do not impact your quota attainment, they are monitored by SDR leadership. 
* Results
  * Pipeline value of SDR sourced opportunities
  * IACV won from opportunities SDR sources
* Activity
  * % of named accounts contacted
  * Number of opportunities created
  * Number of calls made
  * Number of personalized emails sent
  * Daily outbound metrics 
    * At least 30 emails/day
    * At least 30 dials/day

## SDR Standards
* Meet monthly quota of Sales Accepted Opportunities (SAOs)
* Be able to identify where a prospective customer is in their buying cycle and take appropriate action to help them along their journey towards becoming a customer.
* Have a sense of urgency - faster response time directly influences conversion rates.
* Work lead records within Salesforce by leveraging sequences in Outreach
* Maintain a sense of ownership of data integrity in Salesforce and Outreach: cleaning up and assuring accuracy and consistency of data.
* Adding any information gathered about a LEAD, CONTACT, ACCOUNT from the data sources Datafox & DiscoverOrg.
* Attend each initial qualifying meeting (IQM) with your AE/SAL. Document notes and communicating with your AE/SAL after the meeting.

## SDR Tools
* Salesforce
* [Outreach.io](/handbook/marketing/marketing-operations/outreach/)
* DataFox
* DiscoverOrg
* LinkedIn Sales Navigator
* Chorus
* [Drift](/handbook/marketing/marketing-operations/drift/)
* [LeanData](/handbook/marketing/marketing-operations/leandata/)

## SDR Workflow & Process
As an SDR, you will be focused on leads - both inbound and outbound. At the highest level, a lead is a person who shows interest in GitLab through inbound lead generation tactics or through outbound prospecting.

### Qualified Meeting

**What is considered a Qualified Meeting?**

A Qualified Meeting is a meeting that occurs with a prospect who is directly involved in a project or team related to the potential purchase of GitLab within a buying group, either as an evaluator, decision maker, technical buyer, or *influencer* within an SDR Target Account. 
To be considered qualified, the meeting must occur (prospect attends) and new information must be uncovered that better positions the SAL and Regional SDR to create a SAO with that particular account based on the SAO Criteria as stated in the handbook.

**Qualified Meeting Process**

A “New Event” IQM gives an SDR the ability to track every meeting that is scheduled and held in Salesforce without having to prematurely create a net new opportunity. SDRs can tie any event (IQM) to an existing account, opportunity, etc. The purpose of creating an IQM in salesforce is to provide the ability to track how many IQMs are being created per SDR and what the results are of these IQMs. 

The purpose of creating an Initial Qualifying Meeting (IQM) in Salesforce is to track top of funnel pipeline meetings that are booked, occur, and convert to Sales Accepted Opportunities (SAOs). SDRs can track every meeting that is scheduled and held in Salesforce without having to prematurely create a net new opportunity prematurely. SDRs can tie any IQM to an existing account or opportunity from a Lead or Contact record. 

**How to create an IQM in SFDC**

Within Salesforce there is a button titled “New Event” which you can access in the global actions bar on your Salesforce “Home Screen”, and in the “Open Activity” section of any Lead, Contact, Account, and Opportunity record. [This image](https://docs.google.com/document/d/1NUP7Ze_e-1sKGAELim-_yaLDnN9rf0wVKWfug0lhtyI/edit?usp=sharing) will show you where to click!

If creating an Event: IQM under a Lead or Contact record, click the event button in the global actions bar and complete all relevant fields which should include your pre-IQM notes in the description field. 

Once a meeting with a prospect is confirmed, the SDR will send either a Google calendar invite to the prospect (include their SAL) and/or send a meeting in Outreach.io with a proposed agenda. SDR should share Event IQM SFDC link with SAL prior to IQM occuring - scheduling a pre/post IQM sync is best practice. 

Once the IQM occurs, then the SDR can add their Post-IQM notes to the Description Field and update the Event Disposition accordingly. If the IQM meets criteria and has next steps, then it's an SAO and the SDR should convert Lead to Contact record and create new opportunity with same process as outlined in Handbook. If the IQM occured and the SDR/SAL/AE uncovered helpful information but ultimately, there are no next steps, the the SDR should dispostiion Event IQM as Qualified Meeting (do not create a new opportunity). If Event IQM was a no-show and/or needs to be rescheduled, then SDR owns reaching out to prospect to reschedule and dispoistions Event IQM accordingly. 

**How to Create an IQM in Outreach**

In Outreach, you have the ability to book a meeting directly from a prospect overview. Even better, this can be done while you’re on a call with a prospect or immediately after without having to leave Outreach. Booking a meeting from Outreach will automatically pull the following fields directly into an SFDC event object:
* Subject (Lead/Contact Name - IQM)
* Location (Zoom Link)
* Start & End times
* Assigned To (SDR name)
* Event Disposition (Required)
* Activity Source (Required)
* Booked By
* Attributed Sequence Name
* Description (add Pre/Post IQM Notes)
* Related To (You can select a parent opportunity if applicable or relate to the opportunity you create from Qualified Meetings)

Select “book meeting” from the prospect actions menu. For a visual/video walkthrough of this process take a look [here](https://docs.google.com/document/d/1oB3RsjqCIUoXvX3F4DTrYOLh4X0K-7tzUPs3jYAQrwg/edit?usp=sharinghttps://docs.google.com/document/d/1oB3RsjqCIUoXvX3F4DTrYOLh4X0K-7tzUPs3jYAQrwg/edit?usp=sharing).

**Inbound Lead Generation**

Inbound lead generation often uses digital channels - social media, email, mobile/web apps, search engines, websites, etc - as well as in-person marketing activities to meet potential buyers where they are. When people interact with GitLab, we use lead scoring to assign a numerical value, or points, to each of these leads based on their actions and the information we have about them. Once a lead reaches 90 points, they are considered a [Marketo Qualified Lead](/handbook/business-ops/resources/#mql-definition) or MQL.

**Working Inbound Leads**

SDRs are responsible for following up with MQLs by reviewing their information, reaching out, and working with them to understand their goals, needs, and problems. Once you have that information, you can use our [qualification criteria](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao) to determine if this is someone who has strong potential to purchase our product and therefore should be connected with sales for next steps. As you are the connection between Marketing and Sales you want to make sure every lead you pass to the Sales team is as qualified as possible.

These MQLs will show up in your lead views in Salesforce. The views, listed below, allow you to see your leads in a categorized way to simplify your workflow. Leads are routed to you and flow into your views via the tool [LeanData](/handbook/marketing/marketing-operations/leandata/) which takes each lead through a series of conditional questions to ensure it goes to the right person. Even though all SDRs leverage the same views, they will only show you leads that have been specifically routed to you. You will be responsible for following up with all of the leads in your MQL views by sequencing them using [Outreach.io](/handbook/marketing/marketing-operations/outreach/). Once sequenced, their lead status will change and they will move from your MQL views allowing you to have an empty view. Managers monitor all views to ensure they are cleared out. 

**Lead Views**
* My MQLs - all leads that have accumulated [90 points based on demographic/firmographic and/or behavioral information](/handbook/marketing/marketing-operations/#lead-scoring-lead-lifecycle-and-mql-criteria). These leads should always be your first priority. 
* My Inquiries - leads that haven't quite reached 90 points yet. These leads are to be looked at once your MQLs are worked. 
* My Qualifying - leads that you are actively qualifying in a back and forth conversation
* My Leads w/new LIM - Stale MQL, Nurture, Raw w/new LIM - leads that at one point were an MQL or had a status of nurture or raw but have recently taken an action. 

**Contact Views**

You  also have two contact views. COMING SOON: routing contacts to these contact views if someone is already a contact in our system but has taken a lead qualifying action or requested contact. After a certain amount of time, the record ownership will transition back to SAL/AE ownership.
*  My MQLs - all contacts that have taken a recent lead qualifying action. Determine if this contact is closely engaged with Sales and reach out accordingly
*  My Qualifying - contacts you are actively qualifying in a back and forth conversation

**Lead and Contact Status / Outreach Automation**

Lead/contact statuses allow anyone in Salesforce to understand where a lead is at in their journey. The automation mentioned below is all done through an Outreach.io trigger. 

* Once a lead has been placed in an Outreach sequence, the lead status will automatically change from MQL, Inquiry or Raw to Accepted marking that you are actively working this lead. 
* When a lead responds to you via email, their status will again automatically change. This time it will change from Accepted to Qualifying. *If you are not working on qualifying this lead further, you will need to manually change the status to Nurture so that this lead is back in Marketing nurture and isn’t stuck in your qualifying view. 
* If a lead finishes an Outreach sequence without responding, the lead status will automatically change to unresponsive or nurture in three days if there is still no response.
* The only time you will need to manually change lead status outside of what is mention in the previous bullets is if you for some reason don't use an Outreach sequence to reach out to someone or if you need to unqualify a lead for bad data etc.
* If you check the `Inactive lead` or `Inactive contact` checkbox, signifying that this person no longer works at the company, any running sequence will automatically be marked as finished. 

### Inbound Lead Workflow Checklist

**IMPORTANT:** For Mid Market SDR reps, if the account your lead matches has the account type 'customer,' this lead will need to be worked by your aligned AE. Update the lead owner and let them know. You can determine this via the LeanData section in Salesforce or by leveraging the 'Find Duplicates' button at the top of the lead in Salesforce. Look to see if the account this lead belongs to has a CARR value (total annual recurring revenue this customer brings) associated with it. Lastly, you can [search for the account](/handbook/support/workflows/looking_up_customer_account_details.html#finding-the-customers-organization) to look for the CARR value. Convert the lead to a contact and attach it to the customer account on conversion. 

**Checklist**
* Make sure the lead is in your territory/segmentation
    * [Parent/Child Segmentation:](/handbook/business-ops/resources/#account-ownership-rules-of-engagement) all accounts in a hierarchy will adopt the MAX segmentation of any account in the hierarchy.
    * If the lead is not yours, please route it to the appropriate rep
* Make sure name is capitalized correctly
* Check for duplicates and merge or route to the appropriate rep by using the `Find Duplicates` button at the top of the lead page.
* Add or update any missing information on the lead. The SDR team is responsible for making sure lead data is up to date and as detailed as possible.
    * Check Datafox to confirm and/or add company location and company phone number 
        * If there is no Datafox information, use DiscoverOrg and lastly LinkedIn
        * If you feel there is a discrepancy in segementation/total employee count, you can adress that by following [this](/handbook/business-ops/resources/#sales-segment-and-hierarchy-review-process) process.
    * If you have a company email domain, use LinkedIn to find the lead's job title  
* Has this person opted out of GitLab communication? In order to remain compliant we cannot follow up via phone or email as we don’t have granular opt-out. You need to change lead status to unqualified and unqualified reason to unsubscribe.

**Outreach & Marketo**

If the lead is yours to work based on all of the above, sequence them in Outreach using the master sequence that correlates with the Last Interesting Moment. You can also look at the initial source and Marketo insights to get a holistic view of what a lead is looking into. There is an Outreach collection of sequences for inbound leads for each region. These collections contain a master sequence for nearly every inbound lead scenario. 
* **Master Sequence**: a sequence created by SDR leadership that should be used by all reps to follow up with inbound leads
*  [**Sequence collection**](https://support.outreach.io/hc/en-us/articles/360009145833-Collections): group of sequences compiled by region
*  **Last Interesting Moment**: data pulled in from our marketing auotomation software, [Marketo](/handbook/marketing/marketing-operations/marketo/), that tells you the last action a lead took.
*  [**Initial source**](https://about.gitlab.com/handbook/business-ops/resources/#initial-source): first known action someone took when they entered our database
*  **Marketo Sales Insights (MSI)**: a section on the lead/contact in Salesforce that shows you compiled data around actions a lead/contact has taken
*  **High touch and low touch sequences**: a high touch sequence should be used for high-quality leads. High touch sequences require you to add in more personalization and have more touch points across a longer period of time. Low touch sequneces are typically automated and run for a shorter period of time. A high quality lead has valid information for at least two of the following fields:
    * Company email domain or company name
    * Phone number
    * Title

### Qualification Criteria and SAOs

Qualification criteria is a minimum set of characteristics that a lead must have in order to be passed to sales and become a Sales Accepted Opportunity (SAO). You will work to connect with leads that you get a response from to obtain this information while assisting them with questions or walking them through how GitLab might be able to help with their current needs. The qualification criteria listed in the linked handbook page below aligns to the 'Qualification Questions' sections on the LEAD, CONTACT, and OPPORTUNITY object in Salesforce. In order to obtain an SAO you will need to have the 'required' information filled out on the opportunity.  
*  [Qualification criteria needed](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao)

When a lead meets the qualification criteria and you have an IQM/intro call scheduled with your AE/SAL, you will convert the lead to an opportunity. 
* [ How to create an opportunity](/handbook/business-ops/resources/#creating-a-new-business-opportunity-from-lead-record)

### Questions about a lead?

*  To determine where a lead should go if it was routed to you mistakenly, use the Territories Mapping File - SSoT
*  If you have a question about the behavior of a lead or any other operational issue, use the slack channel #mktgops

**Technical questions from leads?**
* [Docs](https://docs.gitlab.com/)
* Slack channels
    * #questions
    * #support_self-managed
    * #support_gitlab-com

### Working with 3rd Parties

1.  Gather billing and end user details from the reseller:
    * Billing company name/address:
    * Billing company contact/email address:
    * End user company name/address:
    * End user contact/email address:
    * [Snippet in outreach](https://app1a.outreach.io/snippets/362)
2. Create a new lead record with end user details 
    *  Ensure that all notes are copied over to new LEAD as this is the LEAD that will be converted.
3. Converting the new lead
    * Name opp to reflect reseller involvement as shown here: “End user account name via reseller account name”
4. Convert original reseller lead to a contact associated with the reseller account
    *  If an account does not already exist for the reseller, create one when converting the lead to a contact.
    *  Assign record to the same account owner
    *  Do NOT create a new opportunity with this lead.
5. Attach activity to the opportunity
    * On the reseller contact, go to the activity and link each activity related to your opportunity to the opp.
        * Activity History > click edit to the left of the activity > choose 'opportunity' from the 'related to' dropdown > find the new opportunity > save
6. Update the opportunity
    * Change the business type to new business and stage to pending acceptance.
    * Under contacts, add the reseller contact, role as reseller, and primary contact.
    * Under partners, add the reseller account as VAR/Reseller"

### Drift Chat Platform
We use a the chat platform Drift to engage site visitors. As an SDR, you are expected to be active on Drift throughout your work day. The [tool's handbook page](/handbook/marketing/marketing-operations/drift/) will walk you through guidlines, best practices and troubleshooting. 

## Outbound Prospecting 

Outbound lead generation is done through prospecting to people who fall into our target audience and could be a great fit for our product. Prospecting is the process of finding and developing new business through searching for potential customers with the end goal of moving these people through the sales funnel until they eventually convert into customers. 

You will work closely with your dedicated SAL or AE to build a strategy for certain companies, parts of your territory or personas you as an aligned team want to target. It is crucial that you are very intentional and strategic in your approach and always keep the customer experience in mind. When reaching out, offer value and become a trusted advisor to ensure we always leave a positive impression whether there is current demand or not.

### Targeted Accounts

**Targeted account**: an account in which you as an SDR have developed an account plan with your AE or SAL and are actively targeting via outbound prospecting.

Targeted Accounts should be kept up-to-date in Salesforce in real-time, and checked as soon as you begin the account planning process. If the account does not have activity associated with leads/contacts over a 30-day period, the account should not be flagged as targeted. Each SDR is allowed 40 targeted accounts at any given time. If you feel this is too low, please connect with your manager.

If you check the targeted checkbox, you are committed to:
* Developing an account plan with your AE or SAL
* Actively outbounding to ideal personas leveraging person

Keeping your targeted accounts up to date is especially crucial when the Acceleration Team is entering your territory. The Acceleration Team typically generates a regional account target list 60-days prior to entering a region. At that point, any SDR target accounts” will be suppressed. Two weeks prior to kickoff, the Acceleration Team will schedule a kickoff call to:
* Review coverage dates
* Review target account list
* Ensure that targeted accounts and regional suppression lists are up-to-date
* Discuss account plans / targeting strategy

### Outbound Workflow
**IMPORTANT**: EMEA reps, old leads/contacts can only be called or emailed if they were in our system from May 2018 or later. Any lead/contact that was brought into our system prior would need to opt in to communication. Any new leads/contacts can only be called or emailed if they have been brought in from DiscoverOrg. You can also leverage LinkedIn Sales Navigator. 

The [SDR outbound framework](https://docs.google.com/document/d/1GkocgndrILDvKa56NODp-h2qmn5sL2YIzmnoESuhOJU/edit) will walk you through both strategy and execution of outbound prospecting.

### Event Promotion and Follow Up Assistance

The SDR team will assist the Field Marketing and Corporate Marketing teams with campaign and event promotion and follow up. Learn more about [Field](/handbook/marketing/revenue-marketing/field-marketing/) or [Corporate Marketing](/handbook/marketing/corporate-marketing/) via their linked handbook pages. When these teams begin preparing for an event, they will create an issue using an issue template. Within that template is a section titled ‘Outreach and Follow Up’ for the SDR team to understand what is needed from them for each event. 
*  [Field Event Template](https://gitlab.com/gitlab-com/marketing/field-marketing/tree/master/.gitlab/issue_templates)
*  [Corporate Event SDR Support Template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/Event-SDR-Support-Template.md)

#### Field Process

When our Field Marketing Managers (FMMs) begin preparing for an event or micro campaign, they will create an issue using a [regional issue template](https://gitlab.com/gitlab-com/marketing/field-marketing/tree/master/.gitlab/issue_templates). Within that template is a section titled ‘Outreach and Follow Up’ for the SDR team to understand what is needed from them.

The FMM will loop in the appropriate SDR manager(s) on this issue as well as fill out/ensure the following sections are accurate:
* Pre-event/campaign goals
* Post-event/campaign goals
* Targets (title and number of attendees) 
* Post-event needs

Once the sections are populated and up to date, the SDR manager is looped in. They will then elect an SDR with bandwidth to begin completing the steps in each of the above sections. The SDR will: 
* Read the issue to get an understanding of the event
* Complete the tasks under the ‘Outreach and Follow Up’ section based on the deadlines next to each bullet. 
* Slack their manager and/or the FMM if they have questions. 

#### Deadlines
**Event Promotion**

*  The SDR manager needs to be assigned a minimum of 30-days prior to the event, but the sooner they can start prepping, the better.
*  The SDR manager(s) should have their teams begin compiling lists as soon as possible. Target lists need to be completed three weeks prior to the event so that we will have three weeks of promotion
*  The SDR project lead needs to have their target list as well as the shared sequence completed, reviewed and approved at least three weeks prior to event so that we will have three weeks of promotion

**Event Follow Up**

*  SDR project lead needs to have the follow up sequence created and reviewed with management and FMM one week prior to event. 
*  FMM will ensure leads from Field Marketing initiatives will be routed to the SDR within 48 hours.

**Sequence Information**
- Please clone one of the Master Event Sequences found in the [Events Outreach collection](https://app1a.outreach.io/sequences?direction=desc&order=recent&content_category_id%5B%5D=6) and pre populate as many of the variables as possible. If all of the variables can be populated manually or through Outreach/Salesforce, change the email type from ‘manual’ to ‘auto.’ Work with your manager and the FMM to make any other additional adjustments that may be needed.

#### Corporate Process
The corporate process is the same as the field process detailed above with the exception that they have a seperate issue when looping in the SDR team for assitance that will link to the larger event issue. 

### Working with the Community Advocacy Team
[Community Advocates](/handbook/marketing/community-relations/community-advocacy/) support the [Education](/handbook/marketing/community-relations/education-program), [Open Source](/handbook/marketing/community-relations/opensource-program) and [Startups](/solutions/startups) Programs, and their Program Managers. When a lead fills out the form to apply for one of these programs, Salesforce lead ownership will automatically change to the Community Advocate Queue. If this lead was in an Outreach sequence, it will automatically be marked as finished. The Community Advocacy team will then work to qualify the lead. If the lead does not end up qualifying for one of the programs, they will be passed straight to sales.   

# Named SDR Team 
The Named SDR Team supports only Named SALs/Accounts. Their comp structure is based on SAOs and Qualified Meetings (IQMs) per month.

### Named SDR Compensation
The Enterprise Named SDR team compensation is based on two key metrics: Qualified Meetings Occurred and Sales Accepted Opportunties (SAOs).
The Named SDR Team has a monthly goal for both Qualified Meeting (30%) and SAO (70%) component which works out to a 70/30 variable split. Accelerators will be consistent with those outlined in SDR Compensation and Quota. For accelerators, the individual quota is determined by each SDRs SAO attainment.

# ABM Acceleration SDR Team

The ABM Acceleration SDR team is responsible for generating awareness, qualified meetings, and pipeline for the GitLab sales team. The team uses tactics like vertical- / industry-specific targeting, digital advertising, outbound prospecting, social selling, warm calling, and creative outreach to evangelize and educate prospects on what is possible with GitLab. There are two primary focus areas for the Acceleration team in FY21:

1.  [Account-based Marketing Account Management](/handbook/marketing/revenue-marketing/account-based-marketing/#sdrabm-alignment) 
2.  [Territory Warm Up](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#helpful-definitions)

## Expectations

*  Meet monthly quota
*  Maintain a high sense of autonomy to focus on what's most important
*  Continuously contribute to help the team improve quality and efficiency
*  Participate and lead account planning, execution, and cross-functional meetings 
*  Lead account planning meetings with SAL and Regional SDR teams 
*  Participate in initial qualifying meetings, discovery calls, and follow-up conversations
*  Maintain exceptional Salesforce hygiene, logging all prospecting activity, conversations, and account intelligence uncovered
*  Incorporate and train Regional SDRs
*  Generate IACV Pipeline

## Alignment

The ABM Acceleration team aligns to the Tier 1 and Tier 2 account based marketing accounts (ABM) and is also responsible for "Territory Warm-up" where they align to Large geo territories for a period of 60-90 days. To read more about this alignment check out the [Account Based Marketing page](/handbook/marketing/revenue-marketing/account-based-marketing/).

| Region         |  **Role**             | Acceleration SDR      | 
| :------------- |  :--------------------------------- | :----------------- | 
| AMER         |  ABM / Accel SDR   | Shawn Winters     | 
| AMER           |  ABM / Accel SDR         | David Fisher   |
| EMEA - France & Eastern Europe          |  ABM / Accel SDR    | Anthony Seguillon     | 
| EMEA - Germany & CH/AUS           |  ABM / Accel SDR           | Christina Souleles    |
| Global           |  Content Lead         | Michael LeBeau  |
| Global           |  Segmentation Lead       | --  |

## Roles & Responsibilities 

### AMER ABM Acceleration

In AMER, the ABM Acceleration team is aligned to designated Tier 1 and 2 ABM accounts and responsible for:
* Leading account planning efforts and creating in-depth account plans for each target account
* Working with internal teams on campaigns and initiatives to support ABM
* Understanding the campaign briefs and becoming vertical experts
* Aligning outbound prospecting efforts to the GitLab and ABM campaigns
* Educating prospects and making positive impressions
* Generating awareness, meetings, and pipeline for sales

### EMEA ABM Acceleration

EMEA ABM Acceleration SDRs are aligned to their Native Geos and responsible for executing Territory Warm-ups on an as-needed basis. Their responsibilities include:
* Account planning & prioritization
* Working with internal teams on campaigns and initiatives to support the target regions
* Aligning outbound prospecting efforts to the GitLab and ABM campaigns
* Understanding the campaign briefs and becoming vertical experts
* Educating prospects and making positive impressions
* Generating awareness, meetings, and pipeline for sales

## Acceleration Compensation and Quota

The Acceleration team's compensation is based on one key metric: Team Qualified Meetings Occurred

It is important to note that the Acceleration Team has a monthly **team** goal for each geography (AMER & EMEA). Team members must work together to achieve the monthly Qualified Meeting number. Accelerators will be consistent with those outlined in [SDR Compensation and Quota](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#sdr-compensation-and-quota). For accelerators, the individual quota is determined by dividing the *team number* by the *total number of SDRs on the Geo team*.

**The Acceleration Marketing Team will also be measured on the following:**

* Results
  * Pipeline value of Acceleration-sourced opportunities
  * Progression of sourced opportunities beyond the Discovery Stage
* Activity
  * Number of meetings generated
  * Number of SAOs converted
  * Number of calls made
  * Number of personalized emails sent
  * Number of conversations

The above measurements do not impact team quota attainment but are critical to being successful as a member of the Acceleration Team. The team's focus should always be achieving the Qualified Meeting Quota.

**What is considered a Qualified Meeting?**

A Qualified Meeting is a meeting that occurs with a prospect who is directly involved in a project or team related to the potential purchase of GitLab within a buying group, either as an evaluator, decision maker, technical buyer, or influencer within an SDR Target Account. To be considered qualified, the meeting must **occur** (prospect attends) and new information must be uncovered that better positions the SAL and Regional SDR to create a SAO with the account.

## Acceleration Workflow & Process

### Issue Board & Labels

The Acceleration team works from issues and issue boards. If you are needing our assistance with any project, please open an issue and use the ~Acceleration label anywhere within the GitLab repo. 

The Acceleration team uses this [global issue board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1333112?scope=all&utf8=%E2%9C%93&state=opened).

Global labels used by the team:
- `Acceleration`: adds the issue to the Acceleration board. Used for all acceleration initiatives.
- `Acceleration - Planning`: issues that are related to planning of a territory warm up campaign
- `Acceleration - Prospecting`: issues related to active acceleration campaigns or initiatives
- `Acceleration - EMEA`: issues related to the EMEA Large Acceleration team
- `Acceleration - Programs:` work in progress projects and process improvements

### Working with Sales and SDR Teams

The Regional Sales & SDR Teams have a direct impact on the Acceleration SDR team’s success. It is important to set clear expectations and establish open lines of communication. The geo SDR Teams have knowledge and intel on the accounts and territory and are expected to contribute with both prioritization & execution.

For Acceleration coverage, meeting cadence consists of the following:

*  **Initial Coverage Kick-off meeting**: Time - 30 minutes; Discuss tactics, target accounts, protected accounts, events, prospecting strategy, and schedules
*  **Weekly Sync Meetings**
*  **Coverage Recap Meeting**: Time - 30 minutes; Discuss results, account intelligence, actionable insights, and collect feedback
*  **Additional Ad-hoc Meetings** as needed 

Acceleration SDRs should participate in initial IQMs and are required to make the Regional Sales and SDR teams aware of all Meetings, Opportunities, and Next Steps. 

## Territory Warm-up Strategy & Tactics

### FY21 Roadmap / Schedule
The territory warm-up schedule listed below subject to change based on adjustments the hiring roadmap, E-team or RD requests, or manager discretion. This link outlines the current [GitLab sales territories](https://about.gitlab.com/handbook/sales/territories/).

| Region        |  Fiscal Quarter           | Territories Covered      | SDRs Assigned |
| :------------- |  :--------------------------------- | :----------------- | :----------------- | 
| AMER | Q2 FY21 | South Texas, Massachusetts / New England, Gulf Carolinas, Lake Michigan | Shawn Winters, David Fisher |
| AMER | Q3 FY21 | Acceleration transitioned to Geo SDRs | N/A |
| AMER | Q4 FY21 | Owned by Geo SDR teams | N/A |
| EMEA | Q2 FY21 | UKI | Anthony Seguillon, Christina Souleles |
| EMEA | Q3 FY21 | TBD | Anthony Seguillon, Christina Souleles |
| EMEA | Q4 FY21 | TBD | Anthony Seguillon, Christina Souleles |

### Target Account Identification

Identifying *known* and *unknown* Target Accounts is an important first step in the Acceleration Coverage Process. To determine the target accounts for a region, the following process should be used:
1.  Identify *known* accounts from Salesforce.com
- `Pull Accounts by *Account Owner*` using [this report](https://gitlab.my.salesforce.com/00O4M000004dmXG)
- `Exclue ALL Customers`
- `Exclude SDR Targeted Accounts`
- `Exclude Accounts with Active Opportunities`

2.  Identify *unknown* accounts from DataFox using [this template](https://app.datafox.com/dynamic-list/5e9e410f3d6bcf0100aa223e/data)
- `2000+ employees for Enterprise, 1000-1999 for Mid-Market`
- `Exclude Government / Public Sector`
- `Filter by Region` (State or Metro)
- `Exclude Existing Accounts` (via SFDC export and DataFox upload)
- `Match Subisidiaries`

3. Combine Salesforce & DataFox target account lists
4. Dedupe & Review
5. Share Target Account List with ABM team for Demandbase appending and targeting
6. List Match in DiscoverOrg (by company domain)
7. Execute saved Boolean searches and append prioritization data to target lists
8. Run CE Usage Reports & Append data
9. Divvy up target accounts 
10. Review final Target Account document with Regional SDR & SAL teams
11. Ensure SDR Targeted Accounts & SAL suppression lists are up-to-date
12. Finalize Target Account List
13. Pull target prospects (by titles & keywords) from DiscoverOrg
14. Submit Issue for Marketing Ops DiscoverOrg Prospect List upload

While we've made every effort to make this process easy to follow and execute, we understand that there are several steps that can be confusing. In order to address those concerns, you can view a [video walkthrough of the process here](https://drive.google.com/open?id=1Byf41XpOa5qWNBhO0v748XbTHbooA9Do).

### Account Scoring & Prioritization

The next step after identifying all of the Net New target accounts in a region is to prioritize them. The scoring model below should be used to determine the Net New Account Tier which will help guide prioritization and prospecting efforts using the Outbound Prospecting Framework (below).

**A Tier Accounts** - Have at least 4 out of 5 qualifiers below

**B Tier Accounts** - Have 3 out of 5 qualifiers below

**C Tier Accounts** - Have 2 out of 5 qualifiers below

**D Tier Accounts** - Have 1 out of 5 qualifiers below

**F Tier Accounts** - Have 0 out of 5 qualifiers OR zero direct revenue potential in the next 3 years

**Qualifiers:**
*  Current CE Usage
*  250+ employees in IT/TEDD positions
*  Good Fit Industry / Vertical (High Growth, Technology, Financial, Heathcare, Regulated Business)
*  Early Adopters / Innovative IT Shops (Identifiers & Keywords): Kubernetes / Containers, Microservices, Multi-cloud, DevOps, DevSecOps, CICD (and open-source + proprietary tools), SAST / DAST, Digital Transformation
*  Current DevOps Adoption (multiple DevOps roles on staff or hiring for multiple DevOps positions)

### Outbound Prospecting Framework

| **Tier**        |  **Goal**                 | **Priority Level**      | **Outbound Tactics**      |
| :---------- |  :----------                  | :----------------- | :----------------------|
| A Accounts   |    Conversations, IQMs, MQLs         |      High (60% of focus)      | Hyper-personalized, simultaneous targeting, creative, direct mail, targeted ads, groundswell, events |
| B Accounts   |  Conversations, IQMs, MQLs           |      High (30% of focus)      | Hyper-personalized, simultaneous targeting, creative, direct mail, targeted ads, groundswell |
| C & D Accounts |  Conversations, MQLs               | Low  (< 10% of focus)         | Automated role- / persona-based outreach, groundswell |
| F Accounts |  Eliminate from SDR target lists   | Low (< 2% of focus)     | Do not target or attempt to qualify |

## Acceleration Programs

The top priority for the Acceleration team is providing a great experience for the individuals that we engage with. In an effort to better understand and reach prospects, the team is encouraged to iterate and continuously improve how we tell the GitLab story. "Acceleration Programs" are MVCs that are tested, measured, and then expanded as success is proven. 

1.  The first step in launching a project is to share the idea in a team call. 
2.  After the team has an opportunity to provide feedback, a tracking and measurement plan should be created and documented. 
3.  Once that is complete, the tactic or strategy should be incorporated into the individuals' outbound process and measured over a period of four to eight weeks.
4.  During the testing period, measurement is required and results should be documented weekly. 

If, after analyzing results, it appears that the tactic will generate at least a 1% lift if incorporated into the [Outbound SDR Workflow](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#outbound-workflow), an issue should be created with the :acceleration programs: label and a rollout plan should be created.

Active Acceleration Programs can be viewed on our [Global Acceleration issue board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1333112?scope=all&utf8=%E2%9C%93&state=opened).

## Helpful definitions

**Territory Warm Up** - The concept of focusing on a sales region for a period of time in an effort to generate Qualified Meetings, SAOs, and Pipeline. This is an important strategy for developing *new* and *underperforming* regions while generating opportunities for new SALs and AEs. 

**Hyper-personalization** - This is the concept of combining real-time data extracted from multiple sources to create outreach that resonates with prospects on an individual level. The desired outcome is to establish relevance with a prospect as a first step towards a conversation.

**VIP (prospect)** - A Very important top officer, exeutive buyer, C-level prospect, or important influencer. For these individuals, hyper-personization is required. Examples: CTO, CIO, CSIO, C-level, IT Business unit leads, VPs, strategic project leaders.

**Influencer (prospect)** - An individual prospect that is suspected to be involved with IT decision-making, tooling, teams, roadmap, strategic projects, and/or budgets. Examples: Director or Manager of DevOps / Engineering / Cloud / Security, Enterprise Architects, IT buyers, SysAdmins, purchasing managers, and product owners.

**User (prospect)** - A prospect that has limited influence within an IT organization. Examples: Developers, Engineers, QA, consultants, and business users.

**Groundswell** - An outbound strategy focused on filling the top of the funnel by generating engagement, opt-ins, MQLs, and uncovering intent signals. This strategy typcially incorprates more automation than other more direct outbound prospecting tactics. The strategy should be used with lower-level prospects and lower tier accounts.

**Value Stream** - An outbound "play" that is aligned to a prospects' role, title, persona, industry, vertical, current technology stack, desired business outcomes, assumed pain points, or keywords to 1) establish relevance 2) create value and 3) drive engagement and conversations.

**Warm Calling** - The method used to strategically incorporate phone calls and voicemails into an outbound prospecting workflow. The idea is optimize outbound productivity by only using the phone when targeting *engaged*, *validated*, and/or *VIP* prospects. 
