---
layout: handbook-page-toc
title: "SDR Managment Resources"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## General leadership principles
This page will serve as a training resource and operational guide for current and future managers. All SDR Managers should follow the [general leadership
principles](/handbook/leadership/) set out in the handbook. 

- [1-1s](/handbook/leadership/1-1/)
- [Providing regular feedback](/handbook/leadership/#giving-feedback)
- [Dealing with underperformance](/handbook/underperformance/)

## Manager Onboarding
Onboarding is essential for all SDR Managers at GitLab. As part of onboarding, a [Becoming a GitLab Manager](https://gitlab.com/gitlab-com/people-group/Training/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md) issue will be created for each manager when they join, or are promoted. This issue is intended to connect new managers with the crucial information they need, and ensure they have access to all the resources and training available.

## General SDR Leadership Resources

| Resource | Purpose |   
| :----: | :-----: | 
|  [Leadership Toolkit](/handbook/people-group/leadership-toolkit/) | Tools and resources to assist people managers in effectively leading and developing team members at GitLab|
|  [Compensation Review Cycle (Compa Review)](/handbook/total-rewards/compensation/compensation-review-cycle/) | How to review our Compensation Calculator and carry out the Compensation Review Cycle|
|  [360 Feedback](/handbook/people-group/360-feedback/) | Opporunity for managers, direct reports, and cross functional team members to give feedback to each other. Schedule and all information on this page.|
|  [BambooHR](/handbook/people-group/360-feedback/) | All team member HR information |
|  [Transitioning to a Manager Role at GitLab](/handbook/people-group/learning-and-development/manager-development/) | New manager resources and what to expect |

## Lead Routing & Alignment Resources

| Resource | Purpose |   
| :----: | :-----: | 
|  SSoT Sales > SDR Territory Alignment Sheet  | Can be found in Manager Home Base sheet. Single source of truth document for SDR to AE/SAL/Territory Alignment | 
|  [LeanData Change Request Issue Template](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=leandata_change_sdralignment)  | Use this template to request any update in lead routing/SDR alignment. Use a new issue for each SDR. Once this request is received, Marketing Operations will update LeanData, the SSoT alignment spreadsheet, Drift routing/team, and the `SDR Assigned` Salesforce field. Confirmation of completion will be stated on the issue prior to close. Please note: if an SDR or a number of SDRs are changing teams within the org, an [internal transition](/handbook/marketing/revenue-marketing/sdr/sdr-manager-resources/#internal-team-transition) issue will be created with this as one of the steps. | 

##  GitLab Resources

| Resource | Purpose |   
| :----: | :-----: | 
|  [How to update the org chart and team page](/handbook/marketing/website/#updating-the-team-page-and-org-chart) | Update the org chart to ensure the correct memebers of your team roll up to you. Ensure each member of your team has your `slug` listed next to the `reports to` line. You can also update the team page information from the team.yml |
|  [Adding yourself or someone else to the team page](https://www.youtube.com/watch?v=sPdV2nym9Fc) | Video to assist new hires with updating their blank team page placeholder |
|  Create or update members of a Slack user group | A user group is a group of members in a workspace who often need to be notified at once — for example, @managers. To update who is in one of the SDR groups, submit a [single person access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Single_Person_Access_Request) or [bulk access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Bulk_Access_Request) depending on the number of people that need to be added. Fill out the requested info and delete any remaining info that isn't needed. Under 'Account Creation' put Slack User Group: @ Name (i.e. @Managers). You can also use the bulk AR to request the creation of a user group and list the users who should be in it.  |
|  Add someone to the SDR Gmail alias | Submit a [single person access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Single_Person_Access_Request) or [bulk access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Bulk_Access_Request) depending on the number of people that need to be added. Fill out the appropriate info and delete any remaining info that isn't needed. Under 'Account Creation' put the SDR email alias |
|  [Make an edit to the handbook](/handbook/git-page-update/#12-edit-the-handbook) | Guide for how to edit the handbook. *Note: all new hires must do this as part of their onboarding |
|  [Add a new page to the handbook](https://www.youtube.com/watch?v=9NcJG9Bv6sQ) | This GitLab Unfiltered video will walk you through how to create a new handbook page |
|  [Create a new job family](/handbook/hiring/job-families/#job-family-creation-using-web-version-of-gitlab) | For each job at GitLab, the job family is the single source of truth for the expectations of that role. If you need information about when to create a new job family vs when to use an existing one watch [this video](https://www.youtube.com/watch?v=5EcFz1qNj2E&feature=emb_title) |
|  [Rename a handbook page](/handbook/marketing/website/#renaming-a-page) | Update the name of the URL to a handbook page |
|  [Resolve failed pipeline when creating an MR](https://www.youtube.com/watch?v=WlgH-6cX1k8&feature=youtu.be) | Quick overview of how to go about identifying why a pipeline might be failing for a merge request to the handbook page |


##  Tracking & Monitoring Resources

| Resource | Purpose |   
| :----: | :-----: | 
|  [SDR Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/707128)  | Used to track GitLab issues involving the SDR team. This is a global issue board. Please use the purple `SDR` label to notify this team of an issue.  | 
|  [SDR Event Tracker Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/1718115) | Used to follow upcoming events globally | 
|  [Acceleration Issue Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1333112?scope=all&utf8=%E2%9C%93&state=opened) | All issues involving the Acceleration SDR team. This is a global issue board. Please use the blue `acceleration` label to notify this team of an issue. | 
|  [SDR Sisense Dashboard](https://app.periscopedata.com/app/gitlab/641469/WIP:-SDR-Metrics-Update) | Dashboard to monitor SDR leads and meetings  |  
|  [MQL & SAO Performance vs. Target Sisense Dashboard](https://app.periscopedata.com/app/gitlab/628196/Revised-Marketing-Metrics) | Monitoring MQL and SAO performance in comparison to our goals  | 

## Onboarding
GitLab People Experience Associates will [create the onboarding issue](/handbook/general-onboarding/onboarding-processes/#timing) and start completing the onboarding tasks, no later than one week before the new team member joins. People Experience Associates require a minimum of 4 business days (with the new hire timezone as the basis) before the new hire's start date to complete all onboarding tasks. This issue will be automatically assigned to you. As a manager, you will [also have tasks](/handbook/general-onboarding/#managers-of-new-team-members) that need to be completed prior to the new team member's start date. 

With the creation of this issue, an [access request (AR) will also be automatically created](https://about.gitlab.com/handbook/general-onboarding/#managers-of-new-team-members) for a new team member on their second day at GitLab. This AR lists the role based entitlements (pre-defined groups and system-level access that are granted automatically to team members depending on their role) your new hire will need.
     *See what is being auto provisioned on this AR [here](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/tree/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_demand_generation). 

### New Hire's First Day
On your new hire's first day, the assigned People Experience Associate will [schedule a welcome email](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#day-1-onboarding-tasks) to arrive at 7:30am (local time of the new team member) on their start date detailing how your new hire can access GitLab and begin their onboarding process. 

### Manager Onboarding Checklist
##### Prior to Day 1
* General hiring Manager onboarding tasks
     * [Add blank entry to team page](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#hiring-manager-onboarding-tasks). Questions? Drop them in one of these slack channels! #mr-buddies #handbook 
* Complete ‘Manager’ tasks on the onboarding issue *Note: there are tasks to complete prior to your new hire starting
* Schedule a welcome call at the start of your new hire’s first day to discuss:
     * Main focus should be completing as much of your onboarding as possible.
     * When you have available time feel free to move ahead as some onboarding task lists for the day won't take you all day.
     * How to manage meeting invites in your inbox (there are a ton!)
     * You will receive numerous emails in their first two weeks asking you to register or activate your license to a specific tool we use, please go ahead and do all of this.
     * What you can expect in regards to [onboarding at GitLab as an SDR](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/sdr-playbook-onboarding/)
    
##### After new hire has started

* Complete remaining ‘Manager’ tasks on the onboarding issue
* Set up [1:1s](/handbook/leadership/1-1/)

## Internal Team Transition
An Internal Transition Issue will be created when an SDR:
- Changes from Individual Contributor to Manager
- Changes from Manager to Individual Contributor
- Changes teams (within the SDR org *or* at GitLab in general)

## Leave of Absence
If an SDR will be out for a prolonged period of time, please follow the proper processes and complete the SDR leave checklist.
- [Parental Leave](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave)
- [Emergency Situations](/handbook/paid-time-off/#communicating-time-off-for-emergency-situations)
- [Unpaid leave](https://about.gitlab.com/handbook/paid-time-off/#unpaid-leave-of-absence)

### Manager SDR Leave Checklist
- Submit a [LeanData change request issue template](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=leandata_change_sdralignment) to ensure leads will be worked and Drift conversations will reroute to another member of your team during this time. In the additional information section detail the starting date and date of return. Try to give MktgOps a minimum of a weeks notice. 
- Work with your SDR to clean up tasks and mark sequences as finished or pause if needed. 

Similar to onboarding, there is a role-specific tasks section that will automatically be populated for the SDR team. This section includes tasks for the new manager and sales operations that must be filled out to ensure proper alignment, routing and reporting.  

## Offboarding
The full process for offboarding at GitLab differs based on whether it is voluntary or involuntary. These processes can be found on the [offboarding handbook page](/handbook/offboarding/#offboarding). 

### Manager Offboarding Checklist

* The people team will create an [offboarding issue](/handbook/offboarding/#offboarding), complete all ‘Manager’ tasks on the issue. If you have any questions use the #managers #peopleops slack channels. You can also reach out to the assigned people ops team member on the issue. 
* Create and submit an issue using [this issue template](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=leandata_change_sdralignment) to inform Marketing Operations of who will be covering this territory and when that will start. 

