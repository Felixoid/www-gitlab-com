---
layout: handbook-page-toc
title: "Virtual Conference"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

This section shoudl eb used if you are looking for information on how to get started with a virtual confernce. 

## GitLab virtual events decision tree

We have developed a decision tree to help you determine what type of GitLab-hosted virtual event is the best fit for you. PLease only proceed if your event first the criteria listed in the tree.  

![image](/images/handbook/marketing/marketing-programs/DecisionTree-05042020.png)

## Event Requirements/ Best Practices:
### The large conferencing tool is best if:
* You need multiple breakout groups/ stages 
* You need an expo hall
* You want more networking options beyond a zoom call 
* You have more than 500 people
* The event is more than 3 hours 
* The event has multiple breakouts or tracks 
* and You need to capture attendee info
**Note:** Many of the elements included in tool are flexible and can be used for multiple purposes. Discuss with an account admin your options. 

### Platform and Bandwidth Restrictions: 
* We can only host one a day/ in 24 hour period
* You will need at least two full time support people to run the event
* Only suggested for events 3+ hours or more (due to complexity of setup)
* Also suggested for events targeting groups of 500+ or for initiatives that are in line with OKRs
* This event is best executed with more than one person and moderators. 
* Your event has a budget of $5k or more in budgeted spend. You must budget for Hopin event suport at $150/ hr. You need to budget for support for all live event hours as well as dry run hours. 

### Steps:
**Step 1: Start a [hosted vistual conference request issue]() in the digital marketing programs project.**
*  Please use the `Virtual_Conference_Request.md` issue template linked above
*  Please put the target LIVE date of the webcast as the due date
*  @ mention the MPM DRI in the issue comment to confirm the requested date is feasible
*  MPM will check the requested date against [the virtual events calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) to make sure there is no overlapping virtual event that has been pre-scheduled
*  If the requested date is feasible, the speaker(s) have been secured, and the abstract finalized, the DRI should change the status label from `status:plan` to `status:wip`, and add the applicable `FY..` label (to make sure this appears on the [webcast issue board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/922606?&label_name[]=Webcast))
* Must complete details on the issue: timeline, budget, audience goals, features needed ...

**Step 2: MPM will create the Hosted virtual conference EPIC**
*  When "status:wip" is on the issue and necessary elements are documented, and the speakers and webcast dates are secured, the event project is officially in motion. The DRI creates epic for the conference and tags corresponding MPM. 
*  Naming convention: [Conference Title] - [3-letter Month] [Date], [Year]
*  MPM copy/pastes code below into the epic
```
## [Main Issue >>]()

## [GANTT >>]()

## [Landing Page >>]() - `to be added when live`

## [Copy for landing page and emails >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## Event Details
  * `place details from the event issue here`
  * [ ] [main salesforce campaign]()
  * [ ] [main marketo program]()

## Issue creation (for event DRI to complete)

* [ ] Secure presenters and dry runs issue created - DRI
* [ ] Landing page issue created - DRI
* [ ] Optional: New design assets issue created for the design team - DRI
* [ ] Invitation and reminder issue created - DRI
* [ ] Organic social issue created for social media manager - DRI
* [ ] Paid Ads issue created for DMP - DRI
* [ ] PathFactory request issue created - DRI
* [ ] Follow up email issue created - DRI
* [ ] Add to nurture stream issue created - DRI
* [ ] On-demand switch issue created - DRI

/label ~"Marketing Programs" ~"Webcast - GitLab Hosted" ~"Virtual Event" ~"mktg-status::wip"

```
**Step 3: DRI will create the necessary MPM support issues [linked to the virtual conference GANTT]() and add to epic.**

### Project Planning
DRI will immediately:
1. Create the necessary epic and MPM support issues as [outlined above](). 
2. MPM to Ensure the webcast Dry Run and Live dates are added to the Virtual Events Google Calendar by sending over a Gcal invite from there to the webcast execution team. ***Note: This is an important step to make sure no overlapping virtual evenst are scheduled over the desired date/timeslot resulting in a conflict due to the shared hopin license and aupport requrements.***
3. If one or more speakers are external, set up a kick off call with the external speakers. Set timelines, due dates nd share slide deck templates with the speakers. Add dry runs to their caledar(s).
4. Clone and fill out [this Conference GANTT template](https://docs.google.com/spreadsheets/d/1VTrWNX9qfY99b2TnrX93P39aXiRoNnChB6tduTvmysA/edit#gid=1899924336). 

Due dates for each action item and DRIs are outlined on the Conference GANTT template.

### Promotion Best Practices

1. Give yourself at least 30 business days of promotion.
2. Send invitation emails 2 weeks out, 1 week out, and if needed 2 hours before event. Sample emails can be found here.
3. Only send promotional emails Tuesday, Wednesday, or Thursday for optimal results.
4. Send reminder emails to registrants the day before, and one hour before the event.
5. Host webcasts on a Wednesday or Thursday, see note below about scheduling.
6. Post links to additional, related resources during the event.
7. Include "contact us" information and a clear CTA at the end of the presentation.
8. Video recording from event should be uploaded to YouTube within 24 hours after event has occurred.
9. Send the recording to all registrants, whether they attended or not within 72 hours post event.
10. See more virtual event best practices [here](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/2105)

#### Tips for Speakers

Here are some basic tips to help ensure that you have a good experience preparing for and presenting on a video conference. See speaker preparation best practices- link coming 

##### Before Committing as a Speaker
{:.no_toc}
Ask us any questions you have about the time commitment etc. and what exactly our expectations are. Talk about it with your manager if you're on the fence about your availability, bandwidth, or interest. Make sure you're both on the same page. We want this to be a meaningful professional development exercise for you, not a favor to us that you're lukewarm about — if you feel that way, none of will be able to do our best job. We'll be honest with you, so please do the same for us.

##### Before the Dry Run
{:.no_toc}
Select and set up your presentation space. Pick a spot with good wifi, and we recommend setting up an external mic for better audio quality, although this is optional. If you will be presenting from your home, alert your spouse/roommates of the time/date & ask them to be out of the house if necessary. Depending on your preferences and comfort level with public speaking, run through the script several times.

##### Before the Presentation
{:.no_toc}
Try to get a good sleep the night before, and, if the presentation is in the morning, wake up early enough to run through your notes at least once. Review our [Positioning FAQ](/handbook/positioning-faq/), or keep the page handy in case you are asked in the Q&A about how GitLab compares to our competitors.

### Logistical Set up 

#### Adding your talk/ event into the calendar

The [virtual event calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) will be used to log all planned and scheduled Marketing Programs hosted webcasts and their subsequent dry runs. **The purpose of the webcast calendar is to ensure MPMs don't schedule overlapping virtual events.**

**DRI for adding to webcast calendar: MPM executing the webcast.**

##### Planned Hosted Virtual Events:

1. As soon as an issue is created for a webcast request, add the planned event to the virtual event calendar by creating an event on the day you plan to host the event. For events that are still in planning, use the following naming convention `[Hold WC Hosted] Webcast title` (e.g: `[Hold VC Hosted] Mastering CI`) and create it as an all-day event (no time slot selected). Make sure to also include the link to the issue in the calendar description.
2. Please also add the planned webcast to the virtual conference planning issue. When adding to the issue, please use :asterisk: emoji prior to the webcast name to indicate this is still in planning.

##### Confirmed Virtual Hosted Conferences:

1. Once the date/time of the conference has been confirmed, go to your calendar event and remove `Hold` from the event title `[VC Hosted] Webcast title` (e.g: `[VC Hosted] Mastering CI`). Specify the time on the calendar event and swap the issue link in the calendar description with the Epic link. *Note: In the spirit of efficiency, please be sure to add all presenters (internal GitLabbers and external speakers), the epic or issue (if you have one) and your event invite info to the calendar invite so you're not having to create multiple calendar invites.*
2. On the planning issue, please also update the webcast as confirmed by switching out the :asterisk: emoji with :white_check_mark: emoji.
3. Make sure to also add dry runs to the virtual events calendar. When creating the dry run event(s), please use the following naming convention `[DR WC Hosted] Event title` (e.g: `[DR WC Hosted] Mastering CI`) and specify the date/time on the calendar event.

### Creating the Event Registration page
* see the steps on how to set up an event with Hopin below. 

##### Step 2: Add the conference to the /events page
*  To add the webcast to the /events page follow this [step by step guide](/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents).

## Setting up an Event with Hopin- for hopin Admin only
* You need admin access to do so. Only 5 accounts are held internally and shared bwteen the marketing team. 
* Loginto your account, find our organization and Create a new event. 
* See screen shots 

**Additional Notes for organizer:**
* Hopin pricing model is based on support, some features and registration numbers. We have 6000 attendee slots for the year. Be mindful fo event goals beore exceeding this number. 
* We pay hourly for event support. 

**Adding speakers and panelist to your event**

**Adding an expo hall**

**Adding networking**

##### Step 4: Set up the event program in Marketo and SFDC
Needs updating to be confernece specific:
1. Create the webcast program in Marketo by cloning the YYYYMMDD_WebcastTopic_Region (Single time slot) template.
    * Select clone to `A campaign folder`.
    * Title the webcast in the following format: YYYYMMDD_{Webcast Title}_[Region - only if applicable]. For example, 20170418_MovingToGit.
    * Save to the `GitLab Hosted` folder.
    * Click salesforce campaign sync and select create new to create campaign in SFDC. Make sure to put the landing page url and also the link to the epic in the description.

2. Update `My Tokens` at the webcast program level.
   * DO NOT UPDATE THE `apiKey` or `apiSecret`. These should be the same for every webcast.
   * Update the `{{my.email header alt}}` token with the webcast title.
   * Update the `{{my.email header image url}}` with the  image url in marketo design studio.
   * Update the `{{my.landingPageUrl}}` token with the webcast landing page url.
   * Update the `{{my.utm}}` token by appending the program name to the utm campaign token.
   * Skip updating the `{{my.ondemandUrl}}` token for now (until the LIVE webcast has been completed).
   * Update the `{{my.webcastTitle}}` token with the webcast title.
   * Update the `{{my.zoomWebinarId}}` token with the webinar ID from the Zoom webcast created in step 3. (numbers only, no hyphens)

3. Turn on smart campaigns in Marketo.
  * Activate the `Attended` campaign.
  * Within the `Registration from Landing Page` smart campaign, on the `Fills out Form` trigger, add the webcast landing page url without the https (e.g: about.gitlab.com/webcast/securing-serverless/), then activate the campaign.
  * Activate the `Registration From Zoom` campaign.
  * Activate the `Interesting Moments` campaign.

4. Go to the campaign in salesforce.
  * Change the campaign owner to your name.
  * Change the status to `in progress`.
  * Edit the Bizible touchpoint field to `Include only "Responded" Campaign Members`.

5. **Return to the Zoom set up window you were working in during step 3.** You will now set up Marketo integration within Zoom by clicking on the Integration tab.
   *  Click Edit next to Generate Leads in Marketo section.
   *  Check  Send registration information to a Smart Campaign and select the `Registration From Zoom` smart campaign that you set up in marketo for this webcast.
   *  Check  Send attendee information to a Smart Campaign and select the `Zoom Attended` smart campaign hat you set up in marketo for this webcast.
   *  In the `Gather other information to Marketo (optional)` section, select the ZoomWebinarOtherInfo custom object, check the following boxes, and select the corresponding Marketo Custom Object Fields:
      * Webinar ID
      * Webinar Topic
      * Q&A
      * Poll
      
##### Step 5: Test your set up

1. Submit a test lead using your gitlab email on the LIVE landing page to make sure the registration is tracked appropriately in the Marketo program and you get a confirmation email from zoom.

#### Email invitation Campaign

#### Posting the event recordings

##### Sending follow up emails

1. Go to the `No shows` smart campaign and click run once to make sure the no shows get dispositioned into the correct status. Currently, no integrated exists between zoom and Marketo to automatically disposition no shows so they remain in `Registered` status until you run this smart campaign.
2. Update email `Outbound -attendees` and email `Outbound -no shows` with content with relevant copies related to the webcast.
3. On the Follow-up - Attendees and Follow up - No Shows email programs, update the `{{my.CTA..}}` tokens to point to the correct offers and use them in the hyperlinks.
4. Approve copy and send samples to the requestor, and the presenter (if different from requestor).
5. Once you get approval for the sample copy, schedule email sends within follow-up no show and follow-up attended email programs. 

##### Converting the event content to an On-Demand gated asset

1. Upload the recording to our main GitLab channel, fill in the title with the event title, and fill in the description with a short paragraph of what the webcast is about. Make sure the video set as `Unlisted` so only people with the URL to the video can find it.
2. Once the recording has been uploaded, copy the video link on the right.
3. **Login to PathFactory.** Add the copied youtube link to Pathfactory as a new content by following the instructions outlined [here](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#how-to-upload-content). 
4. **Login to Marketo.** Create the [listening campaign](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/#listening-campaigns) in Marketo by cloning this [program template](https://app-ab13.marketo.com/#PG3875A1). In the  `PF - Listening (Triggered)` smart campaign nested to the program, modify the `PathFactory Content Journey` filter to reflect your asset's Pathfactory custom url slug in the following format `[your assets custom url slug]`. Activate the smart campaign and then set up the Salesforce campaign sync for the listening campaign.
5. **Login to Salesforce.** Find the subsequent SFDC campaign for the listening campaign. Add the subsequent webcast campaign to the `Parent Campaign` field. Set the `Enable Bizible Touchpoints` field to `Include only "Responded" Campaign Members`.
6. **Navigate back to:  www-gitlab-com/data/webcast.yml.**
7. Click edit and add the following code snippet `youtube_url: ''`under the `url` code snippet for your landing page.
8. Change the `form:` to `2076`.
9. Change the `success_message:` to `Thank you for downloading. <a id="destination-url" href="YourWebcastPathFactoryLink&lb_email=">Click here</a> to view the on demand webcast. We will also email you a link to the webcast.`
10. Add commit message to name your Merge Request using syntax  `Add PathFactory link for [webcast name] landing page` (e.g. `Add PathFactory link for Debunking Serverless security myths webcast landing page`).
11. Create a name for the target branch - NEVER leave it as the master (i.e. `20191130-Debunking-WC-LP`).
12. On the next screen (New Merge Request), add `WIP:` to the beginning of the title and add a quick description (`Add PathFactory link to LP for [webcast name] will suffice`).
13. If you have merge access, assign Merge Request to yourself. If you don't have merge access, assign Merge Request to Jackie Gragnola or Agnes Oetama. Scroll down, check the box for `Delete source branch when merge request is accepted`.
11. Click Submit Merge Request.
12. You’ve now created the Merge Request.
14. **Login to Marketo.** Go to the webcast program and update the `{{my.ondemandUrl}}` token with the webcast PathFactory link.
15. Go to the assets folder within your webcast program and update the `On-demand Autoresponder` email with relevant copies related to the webcast. 
16. Navigate to the `Viewed On Demand` Smart campaign within your webcast program.
17. Modify the webpage link with the webcast landing page url without the https (e.g: `about.gitlab.com/webcast/securing-serverless/`), then activate the `Viewed On Demand` smart campaign.
18. Deactivate the `Attended` ,`Registration from Landing Page`, and `Registration From Zoom` smart campaigns within your webcast program.
19. **Go back to your MR.** Once the pipeline passes and if everything looks okay in the review app remove WIP and merge (if you have merge access). If you don't have merge access, ping @jgragnola or @aoetama in the MR comment to merge.
20. Add your webcast to the /resources page by following the instructions [outlined here](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#8%EF%B8%8F%E2%83%A3-add-your-page-to-the-rsesources-page).

