---
layout: handbook-page-toc
title: "Security Compliance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Security Compliance Mission

1. Enable GitLab sales by providing customers information and assurance about our information security program and remove security as a barrier to adoption by our customers.
1. Implement a comprehensive compliance program at GitLab to document and formalize our information security program through independent evaluation.
1. Reduce and document GitLab risk as it relates to information security.

## Roadmap

Our [internal roadmap](https://gitlab.com/groups/gitlab-com/gl-security/compliance/-/roadmap) shows our current and planned projects and the currently defined components of work for each.
   * **Note: This link (and other links on this page) will only display if you are logged in as a GitLab team-member and will not be visible to the public.**

### Active security compliance work includes:
1. Implement and remediate a prioritized set of [security controls](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html) needed for PCI, Sarbanes–Oxley (SOX), and SOC2.
1. Prepare for the [SOC2 Type 2](/handbook/engineering/security/security-assurance/security-compliance/soc2.html) external audit set to kick off around the end of 2020
1. Meet our SOX-readiness needs relating to our security controls
1. Meet our [PCI compliance](/handbook/engineering/security/security-assurance/security-compliance/pci.html) needs as a level-4 merchant
1. Perform ongoing [risk assessments](/handbook/engineering/security/security-assurance/security-compliance/risk-management.html) of GitLab service and organization
1. Manage security needs relating to the GitLab procurement process and perform [third-party security reviews](/handbook/engineering/security/security-assurance/security-compliance/third-party-vendor-security-review.html) as needed
1. Facilitate quarterly access reviews for GitLab as a product and company
1. Business Continuity Plan testing

## GitLab's Control Framework (GCF)

GitLab has adopted an umbrella control framework that provides compliance with a number of industry compliance requirements and best practices. For information about how we developed this framework and a list of all of our security controls, please see the [security controls handbook page](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html).

### Control and Program/Project Owners

The following are the [directly responsible individuals](/handbook/people-group/directly-responsible-individuals/) (DRIs) for the different areas within the security compliance team:

* Controls: A [control domain owner](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/1131) is the DRI for the rollout and operation for the controls in their respective domain(s): 
* [SOC2](/handbook/engineering/security/security-assurance/security-compliance/soc2.html): [@lcoleman](https://gitlab.com/lcoleman)
* SOX: [@twsilveira](https://gitlab.com/twsilveira)
* [PCI](/handbook/engineering/security/security-assurance/security-compliance/pci.html): [@nsarosy](https://gitlab.com/nsarosy)
* [Risk Assessments](/handbook/engineering/security/security-assurance/security-compliance/risk-management.html): [@sttruong](https://gitlab.com/sttruong)
* [Third-party security](/handbook/engineering/security/security-assurance/security-compliance/third-party-vendor-security-review.html): [@jblanco2](https://gitlab.com/jblanco2)
* [Quarterly Access Reviews](/handbook/engineering/security/security-assurance/security-compliance/access-reviews.html): [@nsarosy](https://gitlab.com/nsarosy)
* Annual Business Continuity Plan testing: [@uswaninathan](https://gitlab.com/uswaminathan)
* [Security Compliance Observation Management](/handbook/engineering/security/security-assurance/security-compliance/observation-management.html): [@jburrows001](https://gitlab.com/jburrows001)

If you have any feedback on the security compliance stable counterparts please add a comment to [this issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/-/issues/1798).

### Stable Counterparts

Stable counterparts are meant to increase efficiency and consolidate security compliance requests to and from different GitLab teams. The hope is that by having a primary and secondary counterpart for each major stakeholder of the team we can efficiently batch requests and more easily keep up with stakeholder progress and initiatives. A breakdown of each stakeholder team and the related counterparts are below:

| GitLab Team | Primary SecComp counterpart | Backup SecComp counterpart |
| :---: | :---: | :---: |
| Infrastructure | [@nsarosy](https://gitlab.com/nsarosy) | [@lcoleman](https://gitlab.com/lcoleman) |
| SecOps | [@lcoleman](https://gitlab.com/lcoleman) | [@sttruong](https://gitlab.com/sttruong) |
| AppSec | [@twsilveira](https://gitlab.com/twsilveira) | [@nsarosy](https://gitlab.com/nsarosy) |
| BizOps | [@uswaninathan](https://gitlab.com/uswaminathan) | [@sttruong](https://gitlab.com/sttruong) |
| ITOps | [@uswaninathan](https://gitlab.com/uswaminathan) | [@sttruong](https://gitlab.com/sttruong) |
| Internal Audit | [@twsilveira](https://gitlab.com/twsilveira) | [@sttruong](https://gitlab.com/sttruong) |
| Data Team | [@sttruong](https://gitlab.com/sttruong)  | [@jblanco2](https://gitlab.com/jblanco2) |
| People team (recruiting) | [@jblanco2](https://gitlab.com/jblanco2) | [@nsarosy](https://gitlab.com/nsarosy) |
| People team (learning and development) | [@jblanco2](https://gitlab.com/jblanco2) | [@nsarosy](https://gitlab.com/nsarosy) |
| People team (onboarding) | [@jblanco2](https://gitlab.com/jblanco2) | [@nsarosy](https://gitlab.com/nsarosy) |
| Security Automation | [@nsarosy](https://gitlab.com/nsarosy) | [@twsilveira](https://gitlab.com/twsilveira) |



## Contact the Compliance Team

* Email
   * `security-compliance@gitlab.com`
* Tag us in GitLab
   * `@gitlab-com/gl-security/compliance`
* Slack
   * Feel free to tag is with `@sec-compliance-team`
   * The #security-department slack channel is the best place for questions relating to our team (please add the above tag)
* [GitLab compliance project](https://gitlab.com/gitlab-com/gl-security/compliance/compliance)

**Note: If you have an urgent request and you're not getting a response from the above team tags, the security compliance manager (@jburrows001) has their cell phone number in their slack profile. **
