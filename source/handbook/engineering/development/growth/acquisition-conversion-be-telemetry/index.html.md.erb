---
layout: handbook-page-toc
title: Acquisition, Conversion, and Backend Telemetry Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

The Acquisition, Conversion, and Backend Telemetry Groups are part of the [Growth section](/handbook/engineering/development/growth/). The Acquisition Group is responsible for promoting the value of GitLab and accelerating site visitors transition to happy valued users of our product. The Conversion Group is responsible for continually improving the user experience when interacting with locked features, limits and trials. The Telemetry Group is responsible for the collection and analysis of data to improve GitLab's product which includes being the primary caretaker of the versions app.

**I have a question. Who do I ask?**
Questions should start by @ mentioning the Product Manager for the [Acquisition group](/handbook/product/categories/#acquisition-group), the [Conversion group](/handbook/product/categories/#conversion-group), the [Telemetry group](/handbook/product/categories/#telemetry-group) or by creating an issue in our [Workflow board](https://about.gitlab.com/handbook/engineering/development/growth/acquisition-conversion-be-telemetry/#workflow-boards).

## Team Members

The following people are permanent members of the Acquisition, Conversion, and Backend Telemetry Group:

<%= direct_team(manager_role: 'Backend Engineering Manager, Growth:Acquisition and Conversion and Telemetry') %>

## How We Work

* In accordance with our [GitLab values](/handbook/values/)
* Transparently: nearly everything is public
* We get a chance to work on the things we want to work on
* Everyone can contribute; no silos
* We're data savvy

## Product Development Flow

Our team follows the [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/#workflow-summary) which defines a continuous workflow for delivering product value. 
- We continuously progress issues to the next workflow stage.
- We keep our workflow boards up to date at all times.
- We adhere to the defined "Completion Criteria" for each stage.
- We adhere to the defined "Who Transitions Out" to progress issues to the next stage.
- We avoid waiting for synchronization points. For example, we constantly break down, estimate, and start development on issues rather than wait for milestone planning.

### Workflow Boards

We use workflow boards to track issue progress throughout a milestone cycle. Workflow boards should be viewed at the highest group level for visibility into all nested projects in a group.

There are three groups we use:
- The [gitlab.com/gitlab-org](https://gitlab.com/gitlab-org/) group includes the [gitlab](https://gitlab.com/gitlab-org/gitlab), [customers-gitlab-com](https://gitlab.com/gitlab-org/customers-gitlab-com), and [license-gitlab-com](https://gitlab.com/gitlab-org/license-gitlab-com) projects.
- The [gitlab.com/gitlab-com](https://gitlab.com/gitlab-com/) group includes the [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com) project.
- The [gitlab.com/gitlab-services](https://gitlab.com/gitlab-services/) group includes the [version-gitlab-com](https://gitlab.com/gitlab-services/version-gitlab-com) project.

| gitlab-org | gitlab-com | gitlab-services | all groups |
| ------ | ------ | ------ | ------ |
| [Acquisition Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition) | [Acquisition Workflow](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition) | [Acquisition Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition) |
| [Conversion Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [Conversion Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) |
| [Telemetry Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Atelemetry) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Atelemetry) | [Telemetry Workflow](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Atelemetry) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Atelemetry) |

## Product Development Timeline

Our work is planned and delivered on a monthly cycle using [milestones](https://docs.gitlab.com/ee/user/project/milestones/). Our team follows the [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline).

- Milestone planning issue is created by the 4th of the month.
- Milestone commitment is finalized by the 13th of the month.
- Milestone starts on the 17th of the month
- Milestone ends on the 18th of the following month.
- Milestone retrospectives start on the 19th of the following month.

### Milestone Boards

We use milestone boards for high level planning and roadmapping across several milestones.

| gitlab-org | gitlab-com | gitlab-services |
| ------ | ------ | ------ |
| [Acquisition Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition) | [Acquisition Milestones](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition) | [Acquisition Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aacquisition) |
| [Conversion Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) | [Conversion Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aconversion) |
| [Telemetry Milestones](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Atelemetry) | [-](https://gitlab.com/groups/gitlab-com/-/boards/1547281?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Atelemetry) | [Telemetry Milestones](https://gitlab.com/groups/gitlab-services/-/boards/1547332?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Atelemetry) |

### Milestone Planning

Planning a milestone is a collaborative effort between Product, Engineering, and UX.

#### Milestone Planning Issue

We utilize a milestone planning issue to keep our team organized during the planning phase. Here is an [example milestone planning issue](https://gitlab.com/gitlab-org/gitlab/-/issues/216592). A milestone planning issue consists of:
- Holidays
- Validation Track
  - UX Capacity
  - Priorities
- Build Track
  - Engineering Capacity
  - Priorities
- Roadmap
- OKR Alignment
- Things to Pay Attention To
- Planning Tasks

#### Planning Tasks

The following tasks need to be completed for planning a milestone:
* [ ] PM creates milestone planning issue
* [ ] Team updates Holidays section
* [ ] Designers update Validation Track - UX Capacity section
* [ ] PM updates Validation Track - Priorities section
* [ ] Engineers update Build Track - Engineering Capacity section
* [ ] PM updates Build Track - Priorities section
* [ ] PM updates Roadmap section
* [ ] PM updates OKR Alignment section
* [ ] Team updates Things to Pay Attention To section
* [ ] On workflow board, PM adds `~milestone::p1/p2/p3/p4` labels to issues
* [ ] On workflow board, Engineers create a milestone commitment by tagging issues with the current milestone and the `Deliverable` label until the sum of tagged issue weights equals the milestone capacity. 

#### Milestone Capacity

Our milestone capacity tells us how many issue weights we can expect to complete in a given milestone. This is calculated by taking the sum of issue weights completed in the last milestone prorated by holidays. If there is a large variation in the estimated capacity of the last milestone and the one before it, we will use an average estimated capacity of the last few milestones. Here is an example of how we calculate capacity:

**Last Milestone:**
* **Total weights completed:** 24
* **Available work days:** 21.6 * 1 engineers = 21.6 available days
* **Actual work days:** 21.6 available days - 5 days off = 16.6 actual days
* **Ratio of available:actual work days:** 21.6 / 16.6 = 1.30
* **Maximum capacity:** 24 * 1.3 = 31 weights

**Current Milestone:**
* **Available work days:** 21.6 days * 1 engineers = 21.6 available days
* **Actual work days:** 21.6 av-days - 0 days off = 21.6 actual days
* **Ratio of available:actual work days:** 21.6 / 21.6 = 1
* **Maximum capacity:** 31 * 1 = 31 weights

In this example, the current milestone capacity is 31 weights.

#### Milestone Commitment

A milestone commitment is a list of issues our team aims to complete in the milestone. After issues are broken down, estimated, and prioritized, we begin tagging issues with the current milestone until the sum of the tagged issue weights equals our team's milestone capacity. All issues in a milestone commitment should also have the `~Deliverable` label.


## How We Use Issues

To help our team be [efficient](/handbook/values/#efficiency), we explicitly define how our team uses issues.

### Issue Creation

We aim to create issues in the same project as where the future merge request will live. For example, if an experiment is being run in the Customers application, both the issue and MR should be created in the Customers project.

We emphasize creating the issue in the right project to avoid having to close and move issues later in the development process. If the location of the future merge request cannot be determined, we will create the issue in our catch-all [growth team-tasks project](https://gitlab.com/gitlab-org/growth/team-tasks/issues).

**1:1 Ratio**

We aim to maintain a 1:1 ratio between issues and merge requests. For example, if one issue requires two merge requests, we will split the issue into two issues. We aim for a 1:1 ratio as we believe it emphasizes keeping MRs small, it makes estimation on issues straight forward, and it provides more visibility into an issue's progress.

### Issue Hierarchy
We have two types of issues we work with:

#### Parent Issues
Parent issues are GitLab issues that are used to group several child issues together. These issues are typically focused on product and the high level overviews of a feature or experiment.

Our groups have decided to use Parent Issues instead of [epics](https://docs.gitlab.com/ee/user/group/epics/) due to some of the limitations with epics (not being able to add labels, add milestones, assign team members, or link issues across projects). Here are guidelines we follow when using parent issues:
- Acts as a single source of truth for a feature or experiment
- Consolidates all high level conversations
- Issue is labelled with `~link::parent issue`
- Child Issues are linked under "Linked issues"
- To see the progress of a parent issue, you can see the ratio of “Linked issues” that are open versus closed.
- Parent issues can sit in workflow::in dev while the Child Issue is being worked on.

#### Child Issues
Child issues are small broken down issues of Parent Issues. These issues are typically focused on engineering implementation or design work.
- Issue is labelled with `~link::child issue`
- Issue description should reference the Parent Issue
- Contains conversations about the finer details. High level conversations should be moved to the Parent Issue.
- It is fine to only provide limited details in a Child Issue, when the issue is just there to track the engineering work.

### Issue Labels
We use issue labels to keep us organized. Every issue has a set of required labels that the issue must be tagged with. Every issue also has a set of optional labels that are used as needed.

**Required Labels**
- [Stage:](https://about.gitlab.com/handbook/engineering/development/growth/#how-we-work) `~devops::growth`
- [Group:](https://about.gitlab.com/handbook/engineering/development/growth/#how-we-work) `~group::acquisition`, `~group::conversion`, `~group::telemetry`
- [Workflow:](https://about.gitlab.com/handbook/product-development-flow/#workflow-summary) `~"workflow::planning breakdown`, `~"workflow::ready for development`, `~"workflow::In dev`, etc.
- [Parent Child Link:](https://about.gitlab.com/handbook/engineering/development/growth/acquisition-conversion-be-telemetry/#issue-hierarchy) `~link::parent issue`, `~link::child issue`

**Optional Labels**
- [Experiment:](https://about.gitlab.com/handbook/engineering/development/growth/#experiment-issue-creation) `~growth::experiment`
- [Experiment Status:](https://about.gitlab.com/handbook/engineering/development/growth/#experiment-issue-creation) `~"experiment::active`, `~"experiment::validated`, etc.
- [Release Scoping:](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/contributing/issue_workflow.md) `~Deliverable`
- [UX:](https://about.gitlab.com/handbook/engineering/development/growth/#ux-workflows) `UX`
- Other labels in [issue workflow](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/contributing/issue_workflow.md)

### Merge Request Labels
MR labels can mirror issue labels (which is automatically done when created from an issue), but only certain labels are required for correctly measuring [throughput](#throughput).

**Required Labels**
- [Stage:](https://about.gitlab.com/handbook/engineering/development/growth/#how-we-work) `~devops::growth`
- [Group:](https://about.gitlab.com/handbook/engineering/development/growth/#how-we-work) `~group::acquisition`, `~group::conversion`, `~group::telemetry`
- [Throughput:](https://about.gitlab.com/handbook/engineering/management/throughput/#implementation) `~security`, `~bug`, `~feature`, `~backstage`

## Prioritization
Prioritization is a collaboration between Product, UX, Data, and Engineering. To help our group prioritize issues within a milestone, we utilize the milestone priority labels:
- `~"milestone::p1"`
- `~"milestone::p2"`
- `~"milestone::p3"`
- `~"milestone::p4"`

In line with the process for [choosing something to work on](https://about.gitlab.com/handbook/engineering/workflow/#choosing-something-to-work-on), we also work from the highest priority to the lowest priority. For UX prioritization, see [priority for UX issues](https://about.gitlab.com/handbook/engineering/ux/ux-designer/#priority-for-ux-issues).

## PM and Engineering Initiatives

The work done by our engineering teams mainly fall into two categories: PM Initiatives and Engineering Initiatives.

**PM Initiatives:** This work is primarily related to driving value for our customers. This work is typically outlined by the product manager in the milestone planning issue.

**Engineering Initiatives:** This work is primarily related to driving value for internal teams. Some examples of this work are bug fixes, follow-up issues, refactoring, career development work, or anything an engineer thinks is important enough to be worked on.

Engineers should aim to prioritize PM Initiatives to accomplish each milestone, however, they can also work on Engineering Initiatives during any down time or by carving out dedicated time during [capacity planning](#milestone-capacity). We should aim to carve out dedicated time for Engineering Initaitives that take one or more full working days. To carve out time, simply add a comment to the milestone planning issue. Here's an [example of carving out time for Performance training](https://gitlab.com/gitlab-org/telemetry/-/issues/385#note_342212425).

## Iteration

We always push ourselves to be iterative and make the [minimal viable change](https://about.gitlab.com/handbook/product/#the-minimal-viable-change-mvc). Defining the minimal viable change takes practice. 

![build a car vs start with skateboard and move to car](https://gitlab.com/gitlab-org/gitlab/uploads/590251a5311fb50e4dcb174f214e1340/Screen_Shot_2020-02-06_at_3.29.48_PM.png)

The image above illustrates how we iterate. The goal is to build something quick and functional. Our first iteration gets us from A to B even though it doesn't have all the bells and whistles. A skateboard is not as fast as a car, but it is fully functional. Each subsequent iteration provides the user with more speed, more control, and a better aesthetic. 

Building a skateboard is low complexity and can be assembled in a day. Building a car is high complexity and takes thousands of parts and a much longer assembly time. Start with the skateboard first, iterate to the scooter next, then continue iterating and eventually you can build a car. 

A common misconception of iteration is that there is no waste. Using the example above, the parts of a skateboard can be reused in a scooter, however, they cannot be reused in a car. Iteration often requires us to throw away code to make way for a better product.

[Technical debt](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#technical-debt-in-follow-up-issues): It's common to discover technical debt during development of a new feature. In the spirit of "minimum viable change," the resolution of technical debt can be deferred to a follow-up issue.

### Async Daily Standups

We have daily asynchronous standups using [status hero's](https://statushero.com/integrations/gitlab) Slack integration. The purpose of these standups are to allow team members to have visibility into what everyone else is doing, allow a platform for asking for and offering help, and provide a starting point for some social conversations.

Three questions are asked at each standup:
* How do you feel today?
* What are you working on today?
  * Our status updates consist of the various issues and merge requests that are currently being worked on. If the work is in progress, it is encouraged to share details on the current state.
* Any blockers?
  * We raise any blockers early and ask for help.

## Sync Meetings

Each group holds synchronus meetings at least once a week to gain additional clarity and alignment on our async discussions.

* The Conversion Group meets on Tuesdays 02:30pm UTC and Thursdays 03:00pm UTC
* The Telemetry Group meets on Mondays 03:00pm UTC and Wednesdays 03:00pm UTC

**Meeting Agenda**

The agenda for each meeting is structured around the [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/#workflow-summary).
* **[Validation:](https://about.gitlab.com/handbook/product-development-flow/#validation-track)** Used for work in `~workflow::start`, `~workflow::problem validation`, `~workflow::design`, `workflow::solution validation`
* **[Build:](https://about.gitlab.com/handbook/product-development-flow/#build-track)** Used for work in `workflow::planning breakdown`, `workflow::blocked`, `workflow::ready for development`, `workflow::In dev`, `workflow::In review`
* **FYI:** Used for any other topics

**Meeting Rules**
* Agenda items should be filled in 6 hours before meetings otherwise it's possible to cancel the meeting.
* It's fine to add agenda items during the meeting as things come up in sync meetings we might not have thought about beforehand.
* Meetings start :30 seconds after start time
* Whoever has the first agenda item starts the meeting.
* Whoever has the last agenda item ends the meeting.
* Meetings end early or on time.
* Any missed agenda items are bumped to the next meeting.

## Throughput

One of our main engineering metrics is [throughput](https://about.gitlab.com/handbook/engineering/management/throughput/) which is the total number of MRs that are completed and in production in a given period of time. We use throughput to encourage small MRs and to practice our values of [iteration](https://about.gitlab.com/handbook/values/#iteration). Read more about [why we adoped this model](https://about.gitlab.com/handbook/engineering/management/throughput/#why-we-adopted-this-model).

We aim for 10 MRs per engineer per month which is tracked using our [throughput metrics dashboard](https://app.periscopedata.com/app/gitlab/559676/Growth:Acquisition-and-Conversion-Development-Metrics).

## Common Links

* [Growth Section](/handbook/engineering/development/growth/)
* [Growth UX Section](/handbook/engineering/ux/stage-group-ux-strategy/growth/)
* `#s_growth` in [Slack](https://gitlab.slack.com/archives/s_growth)
* [Growth Performance Indicators](/handbook/engineering/development/growth/performance-indicators/)
* [Growth Meetings and Agendas](https://docs.google.com/document/d/1QB9uVQQFuKhqhPkPwvi48GaibKDwGAfKKqcF-s3Y6og)
