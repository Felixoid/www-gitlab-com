---
layout: handbook-page-toc
title: Learning & Development
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the Learning & Development (L&D) page at GitLab! L&D is an essential part of any organization's growth, success and overall business strategy. We want to support the growth of our GitLab team-members' competencies, skills and knowledge by providing them with the tools they need and also the opportunities to progress their own personal and professional development.  

## How to Communicate with Us

Slack: [#learninganddevelopment](#learninganddevelopment)

Email Us: `learning@gitlab.com`


## Mission
Our mission is to provide resources to enable our team members to enhance success in their current roles as well as develop new skills to further their professional and personal development. We provide adaptive and blended learning and growth opportunities, including skills building, career development, and technical training that aligns to our strategic priorities as an organizaiton.

## GitLab Learning & Development Principles

1. **Meaningful and relevant content.** We deliver learning solutions that drive the development and growth of team members throughout their life cycle at GitLab.
1. **Values aligned.** Our learning solutions reinforce GitLab’s values, and foster continuous learning and curiosity.
1. **Diverse approaches to learning.** We apply a blended learning model for learning solutions, and adapt to various learning needs.
1. **Community.** We make our L&D offerings available to the public, aligned to our mission that everyone can contribute.

## Learning and Development Responsibilities

* Set learning strategy to develop and attract GitLab talent through a blend of immersive learning experiences
* Identifies and develops strategic relationships across the organization to motivate and develop team members
* Demostrate thought leadership and subject matter expertise in learning while applying adult learning theories
* Design learning solutions and experiences in support of organization values and culture, leadership principles, people manager core capabilities, career development, and more
* Deliver and develop training content to meet our strategic goals
* Perform learning needs analysis with leadership and e-group to understand and execute on learning and development opportunities

## L&D Organization

We are a small team but we've got a big role to play at GitLab! 

* [Learning and Development Partner ](https://about.gitlab.com/job-families/people-ops/learning-development-specialist/): [Josh Zimmerman](https://about.gitlab.com/company/team/#learning-dev-partner)
* [Learning and Development Generalist](https://about.gitlab.com/job-families/people-ops/learning-development-specialist/#learning--development-generalist): [Jacie Bandur](https://about.gitlab.com/company/team/#jbandur)


## Learning Sessions

### Live Learning
Live Learning sessions will be conducted on a monthly basis. There will be a Zoom video conference set up for each session. Official dates and topics will be added to the [schedule](/handbook/people-group/learning-and-development/learning-sessions/#live-learning-schedule) as confirmed. If you were unable to attend a live learning session but still want to learn, check out our [past live learning sessions](https://about.gitlab.com/handbook/people-group/learning-and-development/learning-sessions/#past-live-learning-sessions).

Format for 25 minute sessions:
* 10 minutes - introduction/content
* 10-15 minutes - Q&A

Format for 50 minute sessions (times below are approximate):
* 10-15 minutes - introduction/content
* 10-20 minutes - breakout session
* 10-20 minutes - debrief
* 5 minutes - conclusion  

### Action Learning
[Action Learning](https://wial.org/action-learning/) sessions are designed to give team members a place to practice coaching skills by helping others work through specific challenges. Official dates and topics will be added to the [schedule](/handbook/people-group/learning-and-development/learning-sessions/#action-learning-schedule) as confirmed.

Format for 25 minute sessions:
* 3 minutes - introduction
* 20 minutes - open up for attendees to present a current challenge they are facing. [Note: Other participants ask open ended questions about the challenge. No leading questions or advise is to be given. Once the person with the challenge feels they have received enough coaching, the group works on another participant's challenge
* 2 minutes - conclusion

### Leadership Forum

An [overview](/handbook/people-group/learning-and-development/leadership-forum/#overview) about the format as well as topics and dates can be found on the [Leadership Forum](/handbook/people-group/learning-and-development/leadership-forum/) handbook page.

## Learning Initiatives 

### Certifications

We provide our team members with certifications to demonstrate their knowledge on specific topics. We have [outlined](/handbook/people-group/learning-and-development/certifications/) our current certifications as well as planned and upcoming certifications for the year.

### Career Development

Everyone's career development is different, but we have [outlined](/handbook/people-group/learning-and-development/career-development/) what it can look like at GitLab. Career development also includes our [GitLab coaching framework](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/coaching/) to support managers with holding coaching discussions with their team. 

### Developing Emotional Intelligence

Whether you are a People Manager or an Individual Contributor, being skilled in "emotional intelligence" (also referred to as EQ) is a key attribute to interpersonal effectiveness. We have [outlined](/handbook/people-group/learning-and-development/emotional-intelligence/) the definition of emotional intelligence, how to understand your own EQ, how to develop your EQ in a [remote setting](/company/culture/all-remote/guide/), and building an inclusive environment with EQ. 

Another strategy to improve emotional intelligence is to apply the [Social Styles framework](https://about.gitlab.com/handbook/people-group/learning-and-development/emotional-intelligence/social-styles/) within your team to increase interpersonal interactions and team dynamics. 

### Language Courses

If you have any language courses you would like to recommend or links to websites please add them to this section.

 - [The 8 Best Interactive Websites for Adults to Learn English](https://www.fluentu.com/blog/english/best-websites-to-learn-english/)

There is also a way to practice foreign languages and ask for advice in several Slack channels, each dedicated to a specific language. You can find all these channels by searching for channels starting with #lang. If you're missing a channel for your target language, feel free to create one and mention it in #whats-happening-at-gitlab so that fellow GitLab team-members can join too!

### New Manager Enablement Program

GitLab has a growing [resource](/handbook/people-group/learning-and-development/manager-development/) to enable all team members transitioning to a manager role. It contains a link to a checklist, readings, and a form to help learning and development customize your development as a manager.

## New Learning Content at GitLab

We are always working to create more learning content for our team members. If you have a learning request that you would like the Learning & Development team to develop in partnership with your team, please fill out a `learning-and-development-request` issue template in our [issue tracker](https://gitlab.com/gitlab-com/people-group/learning-and-development/-/issues). Our team will review and set the priority for your request based on the scale and impact across the organization. Learning items that will be applied and used across the company will take priority. 

If you have developed the learning content and would like the Learning & Development team to review, fill out a `learning-and-development-review` issue template in our [issue tracker](https://gitlab.com/gitlab-com/people-group/learning-and-development/-/issues). 

Our team will review and set the priority for your content request or review based on the scale and impact across the organization. Learning items that will be applied and used across the company will take priority. 

Once the new content has been created, a link to the content should be added to our [SSOT Training](/handbook/training/) page.

### How the L&D team prioritizes requests: 
*  Evaluate strategic impact of the learning session
*  Determine the level of work associated with the learning requirement
*  Assess the impacted audience groups of the session 
*  Identify measures of success 
*  Assess dates of delivery with course schedule and forecast future date

### Top five training content development principles

If you are devleoping content to meet your learning needs or partnering with the L&D team, here are five key principles to consider when formulating a learning session: 

1. **Know Your Audience** - Analyze and assess the course audience. Ensure that all audience needs are accounted for at every level in the organization you are delivering the training too. 

2. **Define Learning Objectives** - Highlight what you want the learner to walk away from the session with. Consider developing two to three broad overall statements of what the audience will acheive. 

3. **Break Down Complex Information** - Consider breaking down complex information into easy to digest visuals or text. Reference the handbook but do not be afraid to create a visual representation or use storytelling for the audience.

4. **Engage the Learner** - Adults learn through practice and involvement. Consider using tools to engage learners in a virtual setting like [Mentimeter](https://www.mentimeter.com/) or [Kahoot](https://kahoot.com/business-u/) to stimulate interactivity. Ask the [L&D team](https://about.gitlab.com/handbook/people-group/learning-and-development/) for more insight on learning engagement tools. There are a lot you can leverage! 

5. **Implement Blended Learning Course Content** - Give the audience some pre-course work to read and review before the learning session. Use off-the-shelf resources and ensure the content is applicable to what will be covered in the session. Follow up with the audience following the session to gauge how they've applied what they've learned on the job through surveys and questionmaires. 

### Application of Adult Learning Theory

Adults learn differently in the workplace than they would in traditional learning environments or how they learned growing up. If you are developing training, consider applying principles related to Adult Learning Theories, those include: 

1. **Transformative learning:** The learning experience should aim to change the individual through transformative learning approaches. Start with learning experiences that appeal to your specific audience, and then move to activities that challenge assumptions and points of view.   

2. **Self-directed learning:** Most of the learning that adults do is outside the context of formal training, so there should be an emphasis on augmenting those informal learning experiences. Infuse applications of pre-reads and post-course follow up. Have the participants bring up examples of self-directed learning that they have taken that is related to the training course. 

3. **Experiential learning:** Adults learn through experiences and by doing. When designing a learning experience, apply activities to stimulate learning by doing through role-playing, simulations, virtual labs, case studies, etc. 

4. **Andragogy:** Recognize that adults learn differently than children. Design learning experiences with the assumption that your participants will come to the table with their own set of life experiences and motivations. Adults tend to direct their own learning, tend to learn better by doing, and will want to apply their learning to concrete situations as soon as possible. 


## Learning Management Systems

We are currently evaluating different learning management systems (LMS) to provide growth opportunities in a more structured, on-demand format. To help us continue to be [Handbook First](/handbook/handbook-usage/#why-handbook-first), course content should be structured in the following way:

- Text in handbook
- Video on Youtube
- Test and certification in an open source platform or something without a price per user/student so we can invite all our million of users without incremental costs.
- Don't put text or video in the platform itself, only link to the handbook and video, so we  have a [single source of truth](/handbook/handbook-usage/#style-guide-and-information-architecture) and don't end up with duplicate content that is hard to keep up to date.

## Compliance Courses

GitLab has a number of compliance courses that are required. Explore the drop downs below to learn more. 

<details>
  <summary markdown='span'>
    Common Ground: Harassment Prevention Training
  </summary>

All new team members will have a task in their onboarding issue to complete this training using <a href="https://learning.willinteractive.com/">Will Interactive's Platform</a> within the first 30 days of hire. Once you get to that step in your onboarding issue, please do the following:

1. Log into BambooHR
1. On the Training tab, click on the Harassment Prevention training that aligns with your role (Supervisor or Team Member) and location (U.S. or Non-U.S.).
   1. The list of courses to choose from are: **FY21 Anti-Harassment Training for Non-U.S. Team Members**, **FY21 Anti-Harassment Training for Non-U.S. Supervisors**, **FY21 Anti-Harassment Training for U.S. Team Members**, or **FY21 Anti-Harassment Training for U.S. Supervisors**. You only need to complete one training.
   1. For managers and leaders, the course is 2 hours long, but you can stop and come back to it. For all other GitLab Team Members, this is 1 hour long.
1. Click on the **Sign Up Now** link
1. Enter in your name and GitLab email address
1. Create a password
1. You may be sent a link to verify your account
1. Once you have logged in successfully you will be taken to the course you selected in BambooHR
   1. You can use the navigation bar at the top right-hand side of screen for volume and screen settings
   1. To the left and right of the center screen you should see this symbol: > which you can click on to move forward and back through the training.
1. Once completed, please upload a copy of your certificate in BambooHR in the *Employee Uploads* folder
1. You may also keep a record of the certificate for your own files. To create the certificate, click on *view* in the course title
1. Scroll down to *users* then click on *completion certificates* to download the PDFs


If a Team Member moves from an individual contributer role to a manager role, they will be assigned a New Manager Enablement issue to work through. In this issue it will be verified if they have completed the correct level of harassment prevention training. If the team member has completed the "Team Member" level of the training, they will have to take the "Supervisor" level of the training as well.

Our <a href="https://about.gitlab.com/handbook/anti-harassment/?private=1">Anti-Harassment Policy</a> outlines guidelines, reporting and disciplinary action.  

</details>

If you have any questions or need further help please ping people ops in the `#peopleops` channels in slack.

## Performance Indicators

### Engagement Survey Growth and Development Score > X%

Questions related to growth and development on the semi-annual [Engagement Survey](/handbook/people-operations/engagement/) have a favorable score. The exact target is to be determined.

### Rate of internal job promotions > X%

Total number of [promotions](/handbook/people-operations/promotions-transfers/) in a rolling six month period/total number of employees. The target for this is to be determined.

### 12 month voluntary team member turnover related to growth < X%
This is calculated the same as [12 month voluntary team member turnover KPI](/handbook/people-operations/people-operations-metrics/#team-member-turnover) but is using the number of team members actively choosing to leave GitLab to growth and development related reasons only. The target is to be determined.
