---
layout: handbook-page-toc
title: Learning Sessions
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the Learning Sessions page! Here you will find the schedules for upcoming learning sessions as well as links to content for past learning sessions.  

## Live Learning Schedule

1. The 2020 Live Learning schedule is as follows:

   - May - TBC
   - June - TBC
   - July - TBC
   - August - TBC
   - September - TBC
   - October - TBC
   - November - TBC
   - December - TBC

## Past Live Learning Sessions
### 2020
1. January - [Compensation Review: Manager Cycle (Compaas)](https://youtu.be/crkPeOjkqTQ)
1. January - [Ally Training](/company/culture/inclusion/ally-training/)
1. February - Live Learning: Communication - Receiving Feedback (2020-02-25)

### 2019
1. November - [Communicating Effectively & Responsibly Through Text](/company/culture/all-remote/effective-communication/)
1. December - [Inclusion Training](/company/culture/inclusion/inclusion-training/)

## Action Learning Schedule

1. The 2020 Action Learning schedule is as follows:

   - May - TBC
   - June - TBC
   - July - TBC
   - August - TBC
   - September - TBC
   - October - TBC
   - November - TBC
   - December - TBC

## Past Action Learning Sessions
### 2019
1. November - Annual Compensation Review
