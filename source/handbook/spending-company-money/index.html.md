---
layout: handbook-page-toc
title: "Spending Company Money"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
In keeping with our values of results, freedom, efficiency, frugality, and boring solutions, we expect GitLab team members to take responsibility to determine what they need to purchase or expense in order to do their jobs effectively. We don't want you to have to wait with getting the items that you need to get your job done. You most likely know better than anyone else what the items are that you need to be successful in your job.

The guidelines below describe what people in our team commonly expense. Some of the items below have the average amount that folks typically spend on them as a helpful guide. If you need to spend more to get the right tool for you to work productively, please do so. If you are uncertain about how much reasonable to spend on a more expensive item, please reach out to your manager and ask for help.

## Guidelines

1. Spend company money like it is your **own** money. _No, really_.
1. You don't have to [ask permission](https://m.signalvnoise.com/if-you-ask-for-my-permission-you-wont-have-my-permission-9d8bb4f9c940) before making purchases **in the interest of the company**. When in doubt, do **inform** your manager before the purchase, or as soon as possible after the purchase.
1. It is generally easiest and fastest for you to make any purchases for office supplies yourself and expense them. If you are unable to pay for any supplies yourself, please follow the [Advance instructions](/handbook/spending-company-money/advance).
1. You may privately use GitLab property, a MacBook for example, to check your private e-mails or watch a movie as long as it does not violate the law, harm GitLab, or interfere with [Intellectual Property](/handbook/general-guidelines/#sts=Intellectual Property). More details can be found in the [internal Acceptable Use Policy](/handbook/people-operations/acceptable-use-policy/).
1. If you make a purchase that will cost GitLab $1000 USD per item (or over), this is classed as company property, you will be required to return the item(s) if you leave the company.
1. Employees: file your expense report in the same month that you made the purchase in.  Please combine multiple expenses on one report if possible. Contractors: include receipts with your invoices.
1. Any non-company expenses paid with a company credit card will have to be reported to your manager as soon as possible and **refunded** in full within 14 days.
2. If team members submit expenses for reimbursement, on either on the company credit card or via their personal credit card, for personal purchases that are not covered in the expense policy, this is a violation of our [code of conduct](https://about.gitlab.com/handbook/people-group/code-of-conduct/#maintain-accurate-financial-records--internal-accounting-controls).  

## Office Equipment and Supplies

The company will reimburse for the following items if you **need it for work or use it mainly for business**, and local law allows us to pay for it without incurring payroll taxes. Please keep in mind that while the amounts below are guidelines and not strict limits, any purchase (other than a laptop) that will cost GitLab $1000 USD per item (or over) will require approval from your Manager and Accounting.

The below averages are what GitLab **usually** reimburse. If you prefer to spend more on a given item, that's OK considering the average price. You may attach the receipt during expensing which shows the full purchase price. You are also welcome to expense the `GitLab Average Price in USD` portion. For example, if you purchase a high-end Steelcase ergonomic chair, you are welcome to expense the average price per the table below and cover the rest with personal funds. We are encouraging our members to use their best judgment when spending the company's money. 

Team members should not use a Corporate Card to purchase office equipment for their personal workspace.  All office equipment purchases for a team member's personal workspace should be made on a personal credit card and expensed.  For Laptop Purchases/Refreshes, please refer to [IT Ops Laptop](/handbook/business-ops/it-ops-team/#laptops) policy and procedure.

### Hardware

It is uncommon for you to need all of the items listed below. Read [GitLab's guide to a productive home office or remote workspace](/company/culture/all-remote/workspace/), and use your best judgement and buy them as you need them. If you wonder if something is common, feel free to ask IT Ops (and in turn, IT Ops should update the list).

| Item                                                         | Average Price in USD | Importance | Why                                                          |
| ------------------------------------------------------------ | -------------------- | ---------- | ------------------------------------------------------------ |
| [Height-adjustable desk](/company/culture/all-remote/workspace/#desks) | $500                 | 10/10      | We use our desks everyday and need them to be ergonomic and adjustable to our size and needs, whether that is sitting or standing. |
| [Ergonomic chair](/company/culture/all-remote/workspace/#chairs) | $200                 | 10/10      | We use our desk chairs hours at a time, and need them to be healthy, supportive, and comfortable. |
| [Headphones (wired or wireless, with mic ability)](/company/culture/all-remote/workspace/#headphones) | $200                 | 10/10      | We use our headphones everyday during our [meetings](/company/culture/all-remote/meetings/), to connect with our fellow team members. We need our headphones to be comfortable, functional, and great quality. |
| [External monitor](/company/culture/all-remote/workspace/#monitors) | $380                 | 10/10      | Finding a monitor that is large, comfortable to use with sharpness is extremely important for our eyes and health. |
| [Keyboard](/company/culture/all-remote/workspace/#external-keyboard-and-mouse) | $110                 | 10/10      | Find a keyboard that works for you and is comfortable for your workflow and hand size. |
| [Mouse or Trackpad](/company/culture/all-remote/workspace/#external-keyboard-and-mouse) | $80 or $145          | 10/10      | Find a mouse/trackpad that works for you and is comfortable for your workflow. |
| Laptop stand                                                 | $90                  | 10/10      | Your eyes and head must look at the same angle as you work and so your laptop must be elevated if you are working with an external monitor. |
| [Webcam](/company/culture/all-remote/workspace/#webcams)     | $80                  | 9/10       | If you would like a much better image quality than from the camera in your laptop, a webcam can make video conversation better. For those who interface routinely with clients, leads, and external parties, you may also consider a pricier [mirrorless or DSLR camera as a webcam](https://docs.crowdcast.io/en/articles/1935406-how-to-use-your-dslr-as-a-webcam). |
| [Dedicated microphone](/company/culture/all-remote/workspace/#microphones) | $130                 | 6/10       | For those who routinely interface with clients, leads, media, and external parties — or create regular content for GitLab channels — you may also consider a dedicated microphone to capture your voice with added richness and detail. |
| Portable 15" external monitor                                | $200                 | 9/10       | You have the freedom to work from any location, and having a portable monitor allows that your workflow does not suffer from being constrained to a single small laptop screen. |
| USB-C Adapter                                                | $80                  | 9/10       | Most MacBooks only have 1 free USB-C port, so an adapter with additional ports is a necessity. |
| HDMI/monitor cable                                           | $15                  | 9/10       | Find a quality cable so that the connection between your laptop and monitor is healthy and secure. |
| [Yubikey](https://www.yubico.com/store/)                     | $50                  | 8/10      | Per our [Security Practices](https://about.gitlab.com/handbook/security/), purchasing Yubikey is not mandatory, but is considered as an extra layer of authentication for better security. |
| Monitor Privacy Filter                                       | $80                  | 8/10       | Important if you work in public places and need to be certain your work cannot be seen. |
| WiFi Router with guest functionality     | $80                  | 7/10       | If your existing router does not allow for isolating your work notebook from your personal devices in your home network, consider buying a router that does. |
| Laptop bag or backpack                                       | $60                  | 7/10       | Carry your laptop and external monitor safely with this travel bag. We recommended you get a bag (or backpack) with straps so the device stays on you when you need your hands free. |
| [Earpods](https://www.apple.com/shop/product/MNHF2AM/A/earpods-with-35-mm-headphone-plug) | $30                  | 7/10       | If you use your headphones at home but would like lighter headphones to use while on the go, these earpods are the way to go. |
| Ethernet connector                                           | $20                  | 6/10       | This is if you choose to connect to your internet directly versus by Wi-Fi. |

### Software
1. We have central [license management](/handbook/tools-and-tips/other-apps/#jetbrains) for you to
   request licenses for JetBrains' products like RubyMine / GoLand.
1. We do not issue Microsoft Office 365 licenses, as GitLab uses Google's G Suite
   ([Docs](/handbook/communication/#google-docs), Slides, Sheets, etc.) instead.
1. For security related software, refer to the security page for [laptop and desktop
   configuration](/handbook/security/#laptop-or-desktop-system-configuration).

### Other
1. Business cards ordered from Moo as per the [instructions](/handbook/people-group/#business-cards) provided by PeopleOps.
	* Urgent Business cards needed for day of start can be requested by emailing peopleops@gitlab.com. As a last resort, Moo does offer 3 to 4-Day Express service.
1. Work-related books.

### Transport/Delivery of free procurements
Also feel free to check your local [Freecycle group](https://www.freecycle.org/) or similar second-hand/free markets when looking for equipment, especially furniture such as desks and chairs. GitLab will reimburse the cost of any transport and delivery services you need to procure the item(s) provided the total cost does not exceed the average spend for the item based on the table above.

## Not sure what to buy?

Look at our [equipment examples page](/handbook/spending-company-money/equipment-examples) to see some of the items that other GitLab team members have purchased, and please consider adding to the list if there's something you'd like to share.

## Setting up a home office for the first time?

Take inspiration from our [all-remote page covering key considerations for a comfortable, ergonomic workspace](/company/culture/all-remote/workspace/). You can also consult the `#questions` and `#remote` channels in Slack for recommendations from other GitLab team members.

## Expenses

The company will reimburse for the following expenses if you need it for work or use it mainly for business, and local law allows us to pay for it without incurring taxes:
1. Mileage is reimbursed according to local law: [US rate per mile](http://www.irs.gov/Tax-Professionals/Standard-Mileage-Rates), [rate per km in the Netherlands](http://www.belastingdienst.nl/wps/wcm/connect/bldcontentnl/belastingdienst/zakelijk/auto_en_vervoer/auto_van_de_onderneming/autokosten/u_rijdt_in_uw_eigen_auto), or [rate in Belgium](https://fedweb.belgium.be/nl/verloning_en_voordelen/vergoedingen/vergoeding-voor-reiskosten). Add a screenshot of a map to the expense in Expensify indicating the mileage.
1. Internet connection subscription.
	* For employees outside the Netherlands: follow normal expense report process.
	* For employees in the Netherlands: fill in and sign the [Regeling Internet Thuis](https://docs.google.com/a/gitlab.com/document/d/1J70geARMCjRt_SfxIY6spdfpTbv_1v_KDeJtGRQ6JmM/edit#heading=h.5x5ssjstqpkq) form and send it to the People Experience team at `people-exp@domain`. The People Experience team will then send it to the payroll provider in the Netherlands via email. The details of the payroll provider can be found in the PeopleOps vault in 1Password under "Payroll Contacts". 
1. VPN service subscription. Please read [Why We Don't Have A Corporate VPN](/handbook/security/#why-we-dont-have-a-corporate-vpn) for more information about VPN usage at GitLab.
1. Mobile subscription, we commonly pay for that if you call a lot as a salesperson or executive, or if your position requires participation in an oncall rotation. If your device cost is part of your monthly subscription cost, please do not include the device cost from the expense reimbursement.
1. Telephone land line (uncommon, except for positions that require a lot of phone calls)
1. Skype/Google Hangouts calling credit (uncommon, since we mostly use [internet-based services such as Zoom](/blog/2019/08/05/tips-for-mastering-video-calls/))

### Coworking or external office  space
If working from home is not practical you may submit for reimbursement for the cost of a co-working space. This can include non-traditional spaces that require a recurring (full-time monthly) membership as long as you average at least ~4 working days per month at the space. If flexible membership options exist in the form of daily passes or hourly packages, then these can be expensed as well, as long as the prorated cost per month does not exceed that of a recurring membership subscription. For instance, if both the monthly subscription and a hypothetical 10-day pass is $200 USD, then you can only expense one such pass each month.

Any agreement must be between the team member and the co-working space (i.e. GitLab will not sign or appear on the agreement). All expenses must be submitted through the normal [travel and expense reimbursement policy](/handbook/finance/accounting/#reimbursable-expenses). The Company will not be responsible for any expense that relates to office space subsequent to the termination of service between GitLab and the team member.

### Work-related online courses and professional development certifications

GitLab team members are allotted [$500 USD](/handbook/total-rewards/compensation/#exchange-rates) per fiscal year to spend on one or multiple training courses. Reimbursement past the [$500 USD](/handbook/total-rewards/compensation/#exchange-rates) total allotment requires manager approval.
1. The company will pay for all courses related to learning how to code (for example [Learning Rails on Codecademy](https://www.codecademy.com/learn/learn-rails)), and you may also allocate work time to take courses that interest you. If you are new to development, we encourage you to learn Git through GitLab, and feel free to ask any questions in the #git-help Slack channel.
1. Work-related conferences, including travel, lodging, and meals. If total costs exceed [$500 USD](/handbook/total-rewards/compensation/#exchange-rates), reimbursement requires prior approval from your manager.
	* Before scheduling any travel or time off to attend a conference a team member should review the request with their manager. The manager will approve the request if the conference is work-related and the timing doesn't interfere with GitLab deliverables. After  manager approval the team member can schedule travel and will be reimbursed for related expenses.
	* We encourage people to be speakers in conferences. More information for people interested in speaking can be found on our [Corporate Marketing](/handbook/marketing/corporate-marketing/#speakers) page.
	* We suggest to the attendees bring and share a post or document about the news and interesting items that can bring value to our environment.

### Year-end Holiday Party Budget

GitLab grants [$100 USD](/handbook/total-rewards/compensation/#exchange-rates) per GitLab team member for a holiday in December We encourage GitLab team members to self organize holiday parties with those close by, but meeting up with GitLab team members is not a requirement as it is understood that timing and location of holiday parties will play a factor in ability to attend.

For those who do not meet up with GitLab team members, you may expense up to $100 USD December or January for a holiday celebration of your choosing.

If a group of team members, Department or Division so choose, they are welcome to donate a portion of their per person Holiday budget to Charity.

### Travel

1. For travel to other team members please see our [visiting grant](/handbook/incentives/#visiting-grant).
1. If you are taller than 1.95m or 6'5", you can upgrade to Economy Plus. There is no dollar restriction on this since it will be hard to fit in economy with that height.
1. For flights longer than 8 hours, you can expense:
   * Up to the first [$300 USD](/handbook/total-rewards/compensation/#exchange-rates) for an upgrade to Business Class on flights longer than 8 hours if you are taller than 1.95m or 6'5".
    * Up to the first [$100 USD](/handbook/total-rewards/compensation/#exchange-rates) for an upgrade to Economy Plus (no height restriction) on flights longer than 8 hours.
1. GitLab does not cover expenses for Significant others or family members for travel or immigration. This includes travel and visas for GitLab events.
	* There are other things that [the company will not reimburse](/handbook/finance/accounting/#8-employee-reimbursements---expensify), such as dog boarding.

For additional Company Travel related questions, please refer to our [Travel Handbook](/handbook/travel/#booking-travel-through-tripactions-) regarding booking travel through TripActions.

### Something else?

No problem, and consider adding it to this list if others can benefit as well.

1. Customer/Partner Facing Events
    * Gala/Black Tie Events: Tuxedo or Gown Rental, $150-$225 USD per event.
    * The event must be customer specific and the invitation must state black tie only.

## Expense Reimbursement

1. Effective 2019-07-01, all expense reports must be submitted to your manager for approval prior to being sent to Finance for payment.
1. If you are a team member from Nigeria, please submit your expense in your salary invoice (a template can be found [here](/handbook/finance/#invoice-template-and-where-to-send)) with receipts attached to <payroll@gitlab.com>.  Please note, this is a temporary solution while we are transition over to a PEO.
1. If you are a team member and incurred an expense charged in a currency different from the one you use to submit your invoices, use the conversion rates specified in the [global compensation section of the handbook](/handbook/total-rewards/compensation/#exchange-rates). If the expense currency doesn’t exist in that list, refer to the conversion rates in [oanda](https://www.oanda.com/currency/converter/). Make sure to set the expense date in the currency converter form.
1. GitLab uses Expensify to facilitate the reimbursement of your expenses. As part of onboarding you will receive an invitation by email to join GitLab's account. Please set up your account by following the instructions in the invitation.
	* If you are a team member in Spain or France, please submit your expenses through Safeguard in-house expense reimbursement management system and also submit them through Expensify.  Payroll will review, approve, and send the approval of your expense reports in Expensify to your gitlab email address.  You will need to forward the approval email to Safeguard enable for them to process your expense reimbursement via payroll.
1. Please make an effort to combine multiple expenses into a single report as it will save the company money by avoiding excessive Expensify fees.
	* If you are new to Expensify and would like a brief review, please see [Getting Started](http://help.expensify.com/getting-started/)
	* For step by step instructions on creating, submitting, and closing a report please see [Create, Submit, Close](https://docs.expensify.com/en/articles/2921-report-actions-create-submit-and-close)
	* For US team members, the approved expense amount will be deposited into your account a few days after the report has been approved by payroll.
	* For Australia, Belgium, Germany, India, and Netherlands, AP will process the approved report on Friday once payroll approved the report.  The payment will be deposited into your account no later than three business days the following week.
	* For all team members being pay by Safeguard, iiPay, or Vistra, the approved expense amount will be deposited in your account with your monthly salary.
1. If you are a team member with a company credit card, your company credit card charges will automatically be fed to a new Expensify report each month. Please attach receipts for these expenses (per the Expense Policy, see below) within 5 business days after the end of the month. These amounts will not be reimbursed to you but Expensify provides a platform for documenting your charges correctly.

## Expense Policy

1. Max Expense Amount - [$5,000 USD](/handbook/total-rewards/compensation/#exchange-rates) - NOTE - If you are a corpoate credit card holder, please refer to the [corporate credit card policy section](https://about.gitlab.com/handbook/finance/accounting/#credit-card-use-policy) for those specific instructions and thresholds.
1. Receipt Required Amount - [$25 USD](/handbook/total-rewards/compensation/#exchange-rates)
1. Expenses must be submitted within 90 days of purchase.

## Advance

These instructions apply if a team member is unable to purchase items, for whatever reason.

1. New team member will make a list of requested items and prices, noting if they are out of the budget range listed on this page (if applicable), and send to their manager for approval. We ask that only one list be sent, versus multiple lists.
1. The team member's manager will send the approved (or edited) list to Accounting (nonuspayroll@domain.com OR uspayroll@domain.com, and CC ap@domain.com) for final approval and dispensation.
1. Once approved, Payroll will send the team member an invoice template to fill with the approved items, prices and the team member's bank information.
1. The approved final amount will be sent to the team member's bank and they can then purchase their approved items.

## Approving Expense Reports

1. Expensify will send a notification email when a team member submitted an expense report
    * Click on the report name in the body of the email
    * Review each expense for the correct amount of the receipt and the report
    * Check for customers or project name if applicable under Tag
    * We required a receipt for any expense greater than $25 (except for Billable policy)
    * Select [Approve and Forward] option and Expensify pre-populated the email address.  Note, Expensify is updating their coding to address a small glitch in this field.  If it is empty, please send it to **Montpac** (gitlab-expensify-mp@montpac.com)
    * **Important** - please do not use [Final Approval] because Expensify will not send the email notification for payment approval and it will delay the reimbursement process
    * Manager can delegate the approval process during PTO:
        *  Settings
        *  Your Account
        *  Vacation Delegate
        *  Enter the email address of the backup approval
    * All expense question(s) can be addressed via expenses@gitlab.com or in the #Finance and #expense-reporting-inquires
Slack channel

1.  **Expenses Reports approval deadline**
    * Australia, Germany, India, Netherlands, United States - as soon as possible
    * United Kingdom - all expense reports must be approved by manager no later than the 14th of each month.  Team members - please be sure to submit your report(s) couple days before the due date so your manager has enough time for approval.
    * All non-US contractors - all expense reports must be approved by manager no later than the 8th of each month. Team members - please be sure to submit your report(s) couple days before the due date.

## Laptops

1. The [IT Ops](/handbook/business-ops/it-ops-team/#laptops) page outlines laptop purchasing for new hires and for repairs and EOL for existing employees.
1. Laptops paid for by the company are property of GitLab and need to be reported with serial numbers, make, model, screen size and processor to IT Ops by adding it to this form: [GitLab laptop information](https://docs.google.com/forms/d/e/1FAIpQLSeUOlP11qeLdLZHTI62CFr7MSHAoI_1M6CRpnUA6fegkEKCOQ/viewform) for proper [asset tracking](/handbook/finance/accounting/#asset-tracking). Since these items are company property, you do not need to buy insurance for them unless it is company policy to do so (for example, at the moment we do not purchase Apple Care). You do need to report any loss following [Lost or Stolen Procedures](https://about.gitlab.com/handbook/people-group/acceptable-use-policy/#lost-or-stolen-procedures) or damage to IT Ops as soon as it occurs.
1.  **Repairs to company issued equipment.**
    * If you need to replace a battery or something small that does not affect the productivity or speed of the device, please go ahead and get that small item replaced and expensed.
    * Please get approval from your Manager if your equipment appears to be damaged, defective, or in need of repair. Business Operations can advise on next steps to ensure you have the proper equipment to work.
    * With GitLab being in [55 countries and growing](/company/team/#countries), please share your experiences in getting your laptops repaired at an authorized dealer in this [issue](https://gitlab.com/gitlab-com/business-ops/it-ops/issue-tracker/issues/6)
    * For loaner laptops: Do not hesitate when expensing a loaner laptop while your primary laptop is being repaired. Use your best judgment identifying a local vendor and share your experiences in this [issue](https://gitlab.com/gitlab-com/business-ops/it-ops/issue-tracker/issues/6)

## English lessons

At GitLab the lingua franca is [US English](/handbook/communication/#american-english), when English is not your native language it can limit you in expressing yourself.

## Currency Conversion
If you need help with calculating the conversion rate to $ USD, [please refer here.](https://www1.oanda.com/currency/converter/)
