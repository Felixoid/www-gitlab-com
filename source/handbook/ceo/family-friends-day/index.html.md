---
layout: handbook-page-toc
title: "Family and Friends Day"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

---

At GitLab, we are [family and friends first, work second](/handbook/values/#family-and-friends-first-work-second). Because we want to ensure that people are making their well-being a top priority and that we are living out our values, we will occassionally call a "Family and Friends Day." On this day, we will close the doors to the GitLab virtual office, reschedule all meetings, and have a **publicly visible shutdown**.

Team members can share about their Family and Friends Day in the #family-and-friends-day Slack channel after the event, or publicly on social media such as Twitter, LinkedIn, or wherever they're most comfortable using the hashtag #FamilyFriends1st. Sharing is optional. Taking the day off is strongly encouraged if your role allows it.

The first Family and Friends Day was 2020-05-01. Due to its success in reinforcing our message and supporting team member wellness, another Family and Friends Day has been scheduled for 2020-06-12. We are living in unprecedented times and are trying to give our Team Members some extra space in their lives outside of work. In line with our [Paid Time Off](/handbook/paid-time-off/) policy, we encourage GitLab Team Members to continue to take additional days off, as needed. Family and Friends Day is a reminder to do this.

## FAQ about Family and Friends Day

### I'm in a role which requires me to work that day. How can I still benefit from this initiative? 
If you are in a role that requires you to work on Family and Friends Day, you can work with your manager to find an alternative day. We encourage you to consider the following business day as the preferred second choice for a day away, but do what works best for you and your team.

### What if the date is a public holiday or non-working day in my country? How does this apply to me?
We encourage you to take off the next working day. If this day isn't an option, work with your manager to find another day that works for you and your team.

### How is this any different than our vacation policy?
Nothing about our [Paid Time Off](/handbook/paid-time-off/) policy is changing. We wanted to designate a specific day in order to more proactively force a pause for team members. If most of the company isn't working, there is less pressure for you to do so.

### What about client or prospect meetings that conflict? 
If you feel that this meeting can be rescheduled without any setbacks to the business, please go ahead and do so. If you have a meeting that would be hard to reschedule or would jeopardize the business results, please work with your manager to find another day that would work for both you and your team.

### What if I'm out sick on either of those days?
Feel better! Please work with your manager to find another day that works for you and your team.

### How do I communicate that I'm off that day?  
We'll assume that most people are off on Family and Friends Day, but we know that some people will take other days. Please update PTO Ninja in Slack. Feel free to block your calendar with "Family and Friends Day" to share whatever day you take.
