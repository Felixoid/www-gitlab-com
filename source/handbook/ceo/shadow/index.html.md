---
layout: handbook-page-toc
title: "CEO Shadow Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

### Introduction

At GitLab, being a CEO shadow at is not a job title, but a temporary assignment to shadow the CEO.
The shadows will be present at [all meetings of the CEO](/handbook/ceo/#ceo-meeting-cadence) during their rotation. 
GitLab is an [all-remote](/company/culture/all-remote/) company, but the CEO has in-person meetings with external organizations. 
Unless you're joining the program during one of our [remote rotations](#remote-shadow-rotations), you will stay in San Francisco during the entire [rotation](#rotation-rhythm) and travel with the CEO.

### Goal

The goal of the CEO Shadow Program is to give current and future [directors and senior leaders](/company/team/structure/) at GitLab an overview of all aspects of the [company](/company/).
This should enable leadership to better perform [global optimizations](/handbook/values/#global-optimization).
You'll gain this context through the [meetings you attend](#meetings--events) and while [completing](/handbook/values/#bias-for-action) short-term [tasks](#tasks) from across the company.
The program also creates opportunities for the CEO to build relationships with team members across the company and to identify challenges and opportunities earlier.
The shadows will also often connect with one another, developing new cross-functional relationships.

### What it is not
The CEO Shadow Program is not a performance evaluation or the next step to a promotion. Being a CEO shadow is not needed to get a promotion or a raise, and should not be a consideration factor for a promotion or raise, as diverse applicants have different eligibilities.

### Benefits for the company

Apart from creating leadership opportunities, the CEO Shadow Program:
* Leaves a great impression on both investors and customers
* Gives feedback immediately to the CEO
* Enables the CEO to create immediate change

This is why the program is worth the extra overhead for the CEO and [EBA team](/handbook/eba/).

### Naming of the program

For now, this role is called a [CEO shadow](https://feld.com/archives/2015/03/ceo-shadowing.html) to make it clear to external people why a shadow is in a meeting.

Other names considered:

1. Technical assistant: This title could be mixed up with the [executive assistant](/job-families/people-ops/executive-business-administrator/) role. ["In 2003, Mr. Bezos picked Mr. Jassy to be his technical assistant, a role that entailed shadowing the Amazon CEO in all of his weekly meetings and acting as a kind of chief of staff."](https://www.theinformation.com/articles/amazons-cloud-king-inside-the-world-of-andy-jassy).
1. Chief of Staff: This commonly is the ["coordinator of the supporting staff"](https://en.wikipedia.org/wiki/Chief_of_staff) which is not the case for this role since people rotate out of it frequently.
1. [Global Leadership Shadow Program:](https://www2.deloitte.com/gr/en/pages/careers/articles/leadership-shadow-program.html) This is too long if only the CEO is shadowed.

### Reasons to participate

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Cg0LzET_NWo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### What is it like?

Considering joining the program? Hear from past shadows about their experience: 

1. [Day 2 of Erica Lindberg](https://www.youtube.com/watch?v=xrWR0uU4nbQ)
2. [Acquisitions, growth curves, and IPO strategies: A day at Khosla Ventures](/blog/2019/04/08/khosla-ventures-gitlab-meeting/)
3. [GitLab CEO Shadow  Update - May 30, 2019](https://www.youtube.com/embed/EfBMu9dTpno)

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/EfBMu9dTpno" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

4. [Key takeaways from CEO Shadow C Blake](https://youtu.be/3hel57Sa2EY)
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/3hel57Sa2EY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

5. AMA with the CEO Shadow Alumni on 2019-08-23
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/TxivABJ16jE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

6. [Reflecting on the CEO Shadow Program at GitLab](https://youtu.be/DGJCuMVp6FM)
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/DGJCuMVp6FM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

7. [Key Takeaways and Lessons Learned from a Remote GitLab CEO Shadow Rotation](https://youtu.be/4yhtYcOZn3w)
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/4yhtYcOZn3w" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->

### What is the feedback from the CEO?

Hear what our CEO has to say about the CEO shadow program.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/gJWMBI64sZk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Partipating in the program

### Eligibility

You are eligible to apply for the program if you have been with GitLab for at least 1 month (recommended more than 3 months) and you are a:

1. [Director or up](/company/team/structure/#layers), Distinguished engineer or up ([backend](/job-families/engineering/backend-engineer/) or [frontend](/job-families/engineering/frontend-engineer/)),  [Senior Product Manager or up](/job-families/product/product-manager) or [Manager](/company/team/structure/#manager) in the People Group
1. [Manager](/company/team/structure/#layers), Staff engineer ([backend](/job-families/engineering/backend-engineer/) or [frontend](/job-families/engineering/frontend-engineer/)), [SAL](/job-families/sales/strategic-account-leader/), [PMM](/job-families/marketing/product-marketing-manager/), or [Product Manager or up](/job-families/product/product-manager/), if there is 1 consideration.
1. [Individual Contributor](/company/team/structure/#layers), if there are 2 considerations. This includes TAM and SA roles.

Considerations are cumulative and can be:

1. You belong to an under-represented group as defined in our [referral bonus program](/handbook/incentives/#referral-bonuses). Multiple under-represented groups are cumulative.
1. You are a recipient of GitLab’s Value Award of Transparency, Collaboration, Iteration, Efficiency, Results or Diversity at previous GitLab Contribute events.
1. There is last minute availability. Last minute means the first day of a rotation is less than a month out.
1. You are based in APAC.

You're also eligible if you work(ed) at an investment firm and while there helped to lead a private round in GitLab.

**COVID-19 Note:** During this time, all shadow rotations are fully remote. 
Given the CEO generally works from 8 a.m. to 6 p.m. Pacific,
it's best for remote shadows to be in Pacific, Mountain, or Central time zones. 
Other time zones will be considered on a case-by-case basis. 

Learn more about what to expect from a [remote shadow rotation](#remote-shadow-rotations).

Shadows with scheduled rotations always have the option to delay to later in the year if they'd prefer to do their rotation in person. 
Shadows who have chosen to delay their rotations until another time include: 
* Jarka Kosanova
* James Ramsey

### How to apply

1. Create a merge request to add yourself to the [rotation schedule](#rotation-schedule). Ensure the merge request description highlights how you meet the eligibility criteria (merge request examples: [Anastasia Pshegodskaya](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/43159), [Jackie Meshell](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/42958), [Philippe Lafoucrière](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/51265)).
1. Ask your manager to approve (but not merge) the merge request. Managers, please ensure the candidate meets the eligibility criteria.
1. Assign the merge request to the Executive Business Admin supporting the CEO, link it in the `#ceo-shadow` channel, and @mention the Executive Business Admin supporting the CEO in the message.

Please keep in mind when selecting dates that the CEO's schedule is fluid and subject to constant change, which also means that the CEO shadow rotation is subject to constant change. The posted dates are not guaranteed. We will work with you to reschedule your rotation if a conflict arises.

### Parental Participation

We understand that participation in the CEO Shadow Program is optional and can cause hardships at home. To help overcome these challenges and to allow flexibility for parents to participate, there will be some rotations identified as "parent-friendly" weeks. These are weeks where Sid doesn't need a shadow for the full 5 workdays or where the program is split so the weeks are not consecutive.

### Rotation rhythm

We want many people to be able to benefit from this program, therefore we rotate often.
It is important that an incoming person is trained so that the management overhead can be light.
Currently, a rotation is two weeks:

1. See one, you are trained by the outgoing person.
1. Teach one, you train the incoming person.

The shadow should be available for the full two weeks.

When the CEO has a week or more of paid time off, or during [Contribute](/events/gitlab-contribute/), the program will pause, one shadow will "see one" before the break and "teach one" after the break.
The rotations with breaks of one or more weeks without a shadow are great if you can't be away from home for more than one week at a time.

If you need childcare to be able to participate, GitLab will [reimburse you](/handbook/spending-company-money/) for it.

This program is not limited just to long-term GitLab team members.
For new team members, this might even be the first thing they do after completing our [onboarding](/handbook/general-onboarding/).
Exceptional community members may be able to participate, as well.

### Rotation schedule

| Start date | End date | See one | Teach one |
| ------ | ------ | ------ | ------ |
| 2020-05-26 | 2020-05-29 | [Sophie Pouliquen](https://gitlab.com/spouliquen1) - Senior Technical Account Manager  | [Candace Williams](https://gitlab.com/cwilliams3) - Diversity, Inclusion & Belonging  Manager |
| 2020-06-01 | 2020-06-05 | [Jackie Meshell](https://gitlab.com/jmeshell) - Senior Product Manager, Release Management  | [Sophie Pouliquen](https://gitlab.com/spouliquen1) - Senior Technical Account Manager |
| 2020-06-08 | 2020-06-12 | NO SHADOWS - PTO | NO SHADOWS - PTO |
| 2020-06-15 | 2020-06-19 | [Wayne Haber](https://gitlab.com/whaber) - Director of Engineering, Defend | [Jackie Meshell](https://gitlab.com/jmeshell) - Senior Product Manager, Release Management |
| 2020-06-22 | 2020-06-26 | AVAILABLE - REMOTE | AVAILABLE - REMOTE |
| 2020-06-29 | 2020-07-03 | AVAILABLE - REMOTE | AVAILABLE - REMOTE |
| 2020-07-06 | 2020-07-10 | NO SHADOWS - PTO | NO SHADOWS - PTO |
| 2020-07-13 | 2020-07-17 | [David DeSanto](https://gitlab.com/ddesanto) - Director of Product, Secure & Defend | [Wayne Haber](https://gitlab.com/whaber)  - Director of Engineering, Defend |
| 2020-07-20 | 2020-07-24 | NO SHADOWS - PERSONAL  | NO SHADOWS - PERSONAL |
| 2020-07-27 | 2020-07-31 | AVAILABLE - REMOTE | [David DeSanto](https://gitlab.com/ddesanto) - Director of Product, Secure & Defend |
| 2020-08-03 | 2020-08-07 | NO SHADOWS - PTO | NO SHADOWS - PTO |
| 2020-08-10 | 2020-08-14 | AVAILABLE - REMOTE | AVAILABLE - REMOTE |
| 2020-08-17 | 2020-08-21 | AVAILABLE - REMOTE | AVAILABLE - REMOTE |
| 2020-08-24 | 2020-08-28 | AVAILABLE - REMOTE | AVAILABLE - REMOTE | 
| 2020-08-31 | 2020-09-17 | NO SHADOWS - PERSONAL  | NO SHADOWS - PERSONAL | 
| 2020-09-21 | 2020-09-25 | [Philippe Lafoucrière](https://gitlab.com/plafoucriere) - Distinguished Engineer, Secure & Defend | AVAILABLE - REMOTE | 
| 2020-09-28 | 2020-10-02 | AVAILABLE | [Philippe Lafoucrière](https://gitlab.com/plafoucriere) - Distinguished Engineer, Secure & Defend | 
| 2020-10-05 | 2020-10-09 | NO SHADOWS - Presidents Club | NO SHADOWS - Presidents Club | 
| 2020-10-12 | 2020-10-16 | NO SHADOWS - PTO | NO SHADOWS - PTO |
| 2020-10-26 | 2020-10-30 | AVAILABLE - REMOTE | AVAILABLE - REMOTE | 
| 2020-11-02 | 2020-11-06 | AVAILABLE - REMOTE | AVAILABLE - REMOTE | 
| 2020-11-09 | 2020-11-13 | AVAILABLE - REMOTE | AVAILABLE - REMOTE | 
| 2020-11-16 | 2020-11-20 | AVAILABLE - REMOTE | AVAILABLE - REMOTE | 
| 2020-11-23 | 2020-11-27 | NO CEO SHADOWS - Thanksgiving | NO CEO SHADOWS - Thanksgiving | 
| 2020-11-30 | 2020-12-04 | AVAILABLE - REMOTE | AVAILABLE - REMOTE | 
| 2020-12-09 | 2020-12-13 | AVAILABLE - REMOTE | AVAILABLE - REMOTE | 

The week of 2020-12-13 will be the last CEO Shadow Rotation of 2020 to allow our team members to focus on the holiday season and put family and friends first. 

If you have questions regarding the planned rotation schedule, please ping the Executive Admin to the CEO. The EBA to the CEO manages the rotation schedule, please do not add new dates to the schedule when adding in your rotation.

## Preparing for the Program
### Important things to note
1. This is not a performance evaluation
1. Plan to observe and ask questions.
1. Give feedback to and receive feedback from the CEO.
1. Don't plan to do any of your usual work. Prepare your team as if you were on vacation.
1. Be ready to add a number of [handbook](/handbook/handbook-usage/) updates during your shadow period.
1. Participating in the shadow program is a privilege where you will be exposed to confidential information. This is underpinned by trust in the shadows to honor the confidentiality of topics being discussed and information shared. The continuation of this program is entirely dependent on shadows past, present and future honoring this trust placed in them.

### What to wear
You **do not need to dress formally**; business casual clothes are appropriate. For example, Sid wears a button up with jeans most days. GitLab shirts are acceptable when there aren't any external meetings. Review Sid's calendar to check if there are formal occasions - this may require different dress. If unsure, please ask the Executive Business Administrator (EBA) in the `#ceo-shadow` Slack channel
Make sure to bring comfortable shoes with you to Mission Control any time there are meetings in the city. Wear whatever you are comfortable in, keeping in mind that Sid prefers to walk, even if his calendar says Uber.


### Pre-Program Tasks

#### Coffee chat with Co-shadow

Before your scheduled rotation, try to schedule coffee chats with your co-shadows before you start the program. This will give you the opportunity to get to know them and help set expectations for the rotation.

#### Coffee Chat with CEO Shadow Alumni

Feel free to schedule a coffee chat with any of the CEO Shadow Alumni. You can review the list of [CEO Shadow Alumni](/handbook/ceo/shadow/#alumni) below. These chats can be helpful when deciding whether to apply to participate or if you're unable to participate but want to hear about the experience and what alumni have learned while shadowing.

#### Create an onboarding issue

Outgoing shadows are responsible for training incoming shadows. We currently track on-boarding in the [ceo-shadow](https://gitlab.com/gitlab-com/ceo-shadow) project.
The incoming shadow is responsible for creating their onboarding issue using the `on-boarding` template. If your [rotation is remote](#remote-shadow-rotations), be sure to use the `onboarding-remote` template. 
Assign both incoming and outgoing shadows to the issue.

#### Explore the CEO Shadow project

CEO Shadows use the [ceo-shadow](https://gitlab.com/gitlab-com/ceo-shadow) project to track issues and coordinate the requests that result from the [CEO's meetings](#meetings--events). It is linked in the CEO Shadow channel description on Slack.
Check out the ongoing CEO Shadow tasks on the [To Do issue board](https://gitlab.com/gitlab-com/ceo-shadow/tasks/-/boards/1385894).

#### Review the CEO's calendar

Review the [CEO's calendar](#ceos-calendar) to get an idea of what your upcoming weeks will be like.

#### Review the [CEO Handbook](/handbook/ceo)

The CEO has a [section in the handbook](/handbook/ceo) that details processes and workflows specific to him as well as his background, communiction style, strengths, and flaws. Take time to invest in your relationship with him upfront by reviewing this part of the handbook. Here are some helpful sections:

- [Communication](/handbook/ceo/#communication)
- [Pointers from direct reports](/handbook/ceo/#pointers-from-ceo-direct-reports)
- [Strengths](/handbook/ceo/#strengths)
- [Flaws](/handbook/ceo/#flaws)

## What to expect during the program

### Tasks

The value of the CEO Shadow Program comes from the [broader context](#goal) you'll gain and the interesting conversations you'll witness.

Since your rotation is over a short period of time, there are no long-term tasks you can take on.
However, there are many short-term administrative tasks you'll be asked to perform as shadow. Here are some examples:

1. Make [handbook](/handbook/) updates (use the [ceo-shadow](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests?scope=all&utf8=%E2%9C%93&state=all&label_name[]=ceo-shadow) label).
   Post the MR links in the `#ceo-shadow` Slack channel so the CEO knows they have been completed. It is not required to create issues for these tasks. Go directly to a merge request if it is more [efficient](/handbook/values/#efficiency).
1. Prepare for, take notes during, and follow up on meetings. See more details below about your [meeting responsibilities as a shadow.](#responsibilities)
1. Go through open merge requests and work towards merging or closing any that have [not been merged](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?label_name%5B%5D=ceo-shadow).
1. Publicly advise when people are not following the [communication guidelines](/handbook/communication/) ([see more details below.](/handbook/ceo/shadow/#promote-communication-best-practices)). For example, remind team members to stop screensharing to encourage communication.
1. Iterate and complete small tasks as they come up. Clear them out immediately to allow for rapid iteration on more crucial tasks. Communicate updates on those tasks in the `#ceo-shadow` channel.
1. Solve urgent issues. For example, help solve a complaint from a customer or coordinate the response to a technical issue.
1. Share [thanks](/handbook/communication/#say-thanks) in the `#thanks` channel in Slack when it comes from a customer or wider community member in a meeting.
1. Compile a report on a subject.
1. Write an [Unfiltered blog post](/blog/categories/unfiltered/) based on a conversation, something you learned, or your experience. These do not need to be approved by the CEO but he will happily review him if you'd like. Please see [the blog handbook](/handbook/marketing/blog/unfiltered/) for information about the publishing process, and be sure to read previous CEO shadows' blog posts before you start writing to ensure that your post has a new angle.
1. Ensure visual aids and presentations are visible to guests during in-person meetings.
1. Control slides and muting during the board meeting.
1. Prepare for and receive guests at Mission Control.
1. Offer GitLab swag to guests at Mission Control before they leave.
1. Answer the phone and door at Mission control.
1. Do and publish a [CEO interview](https://gitlab.com/gitlab-com/www-gitlab-com/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CEO%20interview).
1. Improve and provide training to incoming CEO Shadows.
1. Speak up when the [CEO displays flawed behavior](/handbook/ceo/#flaws).

#### Collecting and managing tasks
{:.no_toc}

The CEO shadows maintain a project called [CEO Shadow Tasks](https://gitlab.com/gitlab-com/ceo-shadow/tasks/-/boards/1385894).
It is linked in the `#ceo-shadow` Slack channel description.
Collect tasks using the first name of the shadow who captured it.
Add at the end to indicate who will action it.
Once those items have an open MR against them, post in the `#ceo-shadow` channel.

### Meetings & Events
#### CEO's Calendar
1. At the start of the week, review the CEO's calendar. The CEO's calendar is the single source of truth. Shadows should check the CEO's calendar for updates often. You will not be invited to each meeting. Meetings that the shadows may **not** attend will have a separate calendar entry on the CEO's schedule that states "No CEO Shadows". When in doubt, reach out to CEO Executive Business Admin to confirm if you should attend or not. There will be some meetings and events the shadows do not attend. Do not feel obligated to attend every meeting — all meetings are considered optional.
1. Add the CEO's calendar to your Google Calendar by clicking the `+` next to "Other Calendars".  Then click `Subscribe to Calendar`, search for the CEO and click enter. 
1. Because candidate interviews are marked as "private" (busy) for confidentiality reasons, the Executive Business Admin will invite the shadows to those events directly. As a result, you will get an email from Greenhouse asking for candidate feedback, which is not necessary.

#### Types of meetings

There are three types of meetings one the CEO's calendar: GitLab meetings, Valley meetings, and personal meetings. This is probably the most open program in the world.
The program's continued success depends on the participants respecting confidentiality during the program, after the program, and after they leave GitLab.

##### GitLab Meetings
{:.no_toc}

You will attend all GitLab meetings of the CEO, including but not limited to:

1. [1-1s](/handbook/leadership/1-1/) with reports.
1. Interviews with candidates.
1. Conversations with board members.

Like all meetings at GitLab, meetings will begin promptly, regardless of the shadows' attendance. You will travel with the CEO to meetings, team off-sites, and conferences outside of San Francisco per the CEO's schedule. 
Executive Business Admin to the CEO will assist you with conference registration and travel accommodations during these time frames.

The CEO's Executive Business Admin will ask external people if they are comfortable with the shadows joining prior to the scheduled meeting, and will share a link to the CEO shadow page to provide context.

Meeting agendas should be shared with `ceo-shadow@gitlab.com`, as shadows will be added to this email alias prior to the rotation, and removed at the conclusion of it.
For agendas that contain sensitive information, the sensitive information should be removed and the document shared with "View only" access to restrict access to the document's history.
Not all agendas will be shared, though, and the CEO Shadows should feel empowered to ask for access if that is the case.
Sometimes, the answer will be "no" for sensitive reasons.

These meetings can have different formats:

1. Video calls.
1. In-person meetings.
1. Dinners that are business related.
1. Customer visits.
1. Conferences.

You will not attend a meeting when:

1. Someone wants to discuss a complaint and wants to stay anonymous.
1. If any participant in the meeting is uncomfortable.
1. If the CEO wants more privacy.

###### "Take a break" calls
{:.no_toc}

When it comes time for the ["Take a break" call](/handbook/communication/#take-a-break-call), CEO shadows should attend on their own.
You are encouraged to share your experience as a shadow with your call group while you are shadowing.

###### Media Briefings
{:.no_toc}

CEO Shadows may be the point of contact for helping coordinate (not schedule) media briefings.
Take initiative, for example finding a quiet space for the CEO to take the call, if it is done while traveling.
When participating in media briefings, CEO Shadows are to act as silent participants, except when directly asked a question.

###### Candidate Interviews
{:.no_toc}

If the candidate is comfortable with it, CEO shadows will attend interviews performed by the CEO.
When scheduling an interview with the CEO, the EBA to the CEO will create a shared Google Doc for notes between the shadows and the CEO. The doc template can be found by searching "Notes Doc for Candidate Interviews" in Google Drive. If you have any questions, please @ mention the EBA to CEO in `#ceo-shadow` in Slack.
This notes document is then added to the scorecard for the candidate in Greenhouse.

* Shadows should ensure they mark comments they provide with their full name.
* Please **do not** complete the automated Greenhouse report that follows the interview, entitled `REMINDER: Please fill out your scorecard for [NAME]`. CEO shadows are asked to simply delete this email. 


##### Valley Meetings
{:.no_toc}

The CEO may occasionally invite you to optional meetings that may not be explicitly GitLab related, but can help provide insight into his day-to-day activities. In these meetings, it is asked that you not take notes because we don't want you to do work that isn't for GitLab. These meetings are optional and you can leave at any time.

##### Personal Meetings
{:.no_toc}

Personal meetings will be marked as "busy" on the calendar. Shadows do not attend personal calls.

#### Removing yourself from personal CEO documents

For certain meetings, such as Valley Meetings, a CEO Shadow may be added to an agenda document which is accessible to people outside of the GitLab organization. 

At the conclusion of the call, the CEO Shadows should remove themselves from document(s) they were added to via the following steps.

1. Click the `Share` button atop the Google Doc
1. On the resulting pop-up, click into `Advanced`
1. Click the `X` by your name to remove yourself
1. Click `Save changes`
1. On the resulting `Are you sure?` dialog box, click `Yes`
1. You should see a dialog appear noting that `Your access has expired`

#### Responsibilities

Meetings come in many different formats, as listed above. Your responsibilities will change based on the kind of meeting. Here are the responsibilities shadows have during meetings:

##### Taking notes
Assume that you are taking notes in a Google Doc affixed to the meeting invite unless it is a [1:1](/handbook/leadership/1-1/) between the CEO and his direct reports, a board meeting, an internal meeting where the CEO is not the host, or it is explicitly stated not to take notes. If you're unsure whether or not to take notes, default to taking them or ask the CEO. The goal of the notes is to collect the main points and outcomes, not a full transcript.

In many cases, shadows may not understand fully what is being discussed. As you are taking notes, be mindful that the goal of the program is to absorb what is being said in the meetings you are in. When you do not understand what it is being said, pause to listen and avoid documenting incorrect information.
If a notes document is not already linked, see the [templates available here](/handbook/eba/#meeting-request-requirements).

```
Tip: It's helpful if Shadow 1 takes notes as the first speaker is talking, then Shadow 2 starts when the next speaker continues the conversation. Shadow 1 can pick up note taking again when the next speaker contributes. By alternating this way, the shadows are better able to keep up with all the participants in the conversation.
```

##### Keeping time
Shadows are responsible for being aware of the current time and providing a verbal 5-minute warning to the meeting participants so that they can comfortably wrap up the meeting. Don't wait minutes for a break, just say 'we have 5 minutes left.' Please use the calendar invite as an indication how long the meeting should last. We do [speedy meetings](/handbook/communication/#scheduling-meetings). *Note: Just like notes, timekeeping is _not_ required for 1-1s between the CEO and direct reports.*

Shadows should also notify meeting participants if a meeting is running over the allocated time by saying 'we're in overtime'.

##### If you see something, say something

Shadows should notify GitLab meeting participants if their [name and job title](/handbook/tools-and-tips/#adding-your-title-to-your-name) are not mention on Zoom.

Shadows need to speak up in video calls, and speak up when the CEO's camera isn't working or when the green screen isn't working correctly because of the sun angle.
![Sun on Green Screen Zoom Issue](/images/ceoshadow/ceo_sun_zoom_issue.png){: .shadow.medium.center}

#### CEO shadow introductions

At the start of meetings, CEO shadows will introduce themselves. There is no set order for which shadow introduces themselves first. Sometimes one shadow will arrive to the meeting first, and make their introduction as the **first shadow** to speak. The terms **first** and **second** shadow define the order of who has decided to speak in any relevant meeting.

When introducing yourself in a meeting as the first shadow, say:

- I'm NAME.
- I normally am a/the TITLE.
- This is my first/last week in the two-week CEO shadow program.
- The goal of the program is to give participants an overview of the functions at GitLab.

When introducing yourself in a meeting as the second shadow, say:

- I'm NAME.
- I normally am a/the TITLE.
- This is my first/last week in the two-week CEO shadow program.

#### Finding meeting recordings

If Sid records a video to the cloud in a meeting it will eventually end up
being uploaded to [the Google Drive
folder](https://drive.google.com/drive/u/1/folders/0B5OISI5eJZ-DX0VkSkpfTm1ONjA).
Finding the video will require searching based on the calendar event name and
checking the "last modified" date.

#### Attending in-person events with the CEO


When attending events with the CEO, keep the following in mind:
1. Remind the CEO to bring extra business cards before leaving. And bring a few for yourself.
1. The CEO has outlined his [transport preferences](/handbook/ceo/#transport).
1. When traveling to events in the Bay Area by car, the CEO will request the ride sharing service.
1. When traveling to events on foot, CEO shadows should take responsibility for navigating to the event.
1. After a talk or panel, be ready to help the CEO navigate the room, particularly if there is a time-sensitive obligation after the event.

The CEO often has work events that are also social events.
In Silicon Valley, social and work are very intertwined.
These mostly take the form of lunches or dinners.
CEO shadows are invited unless otherwise specified, but there is no expectation or obligation to join, this is an optional part of the program.

### Promote Communication Best Practices

It's important that everyone encourages others to follow the [communication guidelines](/handbook/communication/), not just the CEO. As shadows, in Group Conversations and other settings, you should remind team members to:
*  Verbalize questions
*  Stop sharing their screens to encourage conversations
*  Provide full context for the benefit of new team members
*  When someone starts a Group Conversation with a presentation, ask them to stop and [record a video next time](/handbook/people-group/group-conversations/#presentation)

### Email Best Practices
In order to ensure continuity across CEO shadow participants. Always, cc `ceo-shadow@gitlab.com` on emails as part of the program. This ensure that even after
you've left the program the response and follow up can be tracked.


### Friendly competition


CEO shadows label the handbook MRs they create with the `ceo-shadow` label.
It's a point of competition between CEO shadows to try to best the previous shadows number of merge requests.

<iframe class="dashboard-embed" src="https://app.periscopedata.com/shared/40ce74a5-00ff-4a5d-8162-b1fe212d8cbc?embed=true" height="700"> </iframe>


### Follow activity from the CEO
Shadows are encouraged to follow the CEO's activity on various platforms to get a complete picture of his everyday activities and where he directs his attention.

#### In Slack
{:.no_toc}
Go to the Slack search bar and type "from:@sid" and it will populate the results.

![Slack User Activity](/images/ceoshadow/slackuseractivity.png){: .shadow.medium.center}
Follow Sid's Slack activity to follow his everyday engagements
{: .note.text-center}

#### In GitLab
{:.no_toc}
This can be seen on the CEO's [GitLab activity log](https://gitlab.com/users/sytses/activity).

![GitLab Activity Log](/images/ceoshadow/gitlabactivitylog.png){: .shadow.medium.center}
See what issues and MRs Sid is interacting with
{: .note.text-center}

#### On Twitter
{:.no_toc}
Check out [Sid's Twitter account](https://twitter.com/sytses).

![Twitter notification](/images/ceoshadow/twitternotification.png){: .shadow.medium.center}
Sign up for Twitter notifications (Twitter account required) to follow his everyday engagements.
{: .note.text-center}

### Documentation focus

An ongoing shadow program with a fast rotation is much more time consuming for the CEO than a temporary program or a rotation of a year or longer.
That's why most organizations choose to either have a shadow for a couple of days, or have someone for a year or more.
We want to give as many people as possible the opportunity to be a shadow, which is why we rotate quickly.
To make this happen without having to invest a lot of time with training, we need great documentation around the program.
A quick turnaround on documentation is crucial, and the documentation will have a level of detail that may not be necessary in other parts of the company.

### Traveling with the CEO

When traveling with the CEO, keep the following in mind:
1. Book flights that will allow you to land before the CEO so there is no delay in transportation to the next event.
1. For airport pickup with the CEO, research the terminal the CEO arrives in and plan to be there to meet with the driver before the CEO.
1. Keep the EBA to the CEO and onsite EBA updated regularly and promptly on estimated arrival time in `#ceo-shadow` Slack channel to ensure the schedule is on time.
1. If travel plans change, please update the EBA(s) in Slack immediately so cancellations to prior transportation can be made promptly to not incur fees.
1. When returning to San Francisco if on a different airline, be sure to arrive before the CEO and communicate a [meetup location if traveling back to Mission Control](#rideshare-from-airport) together.

### Remote shadow rotations

![GitLab all-remote mentor](/images/all-remote/ceo-shadow-gitlab-awesomeness.jpg){: .shadow.medium.center}

The CEO Shadow Program is temporarily fully remote because of COVID-19 [travel restrictions](#travel-guidance-covid-19). 
The shadows will participate in all meetings from their usual work environment. 

While remote shadows won't get to work from Mission Control or attend in-person meetings with the CEO, they will still get an immersive experience through the program. 
A remote rotation may also be an ideal opportunity for a team member who has been unable to travel for an in-person rotation in the past. 

For insights on maximizing a remote CEO Shadow rotation, view takeaway recap videos from [Betsy](https://youtu.be/DGJCuMVp6FM) (Talent Brand Manager) and [Darren](https://youtu.be/4yhtYcOZn3w) (Head of Remote), as well as Darren's [blog](/blog/2020/05/22/gitlab-remote-ceo-shadow-takeaways/).

#### Tips for remote shadows

- Be sure that you have an [ergonomic workspace](/company/culture/all-remote/workspace/). You'll be taking lots of notes during meetings, and will want a comfortable setup.
- Communicate clearly with your co-shadow about shared tasks since you will not be working together in person. 
- Take breaks from your desk when there is a break in the CEO's schedule. Because you're not working from Mission Control or traveling to other meetings with the CEO, it's important to make time to [move around.](/company/culture/all-remote/tips/#dedicate-time-for-health-and-fitness)
- Depending on your time zone, working in Pacific Time may be an adjustment to your typical working hours. [Plan ahead,](/company/culture/all-remote/getting-started/#establish-routine-with-family-and-friends) especially if you're balancing responsibilities with family, pets, roommates, etc. 
- Consider switching locations in your home or workspace during the CEO's 1:1 meetings. Since you [won't be taking active notes](#responsibilities) in these calls, this is a good opportunity to change position or scenery intermittently throughout the day.

## Mission Control Guide

### Working from Mission Control

You are welcome to work from Mission Control, but it is not required to be there in person unless there is an in-person meeting, event, or dinner. It's up to you to manage your schedule and get to places on time. If you are traveling somewhere, meet the CEO at Mission Control at the beginning of the allotted travel time listed on the calendar.

If there is a day during your rotation where all meetings are Zoom meetings, you can work from wherever you want, as you normally would.
You can also work from Mission Control if you prefer.
If you decide to split your day between remote work and working from Mission Control, make sure you give yourself enough time to get to Mission Control and set up for the guest.
It's OK to join calls while mobile.
In addition, feel free to coordinate or join a co-working day with team members in the Bay Area.
To coordinate join the `#loc_bayarea` Slack channel.

Shadows are welcome at Mission Control 10 minutes prior to the first meeting until 6 p.m. Feel free to ask if you can stay later. Don't worry about overstaying your welcome, if Karen or Sid would like privacy they will ask you to leave explicitly. When you arrive and depart, please make sure the door to Mission Control closes fully.

One more thing: the cat feeder is automatic and goes off daily at 10:22am PT (as well as another time, it's a surprise!). No need to be alarmed by the metallic clanging sound.

### Working outside Mission Control

Outside of Mission Control hours, shadows have the following options:

* If not an in person meeting, you are welcome to take the meeting from your lodging and then proceed to Mission Control during the Group Conversation or Company call.
* There are coffee shops with early opening hours with Wifi access near Mission Control. This is a great venue to meet with your co-shadow if coordination is needed before heading in.
  * [Philz Coffee on Folsom Street](https://www.google.com/maps/place/Philz+Coffee/@37.7890402,-122.3975675,16z/data=!4m8!1m2!2m1!1sphilz+coffee!3m4!1s0x0:0xba68b065a8d2c93b!8m2!3d37.7887131!4d-122.3930718), opens at 05:30am.
  * [Philz Coffee on Front Street](https://www.google.com/maps/place/Philz+Coffee/@37.7890402,-122.3975675,16z/data=!4m8!1m2!2m1!1sphilz+coffee!3m4!1s0x0:0x4b3009758e346914!8m2!3d37.7917908!4d-122.3990035), opens at 05:30am.
  * [Starbucks Reserve on Mission Street](https://www.google.com/maps/place/Starbucks+Reserve/@37.7896722,-122.3933832,17z/data=!4m8!1m2!2m1!1sstarbucks!3m4!1s0x0:0x76ac8e682da13edb!8m2!3d37.791511!4d-122.3951029), opens at 05:00am.
  * [Starbucks on the 5th floor of the Salesforce tower](https://www.google.com/maps/place/Starbucks/@37.7899796,-122.3966609,18z/data=!4m8!1m2!2m1!1sstarbucks!3m4!1s0x0:0x4e742c0956f70966!8m2!3d37.7899051!4d-122.394609), opens at 04:00am.

### Mission Control access

When entering the building, the doorperson may ask who you are there to see. Don't say "GitLab" since there is no GitLab office. The doorperson will direct you to the correct lobby.

While there are two sets of keys, it's worthwhile coordinating access to Mission Control with the outbound shadow on your first day. Meeting up on Sunday evening or at a specific time on Monday morning. This will enable the incoming shadow to be introduced into Mission Control without impacting Sid and/or Karen.

### Recommended Food/Drinks nearby

#### Food
{:.no_toc}

There is a [Yelp list](https://www.yelp.com/collection/sVnYVUU_npZzJ6h9koBbxw) that contains recommendations from previous shadows, as well as the [CEO's Favorite Restaurants](/handbook/ceo/#favorite-restaurants).

The list is administered by the `ceo-shadow@` email address. Login to Yelp with the credentials in the CEO Shadow 1Password vault to add restaurants, update notes, or remove items from the collection.

There are usually also food trucks in front of the Salesforce Tower and on the opposite side of Mission Street from the Salesforce Tower [in an alley](https://goo.gl/maps/oSJMfJ26QerGVPqPA).

#### Drink
{:.no_toc}

1. [Bluestone Lane](https://www.google.com/maps/place/Bluestone+Lane/@37.7900021,-122.4031358,16.56z/data=!4m12!1m6!3m5!1s0x8085808814a10285:0x3b8f6e4330a367d9!2sBluestone+Lane!8m2!3d37.7878896!4d-122.4028954!3m4!1s0x8085808814a10285:0x3b8f6e4330a367d9!8m2!3d37.7878896!4d-122.4028954)
    * Tye Davis Favorite (AMAZING COFFEE) - long walk
1. [Starbucks](https://www.google.com/maps/place/Starbucks/@37.7899941,-122.3977364,18.43z/data=!4m12!1m6!3m5!1s0x8085808814a10285:0x3b8f6e4330a367d9!2sBluestone+Lane!8m2!3d37.7878896!4d-122.4028954!3m4!1s0x80858067244672f9:0xea9a5743328af8ff!8m2!3d37.789545!4d-122.397592)

#### Rewards Cards ("punch cards")
{:.no_toc}

Occasionally, food trucks or restaurants have loyalty rewards cards. It is **not required** but if you get one and want to leave it for future shadows to use, please add to this list and put the reward card in the CEO shadow drawer at Mission Control.

1. [Bowld Acai](https://www.bowldacai.com/) Food Truck

### Mission Control operations

#### Monitors

We have six monitors at Mission Control. They show the following content:

|:---:|:---:|:---:|
|**Top Left**<br/>[Team](/company/team)|**Top Middle**<br/>[Category Maturity](/direction/maturity/)|**Top Right**<br/>[Is it any good?](/is-it-any-good/#gitlab-has-yoy-growth-in-adoption-of-version-control-services-study-while-github-and-bitbucket-both-decline)|
|**Bottom Left**<br/>[Category Maturity](/direction/maturity/) | **Bottom Middle**<br/>[Who we replace](/devops-tools/)| **Bottom Right**<br/>[Remote Manifesto](/company/culture/all-remote/) on how to work remotely |

##### Configuring the Monitors
{:.no_toc}

Turning all screens on or off might be challenging, since a single remote controls all of them. The easiest way to do that is by covering the tip of the remote with your hand and getting as close as possible to a single screen while turning it on or off.

To configure the sales dashboards:

1. Go to [Clari](https://app.clari.com).
1. Go To Pulse tab.
1. Open the left side bar.
1. Click on the funnel icon. Select “CRO”.
1. Click on the gear icon. Go to Forecasting. Select Net IACV.

##### How to use keyboard and mouse to update screens
{:.no_toc}

The wireless mouse and keyboard are connected to the bottom left TV by default because that one is visible from both sides of the conference table. To update the view on another TV, you have to connect the wireless keyboard and mouse to the desired screen. Afterwards, don't forget to return it to the bottom left position for use during meetings.

1. Find the USB jacks and Logitech receiver underneath the bottom, right TV (they're all labeled).
![USB jacks and Logitech Reciever](/images/ceoshadow/HDMI_Logitech.png "USB jacks & Logitech Reciever ")
1. Connect the Logitech receiver to USB receiver for the desired screen.
1. To log into the chrome devices at Mission Control, use the login information in the "[CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi)" in 1Password.

##### Troubleshooting the monitors
{:.no_toc}

Turning all screens on or off might be challenging since a single remote controls all of them. The easiest way to do that is to make a cone with the foil (a piece of foil can be found on the white TV-remote tray) around the front edge of the remote, and getting as close as possible to a single screen while turning it on or off.

Each of the screens at Mission Control use an Asus Chromebit to display the preferred content and are connected to the HDMI1 port of each TV. If you turn on the TVs and one displays a blank screen while on the HDMI1 input, the Chromebit may need to be reset. You can do this by power cycling the Chromebit (they are located behind the bottom right TV and are labeled), connecting the Chromebit to the [keyboard and mouse](/handbook/ceo/shadow/#how-to-use-keyboard-and-mouse-to-update-screens), and logging into it using the credentials in the "[CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi)" via 1Password. Once you have logged into the Chromecast, you can quickly find the proper content as listed in [Configuring the Screens](/handbook/ceo/shadow/#configuring-the-screens) via the browser's recent history.

##### Updating the software on the monitors
{:.no_toc}

To check that a Samsung TV is up to date:

1. Grab the remote labled for the TV
1. Hit the home button
1. Go to "Settings"
1. Go to "Support"
1. Select "Software Update"
1. Select "Update Now"

#### AirPlay

To screencast from an iPad or MacBook to the top left screen, switch the "Source" on the **top left** screen to "Apple TV" (HDMI 2).

Using the larger remote (with the white buttons), you can press the white center button in the top row of buttons; this will bring up a list of sources. There is a direction pad on the remote towards the bottom that has `< ^ > v` buttons, as well as the selection button in the center.

When the TV is on the Apple TV source, you may need to change the Apple TV to
AirPlay mode. The Apple TV remote is the small black one with only 5 buttons.
You can click "MENU" until it says AirPlay.

To return to the normal configuration, choose "Source" then "HDMI 1".

#### iPads

There are two iPads at Mission Control (without password protection). You can use the [duet app](https://www.duetdisplay.com/) as a second monitor if desired. Both iPads are linked to the EBA to the CEO's Apple ID. Please verify any purchases ahead of time with the EBA to the CEO.

#### Printer

The printer at Mission Control is called `HP Officejet Pro 8610` and is available over AirPlay/Wifi. The printer is located in Sid's office.

#### Zoom Room

Zoom Rooms is an application used for team members not in San Francisco to participate in meetings happening at Mission Control. There's a separate screen (the **large** one on wheels), a Mac Mini and, iPad at Mission Control for this purpose. The Mac Mini is connected to HDMI1 on the screen, and the iPad operates as a remote control for Zoom Rooms.

1. **Setup**
    1. Turn on the big screen on wheels.
    1. Turn on the Mac Mini.
    1. Start the Zoom Rooms application on the Mac Mini.
    1. If you have to log in or, provide a passcode, both are in the [CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi).
    1. Start the Zoom Rooms application on the iPad (you may need the passcode).
    1. If there's any problem connecting to the service, log out and back in. If that fails, contact the Executive Admin to the CEO.
1. **If you do not have the option to join a meeting on the iPad (It's in "Scheduling mode" - you don't have a "Meet Now" or "Join" button):**
    1. Make sure Zoom Rooms on the iPad is logged in.
    1. Click the settings "gear" icon in the top right hand corner.
    1. Disable the "lock settings" option (you'll need the passcode).
    1. Tap on "Zoom Room Mission Control".
    1. Tap "Switch to Controller".
1. **If the Zoom Rooms app on the iPad is not connecting:**
    1. Check the credentials for the Zoom Room in the [CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi). If unable to log in, please contact the Executive Admin to the CEO on slack.
    1. Log out of the Zoom Rooms app on the iPad.
    1. Log in using the PeopleOps Zoom account credentials in the [CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi).

If the EBA to the CEO is unavailable, the CEO shadow may be responsible for handling the technical details of coordinating Zoom meetings. If using a **webinar**, you will need to be a co-host in order to promote participants to panelists so that they can verbalize their own questions.

#### Podcasts on Zencaster
When joining a podcast in Zencaster, a microphone error can be avoided by leaving your microphone unmuted for the first 30 seconds / minute.
    1. Zencaster checks that your mic is working by recording audio so, muting the mic causes the error.
    1. The system check happens when you first load the page
    1. If you get the microphone error, reload the page and wait for the checks to finish before muting.

#### Maintaining Software and Hardware

The devices in Mission Control are configured to auto-update when new versions are available. As a precaution, shadows in their second week should confirm all software in Mission Control is update to date.


### Visitors
#### Preparing for Visitors

In preparation for guests (customers, investors, etc.) who will be meeting with the CEO or other team members at Mission Control, please note the following prior to the meeting start:

1. All GitLab team members sit on one side. This allows for easy communication with the guests.
1. Set the lighting mode to 'Evening' - lighting controls are located next to the kitchen entrance.
1. Have drinks from the fridge (on the tray) available on the table.
1. Get the glasses from the cupboard above the coffee machine and place them next to the drink tray.
1. Add the Brita water pitcher on the table.
1. Place the keyboard and the mouse (used for Mission Control screens) on the table.
1. Ensure that all 6 monitors are displaying the [correct content](/handbook/ceo/shadow/#mission-control-device-setup). If any of the screens are blank, follow the [trouble-shooting instructions](/handbook/ceo/shadow/#troubleshooting-the-tv-screens).
1. For investor or banker meetings, see the [confidential issue](https://gitlab.com/gitlab-com/ceo-shadow/tasks/issues/50) with more details on preparations.
1. If someone is attending a meeting at Mission Control via [Zoom Rooms](#zoom-room):
    1. Follow the setup steps for [Zoom Rooms](#zoom-room).
    1. Move the screen to the head of the table.
    1. Click 'Join' in the Zoom Rooms menu and enter the meeting ID from the Google Calendar invite of the meeting in question.
    1. Once the meeting is loaded, click on the participants list and make sure that the iPad is visible from the table.

#### Welcoming Visitors to Mission Control

1. The front desk will call the apartment when a visitor arrives.
1. A Shadow should meet guests at the elevator, greet them, and guide them to Mission Control.
1. Once the meeting is over, a Shadow should escort the guests back to the elevator.

### Suzy (the cat)

Please note that we have a cat named [Suzy](/company/team-pets/#7-suzy). It is a Russian Blue mix which is a [hypoallergenic variety](https://www.russianbluelove.com/russian-blue-cat-allergies/).

Suzy likes attention and will invite you to pet her. If you're allergic to cats consider washing your hands after petting.
If you don't want to wash your hands everytime after petting Suzy you can *gently* paddle pet her using ping pong paddles available in Mission Control. Check out the video's linked below for techniques!
Please note the white pillow on the sofa in Mission Control is the **only** place to paddle pet Suzy. She really enjoys it when you *gently pat* her sides with the ping pong paddles when she is **on** the white pillow, if she steps **off** the pillow stop petting her. When she gets back onto the pillow you can resume gently paddle petting her.
Please don't pet her after/when she meows since that reinforces the meowing which can be annoying during calls and the night.
You can pick her up but she doesn't like it much and will jump out after about 30 seconds. Lastly, Suzy sometimes tries to drink out of the toilet, please make sure to leave the toilet lid down.
![](/images/handbook/suzypetpillow.jpg)

<!-- blank line -->
#### Paddle Pat technique with Eric Brinkman
{:.no_toc}
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/vf-uEPweMUg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->
#### Paddle Pat & Rub technique with JJ Cordz
{:.no_toc}
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/KLWQH0EDtcg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Mission Control FAQs

1. Everything in the fridge that is liquid can be consumed including Soylent and alcohol.
1. Thermometer is located next to the kitchen entrance.
1. Lighting controls are located next to the kitchen entrance, select 'Evening'.
1. Swag (socks and stickers) is located on the bookshelf near the dashboards. (If the swag is running low, reach out to the Executive Admin to the CEO with a list for restocking)

### Food and drinks at Mission Control

Everything in the fridge that is liquid can be consumed including Soylent and alcohol. Coffee machine is located in the kitchen, coffee pods are in the drawer below the coffee machine.

If the beverages are running low, start by checking the top cupboard above the oven (next to the fridge). There's a step ladder in the laundry (last door on the **left** down the hall way)
If items in the cupboard are also running low, reach out to the Executive Admin to the CEO list of items that need to be ordered in the `#ceo-shadow` slack channel tagging the EBA to the CEO so all items can be ordered at once.
This should happen every Monday. Here is a list of items that may need to be restocked:
   - La Croix (usually there are 3 flavours)
   - Red Bull
   - Sugarfree Red Bull
   - Sprite Zero
   - Coca Cola
   - Diet Coke
   - Fiji water
   - Soylent Cacao
   - Snacks in the lock box (try to keep enough so that the box is full) - Preferences of snacks for the incoming shadow and restocking any snack preferences from the outgoing shadow.

## Travel & Expenses

### Lodging

Lodging during the CEO shadow program is provided by the company. Executive Admin to the CEO books the accommodation based on availability and cost. You can express your preference (hotel or AirBnB) via email to the Executive Admin to the CEO in question, however the final decision is made by the Executive Admin based on the distance from CEO and costs. Executive Admin will provide the accommodation details no earlier than 1 month and no later than 2 weeks before the scheduled rotation.

Accommodation is provided only for the active shadowing period, it is not provided during the shadow program pause (cases when the CEO is unavailable).
In case you are coming from a timezone that is more than 6 hours difference with Pacific Time, it is possible to book the weekend before the first shadow work day to adjust to the new timezone.

If your CEO shadow rotation is two consecutive weeks, it is expected you will be staying the weekend. Accommodation is provided during the weekend.

### Airfare

Airfare can be booked according to our [travel policy](/handbook/travel/#booking-travel-and-lodging) or [spending company money](/handbook/spending-company-money/) policy.
In case your shadow rotation includes time without shadowing, it is possible to expense airfare to fly home and back within the continental USA. If you are from outside of the USA, it is also possible to expense airfaire during the time without shadow because of the possible high cost of lodging in San Francisco if you chose to stay at a different location.

#### Rideshare from airport

At San Francisco International airport (SFO), all rideshare apps (Uber, Lyft, etc) pick up on level 5 of the parking structure. When coordinating travel from SFO to Mission Control with other Shadows, GitLabbers or Sid arranging to meet on level 5 of the parking structure is most efficient as each terminal has its own baggage claim area.

### Expensing meals

Shadows are able to expense food and beverage during their rotation and should follow our [spending company money](/handbook/spending-company-money/) policy. Previous shadows have created a [recommendation list](/handbook/ceo/shadow/#recommended-fooddrinks-nearby) of their favorite food places to help.

### Childcare

Childcare is provided during the active shadowing period and will be reimbursed via your expense report. You must book the childcare yourself and it is advised you reach out far in advance as childcare "drop-ins" can be limited depending on the week. Currently, GitLab doesn't have a ["Backup Care"](https://www.brighthorizons.com/family-solutions/back-up-care) program so you must tell the childcare it is for a "drop-in".  Depending on your hotel accommodations, finding a nearby daycare is most convenient or a daycare nearby the [Millennium tower](https://www.google.com/maps/place/Millennium+Tower+San+Francisco/@37.7905055,-122.3962516,15z/data=!4m2!3m1!1s0x0:0x9fe15ebd4a8300d8?sa=X&ved=2ahUKEwiUoZ_hpb_iAhXBop4KHeOAB2QQ_BIwGHoECAsQCA). Some childcare facilities will require payment at end-of-day or end-of-week via cash/check only so request an invoice/receipt for expense submission purposes.

Past Childcare facilities that have been accommodating:

1. [Bright Horizons at 2nd Street](https://child-care-preschool.brighthorizons.com/ca/sanfrancisco/2ndstreet?utm_source=GMB_yext&utm_medium=GMBdirectory&utm_campaign=yext&IMS_SOURCE_SPECIFY=GMB) - This facility is nearest the [Courtyard by Marriott SF Downtown Hotel](https://www.google.com/maps/place/Courtyard+by+Marriott+San+Francisco+Downtown/@37.785751,-122.3997608,16.7z/data=!4m12!1m6!3m5!1s0x8085807c643d1007:0x85815e04bf8d233c!2sCourtyard+by+Marriott+San+Francisco+Downtown!8m2!3d37.7859011!4d-122.3969222!3m4!1s0x8085807c643d1007:0x85815e04bf8d233c!8m2!3d37.7859011!4d-122.3969222).
    * Contact: [Rose - Current Director](https://child-care-preschool.brighthorizons.com/ca/sanfrancisco/2ndstreet)

### Travel Guidance: COVID-19

Our top priority is the health and safety of our team members, please refer to the current [travel policy](/handbook/travel/#travel-guidance-covid-19). The CEO Shadow program is classified as non-essential travel and travel to San Francisco will not be required during the time frame specified in the policy linked. CEO Shadows joining the program should plan on participating in the program remotely and matching the CEO's schedule which is primarily in the Pacific time zone unless the CEO is traveling to another time zone. If you have questions please use `#ceo-shadow` in slack and @ mention the [Staff EBA to the CEO](/handbook/eba/#executive-business-administrator-team)


## Alumni

CEO Shadow program alumni are welcome to join the `#ceo-shadow-alumni` Slack channel to stay in touch after the program.

| Start date | End date | Name | Title | Takeaways |
| ------ | ------ | ------ | ------ | ------ |
| 2019-03 | 2019-04 | [Erica Lindberg](https://gitlab.com/erica) | Manager, Content Marketing | [CEO shadow learnings video](https://www.youtube.com/watch?v=xrWR0uU4nbQ) |
| 2019-04 | 2019-05 | [Mayank Tahil](https://gitlab.com/mayanktahil) | Alliances Manager |
| 2019-04 | 2019-05 | [Tye Davis](https://gitlab.com/davistye) | Sr. Technical Marketing Manager | [Without a shadow of a doubt: Inside GitLab's CEO shadow program](/blog/2019/07/11/without-a-shadow-of-a-doubt/)
| 2019-05 | 2019-06 | [John Coghlan](https://gitlab.com/johncoghlan) | Evangelist Program Manager | [5 Things you might hear when meeting with GitLab's CEO](/blog/2019/06/28/five-things-you-hear-from-gitlab-ceo/)
| 2019-06 | 2019-06 | [Cindy Blake](https://gitlab.com/cblake) | Sr. Product Marketing Manager | [CEO shadow learnings video](https://www.youtube.com/watch?v=3hel57Sa2EY)
| 2019-06 | 2019-06 | [Nnamdi Iregbulem](https://gitlab.com/whoisnnamdi) | MBA Candidate at Stanford University |
| 2019-06 | 2019-06 | [Clinton Sprauve](https://gitlab.com/csprauve) | PMM, Competitive Intelligence |
| 2019-06 | 2019-07 | [Lyle Kozloff](https://gitlab.com/lyle) | Support Engineering Manager |
| 2019-07 | 2019-07 | [Marin Jankovski](https://gitlab.com/marin) | Engineering Manager, Deliver |
| 2019-07 | 2019-08 | Danae Villarreal| Sales Development Representative, West |
| 2019-08 | 2019-08 | [Daniel Croft](https://gitlab.com/dcroft) | Engineering Manager, Package | [GitLab, CEO Shadow August 2019 week one, mind blown](https://www.youtube.com/watch?v=VHcA_2UsC2k) |
| 2019-08 | 2019-08 | [Emilie Schario](https://gitlab.com/emilie) | Data Engineer, Analytics | [What I learned about our CEO's job from participating in the CEO Shadow Program](/blog/2019/10/07/what-i-learned-about-our-ceo-s-job-from-participating-in-the-ceo-shadow-program/) |
| 2019-08 | 2019-08 | [Kenny Johnston](https://gitlab.com/kencjohnston) | Director of Product, Ops |  |
| 2019-09 | 2019-09 | [Eric Brinkman](https://gitlab.com/ebrinkman) | Director of Product, Dev |  |
| 2019-09 | 2019-10 | [Danielle Morrill](https://gitlab.com/dmor) | General Manager, Meltano |  |
| 2019-10 | 2019-10 | [Mek Stittri](https://gitlab.com/meks) | Director of Quality |  |
| 2019-10 | 2019-11 | [Kyla Gradin](https://gitlab.com/kyla) | Mid Market Account Executive |  |
| 2019-10 | 2019-11 | [Clement Ho](https://gitlab.com/ClemMakesApps) | Frontend Engineering Manager, Monitor:Health |  |
| 2019-11 | 2019-11 | [Brendan O'Leary](https://gitlab.com/brendan) | Sr. Solutions Manager |  |
| 2019-11 | 2019-11 | [Gabe Weaver](https://gitlab.com/gweaver) | Sr. Product Manager, Plan: Project Management |  |
| 2019-11 | 2020-01 | [Chenje Katanda](https://gitlab.com/ckatanda) | Technical Account Manager | |
| 2020-01 | 2020-01 | [Dov Hershkovitch](https://gitlab.com/dhershkovitch) | Senior Product Manager, Monitor |  |
| 2020-01 | 2020-01 | [Keanon O'Keefe](https://gitlab.com/kokeefe) | Senior Product Manager, Plan : Portfolio Management |  |
| 2020-01 | 2020-01 | [Dylan Griffith](https://gitlab.com/DylanGriffith) | Staff Backend Engineer, Search | |
| 2020-01 | 2020-02 | [Brittany Rohde](https://gitlab.com/brittanyr) | Manager, Compensation & Benefits | [How the CEO Shadow Program boosted my individual productivity during the COVID-19 Crisis](https://about.gitlab.com/blog/2020/05/29/how-the-ceo-shadow-program-boosted-my-individual-productivity-during-the-covid-19-crisis/) |
| 2020-01 | 2020-02 | [Nadia Vatalidis](https://gitlab.com/Vatalidis) | Senior Manager, People Operations | |
| 2020-02 | 2020-02 | [Diana Stanley](https://gitlab.com/dstanley) | Senior Support Engineer | |
| 2020-02 | 2020-02 | [Chloe Whitestone](https://gitlab.com/chloe) | Technical Account Manager | |
| 2020-02 | 2020-02 | [Sarah Waldner](https://gitlab.com/sarahwaldner) | Senior Product Manager - Monitor: Health | |
| 2020-02 | 2020-03 | [Shaun McCann](https://gitlab.com/shaunmccann) | Support Engineering Manager | [CEO Shadow AMA with Support Engineering](https://www.youtube.com/watch?v=A80HKDBRaNE) |
| 2020-03 | 2020-03 | [Lien Van Den Steen](https://gitlab.com/lienvdsteen) | People Ops Fullstack Engineer | |
| 2020-03 | 2020-03 | [Michael Terhar](https://gitlab.com/mterhar) | Technical Account Manager | [The HyperGrowth Calendar](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/44584) |
| 2020-03 | 2020-04| [Christen Dybenko](https://gitlab.com/cdybenko) | Sr Product Manager |  |
| 2020-04 | 2020-04| [Scott Stern](https://gitlab.com/sstern) | Frontend Engineer  |  |
| 2020-04 | 2020-04 | [Stella Treas](https://gitlab.com/streas) | Chief of Staff |  |
| 2020-04 | 2020-04 | [Bradley Andersen](https://gitlab.com/elohmrow) | Technical Account Manager |  |
| 2020-04 | 2020-04 | [Cassiana Gudgenov](https://gitlab.com/cgudgenov) | People Operations Specialist |  |
| 2020-04-28 | 2020-05-08 | [Betsy Church](https://gitlab.com/bchurch) | Senior Talent Brand Manager | [Reflecting on the CEO Shadow Program at GitLab](https://youtu.be/DGJCuMVp6FM) |
| 2020-05-04 | 2020-05-15 | [Darren Murph](https://gitlab.com/dmurph) | Head of Remote | [GitLab CEO Shadow recap — key takeaways and lessons learned from a remote rotation](https://about.gitlab.com/blog/2020/05/22/gitlab-remote-ceo-shadow-takeaways/) |
| 2020-05-11 | 2020-05-22 | [Emily Kyle](https://gitlab.com/emily) | Manager, Corporate Events |  |
