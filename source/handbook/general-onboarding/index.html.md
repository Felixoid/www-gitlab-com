---
layout: handbook-page-toc
title: "GitLab Onboarding"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Onboarding at GitLab

Onboarding is [incredibly important](/company/culture/all-remote/onboarding/) at GitLab. We don't expect you to hit the ground running from day one.

We highly recommend taking at least two full weeks for onboarding and only in week three starting with team specific onboarding and training. Please feel free to participate in your team's work in your first two weeks, but don't feel like you have to contribute heavily.
All onboarding steps are in the [onboarding issue template](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md) which is owned by the People Experience Team.

Each onboarding issue has a main section that contains tasks relevant to all GitLab team-members and a due date of 30 days. Below the main section are department and role-specific tasks. Some roles and departments have tasks that link to a supplemental issue template or an additional onboarding page.  Reach out to your [onboarding buddy](/handbook/general-onboarding/onboarding-buddies/) or other GitLab team members if you need help understanding or completing any of your tasks.

Through onboarding issues, you should gain access to our team member [baseline entitlements](https://about.gitlab.com/handbook/engineering/security/#baseline-role-based-entitlements-access-runbooks--issue-templates). On Day 2 of onboarding an [Access Request](https://about.gitlab.com/handbook/people-group/engineering/#access-request-issue-creation) will be generated, if a template has been created for the role. Access requests are owned by the IT team. If you have any access requests related questions, please reach out to #it-help in Slack.

The lists below categorize each onboarding resource by its location.

## Managers of New Team Members 

An issue is created for new team members at least 4 business days prior to their start date. The Manager and a People Experience Associate will be assigned to this issue. **Managers, People Experience and IT Ops** all have tasks that need to be completed **prior to the start date** to ensure a smooth and successful onboarding process. For questions or help with any of these tasks feel free to reach out in the issue by mentioning `@gl-people-exp` or adding a question in the `#peopleops` Slack channel. 

## Compliance 

The [People Experience Coordinator] (https://about.gitlab.com/job-families/people-ops/people-experience-associate/#people-experience-coordinator) completes a monthly audit of all open onboarding issues to ensure that the new team member, manager and People Experience team tasks are completed. More importantly, there are certain tasks which need to be completed  in line with our company compliance (security, payroll, etc).  

If any tasks are still outstanding, the People Experience Coordinator will ping the relevant members on the issue requesting action on the items or checking whether the issue can be closed. 

*It remains the responsibility of the onboarding team member or the onboarding team members manager to close the issue.*

The employment bot will automatically close any onboarding issues still open after 60 days.

## Completing Onboarding Issue

To ensure a successful completion of the onboarding issue, it is important that all tasks are checked off, whether the task is applicable to the onboarding team member or not. Checking the box indicates one of the following:

* I have completed this task
* I have checked and this task is not applicable to me

### Onboarding Issue Template Links
These templates are used by the People Experience team to onboard new team members. 

- [All GitLab team-members](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/blob/master/.gitlab/issue_templates/onboarding.md)
- [Intern](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/onboarding_intern_engineering.md)

#### Role Specific Templates 
These are added to the "All Team Member" Template 

- [People Managers](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-people-managers-only)
- [Engineering, such as Developers, Build, Infrastructure, etc.](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-engineering-such-as-developers-build-infrastructure-etc-only)
- [Production and Database Engineering](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-production-and-database-engineering-only)
- [Database Engineering](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-database-engineering-only)
- [Support](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-support-only)
- [Community advocates](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-community-advocates-only)
- [Product Design](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#product-designers)
- [Frontend Engineering](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-frontend-engineers-only)
- [Product Management](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-product-management-only)
- [Marketing Design](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-marketing-design-only)
- [Security](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-security-only)
- [Finance](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-finance-only)
- [People Ops](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-people-ops-only)
- [Recruiting](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-recruiting-only)
- [Core Team members](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-core-team-members-only)
- [Technical writing](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-technical-writers-only)
- [Marketing non-SDR](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-marketing-non-sdrbdr-only)
- [Sales Development Representatives](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-outbound-sdrs-only)
- [Sales](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-sales-only)
- [Customer success](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#for-customer-success-only)

### Supplemental onboarding issue templates

* [Interviewing training issue](https://gitlab.com/gitlab-com/people-ops/Training/blob/master/.gitlab/issue_templates/interview_training.md)
* [Monitor group onboarding issue](https://gitlab.com/gitlab-org/monitor/onboarding/blob/master/.gitlab/issue_templates/Monitor_Onboarding.md)
* [Becoming a GitLab manager issue](https://gitlab.com/gitlab-com/people-ops/Training/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md)
* [Production engineering onboarding issue](https://gitlab.com/gitlab-com/gl-infra/infrastructure/blob/master/.gitlab/issue_templates/onboarding_template.md)
* [Security products technical onboarding issue](https://gitlab.com/gitlab-org/security-products/onboarding/blob/master/.gitlab/issue_templates/Technical_Onboarding.md)
* [Support agent onboarding issue](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Onboarding%20-%20GitLab.com%20Support%20Agent.md)
* [Support engineer onboarding issue](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Onboarding%20-%20GitLab.com%20Support%20Engineer.md)

### Additional onboarding pages

* [Consultant onboarding and offboarding](/handbook/general-onboarding/consultants/)
* [Developer onboarding](/handbook/developer-onboarding/)
* [GitLab onboarding buddies](/handbook/general-onboarding/onboarding-buddies/)
* [Merge Request buddies](/handbook/general-onboarding/mr-buddies/)
* [Onboarding Processes](/handbook/general-onboarding/onboarding-processes/)
* [Quality team onboarding](/handbook/engineering/quality/onboarding/)
* [Sales team onboarding](/handbook/sales/onboarding/)
* [Support team onboarding](/handbook/support/onboarding/)
* [SRE onboarding](/handbook/engineering/infrastructure/sre-onboarding/)
* [Product Designer onboarding](/handbook/engineering/ux/uxdesigner-onboarding/)
* [UX Researcher onboarding](/handbook/engineering/ux/uxresearcher-onboarding/)
