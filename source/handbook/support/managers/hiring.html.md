---
layout: handbook-page-toc
title: Support Hiring
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Hiring new Customer Support team members is a multi-department process. It will involve Recruiting, Support, Engineering, and optionally the CEO. Below is an outline of what responsibilities we, as Customer Support, have.

Support Engineers comprise the vast majority of Customer Support, and so most of this page currently presents information specific to that job family. Exceptions are called out explicity, such as in the [Reviewing Assessments](#reviewing-assessments) section.

## Hiring Flow

Hiring is described on the [jobs page](/job-families/engineering/support-engineer/#hiring-process) for Support Engineers. Below is a breakdown of what to do in the sections we are responsible for.

### Qualified candidates receive a short questionnaire from our Global Recruiters

The assessment questionnaire contains a number of customer scenarios and technical questions relevant to the position, including written
English ability.

### Candidates will be invited to schedule a first interview with a Support Engineer

The technical interview for this role is a practical interview that covers topical areas relevant to the position, including customer scenarios.

### Candidates will then be invited to schedule a Managerial interview with the Support Engineering Manager

The managerial interview may cover any topics of the previous interviews where there was unclear data or a need to dive deeper.

After this interview, the candidate will proceed to the Director of Support. The Support team's role is done!


## Getting involved
If you want to be involved in the hiring process - let your manager know! The main ways you can be involved are:
- reviewing assessments
- doing technical interviews.

After getting approval from your manager open two interview training issues:

- [PeopleOps Interview Training](https://gitlab.com/gitlab-com/people-ops/Training/issues/new?issuable_template=interview_training)
- [Bootcamp - Support Interview Training](https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=Bootcamp%20-%20Support%20Interview%20Training)

### Reviewing Assessments
Assessments (and answers) **for all Support job families** are available in the [people-ops/hiring-processes](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Support) repository.

When reviewing an assessment: 
1. Use the rubrik for the assessment that was sent along to determine if they passed or not.
2. Review their CV, Cover Letter and any attached documentation. Does this person possess the background and skills to succeed in your role?
3. Fill out the scorecard with any comments. Keep in mind that all fields are optional with the exception of the "Overall Recommendation". 
If you're unable to offer an evaluation of an area you may leave it blank. 

### Technical Interviews
The technical interview process is documented in the [GL-Support Group](https://gitlab.com/gl-support/se-interview). Follow the instructions there to proctor this interview. Take any notes in Greenhouse and make a note if you are inclined or not inclined to hire. Your role in the interview process for this candidate is complete!
