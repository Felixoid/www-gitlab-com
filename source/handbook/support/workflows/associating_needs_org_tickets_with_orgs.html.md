---
layout: handbook-page-toc
title: Associating needs-org tickets with appropriate organization
category: Zendesk
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Associating tickets with the `needs-org` tag with appropriate organizations
Occasionally tickets come in without an associated organization, which means that no SLA is applied.

Potential reasons this might occur:
- we've never seen anyone from the organization in ZD before (i.e. this is an organization's first ticket)
- the user is using a generic domain name that we cannot assign to a ZD organization (e.g. `@gmail.com`)
- the company has more than one division with a support level, but only a single email domain
- the user is writing from a domain name that doesn't match what we have in ZD (e.g. they're using a personal email address for a work-related issue)
- the user or organization has a trial license

It could equally be the case that a ZD organization was manually created and the SLA type is out of date / incorrect.
Many of the same principles apply here.

> Please do not manually create organizations. This can break the ZD<>SFDC
> integration and cause users to receive incorrect SLAs. If you notice an
> organization needs to be created, please notify support-ops to rectify this.

### Determining if this workflow applies
This workflow applies if:
- ZD is prompting you to create an organization
- The Tag `needs-org` is applied (implied by above)
- You have reason to believe that this ticket belongs to a customer
- The domain TLD is *not* a generic domain (e.g. `gmail.com`)

![ZD prompts for an org to be created](/images/handbook/support/zendesk_needs_org-create.png)

### Marking Free Users for GitLab.com

For GitLab.com, if a user cannot be identified as a customers, nor a [trial or prospect](#trials-and-prospects), then change the subscription dropdown to "Free user" and choose the appropriate "problem type".

#### Trials and Prospects

Note: Due to the type of subscription/license they receive, trial users often identify themselves as Ultimate or Gold customers. This form field does *not* need to be updated as SLA is tied to the ZD org.

When applicable, tickets should have one of the following tag (not both):

- **Trial** = user or organization that has taken out a trial, typically using the free trial form.
- **Prospect** = organization trialing GitLab that is sales assisted, also often referred to as a POC (proof of concept).

A long term solution is still being worked on, but in the short term, prospects are supposed to email Support using the email address provided to them or use the appropriate form field, which will automatically add the `prospect` tag.

Otherwise, prospects are identified manually either through form content, ticket content, or the sales rep posts about it in one of the Support Slack channels. In the last case, please *add* an internal note on the ticket.

You can manually add the appropriate tag to the ticket. To do it, start typing tag name in `Tags` field, add the tag and Submit the ticket to add the needed tag.

Once tagged, the ticket will move to the appropriate queue, and for prospects, with appropriate SLA. However, [trials do not receive support](/support/) (wording included in our "free user" macro), and prospect SLA is for guidance only.

##### Other ways to identify trials and prospects when necessary

While SFDC syncs organizations nightly to ZD, it does not include trial licenses because no organization account is created in SFDC, only a lead. So organizations on a trial will not show in ZD, and this workflow _does not_ apply and we should _not_ manually create these organizations.

To check if a customer is on a trial: In SFDC (see search instructions below), the `Initial Source` will likely say `Trial` (check that initial date is within the standard trial period).

For prospects, there will likely be an organization account with `Account Type` `Prospect`. However, the presence of an org with type prospect does *not* mean they receive pre-sales support.

For self-managed, you can double check for a license in the [license app](http://license.gitlab.com).

For GitLab.com, in the Customers Portal, trials are marked with an expiration date under the **Trials** column in the `GitLab Groups` Tab next to a namespace. If needed, also check the [dotcom-internal project](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues) for manual plan changes.

### Finding the existing organization in Zendesk

You may attempt to find the organization within Zendesk using the search functionality. Do note that TLDs don't necessarily correspond to company names, so you may need to search in SFDC to find the appropriate organization.

Also, note that users may be using generic mail providers you might not be familiar with, so the TLD on their email address may not correspond with their company at all.

> When in doubt, check SFDC

![Selecting an organization in ZD](/images/handbook/support/zendesk_needs_org-finding-org.png)

### Finding the existing organization in SFDC (aka Salesforce)

See: [Looking up customer account details in Salesforce](/handbook/support/workflows/looking_up_customer_account_details.html#within-salesforce)

#### Organization exists in SFDC but service level does not match Zendesk
At times, the organization exists in Zendesk but has the wrong service level. You can force a resync for the specific organization.

1. Ensure that the **Account** in SFDC says the organization is a `Customer`.
1. `Edit` the `Support Level` to something aside from the current level.
1. `Save`.
1. `Edit` the Account again to change the `Support Level` back to the original.
1. `Save`.

If you refresh the organization page in Zendesk, the proper service level should immediately be reflected. However, for existing tickets where the user was already associated with the organization, the appropriate tag will need to be added manually to the tickets.

### Finding the existing organization in [customers.gitlab.com](https://customers.gitlab.com)

See: [Looking up customer account details in customer.gitlab.com](/handbook/support/workflows/looking_up_customer_account_details.html#within-customersgitlabcom)

### Adding the domain (Zendesk Admins only)
> **Important**: Be extra careful here. If a large company has multiple subscriptions it may not be appropriate
to add the domain. You'll need to add individual customers to the appropriate organization (see below)

Once you've determined the appropriate domain to add and identified the correct ZD Organization, you can click
the `Domains` field to add it.

![Filling in an organization domain in ZD](/images/handbook/support/zendesk_needs_org-adding-org.png)

### Adding a customer to an organization (All Zendesk Users)
If you don't have admin access on ZD, you can still make sure the proper SLA is applied by adding the user to the appropriate
organization.

1. Click on the customer's name in ZD
1. In the "Org" field type the organization name

![Adding a user to an existing organization](/images/handbook/support/zendesk_needs_org-add.png)

### Verifying that the ticket now has the proper SLA applied
Now that you've added the appropriate domain, head back to your original ticket and verify that it is associated with
the appropriate organization and SLA.

![Verifying SLA](/images/handbook/support/zendesk_needs_org-verifying-sla.png)

### Handling tickets without SLA

Sometimes you may notice that a ticket still has no SLA even when a user is associated with an organization.
There are two classes of reasons:

**By-default Zendesk behavior:**

- It can happen if a support person was the last one who replied to this ticket or
this ticket was put to **On-hold** status and reopened automatically. There is nothing to fix in this case.
- This ticket was updated by a person on the customer's side from an e-mail that is not
included in CC or is not the original requestor of the ticket. In this case, we need to add this e-mail to CC
before replying to such ticket. There is a [trigger](https://gitlab.zendesk.com/agent/admin/triggers/360019008340/revisions/3)
that is sending internal note to remind people to do it, see [this issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/1581) for more details.

**Org's status on SFDC side:**

Statuses of organization are propagated to Zendesk from SFDC during the regular sync. SLA will not be shown for organizations
with some special statuses. To check the status of an organization in Zendesk, click organization name and check `GitLab Plan` field.
The following values of `GitLab Plan` may cause the absence of SLA:

- `Community`. Usually it means that the organization is some education institution that obtained license according to
[GitLab for Education](https://about.gitlab.com/solutions/education/), and this license does not have support included.
You may verify it by checking the account on SFDC side according to [Finding the existing organization in SFDC][#finding-the-existing-organization-in-sfdc].

- `Expired`. In general It means that their self-managed license or GitLab.com subscription is expired
but often it can be caused by some incorrect information on SFDC side. Follow the section [Handling customers with expired licenses and updating info on SFDC side](#handling-customers-with-expired-licenses-and-updating-info) to fix it.
Note that we have [an ongoing issue](https://gitlab.com/gitlab-com/sales-team/field-operations/systems/-/issues/634)
that causes a lot of accounts to be incorrectly shown as `Expired` in SFDC. Follow the same workflow to deal with them. 

- `Hold`. Such status can be shown if customer delayes payment or there is some other issue with the sales process. You may contact Account Manager or
other person involved in the sales process to clarify the customer's status in such case.

- If `GitLab Plan` shows some valid value like `Silver`, `Starter`, etc but there is still no SLA then likely there is some issue with tags in the specific ticket. You may explore it on your own or ask support-ops or other support team members for help.

> **Important**: always leave internal comments when doing something on SFDC side or modifying some tags in Zendesk. It will help next engineer
to understand what was done in the ticket.

### Handling customers with expired licenses and updating info on SFDC side

If `GitLab Plan` in Zendesk shows `Expired`, it means that the organization is marked as
`Former Customer` in SFDC, and support level is set to `Expired` there. In such case it is better to check with Sales if the
status is valid or not:

- open Salesforce and find the customer in question
- click `Show feed` button at the upper part of the page

![Salesforce show feed](/images/handbook/support/sfdc_show_feed.png)

- send a message there asking to clarify the customer's status and mention `@Sales-Support`,
they should be able to help with such cases

**Note:** the same workflow applies if you notice that customer-related information is not up-to-date on SFDC side
and you are not able to update it using our generic `Support Admin` account.

Example of the message:
```
John Doe (Support Engineer): @Sales-Support, this organization has
Support Level set to Expired. Can you clarify if the support is really expired
and if we should decline support (they opened new ticket) for this customer or
this is some kind of error and Support Level should be updated? Customer also
provided screenshot of their license and it seems valid.
```

Prepending your message with your name and role (e.g. `John Doe (Support Engineer):`
helps as everyone in Support is using a shared account so it is not possible to
deduce who sent which message.

Once you follow the above procedure, mention that in the internal note of the
ticket (e.g. with Salesforce link) so that others can pick up from where you
left off.

### Fixing tags for tickets with `Expired` organization

If data in the [license app](http://license.gitlab.com) and the [customer portal](#finding-the-existing-organization-in-customersgitlabcom) shows
that the customer has a valid license, you should update the ticket in Zendesk side. This part does not overlap with the steps from [Handling customers with expired licenses and updating info on SFDC side](handling-customers-with-expired-licenses-and-updating-info-on-sfdc-side),
it should be done in combination: first [update information on the SFDC side](#handling-customers-with-expired-licenses-and-updating-info-on-sfdc-side) to fix future tickets,
then follow this section to fix existing tickets:

- Open the ticket in question and find `Tags` fields where all the tags are listed.
- Click `x` next to `former_customer` and `expired` tags.
- start typing the appropriate tag name. E.g. if it is a customer with Bronze subscription, type `bronze` in `Tags` field.
When you see the required tag in the dropdown list, select it.
- After making sure that tags are updated correctly, Submit the ticket to apply the changes.

![Updating tags in a ticket](/images/handbook/support/zendesk_updating_tags.gif)

**Important notes**:

- Due to [internal Zendesk bug](https://support.zendesk.com/hc/requests/5489856), you will see duplicated names of tags when searching for them.
It is UI only issue, and you can select any of the duplicates.
- When you assign a tag, there is a chance that the ticket will breach immediately. It is not necessary but, if possible,
send a public reply in order to prevent breach.

### Associated Trigers:
- [Add need-org tag](https://gitlab.zendesk.com/agent/admin/triggers/360001567348)
- [Remove need-org tag](https://gitlab.zendesk.com/agent/admin/triggers/360017109414)
