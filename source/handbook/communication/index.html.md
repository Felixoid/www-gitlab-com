---
layout: handbook-page-toc
title: "GitLab Communication"
extra_js:
  - libs/moment.min.js
  - libs/moment-timezone-with-data.min.js
  - team-call.js
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

We're an [all-remote](/company/culture/all-remote/#advantages-for-organizations) company that allows people to work from [almost anywhere in the world](/jobs/faq/#country-hiring-guidelines). We hire great people regardless of where they live, but with GitLab team members across [more than 60 countries](/company/team/), it's important for us to practice clear communication in ways that help us stay connected and work more efficiently.
To accomplish this, we use **asynchronous communication as a starting point** and stay as open and transparent as we can by communicating through public issues, [merge requests](#everything-starts-with-a-merge-request), and [Slack channels](/handbook/communication/chat).
We also place an emphasis on ensuring that conclusions of offline conversations are written down.
When we go **back and forth three times,** we jump on a [synchronous video call](/handbook/communication/#video-calls).

We communicate respectfully and professionally at all times.

## Effective & Responsible Communication Guidelines

1. **Assume [Positive Intent](/handbook/values/#collaboration).** Always begin with a position of positivity and grace.
1. **Kindness Matters.** You are looking at a screen, but you are really talking to a person. If you wouldn't say it to a person's face, don't send it to them in a text message.
1. **Express Your Thoughts.** We live in different locations and often have very different perspectives. We want to know your thoughts, opinions, and feelings on things.
1. **Own It.** If you say it or type it, own it. If it hurts the company or an individual, even unintentionally, we encourage you to look at things from other points of view and apologize easily.
1. **Be a Role Model of our [Values](/handbook/values/).**
1. **Feedback is Essential.** It is difficult to know what is appropriate in every one of our team members 60+ countries. We encourage team members to give feedback and receive feedback in a considerate way.
1. **Don't Underestimate a 1:1.** Asynchronous communication (e.g., via text) is helpful and necessary. In some cases (e.g., to clarify misunderstandings) it can be much more effective to jump on a Zoom video call.
1. **Always Adhere to our [Anti-Harassment Policy](/handbook/anti-harassment/) and [Code of Conduct](/handbook/people-group/code-of-conduct/).** Everyone should be comfortable in their work environment.

Embracing text communication and learning to use it effectively requires a mental shift. This can feel unusual or even uncomfortable for those who come from a colocated environment, where in-person meetings and vocalized communiques are the norm. Learn more about [mastering the use of the written word in an all-remote setting](https://about.gitlab.com/company/culture/all-remote/effective-communication/).


## Everyone is a Moderator

If you see something that concerns you in Slack, Issues, Merge Requests, Video, Emails or any other forum, we encourage you to respectfully say something directly to the individual in a 1:1 format. If you are not comfortable reaching out to the individual directly, please reach out to your direct manager or People Business Partner to discuss.

## External communication

There are 8 key practices to consider during any meeting. They are the following:

1. Video Calls - If this is your first time meeting a customer/prospect/partner/etc., turn on your camera when you login to Zoom. This will help to make the customer/prospect feel more comfortable as they are certain your undivided attention is geared towards them.
1. Agenda - Always have an agenda prepped and ready to go. Share this with your audience. Make sure that everything on the agenda is accurate and ask if there’s anything missing that needs to be addressed during this call or for the future. When there is no agenda, it translates to you not caring.
1. 70/30 Rule - Ask open ended questions that leave the audience talking 70% of the time, while you are talking 30% of the time. Please note that this varies based on the type of meeting that you are conducting. Be conscious of what questions needs to be asked and to capture those items.
1. Take Notes - Effective note-taking is a valuable skill that will help you retain and recall any important details. Be the person who remembers all the details of your audience's needs.
1. Adapt to Audience Tone - Before going into the business portion of your meeting, evaluate first the tone of the audience. Adapt your tone accordingly in order to appeal to various types of personalities.
1. Mid-call - Half-way through the meeting, check in with your audience. Ask them what their thoughts are on the progression of this meeting and if what you're presenting is on the right track. This helps both you and the audience by re-aligning expectations and making sure the meeting is going the right direction. 
1. Pre-Close Summary - 10 Minutes (1-hour meetings) or 5 minutes (30 minute meetings) prior to ending the call, ask the audience to build out an agenda for the next step or meeting. This helps to secure next steps and to ensure there are no balls dropped.
1. Post Meeting Action - Immediately write down notes and next steps and input into proper directory (Google Drive, Salesforce, etc.).
1. Two Block Rule - For in person meetings with external parties you should wait until you're more than two blocks from the meeting before discussing the results of the meeting. Nobody wants to hear themselves being discussed in the bathroom.

### Social Media

Please see our [social media guidelines](/handbook/marketing/social-media-guidelines/).

## Everything starts with a Merge Request

It's best practice to start a discussion where possible with a [Merge Request (MR)](https://docs.gitlab.com/ee/user/project/merge_requests/) instead of an issue. An MR is associated with a specific change that is proposed and transparent for everyone to review and openly discuss. The nature of MRs facilitate discussions around a proposed solution to a problem that is actionable. An MR is actionable, while an issue will take longer to take action on.

1. Always **open** an MR for things you are suggesting and/or proposing. Whether something is not working right or we are iterating on new internal process, it is worth opening a merge request with the minimal viable change instead of opening an issue encouraging open feedback on the problem without proposing any specific change directly. Remember, an MR also invites discussion, but it's specific to the proposed change which facilitates focused decision.
1. Never ask someone to create an issue when they can default to the merge request. 
1. Starting with a Merge Request is part of [Handbook First](/handbook/handbook-usage/#why-handbook-first) and helps ensure the handbook is up-to-date when a decision is made. It is also how we make it possible for [Everyone to Contribute](/company/strategy/#mission). This is true, not just for updating the handbook for updating all things. 
1. Merge Requests, by default are **non-confidential**. However, for [things that are not public by default](/handbook/communication/#not-public) please open a confidential issue with suggestions to specific changes that you are proposing. The ability to create [Confidential Merge Requests](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html#merge-requests-for-confidential-issues) is also available. When possible, consider not including sensitive information so the wider community can contribute.
1. Not every solution will solve the problem at hand. Keep discussions focused by **defining the problem first** and **explaining your rationale** behind the [Minimal Viable Change (MVC)](/handbook/values/#minimal-viable-change-mvc) proposed in the MR.
1. Be proactive and consistent with communication on discussions that have external stakeholders such as customers. It's important to keep communication flowing to keep everyone up to date. MRs can appear stale if there aren't recent discussions and no clear definition on when another update will be provided, based on feedback. This leaves those subscribed in the dark, causing unnecessary surprise if something ends up delayed and suddenly jumps to the next milestone. It is important that MRs are closed in a timely manner through approving or rejecting the open requests.
1. Have a **bias for action** and [don't aim for consensus](/handbook/leadership/#making-decisions). Every MR is a [proposal](/handbook/values/#make-a-proposal), if an MRs author isn't responsive take ownership of it and complete it. Some improvement is better than none.
1. **Cross link** issues or other MRs with related conversations. E.g. if there’s a Zendesk ticket that caused you to create a GitLab.com MR, make sure to document the MR link in the Zendesk ticket and vice versa. And when approving or rejecting the MR, include reason or response from Zendesk. Put the link at the top of each MR's description with a short mention of the relationship (Report, Dependency, etc.) and use one as the central one and ideally close the alternate if duplicate.
 1. When providing links to specific lines of code relevant to the MR, **always use a permalink** (a link to a specific commit for the file). This ensures that the reference is still valid if the file changes. For more information, see [Link to specific lines of code](https://docs.gitlab.com/ee/development/documentation/styleguide.html#link-to-specific-lines-of-code).
1. If submitting a change for a feature, **update the description with the final conclusions** (Why an MR was rejected or why it was approved). This makes it much easier to see the current state of an issue for everyone involved in the implementation and prevents confusion and discussion later on.
1. Submit the **smallest** item of work that makes sense. When proposing a change, submit the smallest reasonable commit, put suggestions for other enhancements in separate issues/MRs and link them. If you're new to GitLab and are writing documentation or instructions, submit your first merge request for at most 20 lines.
1. Do not leave MRs open for a long time. MRs should be **actionable** -- stakeholders should have a clear understanding of what changed and what they are ultimately approving or rejecting.
1. Make a conscious effort to **prioritize** your work. The priority of items depends on multiple factors: Is someone waiting for the answer? What is the impact if you delay it? How many people does it affect, etc.? This is detailed in [Engineering Work flow](/handbook/engineering/workflow).
1. When submitting a MVC, **ask for feedback** from your peers. For example, if you're a designer and you propose a design, ping a fellow designer to review your work. If they suggest changes, you get the opportunity to improve your design and propose an alternative MR. This promotes collaboration and advances everyone's skills.
1. Respond to comments within a **threaded discussion**. If there isn't a discussion thread yet, you can use the [Reply to comment](https://docs.gitlab.com/ee/user/discussions/#start-a-discussion-by-replying-to-a-standard-comment) button from the comments to create one. This will prevent comments from containing many interweaves discussions with responses that are hard to follow.
1. If your comment or answer contains separate topics, write separate comments for each, so others can address topics independently using the [Reply to comment](https://docs.gitlab.com/ee/user/discussions/#start-a-discussion-by-replying-to-a-standard-comment) button.
1. For GitLab the product merge request guidelines are in the [Contribution
guide](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#merge-request-guidelines) and code review guidelines for reviewers and maintainers are described in our [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html).
1. Even when something is not done, share it internally so people can comment early and prevent rework.
1. Create a **[Work In Progress (WIP)](/blog/2016/01/08/feature-highlight-wip/)** merge request to prevent an accidental early merge. Only use WIP when merging it would **make things worse**, which should rarely be the case when contributing to the handbook. Most merge requests that are in progress don't make things worse, in this case don't use WIP, if someone merges it earlier than you expected just create a new merge request for additional items. Never ask someone to do a final review or merge something that still have WIP status, at that point you should be convinced it is good enough to go out.
1. If any follow up actions are required on the issue after the merge request is merged (like reporting back to any customers or writing documentation), avoid auto closing the issue.
1. If a project requires multiple approvals to accept your MR, feel free to assign multiple reviewers concurrently. This way the earliest available reviewer can start right away rather than being blocked by the preceding reviewer.

## Issues

Issues are useful when there isn't a specific code change that is being proposed or needed. For example, you may want to start an issue for tracking progress or for project management purposes that do not pertain to code commits. This can be particularly useful when tracking team tasks and creating issue boards. However it is still important to maintain focus when opening issues by defining a single specific topic of discussion as well as defining the desired outcome that would result in the resolution of the issue. The point is to not keep issues open-ended and to prevent issues from going stale due to lack of resolution. For example, a team member may open an issue to track the progress of a blog post with associated to-do items that need to be completed by a certain date (e.g. first draft, peer review, publish). Once the specific items are completed, the issue can successfully be closed. Below are a few things to remember when creating issues:

 1. When **closing** an issue leave a comment explaining why you are closing the issue and what the MVC outcome was of the discussion (if it was implemented or not).
 1. We keep our **promises** and do not make external promises without internal agreement.
 1. Be proactive and consistent with communication on discussions that have external stakeholders such as customers. It's important to keep communication flowing to keep everyone up to date. Issues can appear stale if there aren't recent discussions and no clear definition on when another update will be provided, based on feedback. This leaves those subscribed in the dark, causing unnecessary surprise if something ends up delayed and suddenly jumps to the next milestone. It is important that issues are closed in a timely manner. One way of doing this is having the current assignee set a due date for when they will provide another update. This can be days or weeks ahead depending on the situation, prioritization, and available capacity that we may have.

 ***Pro Tip:*** When creating a Merge Request you can add `closes: #[insert issue number here]` and when the Merge Request is merged, the issue will automatically close. You can see an example of this [here](https://gitlab.com/gitlab-com/people-group/peopleops-eng/employment-automation/-/merge_requests/60).

 1. If a user suggests an enhancement, try and find an existing issue that addresses their concern, or create a new one. Ask if they'd like to elaborate on their idea in an issue to help define the first MVC via a subsequent MR.
 1. **Cross link** issues or MRs with related conversations. Another example is to add "Report: " lines to the issue description with links to relevant issues and feature requests. When done, add a comment to relevant issues (and close them if you are responsible for reporting back, or re-assign if you are not). This prevents internal confusion and us failing to report back to the reporters.
 1. When providing links to specific lines of code relevant to the issue, **always use a permalink** (a link to a specific commit for the file). This ensures that the reference is still valid if the file changes. For more information, see [Link to specific lines of code](https://docs.gitlab.com/ee/development/documentation/styleguide.html#link-to-specific-lines-of-code).
 1. Prioritize your work on issues in the current [milestone](https://gitlab.com/groups/gitlab-org/-/milestones).
 1. Use the public issue trackers on GitLab.com for everything since [we work out in the open](/blog/2015/08/03/almost-everything-we-do-is-now-open/). Issue trackers that can be found on the relevant page in the handbook and in the projects under [the gitlab-com group](https://gitlab.com/gitlab-com/).
 1. Assign an issue to yourself as soon as you start to work on it, but not before that time. If you complete part of an issue and need someone else to take the next step, **re-assign** the issue to that person.
 1. Ensure the issue **title** states what the desired outcome should be.  For instance, for bugs make sure the issue states the desired result, not the current behavior.
 1. **Regularly update** the issue description with the latest information and its current status, especially when important decisions were made during the discussion. The issue description should be the **single source of truth**.
 1. If you want someone to review an issue, do not assign them to it. Instead, @-mention them in an issue comment. Being assigned to an issue is a signal that the assignee should or intends to work on it. So you should not assign someone to an issue and mis-represent this with a false signal.
 1. Do not close an issue until it is [**done**](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#definition-of-done). It's okay to explicitly ask if everyone is on board and in agreement on how to move forward, whether to iterate, close the open issue, or create a subsequent MR to implement a MVC.
 1. Once a feature is [**done**](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#definition-of-done), update the description to add a link to the corresponding documentation. When using a Search Engine, issues often appear before documentation pages, which makes it harder to find the relevant information about the feature.
 1. Write issues so that they exclude private information. This way, the issue can be public. Only use confidential issues, if the issue must contain [non-public information](/handbook/communication/#not-public). **Note:** Confidential issues are [accessible to all members of the project with Reporter access and above](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html#permissions-and-access-to-confidential-issues). You may consider using a Google Doc for items that require a stricter level of confidentiality.
 1. If the content within a public issue transitions to become what is deemed confidential [non-public information](/handbook/communication/#not-public), the issue may be made confidential.
 1. If the content of a public issue draws comments that are deemed in violation of our [code of conduct](/community/contribute/code-of-conduct/) the issue may be locked and may [undergo moderation](/handbook/marketing/community-relations/community-advocacy/workflows/code-of-conduct-enforcement/#overview).

## Internal Communication

1. All written communication happens in English, even when sent one on one, because sometimes you need to forward an email or chat.
1. Use **asynchronous communication** when possible: merge requests (preferred) or issues. Announcements happen on the appropriate slack channels and [people should be able to do their work without getting interrupted by chat](https://m.signalvnoise.com/is-group-chat-making-you-sweat-744659addf7d#.21t7089jk).
1. Discussion in issues or Merge Requests is preferred over everything else. If you need a response urgently, you can Slack someone with a link to your comment on an issue or merge request, asking them to respond there, however be aware that they still may not see it straight away. See [Slack](/handbook/communication#slack) for more.
1. If you choose to email instead of chat it is OK to send an _internal_ email that contains only a short message, similar as you would use in chat.
1. You are not expected to be available all the time. There is no expectation to respond to messages outside of your planned working hours.
1. Sometimes synchronous communication is the better option, but do not default to it. For example, a video call can clear things up quickly when you are blocked. See the [guidelines on video chats](#video-calls) for more detail.
1. It is very OK to ask as many questions as you have. Please ask them so many people can answer them and many people see the answer, so use issues or public chat channels (like #questions) instead of direct messages or one-on-one emails. If someone sends you a handbook link they are proud that we have the answer documented, they don't mean that you should have found that yourself or that this is the complete answer, feel free to ask for clarification. If the answer to a question isn't documented yet please immediately make a merge request to add it to the handbook in a place you have looked for it. It is great for the person who answered the question to see you help to ensure they have to answer it only once. A merge request is the best way to say thanks for help.
1. If you mention something (a merge request, issue, commit, webpage, comment, etc.) please include a link to it.
1. All company data should be **shareable** by default. Don't use a local text file but rather leave comments on an issue.
1. When someone asks something, give back a deadline or that you did it. Answers like: 'will do', 'OK', 'it is on my todo list' are not helpful. If it is small it's better to spend 2 minutes and do the tasks so the other person can mentally forget about it. If it is large you need to figure out when you'll do it, by returning that information the other person might decide to solve it in another way if it takes too long.
1. It is OK to bring an issue to someone's attention with a CC ("cc @user"), but CCs alone are not enough if specific action is needed from someone. The mentioned user may read the issue and take no further action. If you need something, please explicitly communicate your need along with @ mentioning who you need it from.
1. Avoid creating private groups for internal discussions:
    1. It's disturbing (all users in the group get notified for each message).
    1. It's not searchable.
    1. It's not shareable: there is no way to add people in the group (and this often leads to multiple groups creation).
    1. They don't have a subject, so everyone has to remember the topic of each private group based on the participants, or open the group again to read the content.
    1. History is lost when leaving the group.
1. It is perfectly fine to create a channel, even for a single customer meeting. These channels should be named "a_&lt;customer-name&gt;-internal" to indicate their "internal" nature (not shared with customers).
1. Use [low-context communications](https://en.wikipedia.org/wiki/High-context_and_low-context_cultures) by being explicit in your communications. We are a remote-only company, located all over the world. Provide as much context as possible to avoid confusion. Relatedly, we use [ubiquitous language](#ubiquitous-language) for communication efficiency.
1. When discussing concepts, be careful not to lean too much into hypotheticals. There is a tipping point in which it decreases value and no longer becomes constructive at helping everyone come into a unified decision.


### How to make a Companywide Announcement

1. Consider the subject and the audience.
    Questions you might want to ask yourself; is this relevant to all team members globally?
    is this something important, urgent and high priority? is there a better place for this communication, such as a more informal slack channel?
1. Keep it simple, brief and summarize what is important. Cover the 5 W's. What, Why, Who, When, Where (you can also add How, if required as a call to action). The majority of information should still be in the Handbook which you include links to.
1. Common companywide announcements include (but are not limited to): organization changes, policy iterations, requests to participate in a company survey,     unveiling the next GitLab Contribute location, codebase migrations, process improvement and security/safety announcements.
1. Remember Handbook First.
    When you announce anything, include links to the respective Handbook pages for more information. Consider adding link to an Issue if the information is not public yet.
1. Optional AMA.
    If desired and appropriate, offer a companywide Zoom call to host an AMA (Ask Me Anything). Oftentimes, questions can be managed within the Discussion tab of a GitLab Issue or Merge Request. For broad announcements, such as registration opening for GitLab Contribute, an AMA may be better suited for a large volume of inquiries. To schedule a companywide call, please make a request in the #peopleops Slack channel, and include a Google Doc in the invite for questions.

### Posting in #company-fyi

Our companywide announcements channel is **#company-fyi**. It is an **announcement only** channel, meaning that communications need to be approved via Internal Comms before they can be posted (other than by those who have been given permission, such as E-group members).
Please reach out to Internal Communications (@rappleby) to request a #company-fyi announcement. We ask that you make a request **at least one week in advance** so we can better manage our communications schedule, and that when communicating any significant company changes you should involve Internal Comms from the beginning as part of the planning process.

1. Examples of what **should not** go in **#company-fyi** (as per new group guidelines):
    1. Competition prize winner announcements.
    1. Org change or new team member announcements (unless they are E-group).
    1. Promotion of a optional non company wide internal event.

**The above should now all go in the new #whats-happening-at-GitLab channel** (formerly the #company-announcement channel)

### Top misused terms

Below are terms people frequently use when they should use another term.
The format is: misused term => correct term, reason why.

1. GitLabber => GitLab team member, since the wider community shouldn't be excluded from being a GitLabber, we [use team member instead](/company/team/structure/#team-and-team-members).
1. IPO => becoming a public company, because we might do a direct listing instead of an offering.
1. EE => subscribers or paid users, because [EE is a distribution](/handbook/marketing/product-marketing/tiers/#distributions-ce-and-ee) and not all people who use EE pay us.
1. CE => Core users, since [CE is a distribution](/handbook/marketing/product-marketing/tiers/#distributions-ce-and-ee) and many Core users use EE.
1. Hi guys => Hi people, because we want to use [inclusive language](/handbook/values/#inclusive-language--pronouns)
1. Aggressive => ambitious, since we want to [attract a diverse set of people](https://www.huffpost.com/entry/textio-unitive-bias-software_n_7493624)
1. Employees => team members, since we have team members who are [contractors](/handbook/total-rewards/compensation/compensation-calculator/#contract-factor)
1. Resources => people, since we are more than just our output.
1. Community => wider community to refer to people outside of the company, since [people at GitLab are part of the community too](/handbook/communication/#writing-style-guidelines).
1. Radical transparency => Intentional transparency, since radical tends to be absolute and infers a lack of discretion. We are thoughtful and intentional about many things — [informal communication](/company/culture/all-remote/informal-communication/), [handbook-first documentation](/company/culture/all-remote/handbook-first-documentation/), [onboarding](/company/culture/all-remote/building-culture/#intentional-onboarding), etc. — and have [exceptions to transparency](/handbook/communication/#not-public).
1. Sprint => iteration, since we are in it [for the long-term](/handbook/values/#under-construction) and [sprint implies fast, not quick](https://hackernoon.com/iterations-not-sprints-efab8032174c).
1. Grooming => refinement, since the term has [negative connotations](https://pm.stackexchange.com/questions/24133/difference-between-grooming-and-refinement) and the word refinement is used by the [Scrum Guide](https://www.scrumguides.org/revisions.html).
1. Obviously => skip this word, since using things like "obvious/as we all know/clearly" discourages people who don't understand from asking for clarification.
1. They => Do not refer to other teams at GitLab externally as "they" (e.g., "Well, they haven't fixed that yet!").  There is no "they" or "headquarters" - there is only us as team members, and it's one team.  You could instead say something like "The Product team is looking at that issue...". When used in the context of a pronoun, they is as valid as he, she, or any other pronoun someone decides to use.
1. Deprecate => delete or remove, since we usually mean we're removing a document or feature whereas deprecation is actually leaving the thing intact while discouraging its use. Unlike other terms on this list, there *are* times where "deprecate" is the right word, for example in formal [product process](/handbook/product/#deprecating-and-removing-features).
1. Diverse candidate => a single person cannot be "diverse". Using "diverse" as a noun is an “othering” term. Instead, someone may be from an underrepresented group.
1. Non-technical used to describe a person => everyone has a technical knowledge that they used to perform in their role. Usually people mean "non-engineering."
1. Search/Elasticsearch => There are many kinds of search in the GitLab application; use complete feature names to describe areas. Use [Global Search](https://about.gitlab.com/direction/enablement/global-search/) with the features Basic Global Search and [Advanced Global Search](https://docs.gitlab.com/ee/user/search/advanced_global_search.html). Use [Log Search](https://about.gitlab.com/direction/monitor/apm/logging/) for the monitor stage logging feature. Use Issue Search and Merge Request search for filtering and searching in those content areas. Use [Elastic log Stack](https://docs.gitlab.com/ee/user/clusters/applications.html#elastic-stack) when referring to Elasticsearch and Filebeat.
1. Telemetry => Avoid this word if you can be more specific. You can be more clear by using one of the following terms:
   1. Seat link is number of seats on a license
   1. [Version check](https://about.gitlab.com/handbook/sales/process/version-check/) is what version of GitLab a subscription is using
   1. [Usage ping](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html#usage-ping-core-only) is high level data of how GitLab is being used in a license
   1. [Snowplow](https://about.gitlab.com/handbook/business-ops/data-team/snowplow/) is a dynamic site for web event analytics
   1. [Google Analytics](https://about.gitlab.com/handbook/tools-and-tips/#google-analytics) is a static site for receiving data from about.gitlab.com and docs.gitlab.com
   1. Database events is using the database records of Gitlab.com to look at how people are using the application.
1. Rule of thumb => Guideline or Heuristic, since rule of thumb is of [dubious historical origins](https://www.phrases.org.uk/meanings/rule-of-thumb.html) and can be considered a violent or non-inclusive term.
1. Deadline => Due date, since deadline [may have violent origins](https://en.wikipedia.org/wiki/Time_limit#Origin_of_the_term) and we use due date in our user interface for issues.
1. Grandfathered => Legacy, since grandfathered has [discriminatory origins](https://en.wikipedia.org/wiki/Grandfather_clause).
1. Whitelist/Blacklist => Allowlist/Denylist, since Whitelist/Blacklist have connotations about value and map to racial terms.
1. Game Day => Simulation Day, since Game Day may seem more militaristic.
1. GitLab monitoring dashboard => GitLab metrics dashboard, monitoring dashboard is too generic and is often confused with the GitLab's self-monitoring features.

### Asking "is this known"

1. If something is behaving strangely on [https://gitlab.com](https://gitlab.com), it might be a bug.
   It could also mean that something was changed intentionally.
1. Please search if the issue has already [been reported][issue-list].
    1. If it has not been reported, please [file an issue](#issues).
1. If you are unsure whether the behavior you experience is a bug, you may ask in the Slack channel [#is-this-known][is-this-known-slack].
    1. Make sure that no-one has experienced this issue before, by checking the channel for previous messages
    1. If you know which stage of the DevOps lifecycle is affected, it is also okay to ask in #s_{stage}, for example [#s_manage][s_manage-slack].
    1. Describe the behavior you are experiencing, this makes it searchable and easier to understand.
       Different people might look for different things in the same screenshots.
    1. Asking in a single channel helps discoverability, duplicated efforts and reduces noise in other channels.
       Please refrain from asking in general purpose channels like [#frontend][fe-slack], [#backend][be-slack], [#development][dev-slack] or [#questions][q-slack].

[issue-list]: https://gitlab.com/groups/gitlab-org/-/issues
[is-this-known-slack]: https://gitlab.slack.com/messages/CETG54GQ0/
[s_manage-slack]: https://gitlab.slack.com/messages/CBFCUM0RX
[fe-slack]: https://gitlab.slack.com/messages/C0GQHHPGW/
[be-slack]: https://gitlab.slack.com/messages/C8HG8D9MY/
[dev-slack]: https://gitlab.slack.com/messages/C02PF508L/
[q-slack]: https://gitlab.slack.com/messages/C0AR2KW4B/

### Multimodal communication

Employ multimodal communication to broadcast important decisions. To reach our distributed organization, announce important decisions in the company announcements Slack channel, email the appropriate team email lists, Slack the appropriate channels, and target 1:1s or other important meetings on the same day, with the same information.

When doing this, create and link to a [single source of truth](/company/culture/all-remote/handbook-first-documentation/): ideally the [handbook](/handbook/handbook-usage/#why-handbook-first), otherwise an epic, issue, or Google Doc. The email or Slack message should not be the source of truth.

### Not Public
{:.no_toc}

We make things public by default because [transparency is one of our values](/handbook/values/#transparency).

However it is [most important to focus on results](/handbook/values/#hierarchy).
So most things are **public** unless there is a reason not to. The following items are not public by default:

1. Security vulnerabilities are not public since it would allow attackers to compromise GitLab installations. We do make them public after we remediated a vulnerability. Issues that discuss how to improve upon the security posture of an implementation that is working as intended can be made public, and are often labeled as feature proposals. Security implementations that detect malicious activities cannot be made public because doing so would undermine our operations.
1. Financial information, including revenue and costs for the company, is confidential because we plan to be a public company and, as such, need to limit both the timing and content of financial information as investors will use and rely on it as they trade in GitLab stock. As the guideline, if it is a first step to constructing a profit, we need to keep it confidential. Examples include:
   1. the specific [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) of an opportunity
   1. total monthly cash inflow/outflow for GitLab.com
   1. Spend of more than 10%
   1. a department's cost
   1. the Sales pipeline (but the Marketing pipeline can be public)
   1. Net and Gross Retention KPIs (only the actual numbers can't be public. Everything else- the goal, their calculation, etc- can be.)
1. Deals with external parties like contracts and [approving and paying invoices](/handbook/finance/procure-to-pay/).
1. Content that would violate confidentiality for a GitLab team member, customer, or user.
1. Legal discussions are not public due to the [purpose of Attorney-Client Privilege](/handbook/legal/#what-is-the-purpose-of-these-privileges).
1. Some information is kept confidential by the People Group to protect the privacy, safety, and security of team members and applicants, including applications, background check reports, reference checks, compensation, termination (voluntary and involuntary) details, demographic information, and home address. Whistleblower identity is confidential. People Group policies and processes are public, internally and externally. As examples, [Job families](/handbook/hiring/job-families/) and our [Compensation Calculator](/handbook/total-rewards/compensation/compensation-calculator/calculator/) are public.
1. Performance improvement plans, disciplinary actions, as well as individual feedback, are confidential as they contain private negative feedback and [negative feedback is 1-1](/handbook/values/#collaboration) between team members and managers.
1. Partnerships with other companies are not public since the partners are frequently not comfortable with that.
1. Acquisition offers for us are not public since informing people of an acquisition that might not happen can be very disruptive.
1. Acquisition offers we give are not public since the organization being acquired frequently prefers to have them stay private.
1. Customer information is not public since customers are not comfortable with that, and it would make it easier for competitors to approach our customers.  If an issue needs to contain _any_ specific information about a customer, including but not limited to company name, employee names, number of users, the issue should be made confidential. Try to avoid putting customer information in an issue by describing them instead of naming them and linking to their SalesForce account. When we discuss a customer by name that is not public unless we're sure the customer is OK with that. When we discuss a competitor (for example in a sales call) this can be public as our competitive advantages are public.
1. Competitive sales and marketing campaign planning is confidential since we want the minimize the time the competition has to respond to it.
1. [Sales battlecards](https://blog.pandadoc.com/the-basics-of-battle-cards/) are not public since we want to minimize the time the competition has to respond to it. Our [feature comparisons](/devops-tools/) are public.
1. Plans for reorganizations are not public and on a need-to-know basis within the organization. Reorganizations cause disruption and the plans tend to change a lot before being finalized, so being public about them prolongs the disruption. We will keep relevant team members informed whenever possible.
1. Discussions that involve decisions related to country of residence are not public as countries are a core part of people's identity and any communication should have complete context.  The output of such decisions, such as [country hiring guidelines](/jobs/faq/#country-hiring-guidelines) will be public.
1. If public information compromises the physical safety of one or more team members, it will be made not public because creating a safe, inclusive environment for team members is important to how we work. Information that might compromise the physical safety of a team member includes doxxing or threats made against a team member.
1. Information related to a press embargo, or related to an upcoming publication where the response will be managed by our external communications team

Information that defaults to not public and to limited access:

1. Deals with external parties like contracts and [approving and paying invoices](/handbook/finance/procure-to-pay/).
1. Content that would violate confidentiality for a GitLab team-member, customer, or user.
1. Acquisition offers for us are not public since informing people of an acquisition that might not happen can be very disruptive.
1. Acquisition offers we give are not public since the organization being acquired frequently prefers to have them stay private.
1. Customer lists and other customer information are not public since many customers are not comfortable with that and it would make it easier for competitors to approach our customers. If an issue needs to contain _any_ specific information about a customer, including but not limited to company name, employee names, and/or number of users, the issue should be made confidential. Avoid putting customer information in an issue by describing them instead of naming them and by linking to their Salesforce account.
1. Plans for reorganizations. Reorganizations cause disruption and the plans tend to change a lot before being finalized, so being public about them prolongs the disruption. We will keep relevant team members informed whenever possible.
1. Planned pricing changes. Much like reorganizations, plans around pricing changes are subject to shift manage time before being finalized. Thus, pricing changes are limited access while in development. Team members will be consulted before any pricing changes are rolled out.
1. Legal discussions are restricted to the [purpose of Attorney-Client Privilege](/handbook/legal/#what-is-the-purpose-of-these-privileges).
1. Some information is kept confidential by the People Group to protect the privacy, safety, and security of team members and applicants, including: job applications, background check reports, reference checks, compensation, terminations details, demographic information (age and date of birth, family or marital status, national identification such as passport details or tax ID, required accommodations), home address. Whistleblower identity is likewise confidential. Performance improvement plans, disciplinary actions, as well as individual feedback are restricted as they may contain negative feedback and [negative feedback is 1-1](/handbook/values/#collaboration) between you and your manager. However, People Group policies and processes are public (for example, [Job families](/handbook/hiring/job-families/) and our [Compensation Calculator](/handbook/total-rewards/compensation/compensation-calculator/calculator/)), along with information that team members choose to share on the [Team](/company/team/) page.
1. GitLab's Risk Register, which is maintained by the [Security Compliance Team](/handbook/engineering/security/security-assurance/security-compliance/compliance.html). Access to GitLab’s Risk Register is limited to the Security Compliance Team, VP of Security, Executive Leadership and individually identified employees on a need to know basis. This document is not publicly or internally available as there may be risks on the risk register that explicitly call out the mechanisms available for external actors or internal GitLabbers to access content that would violate confidentiality for a GitLab team-member, customer, or user, and may include Personally Identifiable Information (PII). The Security Compliance Team operates a model of eventual transparency, where fully treated risks will be shared internally for visibility.

## Effective Communication Competency

[Competencies](/handbook/competencies/) are the Single Source of Truth (SSoT) framework for things we need team members to learn.

In an all-remote organization effective communication is key to exchanging knowledge, ideas, and information. Effective communication at GitLab is: Using [asynchronous](/company/culture/all-remote/asynchronous/) communication as the starting point and staying as open and transparent as we can by [communicating via text](/handbook/communication/#writing-style-guidelines) through public issues, merge requests, and Slack channels. Placing an emphasis on ensuring that conclusions of offline conversations are written down ensuring a [Single Source of Truth](/handbook/documentation/#documentation-is-the-single-source-of-truth-ssot) and [producing Video](/handbook/marketing/marketing-operations/youtube/) when necessary. 

If you would like to improve your skills or expand your knowledge on topics relating to Communication at GitLab, check out our resources:

* [Communicating effectively and responsibly through text](https://about.gitlab.com/company/culture/all-remote/effective-communication/)

**Skills and behavior of applying effective communication as a Team Member**: 
*  Effectively practices communication via text.
*  Uses asynchronous communication when possible: merge requests (preferred) or issues.
*  Directs all communication to the appropiate channels (Slack, GitLab, email).
*  Recognises when synchronous communication is the more appropriate option.
*  Directs all decisions and discussions to the Handbook as a single source of truth.
*  Records videos to communicate information when that is the most efficient and effective way to consume the content.
*  Employs multimodal communication to broadcast important decisions.
*  Practices low context communication and provides as much background as possible when communicating via text to avoid confusion.

**Skills and behavior of applying effective communication as a People Manager**: 
*  Implements working asynchronously across teams, departments or across the company. Drives communication where possible to asynchronous channels. 
*  Holds team members accountable for effectively communicating via text. 
*  Fosters an environment across teams, departments or divisions where asynchronous communication is the starting point. 
*  Guides team members on when producing video is appropriate. Implements interactive communication tools across their team, department or the company depending on level. 
*  Drives and funnels conversations to the right channels across teams, divisions and the company. 


## Numbering is for reference, not as a signal

When taking notes in an agenda, in the handbook, or on our [OKRs](/company/okrs/), keep items numbered so we can refer to Item 3 or 4a.
The number is not a signal of the importance or rank of the subject unless explicitly stated to be such.
It is just for ease of reference.

## Acknowledgement Receipts (ACK)

In order to effectively communicate an important change to hundreds of distributed employees, we occasionally use an ACK process:

1. To prevent overuse, this should only be used by a member of the exec team (but anyone may ask an exec to sponsor one).
1. As a guideline, we'd expect no more than one per quarter to be sent out (too many ACKs lose power).
1. Clone the form from this [template](https://docs.google.com/forms/d/1BPllKiwhOpvgdRbV_SYTCWevJLWMCAQJyegbVvJ1L6Q/edit) and fill it out.
  1. Link to MRs and Handbook pages instead of duplicating your content in the form. [Why handbook first?](/handbook/handbook-usage/#why-handbook-first)
1. Ask PeopleOps to pull a spreadsheet of email addresses from BambooHR with the column headers First Name, Last Name, Job Title, Department, Manager, and Work Email. Double check it and turn the emails into a comma-delimited string with an excel formula like this: `=TEXTJOIN(", ", true, Sheet1!E2:E432)`
1. Send the form and expect to get 50% of the responses in the first 24 hours. To get the rest:
  1. Post in common Slack channels.
  1. Add to staff meeting agendas.
  1. Suggest to team managers to post to their team Slack channels, ask for explicit `:ack:` and pin to the channel until everyone responds.
  1. Lastly, reach out 1-on-1 to stragglers while being respectful of vacation time.

In informal acknowledgement scenarios, such as on Slack or on issue comments, it is common practice to use the following: 

1. Eyes 👀  => I’ll check this out or seen and will do
1. Thumbs up 👍 => good idea 
1. White checkmark ✅ =>  task is complete or done
1. Heart ❤ ️= expression of gratitude or appreciation
1. cc @mentions => if someone needs to see a message 

## Presentations

1. All presentations are made in Google Slides using [our templates](/handbook/tools-and-tips/#updating-your-slide-deck-theme).
1. Please allow anyone at GitLab to edit the presentation (preferred) or at least comment on the presentation.
1. If the content can be public use File > Publish to the web > Publish to get a URL and paste that in the speaker notes of the first slide (commonly the title slide).
1. The title of every slide should be the message you want the audience to take away, not the subject matter. So use 'Our revenue more than doubled' instead of 'Revenue growth'.
1. At the end of the presentation when you go to Q&A stop presenting in Zoom. This way the other people can see the person who is speaking much better.
1. All presentations at GitLab should be based on screenshots of the handbook, issues, merge requests, review apps, and data from GitLab Insights and SiSense charts. In most cases it shouldn't be needed to make content uniquely for the presentation. If you need something that doesn't exist yet add it to the place it belongs and then copy it into the presentation. This way we can have a [Single Source of Truth](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/#creating-a-home-for-a-single-source-of-truth-ssot) for everything we do. By using screenshots you indicate to people you did the right thing and they can find the canonical source in the appropriate place. Having to find information by digging through old presentations doesn't scale. Consider linking the screenshot to the original source.

## Say Thanks

1. Thank people that did a great job in our `#thanks` Slack channel.
1. If someone is a team member just `@`-mention them, if multiple people were working on something try `@`-mentioning each person.
1. If possible please include a link with your thanks that points to the subject matter that you are giving thanks for, for example a link to a merge request.
1. Please do not mention working outside of working hours, we want to minimize the pressure to do so.
1. Please do not celebrate GitLab contribution graphs that include working for uninterrupted weeklong cycles, as this does not foster healthy [work/life harmony](/company/culture/all-remote/people/#worklife-harmony) for all team members. While GitLab team members are free to time-shift and work weekends in place of weekdays, we discourage celebrating the absence of time away from work.
1. Almost [everyone in the company is active in this channel](https://twitter.com/sytses/status/1100071442576633856) so please don't be shy.
1. Don't thank the CEO or other executives for something that the company paid for, thank GitLab instead.
1. To thank someone who is not a team member please mention a Community Advocate, the name of the person, a quirky gift
and link to their work. For example, "@manager, @communityadvocate: Joe deserves a lawnmower for _link_". The Community Advocate will approach the person in question for their address saying we want to send some swag. We'll ship it in gift wrap with "Thanks for your great work on _link_, love
from @gitlab".

## Values emoji
Add Values emoji reactions to thank you messages in the [`#thanks` slack channel](/handbook/communication/#say-thanks)
or feel free to use them in GitLab.com, other slack channels and
social media, when you see alignment to our values: [GitLab's values](/handbook/values/).

* `:handshake:` = Collaboration
* `:chart_with_upwards_trend:` = Results
* `:stopwatch:` = Efficiency
* `:globe_with_meridians:` = Diversity
* `:footprints:` = Iteration
* `:eye:` = Transparency

![Values emoji](/images/handbook/values-emoji.png)

As a second iteration, we will begin tracking the number of emoji reactions for each value through the Reacji API and update this page with our findings!

## Communicate directly

When working on a problem or issue, communicate directly with the people you need support from rather than working through reporting lines. Direct communication with the people you need to collaborate with is more efficient than working through your manager, their manager, or another intermediary.
Escalate to management if you are not getting the support you need. Remember that everyone is a [manager of one](/handbook/values/#managers-of-one) and they might have to complete their own assignments and inform the reporting lines.

## Not sure where to go?

If there is something that you want to discuss, but you do not feel that it is
a reasonable option to discuss with either your manager or CEO, then you can reach
out to any of the other [C-level GitLab team members](/company/team/org-chart/) or our board member Bruce Armstrong.

## Take a Break Call

1. Take a Break Calls, happen three times a day on Tuesday and Thursday to accommodate all timezones. These calls have 5 predetermined topics for team members to choose from. You can find the topics listed in the invite, as well as the link for that topic. These calls are not mandatory, but are a great way to get to know your fellow team members.
   *  APAC/ EMEA: 9:00AM GMT+2
   *  EMEA/ AMER: 9:00AM GMT-5
   *  AMER/ APAC: 9:00AM GMT+11
1. Every Friday we have an AMA with an executive to talk about anything the people at our company are thinking about.
1. Everyone at GitLab is invited to the Take A Break Call.
1. We use [Zoom](https://gitlab.zoom.us), (please be sure to mute your microphone when not speaking). If using the Zoom web app, you can go to settings and check always mute microphone when joining a meeting.
1. The calls are no longer recorded, but you can view  previous calls in the Google Drive folder called "GitLab Videos". There is a subfolder called "GitLab Company Call", which is accessible to all users with a GitLab.com e-mail account.
1. We start on time and do not wait for people.
1. These calls are unstructured to allow for natural interaction, but everyone should get a chance to share at least once.
1. It helps to pause slightly between talking about different topics to allow for discussion.
1. If you are unsure of who should start or speak next, follow the order listed in Zoom.

Some Topics for Calls:

1. Hobbies and interests.
1. What you've been up to recently.
1. Things you're looking forward to.
1. Sports and wellness.
1. Cooking, entertaining, and creative projects.
1. Travel, kids, family, and pets.
1. Music, books, TV & movies, and video/board games.

## Release Retrospectives and Kickoffs
{: #kickoffs}

After GitLab releases a new version on the 22nd of each month, we have a
30-minute call a few days later reflecting on what could have been
better:

1. What went well this month?
1. What went wrong this month?
1. What could we have done better?

We spend the first part of the retrospective meeting reviewing the action
items from the previous month.

On the 8th of each month (or the next business day), we have a kickoff meeting
for the version that will be released in the following month. The product team and other leads will have already had
discussions on what should be prioritized for that release. The purpose of this kickoff is
to get everyone on the same page and to invite comments.

Both the retrospectives and kickoffs are [live streamed to our GitLab Unfiltered YouTube channel](/handbook/marketing/marketing-operations/youtube/)
and posted to our [Unfiltered YouTube channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A).

## Random
{: #random-room}

1. The [#random](https://gitlab.slack.com/archives/random) Slack channel is your go-to place to share random ideas, pictures, articles, and more. It's a great channel to check out when you need a mental break.

## Scheduling Meetings

1. If you want to ask GitLab team members if they are available for an event please send a calendar invite with Google Calendar using your Google GitLab account to their Google GitLab account. When you add a GitLab team member as a “Guest” in Google Calendar, you can click the See Guest Availability button to check availability and find a time on their calendar.  These calendar invites will automatically show up on all parties calendars even when the email is not opened. It is an easier way to ensure everyone has visibility to the meeting and member’s status. Please respond quickly to invites so people can make necessary plans.
1. Every scheduled meeting should either have a Google Presentation (for example for functional updates that don't require participation) or a Google Doc (for most meetings) linked. If it is a Google Doc it should have an agenda, including any preparation materials (can be a presentation). Put the agenda in a Google Doc that has edits rights for all participants (including people not part of GitLab Inc.). Link the Google Doc from the meeting invite. Take notes of the points and todos during the meeting. Nobody wants to write up a meeting after the fact and this helps to structure the thought process and everyone can contribute. Being able to structure conclusions and follow up actions in realtime makes a video call more effective than an in-person meeting. If it is important enough to schedule a meeting it is important enough to have a Doc linked. If we want to be on the same page we should be looking at that page.
  * No agenda is required for [coffee chats](/company/culture/all-remote/informal-communication/#coffee-chats). Note that only meetings that are primarily social in nature should be labeled as a coffee chat in the calendar invite.
1. If you want to check if a team member is available for an outside meeting, create a calendar appointment and invite the team member only after they respond yes. Then invite outside people.
1. When scheduling a call with multiple people, invite them using a Google Calendar that is your own, or one specific to the people joining, so the calendar item doesn't unnecessarily appear on other people's calendars.
1. If you want to move a meeting just move the calendar appointment instead of reaching out via other channels. Note the change at the top of the description.
1. Please click 'Guests can modify event' so people can update the time in the calendar instead of having to reach out via other channels. You can configure this to be checked by default under [Event Settings](https://calendar.google.com/calendar/r/settings).)
1. When scheduling a meeting we value people's time and prefer the "speedy meetings" [setting in our Google Calendar](https://calendar.google.com/calendar/r/settings).
   This gives us meetings of, for example, 25 or 50 minutes leaving some time to:
   1. Write notes and reflect
   1. Respond to urgent messages
   1. Take a [bio break](https://www.merriam-webster.com/words-at-play/bio-break-meaning-and-origin)
   1. Stretch your legs
   1. Grab a snack
1. When scheduling a meeting, please try to have it start at :00 (hour) or :30 (mid-hour) to leave common start times available for other meetings on your attendees' calendars. Meetings should be for the time needed, so if you need 15 minutes just book that.
1. When creating a calendar event that will be used company wide, please place it on the GitLab Team Meetings Calendar. That way the event is easily located by all individuals.
1. When you need to cancel a meeting, make sure to delete/decline the meeting and choose the option **Delete & update guests** to make sure everyone knows you can't attend and don't wait for you.
1. If you want to schedule a meeting with a person not on the team please use [Calendly](/handbook/tools-and-tips/#calendly). Use Google Calendar directly if scheduling with a GitLab team member.
1. **Materials Review** are scheduled as all day not busy events as a reminder three days before the scheduled call.

### Common Meeting Problems

Meetings are incredibly expensive since they require synchronous time.
The most common meeting problems can all be address by following the above guidelines around scheduling meetings.
Some of the most common meetings problems are outlined below:

| Problem | Solution |
|---------|----------|
| Present instead of Q&A | Pre-record presentations on YouTube, so meetings are only Q&A |
| Meetings are used for brainstorming | People provide thoughtful proposals async |
| No agenda with edit rights for everyone | Ensure that every meeting has an agenda and is available for everyone to edit |
| People are late to meetings or don't have time to use the restroom between meetings | Use Speedy Meetings to give people breathing space before their next meeting |

## Indicating Availability

Indicate your availability by updating your own calendar using Google's ["out of office"](https://www.theverge.com/2018/6/27/17510656/google-calendar-out-of-office-option) feature and include the dates you plan to be away in your automated response. Note that this feature will automatically decline any meeting invitations during the time frame you select.

1. Put your planned away time including holidays, vacation, travel time, and other leave in your own calendar. Please see [Communicating your time off](/handbook/paid-time-off#communicating-your-time-off) for more.
1. Set your working hours in your Google Calendar settings.
1. Utilize [PTO Ninja](/handbook/paid-time-off/#pto-ninja) to keep other GitLab team members aware of your planned time away within Slack.

## Video Calls

1. Use video calls if you find yourself going back and forth in an issue/via email
or over chat. Guideline: if you have gone **back and forth 3 times**, it's time
for a video call.
1. Sometimes it's better to _not_ have a video call. Consider these tradeoffs:
   1. It is difficult (or impossible) to multi-task in a video call.
   1. It may be more efficient to have an async conversation in an issue, depending on the topic.
   1. A video call is limited in time: A conversation in an issue can start or stop at any time, whenever there's interest. It is async.
   1. A video call is limited in people: You can invite anybody into an async conversation at any time in an issue. You don't have to know who are the relevant parties ahead of time. Everyone can contribute at any time. A video call is limited to invited attendees (and those who have accepted).
   1. You can easily "promote" an async conversation from an issue to a video call, as needed. The reverse is harder. So there is lower risk to start with an async conversation.
   1. For a newcomer to the conversation, it's easier and more efficient to parse an issue, than read a video transcript or watch it.
   1. Conversations in issues are easily searchable. Video calls are not.
1. Try to have your video on at all times because it's much more engaging for participants
   1. Don't worry if you [can't pay attention at the meeting](/handbook/communication/#paying-attention-in-meetings) because you're doing something else, you are the manager of your attention. The flip-side of being the manager of your own attention is that others should not hesitate to request your attention when it is needed.
   1. It's okay to eat on video if you're hungry (please turn your mic off)
   1. You should ensure that you are properly dressed for all video calls. Properly dressed means that you are wearing clothing that covers the top and bottom parts of your body. We do not have a strict dress code policy, but want to make sure that all participants on video calls feel comfortable.  If you cannot be properly dressed for the entirety of the call, you should not join, but watch the recording at a later time.
   1. Having pets, children, significant others, friends, and family visible during video chats is encouraged. If they are human, ask them to wave at your remote team member to say "Hi".
   1. Do not feel forced to have your video on, use your best judgement.
1. Additional points for video calls with customers or partners
   1. Results come first. Your appearance, location and background is less important than making customers successful so don't wait for the perfect time / place when you can engage a customer right away.
   1. Communicating that GitLab is an enterprise grade product and service provider is supported by the way you present yourself. Most of the time, if you would not wear something or present yourself in a certain way at a customer's office, candidate interview, or partner meeting in person then it's probably not the right choice on a video call with them either.
   1. Green screens are a great background solution. It's great to work in your garage or basement! Just get a green screen behind you and put up a professional background image to present well externally and still use the rest of the room how you want!
1. We prefer [Zoom](https://gitlab.zoom.us/).
1. Google Calendar also has a [Zoom plugin](https://chrome.google.com/webstore/detail/zoom-scheduler/kgjfgplpablkjnlkjmjdecgdpfankdle?hl=en-US) where you can easily add a Zoom link for a video call to the invite
1. For meetings that are scheduled with Zoom:
   1. If you need more privileges on Zoom (longer meeting times, more people in the meeting, etc.), please contact People Operations Specialist as described [specifically for Zoom](/handbook/tools-and-tips/#sts=Zoom).
   1. Note that if you select to record meetings to the cloud (setting within Zoom), they will be automatically placed in the GitLab Videos folder in Google Drive on an hourly basis via a [scheduled pipeline](https://gitlab.com/gitlab-com/zoom-sync/pipelines). You can find these videos in Google Drive by entering in the search bar: `title:"GitLab Videos" source:domain`. The [script for syncing the files is here](https://gitlab.com/gitlab-com/zoom-sync).
   1. Note also that after a meeting ends, Zoom may take some time to process the recording before it is actually available. The sync to Google Drive happens on the hour mark, so if the recording is not available, it may take another hour to be transferred.
1. As a remote company we are always striving to have the highest fidelity, collaborative conversations. Use of a headset with a microphone, is strongly suggested.
   1. If other people are using headphones then no-headphones works fine. But if multiple people aren't using headphones you get distractions.
   1. Reasons to use headphones:
      1. Computer speakers can cause an echo and accentuate background noise.
      1. Using headphones decreases the likelihood of talking over one another, enabling a more lively conversation.
      1. Better sound quality, avoiding dynamic volume suppression due to echo cancellation.
   1. Leave the no headphones to:
      1. People who don't have them handy at that time
      1. People from outside the company
   1. Suggested headphone models can be found in the handbook under [spending company money](/handbook/spending-company-money/equipment-examples/#headphones-and-earbuds).
   1. If you want to use your [Bose headphones](https://www.bose.com/en_us/products/headphones/noise_cancelling_headphones.html) that is fine but please ensure the microphone is active.
1. Consider using a utility to easily mute/unmute yourself, see [Shush](/handbook/tools-and-tips/#shush) in the tools section.
1. [Hybrid calls are horrible](#hybrid-calls-are-horrible)
1. Always be sure to advise participants to mute their mics if there is unnecessary background noise to ensure the speaker is able to be heard by all attendees.
1. We start on time and do not wait for people. People are expected to join no later than the scheduled minute of the meeting (before :01 if it is scheduled for :00). The question 'is everyone here' is not needed.
1. It feels rude in video calls to interrupt people. This is because the latency causes you to talk over the speaker for longer than during an in-person meeting. We should not be discouraged by this, the questions and context provided by interruptions are valuable.
This is a situation where we have to do something counter-intuitive to make all-remote meetings work. In GitLab, everyone is encouraged to interrupt the speaker in a video call to ask a question or offer context. We want everyone to contribute instead of a monologue.
Just like in-person meetings be cognizant of when, who, and how you interrupt, we don't want [manterrupting](http://time.com/3666135/sheryl-sandberg-talking-while-female-manterruptions/).
1. We end on the scheduled time. It might feel rude to end a meeting, but you're actually allowing all attendees to be on time for their next meeting.
1. Do not use the chat of products like Zoom to communicate during the call, use the linked document instead. This allows everyone to contribute additional questions, answers, and links in the relevant place. It also makes it easier for people in conflicting timezones to contribute questions before the call and makes it easier to review questions and answers after the call, which can be before watching the recording.
1. [Write down your questions](/handbook/values/#write-things-down) in the agenda before vocalizing. Always ask people to vocalize their questions to provide the most detailed context and for any people that only use audio, for example listening to a recording of the call later while running.
1. You do not need to remind other people to vocalize their questions. Just say their name and a keyword of the question. 'Jay about credit-cards'
1. If there isn’t a note taker listed in the document at the start of the meeting, people should self-note-take. Consider asking other people to write down the answers in real-time to allow the person who asked the question to focus on the answer. The person asking the question can touch up the answer when the conversation has moved on to something less relevant to them.
1. Every comment is document worthy, even small support comments such as `+1` or `Very Cool!`.
1. We encourage the recording and [sharing of everything to our YouTube Unfiltered channel](/handbook/marketing/marketing-operations/youtube/)
1. It is unusual to smoke in an open office or video conference, vaping is associated with this. For this reason we ask that you don't vape during calls, and if you absolutely have to, kindly switch your camera off.
1. Speak up when you notice something is not working. If you notice someone's microphone, web cam or latency is causing issues for them it is good to speak up. On a video call it can be harder for the speaker to notice that they aren't being understood compared to a face to face conversation. As such you will be doing them a favour by speaking up early to let them know that they are having a problem. Also see [Hear nothing say something](https://www.youtube.com/watch?v=LZ5spXU5HbU) for further explanation.

### You are the manager of your attention
{: #paying-attention-in-meetings}

You are the manager of your attention, and you decide when you do or don't pay attention in a meeting.

You will always have more work than time in your life.
If you get invited to a meeting you don't think you should go to, you should decline the meeting.
It is better to cancel than to show up and not pay attention.

On the other hand, not every part of a meeting is relevant, but it can sometimes be helpful to have more people in a call.
If you only have one discussion point, if possible, try to reorder the meeting agenda to have your point first and then drop from the call.
If you get asked a question when you're not paying attention, it is an okay use of time to repeat a question every now and then.

We don't use the first 15 minutes of a meeting to read the materials like they [do at Amazon](https://www.forbes.com/sites/carminegallo/2019/06/18/how-the-first-15-minutes-of-amazons-leadership-meetings-sparks-great-ideas-and-better-conversations/#6be165bd54ca). You can use the start of a meeting to review the materials for the meeting if you need to, given you do not have to be paying attention, but that should not delay the start of the meeting for the people that already have questions based on the materials. [Meetings start on time at GitLab.](/handbook/communication/#scheduling-meetings) 

Don't use your camera to signal you're not paying attention; [cameras should always be on](/handbook/communication/#video-calls).

### First Post is a badge of honor

You should take pride in being the first person to add a question to a
meeting agenda, however unlike [the First post
meme](https://knowyourmeme.com/memes/first) we do want the first post to be
more than just "First!". The meeting DRI will be happy to see there is a
question ready before to kick off the meeting. The Meeting DRI should remember
to thank the person for asking the first question.

## Hybrid calls are horrible

In calls that have remote participants everyone should use have their own equipment (camera, headset, screen).

When multiple people share equipment the following **problems arise for remote participants**:

1. Can't hear the sharing people well.
1. Background noise since the microphone of the sharing people on all the time.
1. Can't clearly see facial expressions since each face takes up only a small part of the screen.
1. Can't easily see who is talking since the screen shows multiple people.
1. Hard getting a word in since their delay is longer than for the sharing people.

The **people sharing equipment also have problems** because they don't have their own equipment:

1. Can't easily screen share something themselves.
1. Trouble seeing details in screen sharing since the screen is further away from them.
1. Can't scroll through a slide deck at their own pace.
1. Sharing people can't easily participate (view or type) in a shared document with the agenda and meeting notes.

The disadvantages for remote people are much greater than for the sharing people and hard to notice for the sharing people.
The disadvantages cause previously remote participants to travel to the meeting to be in person for a better experience.
The extra travel is inefficient since it is time consuming, expensive, bad for the environment, and unhealthy.

Theoretically you can have multiple people in a room with their own equipment but in practice it is much better to be in separate rooms:

1. It is annoying to first hear someone talk in the room and then hear it over audio with a delay.
1. It is hard to consistently mute yourself when not talking to prevent someone else's voice coming through your microphone as well.

## User Communication Guidelines

1. Keep conversations positive, friendly, real, and productive while adding value.
1. If you make a mistake, admit it. Be upfront and be quick with your correction. If you're posting to a blog, you may choose to modify an earlier post. Just make it clear that you have done so.
1. There can be a fine line between healthy debate and incendiary reaction. Try to frame what you write to invite differing points of view without inflaming others. You don’t need to respond to every criticism or barb. Be careful and considerate.
1. [Assume positive intent](/handbook/values/#assume-positive-intent) and explicitly state the strongest plausible interpretation of what someone says before you respond, not a weaker one that's easier to criticize. [Rapoport's Rules](https://rationalwiki.org/wiki/Rapoport%27s_Rules) also implores you to list points of agreement and mention anything you learned.
1. Answer questions, thank people even if it’s just a few words. Make it a two way conversation.
1. Appreciate suggestions and feedback.
1. Don't make promises that you can't keep.
1. Guide users who ask for help or give a suggestion and share links. [Improving Open Development for Everyone](/blog/2015/12/16/improving-open-development-for-everyone/), [Types of requests](/blog/2014/12/08/explaining-gitlab-bugs/).
1. When facing negative comment, respond patiently and treat every user as an individual, people with the strongest opinions can turn into [the strongest supporters](/blog/2015/05/20/gitlab-gitorious-free-software/).
1. By default, discussions in issues and MRs are public and could include participation of wider community members. It is important to make the wider community members feel welcome participating in discussions and sharing their view. Wider community members also submit [MRs to help improve our website/handbook](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20Contribution) and this is often their first contribution to GitLab. We want to make sure that we are responsive to their contributions and thank them for helping improve GitLab.
1. Adhere to the [Code of Conduct](/community/contribute/code-of-conduct/) in all communication. Similarly, expect users to adhere to the same code when communicating with the GitLab team and the rest of the GitLab community. No one should accept being mistreated.

## Writing Style Guidelines

1. {: #american-english} At GitLab, we use American English as the standard written language.
1. Do not use rich text, it makes it hard to copy/paste. Use [Markdown](/handbook/markdown-guide/) to format text that is stored in a Git repository. In Google Docs use "Normal text" using the style/heading/formatting dropdown and paste without formatting.
1. Don't use ALL CAPS because it [feels like shouting](http://netiquette.wikia.com/wiki/Rule_number_2_-_Do_not_use_all_caps).
1. We use Unix style (lf) line endings, not Windows style (crlf), please ensure `*.md text eol=lf` is set in the repository's `.gitattributes` and run `git config --global core.autocrlf input` on your client.
1. Always write a paragraph on a single line. Use soft breaks ("word wrap") for readability. Don't put in a hard return at a certain character limit (e.g., 80 characters) and don't set your IDE to automatically insert hard breaks. Merge requests for the blog and handbook are very difficult to edit when hard breaks are inserted.
1. Do not create links like "here" or "click here". All links should have relevant anchor text that describes what they link to, such as: "GitLab CI source installation documentation". Using [meaningful links](https://www.futurehosting.com/blog/links-should-have-meaningful-anchor-text-heres-why/){:rel="nofollow noindex"} is important to both search engine crawlers (SEO) and people with accessibility issues.
This guidance should be followed in all places links are provided, whether in the handbook, website, GoogleDocs, or any other content.
Avoid writing GoogleDocs content which states - `Zoom Link [Link]`.
Rather, paste the full link directly following the word `Zoom`.
This makes the link more prominent and makes it easier to follow while viewing the document.
1. Always use [ISO dates](https://en.wikipedia.org/wiki/ISO_8601#Calendar_dates) in all writing and legal documents since other formats [lead to online confusion](http://xkcd.com/1179/). Use `yyyy-mm-dd`, for example 2015-04-13, and never 04-13-2015, 13-04-2015, 2015/04/13, nor April 13, 2015. Even if you use an unambiguous alternative format it is still harder to search for a date, sort on a date, and for other team members to know we use the ISO standard. For months use `yyyy-mm`, so 2018-01 for January. Refer to a year with CY18 (never with 2018) and a quarter with CY18-Q1 to prevent confusion with fiscal years and quarters.
1. GitLab operates on a [Fiscal Year](/handbook/finance/#fiscal-year) offset from the calendar year. When referring to a fiscal year or quarter, please use the following abbreviations:
   1. "FY20" is the preferred format and means: Fiscal Year 2020, the period running from February 1, 2019 through January 31, 2020
   1. "Q1" = the first quarter of the current Fiscal Year, so on Feb 1, 2020, "Q1" is the period from Feb. 1, 2020 through April 30, 2020. Note that Epics in GitLab follow Calendar Years and Quarters.
   1. When referring to a quarter in a future or past year, combine the two above: "FY21-Q1"
   1. When financial data is presented include a note to indicate fiscal year (e.g. "Fiscal Year ending January, 31 'yy")
1. Remember that not everyone is working in the same timezone; what may be morning for you is evening for someone else. Try to say 3 hours ago or 4 hours from now, or use a timestamp, including a timezone reference.
1. We use UTC as the timezone for engineering (for example production postmortems) and all cross-functional activities related to the monthly release. We use Pacific Time (PT) for all other uses since we are a San Francisco-based company. Please refer to time as "9:00 Pacific" or `9:00 PT`. It isn't often necessary to specify whether a timezone is currently observing Daylight Saving Time, and such references are often incorrect, so prefer "PT" to "PDT" or "PST" unless you have a specific need to differentiate between PDT and PST.
1. When specifying measurements, please include both Metric and Imperial equivalents.
1. Although we're a San Francisco based company we're also an internationally diverse one. Please do not refer to team members outside the US as international, instead use non-US. Please also avoid the use of offshore/overseas to refer to non-American continents.
1. If you have multiple points in a comment or email, please number them. Numbered lists are easier to reference during a discussion over bulleted lists.
1. When you reference an issue, merge request, comment, commit, page, doc, etc. and you have the URL available please paste that in.
1. In making URLs, always prefer hyphens to underscores, and always use lowercase.
1. The community includes users, contributors, core team members, customers, people working for GitLab Inc., and friends of GitLab. If you want to refer to "people not working for GitLab Inc." just say that and don't use the word community. If you want to refer to people working for GitLab Inc. you can also use "the GitLab Inc. team" but don't use the "GitLab Inc. employees".
1. When we refer to the GitLab community excluding GitLab team members please say "wider community" instead of "community".
1. All people working for GitLab (the company) are the [GitLab team](/company/team). We also have the [Core team](/community/core-team/) that consists of volunteers.
1. Please always refer to GitLab Inc. people as GitLab team members, not employees.
1. Use [inclusive and gender-neutral language](https://techwhirl.com/gender-neutral-technical-writing/) in all writing.
1. Always write "GitLab" with "G" and "L" capitalized, even when writing "GitLab.com", except within URLs. When "gitlab.com" is part of a URL it should be lowercase.
1. Always capitalize the names of GitLab products, [product tiers](/pricing/), and [features](/features/).
1. Write a [group](/handbook/product/categories/#hierarchy) name as ["Stage:Group"](/handbook/product/categories/#naming) when you want to include the stage name for extra context.
1. Do not use a hyphen when writing the term "open source" except where doing so eliminates ambiguity or clumsiness.
1. Monetary amounts shouldn't have one digit, so prefer $19.90 to $19.9.
1. If an email needs a response, write the answer at the top of it.
1. Use the future version of words, just like we don't write internet with a capital letter anymore. We write frontend and webhook without a hyphen or space.
1. Our homepage is https://about.gitlab.com/ (with the `about.` and with `https`).
1. Try to use the [active voice](https://writing.wisc.edu/Handbook/CCS_activevoice.html) whenever possible.
1. Refer to environments that are installed and run "on-premises" by the end-user as "self-managed."
1. If you use headers, properly format them (`##` in Markdown, "Heading 2" in Google Docs); start at the second header level because header level 1 is for titles. Do not end headers with a colon. Do not use emoji in headers as these cause links to have strange characters.
1. Always use a [serial comma](https://en.wikipedia.org/wiki/Serial_comma) (a.k.a. an "Oxford comma") before the coordinating conjunction in a list of three, four, or more items.
1. Always use a single space between sentences rather than two.
1. Read our [Documentation Styleguide](https://docs.gitlab.com/ee/development/doc_styleguide.html) for more information when writing documentation.
1. Do not use acronyms when you can avoid them. Acronyms have the effect of excluding people from the conversation if they are not familiar with a particular term. Example: instead of `MR`, write `merge request (MR)`.
    1. If acronyms are used, expand them at least once in the conversation or document and define them in the document using [Kramdown abbreviation syntax](https://kramdown.gettalong.org/syntax.html#abbreviations). Alternatively, link to the definition.
1. We segment our customers/prospects into 4 segments [Strategic, Large, Mid-Market, and Small Medium Business (SMB)](/handbook/business-ops/resources/#segmentation).

## Visuals
Many times an explanation can be aided by a visual.
Whenever presenting a diagram, we should still allow everyone to contribute.
Where possible, take advantage of the handbook's support for [Mermaid](https://docs.gitlab.com/ee/user/markdown.html#mermaid). If you are new to using Mermaid or need help troubleshooting errors in your Mermaid code, the [Mermaid Live Editor](https://mermaid-js.github.io/mermaid-live-editor/) can be a helpful tool.
Where taking advantage of Mermaid isn't possible, link to the original in our Google Drive so that the diagram can be edited by anyone.

## Situation-Complication-Implication-Position-Action-Benefit (SCI-PAB®)

[Mandel Communications refers to SCI-PAB®](https://www.mandel.com/why-mandel/SCI-PAB-how-to-start-a-presentation) at the "surefire, six-step method for starting any conversation or presentation." When you only have a few minutes to present your case or grab your listener's attention, this six-step process can help you communicate better and faster.

1. Situation - Expresses the current state for discussion.
1. Complication - Summarizes the critical issues, challenges, or opportunities.
1. Implication - Provides insight into the consequences that will be a result of if the Complications are not addressed.
1. Position - Notes the presenter's opinion on the necessary changes which should be made.
1. Action - Defines the expectations of the target audience/listeners.
1. Benefit - Clearly concludes how the Position and Action sections will address the Complications.
This method can be used in presentations, emails, and everyday conversations.

Example - The Management team asking for time to resolve a problem

1. S - The failure rate last year for product X1 was an acceptable 1.5%.
1. C - Because of supply shortages in the current fiscal year we are forced to change the material of a key component.
1. I - Unfortunately, that resulted in the failure rate doubling this year.
1. P - It is critical we address this problem immediately.
1. A - Please approve the team 5 days to investigate the specific causes of the increase and establish the necessary next steps.
1. B - By doing this we will reduce the failure rate to an acceptable level and develop guidelines for preventing such problems in the future.

Some examples can be found at [SCI-PAB - Six Steps To Reach Your Audience](https://dzone.com/articles/scipab-six-steps-to-reach-your-audience).

## Company phone number
{: #phone-number}

If you need to provide the details of GitLab's contact information you can take the [address from the visiting page](/company/visiting/) for reference; or the [mailing address](/handbook/people-group/#addresses) of the office in the Netherlands if that is more applicable.

If a phone number is required, leave this field empty by default. If that is not possible, then use
the general number (+1-415-761-1791), but be aware that this number simply guides to a voice message that refers the caller back to contacting us via email.

## Organization code names

1. Listed in Google Sheet under 'Organization code names'
1. To make it easier to recognize code names we base them on classic car models.

There are two types of code names:

1. **Don't mention publicly** We can use code names anywhere we regularly share items in that medium with people outside the company: issue trackers, Group Conversations, etc. For these we don't have to use code names in things that are never published: SalesForce, Zuora, Zendesk, and verbal conversations.
1. **Don't write down** there are organizations that we shouldn't write down anywhere.

## Ubiquitous language

At GitLab we use [ubiquitous language](https://martinfowler.com/bliki/UbiquitousLanguage.html) to increase communication efficiency.

This is defined in [Domain-driven design](https://en.wikipedia.org/wiki/Domain-driven_design) as: "A language structured around the domain model and used by all team members to connect all the activities of the team with the software."

We use it for activities in GitLab, even ones not implemented in software.

By having ubiquitous words to identify concepts we prevent confusion over what is meant, for example we refer to [parts of our organization](/company/team/structure/) as a function, department, or group depending on exactly what is meant.

Make sure that domains don't overlap, for example [organization size](/handbook/sales/#organization-size) and [deal size](/handbook/sales/#deal-sizes) don't reuse words to prevent overlap.

If a term is ambiguous don't use it, for example our [hiring definitions](/handbook/hiring/#definitions) have roles and vacancies but avoid the ambiguous word job.

Make sure that people can infer as much as possible from the word, for example our [subscription options](/handbook/marketing/product-marketing/tiers/) allow you to know if someone if using self-managed or GitLab.com.

Make sure terms don't overlap without clearly defining how and why, for example see our [tier definitions](/handbook/marketing/product-marketing/tiers/#definitions).

Keep terms to one or at most two words to prevent people from introducing ambiguity by shortening a term. When using two words make the first word unique because people tend to drop the second word more often.

## MECEFU terms

MECEFU is an acronym for Mutually Exclusive Collectively Exhaustive Few words Ubiquitous-language.

You pronounce it: MessiFu. Think of the great soccer player Lionel Messi and his [kung fu](https://en.wikipedia.org/wiki/Chinese_martial_arts) or soccer fu skills.

We want to use MECEFU terms to describe a domain to ensure efficient communication. MECEFU terms have 4 characteristics that help with efficiency:

1. Mutually Exclusive: nothing is referenced by more than one term
1. Collectively Exhaustive: everything is covered by one of the terms
1. Few words: the longer terms are the more likely it is people will not use all of them and cause confusion, avoid acronyms because they are hard to remember (we're open to a few words to replace MECEFU as an acronyms :)
1. Ubiquitous language: [defined above](#ubiquitous-language)

An example of a MECEFU term is our [sales segmentation](/handbook/business-ops/resources/#segmentation):

1. Mutually Exclusive: There is no overlap between the numbers and there is a single dimension.
1. Collectively Exhaustive: Everything for 0 to infinite employees is covered.
1. Few words: Mid-market is a natural combination and SMB is abbreviated.
1. Ubiquitous language: We're not using the word 'Enterprise' which already can refer to our Enterprise Edition distribution.

One nit-pick is that the Medium of SMB and Mid of Mid-Market sound very similar.

## Simple Language

Simple Language is meant to encourage everyone at GitLab to simplify the language we use.
We should always use the most clear, straightfoward, and meaningful words possible in every conversation.
Avoid using "fluff" words, jargon, or "corporate-speak" phrases that don't add value.

When you **don't** use Simple Language, you:
- Confuse people and create a barrier for participants of your conversation.
- Cause others to not speak up in a meeting because they don't understand what you're saying.
- Are not inclusive of those whose first language is not English.
- Do not add value with your words.

When you **do** use Simple Language, you:
- Get work done more efficiently.
- Build credibility with your audience (your team, coworker, customer, etc.).
- Keep people's attention while you're speaking.
- Come across more confident and knowledgable.

**Here's an example:**

*Original sentence*
>  We're now launching an optimization of our approach leveraging key learnings from the project's postmortem.

*A Simple Language sentence*
>  We're creating a new plan based on what we learned from this project.

Simple Language is important both when we're speaking to other team members and when we're representing GitLab to people outside the company.

Be sure to use Simple Language in written communications as well.
Our handbook, website, docs, marketing materials, and candidate or customer emails should be clear, concise, and effective.
Corporate marketing maintains guidelines on [GitLab's tone of voice](/handbook/marketing/corporate-marketing/#tone-of-voice-1).

| Instead of... 	| Try... 	|
|-----------------------------------	|----------------------------------	|
| Getting buy-in/Getting alignment 	| Asking for feedback since DRIs make decisions	|
| Synergy 	| Effective Collaboration 	|
| Get all your ducks in a row 	| Be organized 	|
| Don't let the grass grow too long 	| Work quickly 	|
| Leverage 	| Use more explicit phrasing- debt, etc. 	|
| Send it over the wall 	| Share it with a customer 	|
| Boil the ocean 	| Waste time 	|
| Punt 	| Make less of a priority 	|
| Helicopter view/100 foot view 	| A broad view of the business 	|
| Turtles all the way down | Cascade through the organization |

### Inefficient things shouldn't sound positive

For example, do not suggest that you're "working in real-time" when a matter is in disarray. Convey that a lack of organization is hampering a result, and provide feedback and clear steps on resolving.

Do not use a cool term such as "tiger team" when the [existing term of "working group"](/company/team/structure/working-groups/) is more exact. While cool terms such as these may be useful for persuading colleagues to join you in working towards a solution, the right way isn't to use flowery language.

The last example is when we used 'Prioritizing for Global Optimization' for what we now call a [headcount reset](/handbook/product/product-management/process/#prioritize-global). When we [renamed it](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/31101/diffs) we saw a good reduction in the use of this disruptive practice of moving people around.

## Deep Dives

As GitLab continues to grow, sharing knowledge across the community becomes even more important.
The [Deep Dives page](/handbook/communication/deep-dives/) describes initiatives we are trying to encourage this.
This aligns with how we work since everything at GitLab is [public by default](/handbook/values/#public-by-default).

-- specific comms tools


## Email

1. Send one email per subject as multiple items in one email will cause delays (have to respond to everything) or misses (forgot one of the items).
1. Always reply to emails by replying to all, even when no action is needed. This lets the other person know that you received it. A thread is done when there is a single word reply, such as OK, thanks, or done.
1. If you're sending an email to a large group of people (or a distribution list), put those recipients in BCC (rather than in the "To" field) so that a reply all won't ping hundreds of people.
1. If you forward an email without other comments please add FYI (for your information), FYA (for your action), or FYC (for your consideration). If you forward an external request with FYC it just means the person who forwarded it will not follow up on the request and expects you to decide if you should follow up or not, the terms comes from [movie promotion to voters](<https://en.wikipedia.org/wiki/For_Your_Consideration_(advertising)>).
1. Email forwarding rules are specified in the shared [_Email, Slack, and GitLab Groups and Aliases_](https://docs.google.com/document/d/1rrVgD2QVixuAf07roYws7Z8rUF2gJKrrmTE4Z3vtENo/) Google Doc accessible only to people in the company. If you want to be added or removed from an internal alias, change a rule, or add a forwarding email alias, please [suggest an edit](https://support.google.com/docs/answer/6033474?hl=en) in the doc and [submit a new access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Single_Person_Access_Request).
1. Only G-Suite domain admins are allowed to provision Google Groups and email distributions.
1. Emails are asynchronous, for example, if your manager emails you on a weekend it is fine to reply during the workweek.
1. If an email is or has become **urgent** feel free to ping people via chat referencing the subject of the email.
1. If you or your team needs to send an email to a group of team members, not grouped in a current [Google email group](https://gitlab.com/gitlab-com/security-tools/report-gsuite-group-members), and specifically related to PII (location, state, country, etc) please contact a Total Rewards Analyst at total-rewards@domain who can create an email list from BambooHR data, with approval.

## Slack

Slack is to be used for informal communication only. Only 90 days of activity will be retained. Accordingly, Slack should specifically NOT be used for:
   1. obtaining approvals;
   1. documenting decisions;
   1. storing official company records or documents; or
   1. sharing personal or sensitive information regarding any individuals

Internal slack messages between team members are still considered professional communication.  Please do not use or add emoji's to slack that are of a political, religious or of a sexual nature.  You can refer to the [Religion and politics at work](https://about.gitlab.com/handbook/values/) section of the handbook.  When in doubt do not use or add the emoji.  If you have any concerns about an emoji that was used, please reach out to the author or if you are not comfortable doing so please reach out to your [People Business Partner](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division).

### Avoid Direct messages

**Note:** We don't use the term *private message*, because these *direct messages* are not inherently *private* like a phone call or private letter. The messages are potentially accessible by Workspace admins or via Backups. Slack refers to these types of messages as [direct messages themselves](https://slack.com/intl/en-de/help/articles/212281468-What-is-a-direct-message).
{: .note}

1. When using Slack for work-related purposes, please avoid direct messages. [Direct messages discourage collaboration](http://blog.flowdock.com/2014/04/30/beware-of-private-conversations/). You might actually be contacting the wrong person, and they cannot easily redirect you to the right person. If the person is unavailable at the moment, it is less efficient because other people cannot jump in and help. Use a public channel and mention the person or group you want to reach. This ensures it is easy for other people to chime in, involve other people if needed, and learn from whatever is discussed.
1. If someone sends you a work-related direct message, it is okay to let them know you'd like to take the conversation to a public channel, linking to this section of the handbook.  The process might look something like:
  1. In the direct message: `Thanks for reaching out, that's a great question/idea I think the rest of the team could benefit from.  I'm going to move this to #public-channel based on [our desire to avoid direct messages](/handbook/communication/#avoid-direct-messages)`
  1. In the appropriate public channel: `@Person asked "question" in a DM, pulling that out here if anyone else has input.`
  1. Answer the question in a thread on that channel message, allowing others to benefit.
1. If you find yourself getting a lot of direct messages that should go in a public channel, consider changing your Slack status to an attention grabbing emoji and set it to something like:
  1. `Please consider posting in a public channel before direct messaging`
  1. `Why direct message me when you can post in a public channel?`
1. If you must send a work-related direct message, don't start a conversation with "Hi" or "Hey" as that interrupts their work without communicating anything. If you have a quick question, just ask the question directly, and the person will respond asynchronously. If you truly need to have a synchronous communication, then start by asking for that explicitly, while mentioning the subject. e.g., "I'm having trouble understanding issue #x, can we talk about it quickly?".

#### Do not use group direct messages
Use private channels instead of group direct messages. Group direct messages are very hard to maintain, track and respond to. First, consider whether the conversation can take place in a public channel. If not, please use a private channel instead.

#### Why we track % of messages that are not DMs

For all the same reasons that we want to [avoid direct messages](/handbook/communication/#avoid-direct-messages), [use public channels](/handbook/communication/#use-public-channels), and be [handbook-first](/company/culture/all-remote/handbook-first-documentation/), we track the % of messages that are not DMs.
As we grow headcount, we exponentially increase the lines of communication- 3 people have 3 communication lines, 4 have 6, and 41 have 820.
As a result, there is a natural tendency for people to prefer private channels of communication.
The intentions are good, as people are looking to reduce noise for others, but this can lead to the same problems as described elsewhere on this page, notably:
1. communication is siloed
1. as we grow, people may be reaching out to the wrong person
1. if you have a question, other people might have it too

Slack is our primary source of chat communication and is where many personal interactions happen.
We want to continue to encourage folks to build personal relationships with one another which will often happen over DMs.

We know that DMs will always exist.
We don't want to eliminate them.
We set a target of Slack DMs to consist of less than 50% of messages.
At the time that we set this target, it is about 80% of communications.

Everything at GitLab is a work in progress, so if we see a culture shift where Slack is *not* where work is occurring, thus inflating the amount of communication that is personal that is occurring, we can always change this KPI, but the steady growth of Slack messages paralleling the number of team members does not seem to suggest that is the case.

The previous KPI (% of messages sent in public channels) was about public channels but since some necessary parts of the business occur in private channels (discussions around comp, hiring, recruiting- and we do A LOT of hiring), this version of the KPI makes more sense.
Earlier in our history, 50% of all communication *was* in public channels.

<iframe class="dashboard-embed" src="https://app.periscopedata.com/shared/770f45e7-ca4d-417f-a3db-9dfc4403e24d?embed=true" height="700"> </iframe>

### Use Public Channels

1. If you use Slack and plan to message 3 or more people, we recommend a channel for customer/issue/project/problem/partnership.
1. Learn about [common channels and channel-naming conventions](/handbook/communication/chat).
1. If something is important but not urgent - like complimenting or encouraging the entire team - use email or post in the channel without `@`-mentioning the team.
1. It's not rude to leave a channel. When you've had your questions answered or are no longer interested, feel free to leave the channel so it won't distract you anymore.
1. The usage of ChatBots for integrations can sometimes depend upon the name of the channel. You should consult the channel about such integrations before changing the name of commonly used/popular channels to avoid inadvertently breaking integrations.

### Be respectful of your own time
1. You should try avoid information overload in order to be productive and efficient with your time. While it can be tempting to read every message in every Slack channel you subscribe to, it’s very challenging, not expected, and not necessary.
1. One method for avoiding Slack overload is to focus your Slack reading on Starred channels and Threads. Starred channels are like "favorites" and allow you to follow messages from those channels easily. Threads consist of any conversation in which you are mentioned and allow you to easily track conversations in which you have direct involvement.
1. Use your notification settings liberally. Depending on how you use Slack this could range from limiting notifications to critical messages outside of your working hours to turning off Slack notifications entirely. Find the right balance for you and stick to it.

### Be respectful of others' time
1. If you're only referring to someone, but don't actually need their attention, and want to spare them from getting notified, spell out their name normally without `@` mentioning them.
1. Slack messages should be considered asynchronous communication, and you should not expect an instantaneous response; you have no idea what the other person is doing.
1. Because we work globally, you may receive Slack mentions at any time of day. Please consider enabling [Slack's Do not disturb functionality](/handbook/tools-and-tips/#do-not-disturb-hours) so you don't get interrupted, for example, in your offtime.
1. Slack does not currently support automatically activating Do Not Disturb over weekends. Please bear this in mind if you choose to work over weekends, as your Slack message to someone may interrupt their time off. Consider using another form of communication instead.
1. Do not feel obligated to respond to Slack messages when you are not working.
1. Feel free to send a colleague a link to these guidelines if the communication in Slack should be done **asynchronously**.
1. **Please avoid using @here or @channel unless this is about something urgent and important.** In chat, try to keep the use of keywords that mention the whole channel to a minimum. They should only be used for pings that are both urgent and important, not just important. By overusing channel mentions, you make it harder to respond to personal mentions promptly since people get pinged too frequently. Additionally, if you are planning to `@mention` a specific team ([Slack User Group](https://get.slack.help/hc/en-us/articles/212906697-Create-a-user-group#browse-user-groups-and-view-members)), consider the size of the group you are mentioning ([see group membership](https://gitlab.com/gitlab-com/security-tools/report-slack-group-members)) and the impact of pinging all of these people for the particular situation. If something is urgent and important:
   1. Use `@here` to notify all currently _active_ members in the room. Please only use `@here` if the message is important _and_ urgent.
   1. Use `@channel` to notify _ALL_ members in the room, irrespective of away status. Please only use `@channel` if the message is important _and_ urgent.
1. If you are aware that your teammate is on vacation, avoid mentioning them in a high volume channel. It will be difficult to find the information or question when they return. If you need to ensure they refer back to the thread, ensure to send them a link to the relevant Slack message through a direct message.

### General Guidelines
1. If the subject is of value to the wider community, consider commenting on an existing issue or opening a new [issue](#everything-starts-with-an-issue) instead.
1. Use the `:white_check_mark:` emoji or similar to indicate an inquiry has been answered. Anyone can add the emoji. If you're not sure, then feel free to leave it up to the person who asked. An emoji indicator is particularly helpful in channels where lots of questions are posted, such as `#questions`, and `#git-help`.
1. In general, you can think of emoji reactions as equivalent to body-language responses we use in real-life conversations, such as nodding your head as encouragement when a verbal (or in Slack, written) response might be too much.
1. In public channels, [threads](https://get.slack.help/hc/en-us/articles/115000769927-Message-threads) are valuable for keeping conversations together. If you want to respond to a question or comment in a channel, please start a thread instead of responding below them in the channel. This helps to keep the discussion in one place where it is easy to follow, and reduces noise as each message in a thread does not result in an unread message for everyone in the channel.
1. Unless you're in an active chat, don't break up a topic into multiple messages as each one will result in a notification which can be disruptive. Use [threads](https://get.slack.help/hc/en-us/articles/115000769927-Message-threads) if you want to provide extra info to the question/comment you posted.
1. If you are having a hard time keeping up with messages, you can update your preferences to have Slack email you all notifications. To change the setting, go to `Preferences > Notifications > When I'm not active on desktop...` and "send me email notifications."
1. If you agree in a message to start a video call (typically by asking "Call?") the person that didn't leave the last comment starts the call. So either respond to the "Call?" request with a video link or say "Yes" and let the other person start it. Don't say "Yes" and start a call 5 seconds later since it is likely you'll both be creating a video call link at the same time.
1. As an admin of the Slack workspace, if given the option to _"Disable future attachments from this website"_ when removing an attachment from a message **this will block the link/domain from [unfurling](/handbook/tools-and-tips/#unfurling-links-in-messages) in the entire Slack workspace**. Be careful and deliberate when choosing this option as it will impact every user in the workspace.
1. When referencing a Slack thread in a GitLab.com issue, don't _only_ link to the thread. Not only will people outside of the GitLab organization be unable to access the content, but the link will expire after the Slack retention period expires. Instead:
   1. **Review the contents** for confidentiality of users, customers, or any other sensitive information before posting.
   1. Copy and paste the relevant parts of the thread into the issue using blockquote formatting.
   1. Link to the Slack thread and include `(internal)` after the link. For example: https://gitlab.slack.com/archives/C0AR2KW4B/p1555347101079800 (internal)
   1. Post a link to the issue note in the Slack thread to let others know that discussion has moved to the issue.
1. When selecting your Slack display name, please do not have your name in all capital letters as this is often [associated as shouting](https://en.wikipedia.org/wiki/All_caps#Association_with_shouting) in written communications.

### Getting in touch with the e-group
To get in touch with the [e-group](/company/team/?department=executive) on Slack, you can use the following channels.  When in doubt, you can use the general `#e-group` channel to reach out to the entire group.

| Member | Channel |
| ------ | ------ |
| CEO    | `#ceo` |
| CFO    | `#finance` |
| VP of Product | `#product` |
| VP of Product Strategy | `#product-strategy` |
| EVP of Engineering | `#evpe` |
| CRO    | `#sales` |
| CMO    | `#marketing` |
| CPO    | `#peopleops` |
| CLO    | `#legal` |

### Key Slack channels

The alphabetically sorted starter list below spotlights a few of GitLab's [many Slack channels](/handbook/communication/chat/) in an effort to provide guidance to team members regarding the best places to ask specific questions and/or engage in discussion on a variety of topics. See Slack's [Help Center](https://slack.com/help/articles/205239967-Join-a-channel) for instructions on browsing all available channels.

**Learn more in our [Chat handbook section](/handbook/communication/chat/)**.

| Channel | Purpose |
| ------  | ------ |
| `#company-fyi` | Official company announcements, [restricted permission levels](#posting-in-company-fyi) to ensure high-signal; all GitLab team members are automatically added to this channel. |
| `#whats-happening-at-gitlab` | Open to posts from all team members including reminders, events, project updates, etc.; all GitLab team members are automatically added to this channel. |
| `#diversity_inclusion_and_belonging` | Stay up to date on GitLab’s latest [Diversity, Inclusion and Belonging](/company/culture/inclusion/) initiatives and share feedback and thoughts about how we can make our environment even more inclusive. |
| `#expense-reporting-inquiries` | For questions pertaining to [expenses](/handbook/spending-company-money/) (e.g. Expensify). |
| `#git-help` | Specific questions about using Git in the terminal. |
| `#intheparenthood` | Cute kid photos, tips on getting your children ready for school, etc. |
| `#is-this-known` | Get help in finding existing issues for existing problems. |
| `#it_help` | Create a Help Request in this channel should you have any IT general questions or trouble with setup (e.g. 2FA, accounts, etc.). |
| `#loc-specific channels` | Search loc_*insert your location* to connect with GitLab team members in your location (e.g `#loc_italy`, `#loc_chicagoland`, `#loc_mexico`, etc.). |
| `#mr-buddies` | For any questions regarding merge requests. |
| `#new_team_members` | For new GitLab team members to introduce themselves to the company and for existing team members to share updates with new hires. |
| `#office-today` | GitLab is an [all-remote](/company/culture/all-remote/) organization. Where’s your office today? Share a photo or use words to describe it. |
| `#payroll` | For questions pertaining to [payroll](/handbook/finance/payroll/) and [contractor invoices](/handbook/finance/payroll/#non-us/) (e.g. ADP, etc.). |
| `#peopleops` | For general People Ops questions (e.g. [onboarding](/handbook/general-onboarding/), [offboarding](/handbook/offboarding/), [team meetings](/company/culture/all-remote/meetings/), etc.). |
| `#questions` | For any general help with anything, really, and Git. If you have a question but you're not sure in which channel you should ask it, questions is always a great place! |
| `#random` | Anything and everything from photos, news, food, music, etc. |
| `#recruiting` | For questions about [referrals](/handbook/hiring/referral-process/), the [hiring process](/handbook/hiring/), and/or candidate status. |
| `#remote` | To share news, thoughts, feedback and anything else pertaining to remote work! Learn more about [GitLab's approach to remote work](/company/culture/all-remote/). |
| `#team-member-updates` | To stay updated on transitions/promotions, new GitLab team members joining, work anniversaries, etc. |
| `#thanks` | Recognition is an [important part of GitLab's culture](/handbook/communication/#say-thanks); give a public "thanks" to your teammates here! |
| `#total-rewards` | For anything related to compensation, benefits, or equity. You can also check out the Total Rewards [issue tracker](https://gitlab.com/gitlab-com/people-group/total-rewards/issues) and [handbook page](https://about.gitlab.com/handbook/total-rewards/)! |
| `#travel` | A place to discuss all things [travel](/company/culture/all-remote/people/#travelers)! |
| `#women` | Employee resource group for members and allies. |

### Why are we upgraded to the Plus tier?

We upgraded tiers to improve efficiency and security with the ability to use Okta to login into Slack. This will help us scale by improving provisioning and deprovisioning of our corporate systems. This upgrade will also allow us to improve the auditing requirements where identity management is in scope. The Plus tier also includes announcement only channels, 99.99% guaranteed up time, 24x7 support with guaranteed response in four hours or less, and the ability of [Corporate Export.](https://slack.com/help/articles/204897248)

#### When would GitLab use Corporate Export?

The times this feature would be used would be to comply with certain obligations. Corporate Export must be enabled by Slack in accordance with Slack’s policy, which can be found [here](https://slack.com/help/articles/204897248-Guide-to-Slack-import-and-export-tools).

Examples of instances where GitLab may need to use this feature may include, but are not limited to, those situations listed in [Slack’s documentation](https://slack.com/help/articles/204897248-Guide-to-Slack-import-and-export-tools).

#### Are my direct messages and private channel conversations completely private?

Slack is the business-provided internal communications tool to use for collaboration and connecting with team members.
Please keep [GitLab values](/handbook/values/) in mind when communicating directly with other team members.
If you have a confidential personal issue that you do not feel comfortable discussing via a business-provided internal communications tool, it is recommended to use a personal form of communication such as a text message or phone call.
For additional questions, please address in the [issue](https://gitlab.com/gitlab-com/business-ops/change-management/issues/3).

#### Need to add a new app to Slack

GitLab has choosen to restrict the ability to install apps, and we have a process to approve or restrict certain apps for our workspace. In order to add a new app to Slack, you need to create a [vendor approval issue](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=App_Integrations). Once that's approved by all parties, please request approval to add the app to Slack following the steps below:

1. Make sure the app hasn't been pre-approved by our team by clicking on Apps in the left sidebar and find Available Apps. To find pre-approved apps in the App Directory, click Pre-Approved below Categories  in the left column.
2. If the app isn't pre-approved, you can click on Add to Slack.
3. Add a custom message with more context about your request and also link the vendor approval issue.
4. Click Submit. You'll receive a direct message from Slackbot when your request has been reviewed by the team.

**Please note that this is only required for new apps that have not been reviewed or approved.** If your request is to add a new  process or update an existing process for how an application works in slack, please refer to our [Business Technology Change Management](https://about.gitlab.com/handbook/business-ops/business-technology-change-management/) process.

### Emergency Chat
#### Slack is down
{:.no_toc}

Use the "Slack Down!" group chat on Zoom.
  1. In the Zoom desktop app go to the *Contacts* tab
  1. Click `+`
  1. Click "Join a Channel"
  1. Search "Slack down!"
  1. Click "Join"

Once service is restored, go back to Slack.

#### Zoom is down
{:.no_toc}

Use Slack calls.
  1. Navigate to the appropriate Slack channel or direct message.
  1. Use `/call` to trigger a call.
  1. You may need to give permissions if it's the first time you are using Slack calls.

Once service is restored, go back to Zoom.

#### Slack and Zoom are down
{:.no_toc}

Join the [Slack Down!](https://chat.google.com/preview/room/AAAAGAd_BaQ) room on Hangouts Chat.
Once service is restored, go back to Slack and Zoom.

## Google Docs

Never use a Google Doc / Presentations for something non-confidential that has to end up on the website or the **handbook**. Work on these edits via commits to a merge request. Then link to the merge request or diff to present the change to people. This prevents a duplication of effort and/or an out of date handbook.

### Link Sharing

If you _do_ need a Google Doc, create one with your company G Suite (formerly Google
Apps) account and set the visibility and access controls according to the following guidelines:

| **Visibility Setting** | **Use Cases** |
| --- | --- |
|   On - Public on the web  |  If you want the document to be discoverable by anyone on the internet.  |
|   On - Anyone with the link  |  Avoid this setting. Instead, choose **On - GitLab**, then explicitly share the document with desired external individuals. Only use this if you want the document to be public but not indexed by Google.   |
|   **On - GitLab (Recommended Default)**  |   This is the recommended default as it allows anyone within GitLab to easily discover documents via searching for their name within Drive.  |
|   On - Anyone at GitLab with the link  |   Avoid this option as it limits discoverability by others at GitLab.  |
|   Off - Specific people  |  When the document contains highly sensitive or private information such as 1:1s with direct reports  |


| **Access Setting** | **Use Cases** |
| --- | --- |
|   Can Edit  |  Anyone that can view the document can edit it. This is the recommended setting when **On - GitLab** is enabled for the document  |
|   Can Comment  |   Anyone that can view the document can add a comment but cannot edit the document. This is ideal if you want to provide visibility but retain more fine-grained control of document editing.  |
|   View  |  Individuals with access to the document will only be able to view it.  |

The recommended defaults when sharing a document for GitLab internal purposes is setting visibility to **On - GitLab** and access to **Can Edit**. Reference Google's [documentation](https://support.google.com/drive/answer/2494822?visit_id=637102981721473693-3129607436&p=link_sharing_options&hl=en&rd=1#link_sharing) on Link Sharing to learn more.

### Good Practices & Helpful Tips

1. If you have content in a Google Doc that is later moved to the website or handbook, [deprecate the Google Doc](#how-to-deprecate-a-google-doc).
1. When referring to a Google Doc or folder on the Google Drive in the
   handbook, refrain from directly linking it. Instead, indicate the name of
   the doc. If you link the URL people from outside the organization can
   request access, creating workload and the potential for mistakes.  (In the
   past, linking a Google Doc has led to inadvertently opening the sharing
   settings beyond what was intended.) This also helps prevent spam from people
   outside GitLab requesting access to a doc when clicking its link. To save
   people time you can also [link to the search results
   page](https://drive.google.com/drive/search?q=%22this%20is%20a%20link%20to%20the%20search%20results%20page%22)
   which allows people to quickly get to the doc without anyone being able to
   request access.
1. If you are having trouble finding a shared Google Doc, make sure you [Search &lt;your domain&gt;](https://support.google.com/a/answer/3187967?hl=en) in Google Drive.
1. In our handbook, if you find yourself wondering whether it is better to provide a public link to a Google Doc vs. writing out the content on the website, use the following guideline: Is this document frequently adapted / customized? If yes, then provide a link, making sure that the document can be _commented on_ by _anyone_ with the link. For instance, this is how we share our employment [contracts](/handbook/contracts/). If the document is rarely customized, then provide the content directly on the site and deprecate the Google Doc.
1. If you want to quickly find where a team member's cursor is in a Google Doc, click their icon at the top of the document and the app will jump you to the current location. This works in Sheets and Presentations as well.
1. You can set the notifications on a Google Doc so you only get emailed when someone tags you directly instead of getting emails for every comment. Click on "notifications" and select "Only yours". By the way, when you create the doc, it defaults to All, but when you are just shared with it, it defaults to Only yours. There is [no global default](https://productforums.google.com/forum/#!msg/docs/1C3PZX1AY7Q/6EXXQKQSPCMJ).
![Google Doc Notifications](./google-docs-notifications.png).
1. You can find a template of the [GitLab letterhead](https://docs.google.com/document/d/1gN1Z2FHPIfPk7QLIey1KF9dR1yTl0R7QrMSb5_Iqfh4/edit) on the shared Google Drive. Please be sure to make a copy of the document to your Google Drive before customizing the template for your use.
1. If you want to have the Google Doc be readable to the public, do not change the sharing settings from 'Editable by Everyone at GitLab', [publish the document](#how-to-publish-a-google-doc) instead.
1. In all cases, the sharing settings (who a document is shared with, if it is visible to the whole company, etc.) on a Google Doc should be the single source of truth for the confidentiality of the document.

### How to publish a Google Doc

1. Under the 'File' menu, go to 'Publish to the web'.
1. A new pop-up 'Publish to the web' will appear, click 'Publish'.
1. A confirmation will appear on the top of the doc, click 'OK'.
1. Back in the pop-up 'Publish to the web', a link to share will appear.
1. Add the link to the top of the document after the text: "Publicly shared via"
1. Use this link to share to your audience.

### How to deprecate a Google Doc

1. Add 'Deprecated: ' to the start of the title.
1. Remove the content you moved.
1. Add a link to the new location at the beginning of the doc/first slide/first tab.
1. Add a link to the merge request or commit that moved it (if applicable).

## Zoom

GitLab uses [Zoom](https://zoom.us/) for video communications.

### Usage guidelines

Please visit the [Tools and Tips handbook](/handbook/tools-and-tips/#zoom) for Zoom usage guidelines.

### Using Zoom for personal connection

[COVID-19](/handbook/total-rewards/benefits/covid-19) is impacting how team members connect and communicate with family.

Due to school closures, parents are tasked with being responsible for their children while at home. [Family and friends first, work second](/handbook/values/#family-and-friends-first-work-second) is an important Diversity, Inclusion & Belonging  sub-value. To that end, we are encouraging GitLab team members to allow their children to connect with other children around the world.

You can arrange these by [joining the `#kid-juicebox-chats` Slack channel](/company/culture/all-remote/informal-communication/#juice-box-chats) — a spin on our usual [Coffee Chats](/company/culture/all-remote/informal-communication/#coffee-chats).

During this time of physical distancing, GitLab team members are welcome to use Zoom to connect with family if other options like FaceTime, etc. are not an option.  Please ensure that attendees who are not GitLab team members have their own Zoom account. To ensure GitLab does not incur toll charges, please use Internet-based voice when possible.

### Zoom webinars

Please visit a detailed guide covering [everything you need to know about hosting or participating in a GitLab webinar](/handbook/communication/zoom/webinars/).

## Google Calendar

We recommend you set your Google Calendar access permissions to 'Make available for GitLab - See all event details'. Consider marking the following appointments as 'Private':

1. Personal appointments.
1. Confidential & sensitive meetings with third-parties outside of GitLab.
1. 1-1 performance or evaluation meetings.
1. Meetings on organizational changes.

There are several benefits and reasons to sharing your calendar with everyone at GitLab:

1. Transparency is one of our values and sharing what you work on is in line with our message of "be open about as many things as possible".
1. Due to our timezone differences, there are small windows of time where our availabilities overlap. If other members need to schedule a new meeting, seeing the details of recurring meetings (such as 1-1s) will allow for more flexibility in scheduling without needing to wait for a confirmation from the team member. This speaks to our value to be more efficient.

	![Google Calendar - make calendar available setting](/images/handbook/tools-and-tips/google-calendar-share.png)

If you add blocks of time spent on recurring tasks to your Google Calendar to remind yourself to do things (e.g. "Check Google Analytics"), consider marking yourself "Free" for those events so that coworkers know they may schedule a meeting during that time if they can't find another convenient time.

## Posting or Streaming to YouTube

See the [YouTube page](/handbook/marketing/marketing-operations/youtube/) for options and instructions for posting recordings and live streaming to our YouTube channels.


## Communication Certification

Anyone can become certified in GitLab Communication. To obtain certification, you will need to complete this [quiz](https://docs.google.com/forms/d/e/1FAIpQLScyyIQDdC3oN-H6-IJJM1QlZNHaGPI0jESb9ogAfkQlMzKgwQ/viewform) and earn at least an 80%. Once the quiz has been passed, you will receive an email with your certification that you can share on your personal LinkedIn or Twitter pages. If you have questions, please reach out to our L&D team at `learning@gitlab.com`.
