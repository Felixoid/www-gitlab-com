---
layout: handbook-page-toc
title: Benefits
description: Find answers to your questions about GitLab's benefits.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Related Benefits Pages
* [General & Entity Specific Benefits](/handbook/total-rewards/benefits/general-and-entity-benefits)
* [Modern Health](/handbook/total-rewards/benefits/modern-health)
* [Global Benefits Survey](/handbook/total-rewards/benefits/benefits-survey)
* [Resources for COVID-19](/handbook/total-rewards/benefits/covid-19)

## Introduction

On this page, you can find information about the principles that guide GitLab's benefit strategies and decisions.

## Guiding Principles

These principles will guide our benefit strategies and decisions.

* **Collaboration**
  - Work with providers, vendors, and global networks to benchmark benefit data from other similar sized companies.
  - Foster cross-company understanding.
* **Results**
  - Evangelize benefit programs in each entity.
  - Transform “statutory” to “competitive.”
  - Make benefits a very real aspect of compensation during the hiring process.
  - Measure employee engagement and benefit enrollment.
* **Efficiency**
  - Use the GitLab Inc (US) benefits as a starting point, and try to stay as consistent as possible within the US baseline.
  - Iterate on changes for specific countries based on common practices and statutory regulations.
* **Diversity, Inclusion & Belonging **
  - Actively work to ensure that our benefit choices are inclusive of all team members.
* **Iteration**
  - Regularly review current benefits (bi-annually) and propose changes.
  - Everything is always in draft.
* **Transparency**
  - Announce and document benefits to keep them updated.
  - Share upcoming benefit plans internally before implementing them.
  - Invite discussion and feedback.

We value opinions but ultimately People Operations/Leadership will make the decision based on expert advice and data.

### Guiding Principles in Practice

When establishing a new co-employer or entity we will outline the following benefits as to why it is or is not offered.

1. Medical
1. Pension
1. Life Insurance

We do not have specific budgets around benefit costs, but instead look to increasing the ability to recruit and retain team members in favorable locations. We do not take a global approach to offering the same benefits in all countries, but will transparently outline why we do or do not offer the above benefits on their respective [entity specific benefits](/handbook/total-rewards/benefits/general-and-entity-benefits/#entity-specific-benefits) page.
