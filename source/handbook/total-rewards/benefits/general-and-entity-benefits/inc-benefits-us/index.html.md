---
layout: handbook-page-toc
title: "GitLab Inc (US) Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Specific to US based team members
{: #us-specific-benefits}

US based benefits are arranged through [Lumity](https://lumity.com/). The benefits decision discussions are held by People Operations, the CFO, and the CEO to elect the next year's benefits by the carrier's deadlines. People Operations will notify the team of open enrollment as soon as details become available.  

Please review the full [summary plan description](https://drive.google.com/file/d/1K8uybZ_pQc-XxpOaIEqnSN-UlvZCsf1W/view?usp=sharing) of all related health benefits.   

If you have any questions regarding benefits please reach out to People Operations directly. If you have any questions with the Lumity platform, feel free to reach out to their support personnel at `support@lumity.com`.

Carrier ID cards are normally received within weeks of submitting your benefit elections. If you or your medical providers are in need of immediate confirmation of your coverage, please contact the carrier directly. ID cards are also available within the [Lumity](https://www.lumity.com/) platform or through the Lumity mobile app: [IOS](https://apps.apple.com/us/app/lumity-employee-benefits/id1460901256) or [Android](https://play.google.com/store/apps/details?id=com.lumity.mobile).

If you have existing coverage when joining GitLab (e.g. coverage for an additional month from your prior employer), you have the option of enrolling in GitLab's coverage after your prior coverage terminates. If you wish to do this, you should register with Lumity during onboarding and waive all coverages. Once your previous coverage is terminated, you should sign up for coverage through Lumity on or within 30 days of the termination date by initiating a Qualifying Life Event and providing proof of coverage termination.

GitLab covers **100% of team member contributions and 66% for spouse, dependents, and/or domestic partner** of premiums for medical, dental, and vision coverage. Plan rates are locked through December 31, 2020.

More information on the processed deductions in payroll from Lumity can be found under [Lumity Payroll Processes for GitLab Inc.](/handbook/finance/accounting/#lumity-payroll-processes-for-gitlab-inc) on the [Accounting and Reporting page](/handbook/finance/accounting/).

## Group Medical Coverage

GitLab offers plans from United Health Care for all states within the US as well as additional Kaiser options for residents of California, Hawaii, and Colorado. Deductibles for a plan year of 2020-01-01 to 2020-12-31.  

For additional information on how [Benefits](https://drive.google.com/file/d/0B4eFM43gu7VPVS1aOFdRdXRSVzZ5Z3B0dEVKUXJIWm1zZXRj/view?usp=sharing) or [HSAs](https://drive.google.com/file/d/0B4eFM43gu7VPbUo1VFNFVFlQNlUyV0xQZkE0YXQyZENSNU1j/view?usp=sharing) operate, please check out the documentation on the Google Drive or Lumity's [resources](https://employee-resources.lumity.com/help).

_If you already have current group medical coverage, you may choose to waive or opt out of group health benefits. If you choose to waive health coverage, you will receive a $300.00 monthly benefit allowance and will still be able to enroll in dental, vision, optional plans, and flexible spending accounts._

If you do not enroll in a plan within your benefits election period, you will automatically receive the medical waiver allowance.

GitLab has confirmed that our medical plans are CREDITABLE. Please see the attached [notice](https://docs.google.com/document/d/1qFK-MnErpOSTXli4G_c0ze-UeAU4szAx-GMoNnizQ_4/edit?usp=sharing). If you or your dependents are Medicare eligible or are approaching Medicare eligibility, you will need this notice to confirm your status when enrolling for Medicare Part D.

## Qualifying Life Events

Due to IRS guidelines, you cannot make changes to your health insurance benefits outside of GitLab's annual open enrollment period unless you experience a [Qualifying Life Event](https://employee-resources.lumity.com/help/qualifying-life-event).  

A QLE is a change in your situation — like getting married, having a baby, etc that can make you eligible for a special enrollment period. You have ***30 days from the date of your qualifying event*** to submit your requested change to Lumity. You will be asked to include supporting documents (see [Required Documents](https://employee-resources.lumity.com/help/qualifying-life-event)), and list the date the change occurred.

You can start the process by selecting the "Qualifying Events" tile on the [Lumity Dashboard](https://lumity.com/). Lumity support can be contacted directly with any questions. 

### Eligibility

Any active, regular, full-time team member working a minimum of 30 hours per week are eligible for all benefits. Benefits are effective on your date of hire. Others eligible for benefits include:
  * Your legal spouse or domestic partner,
  * Your dependent children up until age 26 (including legally adopted and stepchildren), and/or
  * Any dependent child who reaches the limiting age and is incapable of self-support because of a mental or physical disability

Note: If you and an eligible dependent (as defined above) are both employed by GitLab, you may only be covered by GitLab’s coverage once. This also applies to enrolling in either your own supplemental life insurance or supplemental spouse/dependent life insurance through your dependent who is employed by GitLab, but not both.

### United Health Care Medical Plans

To determine your specific plan name, log in to the Lumity system and click "My Benefits" - View Button > Plan Name (e.g. GitLab UHC HSA 2000 + Infertility). The benefits summary that comes up has the network name IN the plan name (for example: Select Plus PPO HDHP / California  - Select Plus / HSA - Plan AXEX).

When reviewing the UHC [Find a Doctor site](https://www.uhc.com/find-a-physician), you will get to the Network selection page and select the appropriate network - Select Plus or Select, based on plan enrollment.


#### UHC 2020 Calendar Year Plans

**Coverages:**

In Network:

| Plan Details               | [UHC - HSA](https://drive.google.com/open?id=1w0RTAyvhhhLxg6lZC-HuCNum2PVsOC-l)              | [UHC - EPO**](https://drive.google.com/open?id=1d3fFvtnEMIocCprS0e_jFOrf5JmMAW7z)        | [UHC - PPO](https://drive.google.com/open?id=1t7qhKfZzQWQtT1G8MJY733bQHTVyMHtA)         |
|:---------------------------|:----------------------:|:------------------:|:-----------------:|
| Deductible (Single/Family) | $2,000 / $2,800        | $0 / $0            | $500 / $1,000     |
| OOP Max (Single/Family)    | $4,000 / $4,000        | $2,500 / $5,000    | $3,000 / $6,000   |
| Primary Care Visit         | 20%                    | $20 per visit      | $20 per visit     |
| Specialist Visit           | 20%                    | $20 per visit      | $20 per visit     |
| Urgent Care                | 20%                    | $50 per visit      | $50 per visit     |
| Emergency Room             | 20%                    | $100 per visit     | $100 per visit    |
| Hospital Inpatient         | 20%                    | $250 per admission | 10%               |
| Hospital Outpatient        | 20%                    | 0%                 | 10%               |
| Generic                    | $10                    | $10                | $10               |
| Brand - Preferred          | $30                    | $30                | $30               |
| Brand - Non-Preferred      | $50                    | $50                | $50               |
| Specialty Drugs            | $50                    | 20% up to $250     | 20% up to $250    |
| Rx Plan                    | [C2-HSA***](https://drive.google.com/file/d/1f9exxBPTlrEy0ZTMXC8-r6WP40_tCIHX/view?usp=sharing)                 | [Rx Plan: 464***](https://drive.google.com/file/d/1f9exxBPTlrEy0ZTMXC8-r6WP40_tCIHX/view?usp=sharing)    | [Rx Plan: 464***](https://drive.google.com/file/d/1f9exxBPTlrEy0ZTMXC8-r6WP40_tCIHX/view?usp=sharing)      |

** At this time, coverage by the EPO is not available in all states. Where an EPO is not available, an additional PPO plan that is an in-network copayment is available for selection.

*** Due to COVID-19, the following changes have been made to the linked Prescription Drug List:
 * The effective date of the exclusion of these medications is being extended from 5/1 to 7/1 for the following:
   * Diabetes – Insulin: Basaglar KwikPen, Levemir, Levemir FlexTouch, Tresiba (will remain in current tier)
Medications that will remain excluded until 7/1: Lantus, Lantus SoloSTAR, Toujeo Max SoloSTAR, and Toujeo SoloSTAR.
   * Diabetes – Non-Insulin: Janumet, Janumet XR, Januvia.
   * Neuromuscular Disorders: Firdapse.
 * The effective date is being updated from 5/1/20 to 7/1/20 for New Step Therapy for Zomig.
 * The following asthma/respiratory medications will not be excluded at this time. These medications will be continued to be covered:
   * Arnuity Ellipta®
   * Flovent® Diskus®, Flovent® HFA
   * Pulmicort Flexhaler®

Please contact UHC for any questions on the above.

**Team Member Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                     | HSA | EPO  | PPO  |
|--------------------------|:---:|:----:|:----:|
| Team Member Only         | $0  | $0   | $0   |
| Team Member + Spouse     | $0  | $214 | $204 |
| Team Member + Child(ren) | $0  | $164 | $156 |              
| Family                   | $0  | $365 | $348 |

Note: For the **team member only HSA**, GitLab will contribute $100 per month. For residents of California, Alabama, and New Jersey this additional contribution is taxable on the state level. If you are 55 or older and would like to contribute an additional amount for the HSA catch up, please reach out to support@lumity.com and People Ops at GitLab.

Domestic Partner Reimbursements: If the team member is not legally married to their domestic partner, the domestic partner’s expenses are not eligible for disbursement from the HSA. However, if the domestic partner is covered under the family HDHP through the team member, the domestic partner can open their own HSA and contribute up to the full family contribution maximum. The team member may also contribute up to the full family contribution maximum to their own HSA.

#### UHC Telehealth

Telehealth services for UHC members can be accessed by signing into [MyUHC](https://www.myuhc.com/) and selecting "Connect with a Doctor Online." Estimated out-of-pocket cost is disclosed on the virtual visit platform. Currently costs related to this service are waived through June 18, 2020. 

#### UHC Period to Submit Claims

For in-network services: 90 days from Date of Service.

For out-of-network services: 365 days from Date of Service.

### Kaiser Medical Plans

#### Kaiser 2020 Calendar Year Plans  

**Coverages:**

| Plan Details               | [HMO 20 NorCal](https://drive.google.com/open?id=1utx4DFMtnm1ZYwZtMZWyyFk3xyr2x0_V)        | [HMO 20 SoCal](https://drive.google.com/open?id=15uXcg4mwHNhfEOLRuxMyHVCz40sEG-RV)     | [HMO 20 CO](https://drive.google.com/open?id=1TPzU601w7P_b2OQpQzmB87x2wijELGg9)      | [HMO 20 HI](https://drive.google.com/open?id=1Vyg3NMnDPJn1vsQyysd5k39FDeuHdJsx)       |
|----------------------------|:--------------------:|:---------------:|:---------------:|:---------------:|
| Deductible (Single/Family) | $0 / $0              | $0 / $0         | $0 / $0         | $0 / $0         |
| OOP Max (Single/Family)    | $1,500 / $3,000      | $1,500 / $3,000 | $2,000 / $4,000 | $2,500 / $7,500 |
| PCP/Specialist Copay       | $20 / $35            | $20 / $35       | $20 / $35       | $15 / $15       |
| Emergency Room             | $50                  | $100            | $250            | $100            |
| Urgent Care                | $20                  |                 | $50             | $15             |
| Hospital Inpatient         | $250/admit           | $250/visit      | $300/admit      | 10%             |
| Hospital Outpatient        | $35/procedure        | $35/procedure   | $100/procedure  | 10%             |
| **Rx - Deductible**        |                      |                 |                 |                 |
| Generic                    | $10                  | $10             | $10             | $0              |
| Brand - Preferred          | $35                  | $35             | $30             | $0              |
| Brand - Non-Preferred      | $35                  | $35             | $50             | $0              |
| Specialty Drugs            | 20% up to $150       | $35             | 20% up to $150  | $0              |

**Team Member Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                     | HMO CA North | HMO CA South | HMO CO | HMO HI |
|--------------------------|:------------:|:------------:|:------:|:------:|
| Team Member Only         | $0           | $0           | $0     | $0     |
| Team Member + Spouse     | $228         | $228         | $264   | $168   |
| Team Member + Child(ren) | $180         | $180         | $216   | $132   |          
| Family                   | $348         | $348         | $432   | $324   |

#### Kaiser Telehealth

Virtual visits for Kaiser members can be accessed by logging into Kaiser's [online portal](https://healthy.kaiserpermanente.org/). Please consult the online portal and your plan details for your copay amount. 

#### Kaiser Period to Submit Claims

For in-network services: N/A.

For out-of-network services: 365 days from Date of Service.

### Infertility Services

Infertility services are provided through the UHC HSA plan. Please consult the legal SBC for more information or reach out to People Operations.

### Pregnancy & Maternity Care

With medical plans, GitLab offers pregnancy and maternity care. Depending on the plan you selected, your coverages may differ for in-network vs out-of-network, visits, and inpatient care. Please contact Lumity or People Operations with any questions about your plan. Once your child has arrived, please follow the steps outlined above in regards to this [Qualifying Life Event](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/qualifying-life-events).

## Dental

Dental is provided by Cigna, plan: DPPO.

Dental does not come with individualized insurance cards from Cigna, although you can download them by setting up a Cigna account through the [Cigna website](https://my.cigna.com). Lumity's site will house individualized ID cards team members can access at any time. For the most part, dental providers do not request or require ID cards as they look up insurance through your social security number. If you need additional information for a claim please let People Ops know. Cigna'a mailing address is PO Box 188037 Chattanooga, TN, 37422 and the direct phone number is 800-244-6224.

When submitting a claim, you can mail it to Cigna Dental PO Box 188037 Chattanooga, TN, 37422 or fax it to 859-550-2662.

### Dental 2020 Calendar Year Plan

**Coverages:**

| Plan Details                         | [DPPO](https://drive.google.com/open?id=1qkqT5khP4RV5W-r0WGex9SQqzPFoOYw-)       |  
|--------------------------------------|:----------:|
| Deductible                           | $50 / $150 |
| Maximum Benefit                      | $2,000     |
| Preventive Care CoInsurance (in/out) | 0% / 0%    |
| Basic Care Coinsurance (in/out)      | 20% / 20%  |
| Major Care Coinsurance (in/out)      | 50% / 50%  |
| Out of Network Reimbursement         | 90th R&C   |
| **Orthodontia**                      |            |
| Orthodontic Coinsurance (in/out)     | 50% / 50%  |
| Orthodontic Max Benefits             | $1,500     |

**Team Member Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                     | DPPO |
|--------------------------|:----:|
| Team Member Only         | $0   |
| Team Member + Spouse     | $12  |
| Team Member + Child(ren) | $18  |           
| Family                   | $36  |

#### Cigna Dental Period to Submit Claims

For in-network services: N/A.

For out-of-network services: 365 days from Date of Service.

## Vision

Vision is provided by Cigna.

When submitting a claim, you can mail it to Cigna Vision PO Box 385018 Birmingham, AL 35238 or submit it online using the following instructions:

1. Log in or register an account at https://cigna.vsp.com/.
1. Navigate to "Claims & Reimbursement" on the left panel.
1. Choose yourself or dependent from the dropdown depending who the claim is for.
1. Expand the "Customer Reimbursement Form" section.
1. Click "Continue" to be taken to the online claim form. Make sure you attach an itemized receipt when prompted.

### Vision 2020 Calendar Year Plan

**Coverages:**

| Plan Details                      | [Vision](https://drive.google.com/open?id=1grzcuDd0CiuDQthZ2SAGyOYUEQJro1yB)       |  
|-----------------------------------|:------------:|
| Frequency of Services             | 12 / 12 / 12 |
| Copay Exam                        | $20          |  
| Copay Materials                   | -            |
| Single Vision                     | $0           |  
| Bifocal                           | $0           |
| Trifocal                          | $0           |
| Frame Allowance                   | up to $130   |
| Elective Lenses Contact Allowance | up to $130   |

**Team Member Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                     | Vision |
|--------------------------|:------:|
| Team Member Only         | $0     |
| Team Member + Spouse     | $2.40  |
| Team Member + Child(ren) | $1.80  |           
| Family                   | $4.80  |

#### Cigna Vision Period to Submit Claims

For in-network services: 365 days from Date of Service.

For out-of-network services: 365 days from Date of Service.

## Discovery Benefits

If you are enrolled in an HSA, FSA, or commuter benefits, the funds are held through Discovery Benefits. FSAs have a $550 rollover each calendar year.

You will only receive on debit card upon enrollment. To obtain a second card (for a dependent, etc.) you will need to login to your account on Discovery or call and they will send one to your home of record.

If you would like to transfer your HSA from a previous account, please contact Discovery Benefits and request a HSA Transfer funds form. On the form you will put your old HSA provider’s account number and any other required personal information. You will then submit the form to Discovery, and they will get in contact with your old HSA provider and process the transfer of funds. You can reach Discovery Benefits at 866.451.3399.

If you would like to adjust your HSA contributions please log into [Lumity](https://lumity.com/).

## Basic Life Insurance and AD&D

GitLab offers company paid [basic life](https://drive.google.com/open?id=1wl62igCepQ23kjF8QnyVLsvorkr7joxP) and [accidental death and dismemberment (AD&D)](https://drive.google.com/open?id=1wl62igCepQ23kjF8QnyVLsvorkr7joxP) plans through UHC. The Company pays for basic life insurance coverage valued at two times annual base salary with a maximum benefit of $250,000, which includes an equal amount of AD&D coverage.

## Group Long-Term and Short-Term Disability Insurance

GitLab provides a policy through UHC that may replace up to 66.7% of your base salary, for qualifying disabilities. For [short-term disability](https://drive.google.com/open?id=12_xDzUIuBgSJB0AJ6I0oE2VEgMdwymWx) there is a weekly maximum benefit of $2,500; for [long-term disability](https://drive.google.com/open?id=14uIt3CUabd4Yj95BDgNl9Rdfzi6SxrFW) there is a monthly benefit maximum of $12,500.

**Process for Disability Claim**

1. If an team member will be unable to work due to disability for less than 25 calendar days, no action is needed and the absence will be categorized under [paid time off](/handbook/paid-time-off/).
1. Since the short-term disability insurance has a 7-day waiting period, the team member should decide on day 18 whether they will be able to return to work after 25 calendar days. If they will not be able to return, they should inform the Compensation & Benefits team of their intent to go on short-term disability and apply for short-term disability at this time by sending the Compensation & Benefits team a completed [Leave Request Form](https://drive.google.com/open?id=1BZKHKPHvmMoOS1ctjXG3q6RHk8RHYuv1). While the team member is on short-term disability (which covers 66.7%), GitLab will supplement the remaining 33.3% through payroll if the team member has been employed for more than a year. Benefit coverage will also continue for the time the team member is on short-term disability.
1. At the end of the maximum benefit period for short-term disability of 12 weeks, the team member will determine whether they are able to return back to work. If they are unable to, the team member will be moved to unpaid leave and will have the option to continue their benefits by electing [COBRA coverage](https://www.dol.gov/sites/dolgov/files/ebsa/about-ebsa/our-activities/resource-center/faqs/cobra-continuation-health-coverage-consumer.pdf). The team member will be eligible to apply for long-term disability at this time.

## Employee Assistance Program

GitLab team members in the United States are eligible for a complementary [Employee Assistance program](https://drive.google.com/file/d/1MDx0YrIVKq91QUjj-eu8TZ9GcSLi3o6G/view?usp=sharing) as a result of enrollment in the long-term disability plan through UHC. More information can be found in the [UHC brochure](https://drive.google.com/open?id=1mR-vUVOFSIuaB7JvWc4UrGQuYHzDyREh) for the following benefits: counseling services, help with financial and legal issues, family support, help with relationships, coping and depression.

## 401k Plan

The company offers a 401k plan in which you may make voluntary pre-tax contributions toward your retirement.

### Administrative Details of 401k Plan

1. You are eligible to participate in GitLab’s 401k as of your hire date. There is no auto-enrollment. You must actively elect your deductions.
1. You will receive an invitation from [Betterment](https://www.betterment.com) who is GitLab's plan fiduciary. For more information about Betterment please check out this [YouTube Video](https://www.youtube.com/watch?v=A-9II-zBq1k).
1. Any changes to your plan information will be effective on the next available payroll.
1. Once inside the platform you may elect your annual/pay-period contributions and investments.
1. Please review the [Summary Plan Document](https://drive.google.com/file/d/1k1xWqZ-2HjOWLv_oFCcyBkTjGnuv1l_B/view?usp=sharing) and [QDIA & Fee Disclosure](https://drive.google.com/file/d/1gWxCI4XI01gofUu9yqKd4QyXeM27Xv57/view?usp=sharing). If you have any questions about the plan or the documents, please reach out to People Ops.
1. ADP payroll system monitors and will stop the 401(k) contribution when you have reached the IRS limit for the year.

### 401(k) Match

GitLab offers matching 50% of contributions on the first 6% of allocated earnings with a yearly cap of 1,500 USD. As you are eligible to participate in GitLab's 401k as of your hire date, you are *also* eligible for GitLab matching contributions as of your hire date.

All employer contributions are pre-tax contributions. Team members can still make Roth team member contributions and receive pre-tax employer contributions.

**Vesting:**

Employer contributions vest according to the following schedule:

| Years of Vesting Service             | Vesting Percentage |
|--------------------------------------|:------------------:|
| Less than One Year                   | 0%                 |
| One Year but less than Two Years     | 25%                |
| Two Years but less than Three Years  | 50%                |
| Three Years but less than Four Years | 75%                |
| Four or More Years                   | 100%               |

*Employee* contributions are the assets of those team members and are not applicable to this vesting schedule.

**Vesting example**

To help you understand the math, here is a hypothetical vesting chart showing how much the employeed would get if they left the company.

In this example the employee's salary is $50,000 USD and they max out their match every year. 50,000 * 6% = 3000. 3000 * 50% = 1500 match.

Year 1: Put in 3K, GitLab matches 1500. Leave the company get 3K (none vested)
Year 2: Put in 3K, GitLab matches 1500. Leave the company get 6K own money + $750 USD vested contribution.   (3K * 0.25)
Year 3: Put in 3K, GitLab matches 1500. Leave the company get 9K own money + $2250 USD vested contribution. (4500 * 0.50)
Year 4: Put in 3K, GitLab matches 1500. Leave the company get 12K own money + $4500 USD vested contribution. (6000 * 0.75)
Year 5: Put in 3K, GitLab matches 1500. Leave the company get 15K own money + $7500 USD (fully vested)

**Administration of the 401(k) Match:**
* The employer will use the calculation on each check date effective as of January 1, 2019.
* The team member must have a contribution for a check date to be eligible for the employer match.
* Employer matching will be released into participant accounts three business days after the check date.
* For team members who defer more than 6% on each check date, Payroll and People Operations will conduct a true up quarterly.

### 401(k) Committee

The 401(k) Committee will meet quarterly with Betterment to review how the plan is doing as well as updates from the Betterment investment team.

**Committee Memebers:**
Chair: Principal Accounting Officer (PAO)
**Other Members:**
* Principal Accounting Officer
* Chief Legal Officer
* Manager, Total Rewards
* Senior Director, People Success
* Senior Manager, Payroll and Payments

**Gitlab's 401(k) Committee Responsibilities:**
* Maintain and enforce the plan document and features
* Comply with all reporting, testing and disclosure requirements
* Timely data & deposit transmission
* Evaluating plan services and fees
* Review and approval of prepared forms and distributions
* Employee engagement and eligibility
* Adding new eligible employees to the plan

**Betterment's Responsibilities (co-fiduciary):**
* Investment selection and monitoring
* Audit support
* Employee education
* Administrative support on distributions, loans and more
* Employee and Plan Sponsor customer support via email and phone
* Statement and tax form generation

## Optional Plans Available at Team Member Expense

### Flexible Spending Account (FSA) Plans

FSAs help you pay for eligible out-of-pocket health care and dependent day care expenses on a pretax basis. You determine your projected expenses for the Plan Year and then elect to set aside a portion of each paycheck into your FSA. 

#### Special Notice for Dependent Care Flexible Spending Accounts (DCFSA) during COVID-19

Currently, many people are experiencing their child care provider having canceled or limited services due to COVID-19 and are unsure how they are going to use the entire amount elected for their DCFSA. Per IRS regulations, this circumstance would permit you to make changes to your DCFSA as the Qualifying Life Event: change in child care or elder care provider, change in cost, or change in coverage.

In order to submit this Qualifying Life Event, you will need documentation such as an email or letter from your child care provider that highlights the canceled or limited services as of a certain date. You can process this by selecting the "Qualifying Events" tile on the [Lumity Dashboard](https://lumity.com/) and uploading your documentation. Lumity support can be contacted directly with any questions relating to this. 

#### FSA Period to Submit Claims

Up to 90 days after the plan year has concluded (also known as the runout period).

### Supplemental Life/Accidental Death and Dismemberment Insurance

If you want extra protection for yourself and your eligible dependents, you have the option to elect [supplemental life insurance](https://drive.google.com/open?id=1wl62igCepQ23kjF8QnyVLsvorkr7joxP) through UHC:
   * $10,000 Increments up to the lesser of 6x annual salary or $750,000 for team members
   * $5,000 Increments up to the lesser of $250,000 or 100% of team member election for spouses and domestic partners
   * $10,000 of coverage available for children

Certain coverage increases must be approved by the insurance carrier. For more information on completing an Evidence of Insurability, please reference the [EOI flier](https://drive.google.com/drive/u/0/folders/1xUo17QFevsWJ71aAd36ibuUakXaTzasJ)

### Commuter Benefits

GitLab offers [commuter benefits](https://drive.google.com/file/d/0B4eFM43gu7VPek1Ia0ZqYjhuT25zYjdYTUpiS1NFSXFXc0Vn/view?usp=sharing) which are administered through Discovery Benefits. The contribution limits from the IRS for 2020 are $270/month for parking and $270/month for transit. These contributions rollover month to month.

#### Commuter Benefits Period to Submit Claims

For active employees: Up to 180 days after plan year has concluded (also known as the runout period).

For terminated employees: Up to 90 days after the termination date. 

## Team Member Discount Platforms

US team members have access to two discount platforms offered through ADP and Lumity. These platforms provide discounts for national and local brands and services.

To access LifeMart through ADP:
1. Login to ADP using the following link: [(https://workforcenow.adp.com.)]
1. Click on the "MYSELF" tab in the navigation bar, hover your mouse over "Benefits" in the dropdown menu, and click "Employee Discounts - Life Mart".
1. Confirm the email you use to access ADP and click "View my discounts" to enter the website.

To access PerkSpot through Lumity:
1. Navigate to https://lumity.perkspot.com/.
1. Click "Create an Account" and fill out the form in order to register.

## Monthly Health Bill Payments

The Total Rewards Analyst will review and initiate payment for all monthly health bills in the United States.

* All bills are available on the first of the month and should be paid by the 13th.
* Lumity will send a reconciliation report breaking down the bills by department. People Ops will transfer the department breakdown and Group Invoice to the "Lumity Bill Reconciliations" google sheet.
* TODO Build in audit procedure to verify bills against current elections to ensure accuracy.
* People Operations will login to each admin platform and pay the bills using the banking information and links found in the 1password note: "Monthly Heath Bills"
* People Operations will then email a detailed breakdown of the amount/department totals to `ap@gitlab.com` for accounting purposes.

## GitLab Inc. United States Leave Policy:

Based on the Family and Medical Leave Act, or [FMLA](https://www.dol.gov/whd/fmla/), US team members are "eligible to take job-protected leave for specified family and medical reasons with continuation of group health insurance coverage under the same terms and conditions as if the team member had not taken leave." For more information on what defines an eligible team member and medical reason please visit the [Electronic Code of Federal Regulation](http://www.ecfr.gov/cgi-bin/text-idx?c=ecfr&sid=d178a2522c85f1f401ed3f3740984fed&rgn=div5&view=text&node=29:3.1.1.3.54&idno=29#sp29.3.825.b) for the most up to date data.

### Apply For Parental Leave in the US

1. Notify the Compensation and Benefits team of intention to take parental leave at least 30 days in advance, or as soon as reasonable.
    - For a birthing parent (maternity leave), the team member will fill out the [Leave Request Form](https://drive.google.com/open?id=1BZKHKPHvmMoOS1ctjXG3q6RHk8RHYuv1) and email the form to total-rewards@ domain.
      - Add your name, SSN, address, phone number, date of birth, gender, and marital status to the General Demographics box in the Employer section. The Compensation and Benefits team will fill out the rest of the form upon submission.
      - Team member will need to populate the Employee section (pages 3-5) and have their physician complete the "Attending Physician's Disability Statement" (page 8)
      - Review and sign the Disclosure Authorization (page 6). Also, complete the "Authorization of Personal Representative" section (page 7) if there is someone involved in your care that you would like to be able to contact UHC in relation to your disability claim.
      - The Compensation and Benefits team will finish populating the Employer section of the form.
      - Once the form is complete, the Compensation and Benefits team will email the form the UHC using the email address provided on the form.
      - Once the team member has filed a claim for STD, they will need to confirm the start and end dates of the STD.
      - The typical pay for STD is six weeks for a vaginal birth and eight weeks for a c-section. STD can be extended if the team member has a pregnancy related complication (bed rest, complications from delivery, etc).
      - TODO Outline letters to send and process to apply for STD.
    - For non-birthing parents (including maternity leave when adopting and paternity leave), email total-rewards@ domain with details of your planned leave.
1. The Compensation and Benefits team will confirm [payroll details](#payroll-processing-during-parental-leave) with the Controller.  
1. The team member will notify the Compensation and Benefits team on their first day back to work.
1. TODO Outline process to return the team member to work

### Payroll Processing During Parental Leave

**Paternity Leave**
Paternity Leave is not covered under Short Term Disability, so if the team member is eligible for 100% of pay, payroll would enter 100% of the pay period hours under "Leave with Pay."   

**Maternity Leave**
For maternity leave, GitLab will verify 100% of the wages are paid for eligible team members through payroll and Short-term Disability (STD).

1. While the team member is on short-term disability (which covers 66.7%), GitLab will supplement the remaining 33.3% through payroll.
  * The team member will inform People Operations when their short term disability is set to begin and end.
  * There is a seven day elimination period for short term disability that is not paid through Cigna, where GitLab will need to supplement the entire wage through payroll.
    * For instance, if Cigna has approved STD for January 1 - February 12, January 1 - January 7 would not be paid through STD.
    * If any adjustments need to be made to a payroll that has already passed, People Ops will coordinate with the Payroll Lead to ensure retroactive payments are made.
  * For the days the team member is paid through STD, payroll will adjust leave with pay hours to equal 33.3% and leave without pay hours to equal 66.7%.
    * For example, if there are 80 hours in the pay period you would input 26.66 hours Leave with Pay / 53.34 Leave without Pay.
1. When short-term disability ends, payroll will need to have 100% of the hours fall under leave with pay.

Note: Also, ensure the disability is not is not capped out at the current maximum or that will also need to be supplemented in each pay cycle.

## COBRA

If you are enrolled in medical, dental, and/or vision when you terminate from GitLab (either voluntarily or involuntarily), you may be eligible to continue your coverage through [COBRA](https://www.dol.gov/sites/dolgov/files/ebsa/about-ebsa/our-activities/resource-center/faqs/cobra-continuation-health-coverage-consumer.pdf).

### Timeline
1. Typically terminations are updated in BambooHR on the date of the termination and once updated, will sync with [Lumity](https://user.lumity.com/login) by the end of the next business day.
1. Once the termination has synced with Lumity, the information will then be sent over to [Discovery Benefits](https://www.discoverybenefits.com/), our COBRA administrator. Government guidelines give 30 days for Discovery to be notified of the COBRA eligibility, but typically this will take about 1-2 weeks.
1. Once notified, Discovery has 14 days to generate and send the COBRA enrollment packet. Allow normal mailing timelines (5-10 business days) to receive the packet once sent.
1. You will have 60 days from the time you receive the COBRA packet to enroll either through the mail or online. Instructions for how to enroll will be included in your COBRA packet. Coverage will be retro-effective to the date coverage was lost.
1. From the day you enroll, you have 45 days to bring your payments to current.
1. You may remain on COBRA for up to 18 months. Please see the COBRA enrollment packet for information on extending COBRA an additional 18 months, if applicable. The state you reside in may allow for additional time on COBRA, but may be more expensive and include only Medical. Please consult the laws for your state for more information.

If you are currently employed and have any general COBRA questions, feel free to contact the Compensation & Benefits team. If you have terminated already or have specific questions on the administration of COBRA, feel free to contact Discovery Benefits directly: (866) 451-3399.

### Costs per Month 

**Medical**

| Tier                           | UHC HSA   | UHC EPO (A/B) |  UHC PPO   | Kaiser HMO NorCal | Kaiser HMO SoCal | Kaiser HMO CO | Kaiser HMO HI |
|--------------------------------|:---------:|:-------------:|:----------:|:-----------------:|:----------------:|:--------------:|:------------:|
| Team Member Only               | $371.52   |   $497.25     | $496.12    |     $502.20       |     $502.20      |    $625.78     |   $481.83    |
| Team Member + Domestic Partner | $813.61   |   $1,088.96   | $1,086.49  |     $1,159.72     |     $1,159.72    |    $1,376.71   |   $963.66    |
| Team Member + Spouse           | $813.61   |   $1,088.96   | $1,086.49  |     $1,159.72     |     $1,159.72    |    $1,376.71   |   $963.66    |            
| Team Member + Child(ren)       | $705.88   |   $944.76     | $942.61    |     $1,024.48     |     $1,024.48    |    $1,251.56   |   $867.29    |
| Family                         | $1,129.41 |   $1,511.63   | $1,508.19  |     $1,521.31     |     $1,521.31    |    $1,877.34   |   $1,445.49  |
| Spouse Only                    | $371.52   |   $497.25     | $496.12    |     $502.20       |     $502.20      |    $625.78     |   $481.83    |
| Spouse + Child(ren)            | $705.88   |   $944.76     | $942.61    |     $1,024.48     |     $1,024.48    |    $1,251.56   |   $867.29    |
| Child Only                     | $371.52   |   $497.25     | $496.12    |     $502.20       |     $502.20      |    $625.78     |   $481.83    |

**Dental**

| Tier                           | Cigna DPPO |
|--------------------------------|:----------:|
| Team Member Only               |   $38.99   |
| Team Member + Domestic Partner |   $77.48   |
| Team Member + Spouse           |   $77.48   |          
| Team Member + Child(ren)       |   $89.62   |
| Family                         |   $137.49  |
| Spouse Only                    |   $38.99   |
| Spouse + Child(ren)            |   $89.62   |
| Child Only                     |   $38.99   |

**Vision**

| Tier                           | Cigna VPPO |
|--------------------------------|:----------:|
| Team Member Only               |   $7.51    |
| Team Member + Domestic Partner |   $15.05   |
| Team Member + Spouse           |   $15.05   |          
| Team Member + Child(ren)       |   $12.74   |
| Family                         |   $21.01   |
| Spouse Only                    |   $7.51    |
| Spouse + Child(ren)            |   $12.74   |
| Child Only                     |   $7.51    |

## Audit Processes

### Discovery Funding Account Audit

This quarterly audit is conducted to ensure the funding of our account used for FSA, DCFSA, and commuter benefit plans according to Accounts Payable matches the amount of claims incurred in Discovery's system. 

1. Reach out to Accounts Payable to provide an updated payment history report for payments made to Discovery.
1. In the `Ongoing Discovery Audit` spreadsheet, add new entries in the report provided by Accounts Payable to the bottom of the table in the "Discovery Payments History" tab. 
1. Navigate to [Discovery's platform](https://www.discoverybenefits.com/) and log into the employer portal.
   * Select "Benefits Administration" in the left toolbar. 
   * Navigate to the "Reports" tab and select the "Employer Funding Report". 
   * Download all reports for the months that have elapsed since the last audit was conducted. 
1. Add the new monthly report(s) to the `Ongoing Discovery Audit` spreadsheet as new tabs.
1. Reconcile all funding sent by Accounts Payable against the Employer Funding Report details.
   * AP funding will be denoted as "MANUAL EMPLOYER TRANSACTION AND ADJUSTMENT" in these reports.
1. On the "Funding Summary" tab, add the newly downloaded month(s) to the bottom of the summary table:
   * Add the year of the report(s) in column A.
   * Add the month of the report(s) in column B.
   * Copy the formula down for columns C, D, and E.
   * For columns D and E, replace the year and month in the formula with the year and month inputted in a column A and B. 
     * For example, if the formula current has `=sumif('March 2020 Funding Detail'!A:A,"Manual Employer Transaction and Adjustment",'March 2020 Funding Detail'!H:H)` and you are working on the row for April 2020, change the formula to say `=sumif('April 2020 Funding Detail'!A:A,"Manual Employer Transaction and Adjustment",'April 2020 Funding Detail'!H:H)`.
1. In the same "Funding Summary" tab, review the difference calculated in cell L3. This difference should be positive and roughly equivalent to the amount we currently have available in our Funding Account for Discovery, typically in the range of $5,000 to $50,000. 
1. Any discrepancies or problems should be escalated to the Manager, Total Rewards. 

### Discovery Payroll Audit

TODO

### Lumity/Carrier Enrollment Audit

This quarterly audit is conducted to identify any differences in enrollment between the carrier records and what a team member has elected in Lumity.

#### UHC Medical

TODO

#### Kaiser Medical

TODO

#### Cigna Dental/Vision

TODO

#### UHC Life/Disability

TODO
