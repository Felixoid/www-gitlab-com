---
layout: handbook-page-toc
title: Global Benefits Survey
description: Information on GitLab's Global Benefits Survey.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Global Benefits Survey

We've partnered with Culture Amp to run our Global Benefits Survey, which will launch annually in June.  We run this survey to collect feedback from all team members, which will be aggregated and analyzed. Using Culture Amp's reporting tools, we'll analyze the data to find the most common trends for desired benefit iterations. We will use this information to generate a roadmap and share these results with everyone at GitLab.

This is an opportunity to share with the Total Rewards team what benefits you think are great, what benefits are lacking, and what you think is most important to adjust. While we will review and prioritize all feedback, not all input may be implemented in the next few iterations. We will continue to ask the organization what is important to team members, but also take into account our fiduciary responsibility to maintain our operating costs.

**Timeline of the Survey Results:**
* Survey goes out to team members
* Survey closes
* Survey results analyzed
* Survey Results Shared with the GitLab Team
* Survey Results issues opened to begin implementation of any changes
* Q3 OKR to implement changes based on survey results viewed in Q2

Thank you again for playing a part in our efforts to continually improve total rewards at GitLab. If you have any questions, please reach out to the Total Rewards team via email.

### Global Benefits Survey Results 2019

Participation: 62%

**Rating Rubric**
5 - strongly agree
4 - agree
3 - neutral
2 - disagree
1 - strongly disagree

**Rating Rubric Results:**

1. I Understand the Benefits Package
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 4.16  |
    | Female                 | 4.21  |
    | Individual Contributor | 4.13  |
    | Manager                | 4.35  |
    | Leader                 | 4.46  |
    | Company Overall        | 4.18  |
1. The general benefits at GitLab are equal to or better than what is offered by similar employers
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 3.67  |
    | Female                 | 3.66  |
    | Individual Contributor | 3.72  |
    | Manager                | 3.33  |
    | Leader                 | 3.46  |
    | Company Overall        | 3.66  |
1. I believe my benefits package is equal to or better than what is offered by similar employers
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 3.44  |
    | Female                 | 3.50  |
    | Individual Contributor | 3.54  |
    | Manager                | 3.00  |
    | Leader                 | 3.27  |
    | Company Overall        | 3.46  |
1. The general benefits at GitLab save me a great deal of time and/or money, and add significant value to my employee experience
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 3.75  |
    | Female                 | 3.79  |
    | Individual Contributor | 3.83  |
    | Manager                | 3.38  |
    | Leader                 | 3.54  |
    | Company Overall        | 3.77  |
1. My benefits package provides quality coverage for myself and, if applicable, my dependents
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 3.59  |
    | Female                 | 3.63  |
    | Individual Contributor | 3.63  |
    | Manager                | 3.38  |
    | Leader                 | 3.58  |
    | Company Overall        | 3.60  |
1. The wellness offerings at GitLab help me lead a happier, healthier life
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 3.39  |
    | Female                 | 3.28  |
    | Individual Contributor | 3.41  |
    | Manager                | 3.26  |
    | Leader                 | 2.92  |
    | Company Overall        | 3.36  |
1. I believe our benefits package is one of the top reasons why people apply
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 2.87  |
    | Female                 | 2.81  |
    | Individual Contributor | 2.96  |
    | Manager                | 2.40  |
    | Leader                 | 2.35  |
    | Company Overall        | 2.86  |
1. Should I have or care for a(nother) child, the parental leave policy is sufficient	 
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 3.95  |
    | Female                 | 3.44  |
    | Individual Contributor | 3.82  |
    | Manager                | 3.61  |
    | Leader                 | 4.04  |
    | Company Overall        | 3.81  |
1. The vacation policy allows me sufficient time to recharge
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 4.48  |
    | Female                 | 4.40  |
    | Individual Contributor | 4.49  |
    | Manager                | 4.27  |
    | Leader                 | 4.38  |
    | Company Overall        | 4.46  |
1. I believe our benefits package is one of the top reasons why people stay at GitLab
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 3.08  |
    | Female                 | 3.13  |
    | Individual Contributor | 3.21  |
    | Manager                | 2.58  |
    | Leader                 | 2.54  |
    | Company Overall        | 3.09  |
1. I believe investing more of the company's money into improving benefits will help attract and retain top talent
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 4.16  |
    | Female                 | 4.25  |
    | Individual Contributor | 4.20  |
    | Manager                | 4.25  |
    | Leader                 | 3.85  |
    | Company Overall        | 4.18  |

**Comment Responses:**

Please note this is a summary of suggestions based on aggregated global team member feedback, but are not guaranteed to be implemented or adjusted at GitLab.

1. What provider for benefits would you prefer? (Entity/PEO where we have data)
  * Australia: BUPA, HCF, NIB, AHM
  * Canada: Great West Life, Blue Cross, Manulife, Sunlife
  * India: ICICI Lombard, Apollo Munich, Bajaj Allianz
  * Ireland: VHI. Laya, Irish Life
  * Netherlands: UMC Zorgverzekering, VGZ, ABP
  * United Kingdom: Virgin Active, BUPA, AVIVA, AXA PPP
  * United States: 45% of respondents commented, 21% UHC, 14% BCBS, 3% Kaiser, 2% Cigna
1. What general benefit do you think would be the best new addition to the company's offering to implement?
  * Wellness program (gym, fitness, etc) (overwhelming support for this)
  * Global insurance programs to align with US/UK
  * Pension with company match globally
  * Company Annual Bonus or extra month pay for vacation bonus
  * Charitable Giving
  * Team Level Contribute
  * Other Benefits: Loan Assistance, Childcare, Pet Insurance, Food Allowance
1. What benefits would you want GitLab to retain?
  * Unlimited Paid Time Off
  * Parental Leave
  * Stock Options
  * Tuition Reimbursement
  * Contribute
  * Remote Work
  * Travel Stipend/Visiting Grant
  * Employee Assistance Program
  * All Benefits
1. What benefits would be most willing to sacrifice to allow for new benefits?
  * Sales Incentive Dinner
  * Stock Options
  * Employee Assistance Program
  * Tuition Reimbursement
  * Visiting Grant
  * Referral/Discretionary Bonus
  * Travel Accident Insurance

**Action Items/Issues to Open:**

Please note this is the first iteration/more immediate goals, but this section will be updated as we progress through the project.

* Clarify what Benefits Contractors are eligible for: [Merge Request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26491/diffs)
* Ensure all PEO benefits are documented on the respective page: TODO
* Benchmark against other remote companies/survey data to prioritize new benefits to offer/replace: TODO
* Review Implementation of a Wellness Program (Gym/Fitness): [Issue](https://gitlab.com/gitlab-com/people-ops/Compensation/issues/37)
* Review Global Health Benefits, Pension, Life Insurance: TODO
* Establish a benefits budget with Finance to implement new benefits: TODO
* Review all PEO benefits to ensure they align to our global benefits principles: TODO

## Benefits Not Currently Being Implemented

This section serves to highlight benefits that we have previously researched, but have decided not to move forward with implementing at this time. As the company grows, circumstances change, and/or we receive new information, we may decide to revisit any benefits added to this list.

### Telehealth

We researched and selected four vendors to receive more information. Demo calls were conducted with three of these vendors where we learned more about the solutions and pricing. After reviewing the [results of the benefits survey](/handbook/total-rewards/benefits/benefits-survey/#global-benefits-survey-results), there wasn’t enough interest in a telehealth solution to justify the price so we decided to not move forward with any of the vendors at this time.

While we aren't offering a global telehealth solution at this time, team members based in the US who are enrolled in our [UHC](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#uhc-telehealth) or [Kaiser](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#kaiser-telehealth) medical plans can access telehealth services through these plans. Other team members may have options for telehealth available to them through their provider.

Further information and corresponding discussion are available under [Compensation Issue #15](https://gitlab.com/gitlab-com/people-ops/Compensation/issues/15) (internal).

### Coworking Space Agreement

We researched whether there was an account that could be set up with a coworking space chain that would make it easier for team members to utilize these spaces. Three major chains with global coverage were contacted with one responding. After conducting a survey, the type of account this chain offers doesn't align with the way the majority of team members utilize coworking spaces. Team members are still welcome to follow the existing process of [expensing a coworking space](/handbook/spending-company-money/).

Further information, survey results, and corresponding discussion are available under [Compensation Issue #30](https://gitlab.com/gitlab-com/people-ops/Compensation/issues/30) (internal).

### Wellness Program

We researched and selected three vendors and conducted a cost analysis with Finance. At this time, it does not fit into the budget for benefits to be offered for FY21, but will be revisited when we evaluate benefits and budget for the next fiscal year. Further information and corresponding discussion are available under [Compensation Issue #37](https://gitlab.com/gitlab-com/people-group/Compensation/-/issues/37) (internal).
